# Release notes de Squash TM 2.X

## Squash TM 2.1.1

**Sortie le 20/10/2021**

### Corrections

- [8744](https://ci.squashtest.org/mantis/view.php?id=8744) Mauvaise redirection via SAML en 2.0
- L'export des résultats de recherche d'exigences (page de recherche) n'est pas fonctionnel pour 6 profils
- Perte d'accès à la liste des jobs jenkins Squash TF (librairie httpclient Apache)

## Squash TM 2.1.0

**Sortie de 26/08/2021**

### Évolutions

- Serveur de partage de code source : 
    - Support de l'authentification par token à Github
- Xsquash4Jira :
    - Prise en compte de la configuration de Xsquash4Jira dans les modèles de projet et répercussion sur dans les projets liés
    - Affichage dans la bibliothèque et sur la page de consultation des exigences du statut de synchronisation du ticket Jira associé (synchronisé, désynchronisé ou supprimé dans Jira)
    - Ajout d'une option pour simuler une synchronisation afin de visualiser le nombre et le détail des tickets qu'elle contient avant de l'ajouter
    - Ajout d'un bouton pour rafraîchir le bloc "Synchronisations" sur la page de configuration du plugin Xsquash4Jira
    - Optimisation de l'affichage des erreurs de syntaxe lors de la configuration des équivalences entre les valeurs des champs
    - Configuration des champs de reporting Squash vers Jira via l'API
- Cas de test :
    - Ajout de boutons pour copier, coller et supprimer des pas de test en haut de la page 
- Pilotage :
    - Affichage des cas de test BDD dans les cahiers de test au format éditable
- Transverse :
    - Ajout d'un bouton pour vider les filtres appliqués sur les colonnes de certaines tables
    - Affichage des projets sélectionnés en haut de la liste dans le filtre projets
    - Optimisation de l'affichage des infobulles
- Administration :
    - Possibilité de supprimer un utilisateur propriétaire d'un graphique ou rapport
- API REST :
    - Consultation de l'UUID d'un cas de test
    - Consultation des champs d'association d'un cas de test à un script automatisé
    - Non mise à jour du bloc lorsque l'on change de protocole d'authentification
- API REST Admin :
    - [8733](https://ci.squashtest.org/mantis/view.php?id=8733) Recherche d'équipes et d'utilisateurs par nom d'équipe et login
    - [8734](https://ci.squashtest.org/mantis/view.php?id=8734) Association entre équipe et utilisateur par nom d’équipe et login

### Plugins

- Nouvelles versions des plugins suivants :
    - [SCM Git](../plugins/scm-git.md#210release)
    - [Xsquash4Jira](../plugins/xsquash4jira.md#210release)
    - [Redmine Req](../plugins/redmine-req.md#210release)
    - [Cahier de test éditable](../plugins/cahier-test-editable.md#210release)
    - [Assistant campagne](../plugins/assistant-campagne.md#210release)
    - [API REST](../plugins/api-rest.md#210release)
    - [API REST Admin](../plugins/api-rest-administration.md#210release)

### Corrections

- Exigences :
    - Statut de synchronisation non traduit sur l'exigence
    - Impossible de copier/coller une exigence associée à un jalon
    - Modifier le contenu de la popup de suppression d'exigence en mode jalon
    - Popup d'ajout de version d'exigence accessible mais non fonctionnelle pour un profil valideur
    - L'export depuis la recherche ne prend pas en compte tous les filtres
    - Absence du bouton [-] dans les lignes des tableaux "exigences liées" et "CDT liés"
    - Ouvrir le noeud après un glisser déposer dans l'arborescence
    - Le tri par défaut n'est pas le bon dans la table "Cas de test vérifiant cette exigence"
    - Afficher les listes personnalisées et champs personnalisés par ordre alphabétique dans les critères de recherche
    - Synchronisation manuelle Jira et Redmine : Coquille dans le message de la popup 'Synchroniser avec ...'

- Cas de test : 
    - Modifier les icônes en bout de ligne pour un cas de test appelé
    - La mise à jour d'un paramètre n'est pas prise en compte automatiquement dans les prérequis d'un cas de test
    - Mauvais message de suppression lors de la suppression d'une exigence au pas de test sur la page de niveau 2 d'un pas de test
    - Absence d'infobulle sur les boutons d'action dans la recherche de cas de test
    - CT BDD - Erreur lorsqu'on modifie les paramètres d'une action avec des paramètres vides

- Campagnes :
    - [8749](https://ci.squashtest.org/mantis/view.php?id=8749) L'URL de l'exécution présente dans la description des anomalies est erronée
    - Pas de popup d'erreur los de lancement de l'exécution si le script Gherkin n'a pas de scénario
    - Lors de la modification d'un pas de test en cours d'exécution, la case "Liée au PT" n'est pas cochée automatiquement lorsqu'une exigence est associée à un cas de test via la recherche
    - Lors de la modification d'un pas de test en cours d'exéctuion, la popup de dissociation d'une exigence au pas de test est non conforme
    - Affichage de la popup de suppression d'item de plan de test dans une suite (mais non fonctionnelle) pour les profils qui n'ont pas les habilitations
    - Message d'erreur a renommer (Popup et page d'exécution) dans la popup de déclaration d'anomalies si le bugtracker est injoignable
    - Harmoniser les encadrés des champs dans la popup de déclaration d'anomalie
    - Surbrillance du contour des champs Date/Date et heure non uniforme au survol dans la popup de déclaration d'anomalie
    - Comportements anormaux pour des ID de cas de test excessivement grands lors de la recherche d'item de plan de test
    - Non mise à jour automatique des date/heure login au changement de "A exécuter" à "Non testable" sur la page d'une exécution
    - La valeur par défaut de l'état d'une itération doit être "non défini"
    - Le bouton [Lancer les tests automatisés] n'apparaît qu'après avoir rafraîchi la page
    - Bouton de suppression d'anomalie présent mais non fonctionnel dans le bloc anomalie d'une exécution pour certains profils
    - Glisser déposer d'item de plan de test visuellement possible pour certains profils mais non fonctionnel
    - Affichage de la popup de suppression d'un ITPI dans une campagne non fonctionnelle
    - Temps d'affichage très long pour la popup 'Historique des exécutions' sur une grosse base de données

- Pilotage :
    - [8746](https://ci.squashtest.org/mantis/view.php?id=8746) [Report API] La catégorie de rapport "VARIOUS" n'est plus supportée
    - Sur un graphique de type histogramme des barres noires apparaissent
    - Pour les rapports, le champ "Choisir un tag" s'affiche même si aucun champ personnalisé de type tag
    - Filtre boléen toujours affiché après sélection lors de la création d'un graphique

- Administration
    - Suppression d'un type de lien entre exigences visible qu'après rafraîssement de la page
    - La modification du libellé d'une option d'une liste personnalisée n'est pas répercutée sur les objets
    - Mauvais message d'erreur si le login est déjà utilisé dans la popup d'ajout d'utilisateur
    - Les colonnes du bloc "Options" d'un champ personnalisé Liste déroulante ne doivent pas être triables
    - Modifier "Bugtracker" par "Serveur" dans le message d'information lors de la suppression d'un bugracker
    - Non mise à jour du bloc 'Politique d'authentification' lorsque l'on change de protocole d'authentification

- Transverse :
    - [8729](https://ci.squashtest.org/mantis/view.php?id=8729) L'url publique de Squash TM n'est pas prise en compte partout
    - Modification des urls des pièces jointes en 2.0.0, les images ne s'affichent plus
    - L'URL n'est pas mise à jour lorsque l'on navigue d'un groupe d'ancres à l'autre
    - Impossible de naviguer vers un groupe d'ancres via l'URL
    - Erreurs dans les données référentielles lorsque l'utilisateur n'a pas de visibilité sur un des projets du filtre
    - Mise à jour non automatique des chiffres sous les ancres
    - Mauvaise redirection depuis un lien après déconnexion
    - Dans le filtre projets, la couleur du contour de la case à cocher de l'entête "Nom" est de la couleur de l'espace "Exigences"
    - Infobulle au survol d'une cellule même si la valeur n'est pas tronquée
    - Supprimer l'option "Planifié" lors de la recherche par Statut de jalon
    - Les critères associés aux jalons sont visibles/cliquables dans la liste "Ajouter un critère" sur les pages de recherche en mode jalon
    - Décalage des boutons d'actions de la bibliothèque vers la droite en mode jalon
    - Mauvais lien de redirection à la documentation

- Base de données :
    - Supprimer les erreurs de commentaires trop longs dans le script full install
    - Passage du script 2.00.0 trop long dû aux upgrades d'icônes

## Squash TM 2.0.0

**Sortie le 01/07/2021**

### Évolutions

- **Refonte et modernisation de l'interface**
- Barre de navigation :
    - Accès à la documentation en ligne depuis la barre de navigation
    - Déplier/Replier la barre de navigation
    - Passage en mode jalon/référentiel depuis la barre de navigation
- Navigation dans l'application :
    - Navigation au sein des pages via des ancres
    - Accès à un objet via son URL
    - Etat des tables conservé (pagination, filtre, tri) lors de la navigation vers une page de niveau 2
- Consultation d'objets :
    - Ajout d'icônes dans les bibliothèques qui donnent des informations rapides sur le référentiel
    - Ajout de capsules sur les pages de consultation des objets avec les attributs principaux
    - Visualisation rapide du contenu et associations des objets via des indicateurs chiffrés dans les ancres
    - Affichage des jalons associés aux objets sous forme d'étiquette
- Association et glisser-déposer :
    - Association entre objets directement depuis la page de consultation par glisser-déposer
    - Ajout de pièces jointes par glisser-déposer
    - Optimisation du déplacement et de la réorganisation d'objets par glisser-déposer
- Filtres et tris :
    - Modification du tri des bibliothèques depuis la barre d'action des bibliothèques
    - Optimisation des filtres sur les colonnes de certaines tables
    - Optimisation du tri secondaire dans les tables
- Exigences :
    - Prise en compte du statut des cas de test dans les indicateurs de couverture
- Cas de test : 
    - Nouvelle interface de gestion des pas de test
    - Visualisation des étapes des cas test appelés depuis le cas de test consulté
    - Réorganisation de la page de détails d'un pas de test
    - Mutualisation des paramètres et jeux de données dans une même table
    - Optimisation de l'affichage des informations supplémentaires pour les pas de test BDD
    - Présence de SKF dans le listing des technologies disponibles dans Squash TM
    - [8595](https://ci.squashtest.org/mantis/view.php?id=8595) Bouton "Fermer" sur "Ajouter un pas de test" dans Squash
- Campagnes :
    - Ajout de suites de tests depuis la bibliothèque
    - Optimisation de l'affichage du graphique "Avancement cumulé de la campagne/itération"
    - Nouvelle interface de gestion du scénario d'exécution sur la page de consultation d'une exécution
    - Gestion de l'historique des exécutions dans une table
- Recherche :
    - Affichage des critères et résultats de recherche sur une même page
    - Actualisation dynamique des résultats de recherche
    - Recherche à partir d'un périmètre personnalisé (projet, dossier, ensemble d'objets)
- Tableaux de bord :
    - Optimisation de l'affichage des tableaux de bord en fonction de l'espace disponible
    - Affichage d'une infobulle au survol des portions des graphiques
    - Portions des graphiques traduites en critères de recherche au clic pour afficher leur contenu
- Pilotage :
    - Nouvelle interface de création de grahiques personnalisés
    - Désélection des tableaux de bord favoris
    - Nouvelle interface de création d'exports personnalisés de campagnes
    - [8590](https://ci.squashtest.org/mantis/view.php?id=8590) Afficher les titres des graphiques sans données dans les Dashboard
- Administration :
    - Ajout d'une barre de navigation dans l'administration
    - Réorganisation des rubriques de l'administration
    - Optimisation de l'ajout d'utilisateurs et de champs personnalisés à un projet
    - Visualisation des plugins installés
    - Téléchargement des fichiers de log précédents
- Serveur :
    - Utilisation possible du server.servlet.context-path

### Plugins

- Nouvelles versions de tous les plugins : compatibilité avec Squash TM 2.0

### Corrections

- Exigences:
    - Empêcher la liaison d'un jalon non associé au projet à une exigence du projet
    - Non reprise du libellé de la liste personnalisée dans l'export d'exigences
    - Contenu de la colonne Catégorie non traduite dans l'export champs actuels
    - La colonne "Catégorie" ne prend pas le code de la liste
    - Non reprise du libellé de la liste personnalisée
    - Colonnes inutiles dans le gabarit d'import à supprimer
- Cas de test :
    - Nombre d'anomalies affichées sur l'ancre différent du nombre d'anomalies visibles sur les tables
    - Empêcher la liaison d'un jalon non associé au projet à un cas de test du projet
    - Non reprise des jalons associés à l'exigence au niveau du cas de test par héritage
    - [8716](https://ci.squashtest.org/mantis/view.php?id=8716) Affectation automatique au jalon en cours sur le projet lors de la création de cas de test assisté
    - Création de CT par copie d'exigences cassée si CUF obligatoires sur les dossiers de CT
    - La modification en masse est impossible pour une sélection de cas de test liés à une exigence associée à un jalon 'Verrouillé'
    - Un utilisateur sans droit voit les cas de test BDD depuis la recherche
    - Transmission en masse cassée si Gherkin et BDD dans la sélection
    - Mise à jour non automatique du jalon associé au cas de test s'il est déplacé dans un autre projet
    - Colonnes inutiles dans le gabarit d'import à supprimer
    - Impossible de copier/coller un dossier de cas de test contenant des cas de test BDD avec pas de test
- Campagnes : 
    - Modifications possibles sur une campagne associée à un jalon verrouillé ainsi que sur ses descendants
    - Mise à jour non automatique du statut d'avancement d'une suite de test
    - Ajout des statuts d'exécution automatisée dans le plan d'exécution
    - Les ITPI sans exécution disparaissent au tri sur "Mode" ou "Exécuté par" dans la recherche
    - Le profil Testeur peut voir à tort des tests qui ne lui sont pas assignés dans le plan d'exécution d'une campagne
    - La date de fin du filtre sur la colonne 'Dernière exécution' est exclue des résultats affichés
    - Les JDD des items de plan de test peuvent être modifiés même si le Jalon lié à la campagne est verrouillé
    - Le tri de certaines colonnes fait disparaitre des lignes dans la recherche
    - Affichage différent des exigences liées à un cas de test via un appel dans la popup d'exécution
    - Le tri de la colonne JDD ne se fait pas correctement sur la page de recherche
    - [8735](https://ci.squashtest.org/mantis/view.php?id=8735) Le profil invité ne voit pas les dates de planning des campagnes
- Pilotage : 
    - Téléchargement d'un export personnalisé impossible si seul un champ personnalisé est sélectionné dans "Cas de test"
    - Les attributs concernant les jalons s'affichent lors de la création de graphique ou d'export si jalons désactivés de l'instance
    - Non prise en compte du filtre projet dans la popup de sélection du jalon pour les rapports
    - [8737](https://ci.squashtest.org/mantis/view.php?id=8737) Description d'un dossier non enregistrée
- Administration :
    - Popup d'info manquante si on tente de modifier une habilitation quand le nombre de users est de +20%
    - Le lien vers la documentation YAML s'ouvre dans la même page
    - Les utilisateurs du groupe Serveur d'automatisation de tests ne peuvent pas ajouter de pièces jointes
    - Modifier le message de la popup d'erreur si la durée de conservation renseignée n'est pas une valeur entre 0 et 2147483647
    - Remplacer le terme "bugtracker" par "serveur" dans les popups de suppression de serveurs

