# Release notes du plugin SCM Git

## 2.1.0.RELEASE

**Sortie le 26/08/2021**<br />
**Compatible avec Squash TM 2.1**

### Evolutions

- Support de l'authentification par token à Github

## 2.0.0.RELEASE

**Sortie le 01/07/2021**<br />
**Compatible avec Squash TM 2.0**

### Evolutions

- Compatibilité avec Squash TM 2.0