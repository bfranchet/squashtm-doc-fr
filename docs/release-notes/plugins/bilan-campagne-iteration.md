# Release notes du plugin Bilan de campagne et d'itération

## 2.0.1.RELEASE

**Sortie le 26/08/2021**<br />
**Compatible avec Squash TM 2.0+**

### Corrections

- Récupération des anomalies incomplètes dans les bilans d'itération et de campagne

## 2.0.0.RELEASE

**Sortie le 01/07/2021**<br />
**Compatible avec Squash TM 2.0**

### Evolutions

- Compatibilité avec Squash TM 2.0