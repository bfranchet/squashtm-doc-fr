# Release notes du plugin API REST

## 2.1.0.RELEASE

**Sortie le XX/08/2021**<br />
**Compatible avec Squash TM 2.1**

### Évolutions

- Consultation de l'UUID d'un cas de test
- Consultation des champs d'association d'un cas de test à un script

### Corrections

- Impossible de créer un Cas de test BDD avec un pas de test

## 2.0.0.RELEASE

**Sortie le 01/07/2021**<br />
**Compatible avec Squash TM 2.0**

### Evolutions

- Compatibilité avec Squash TM 2.0