# Release notes du plugin Xsquash (Server et Datacenter)

## 2.0.0.RELEASE

**Sortie le 01/07/2021**<br />
**Compatible avec Squash TM 2.0**

### Evolutions

- Compatibilité avec Squash TM 2.0
- Ajout des liens vers les campagnes dans l'onglet 'Exécutions Squash TM'
- Liens Squash s'ouvre dans un nouvel onglet


