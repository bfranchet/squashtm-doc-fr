# Release notes du plugin Redmine Bugtracker

## 2.0.0.RELEASE

**Sortie le 01/07/2021**<br />
**Compatible avec Squash TM 2.0**

### Evolutions

- Compatibilité avec Squash TM 2.0

### Corrections

- Affichage du message "Bugtracker injoignable" si la configuration du bugtracker n'est pas correctement effectuée. 