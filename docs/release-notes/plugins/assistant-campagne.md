# Release notes du plugin Assistant campagne

## 2.1.0.RELEASE

**Sortie le 26/08/2021**<br />
**Compatible avec Squash TM 2.1**

### Corrections

- Les infolistes prises en compte dans les critères avancés sont celles du projet de la campagne

## 2.0.0.RELEASE

**Sortie le 01/07/2021**<br />
**Compatible avec Squash TM 2.0**

### Evolutions

- Compatibilité avec Squash TM 2.0

### Corrections

- Libellé "Projet" à l'étape 3 affiche le nom du projet courant au lieu du projet d'origine