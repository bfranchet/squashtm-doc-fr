# Release notes du plugin Workflow d'automatisation Jira

## 2.0.0.RELEASE

**Sortie le 01/07/2021**<br />
**Compatible avec Squash TM 2.0**

### Evolutions

- Compatibilité avec Squash TM 2.0

### Corrections

- Champs d'automatisation SquashAUTOM/Jenkins font doublons sur le cas de test si Squash AUTOM