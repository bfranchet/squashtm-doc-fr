# Release notes des plugins Squash AUTOM et Squash DEVOPS

[Release notes de l'ensemble des plugins Squash AUTOM & DEVOPS](https://autom-devops-en.doc.squashtest.com/1.1.0/release-note/release-note-by-component.html)

- Squash AUTOM Community
- Squash AUTOM Premium
- Result Publisher Community
- Result Publisher Premium 
- Test Plan Retriever Community
- Test Plan Retriever Premium



