# Release notes du plugin Xsquash4Jira

## 2.1.0.RELEASE

**Sortie le 26/08/2021**<br />
**Compatible avec Squash TM 2.1**

### Evolutions

- Prise en compte de la configuration de Xsquash4Jira dans les modèles de projet et répercussion dans les projets liés
- Affichage dans l'arborescence et sur la page de consultation des exigences du statut de synchronisation du ticket Jira associé (synchronisé, désynchronisé, supprimé dans Jira)
- Ajout d'une option pour simuler une synchronisation afin de visualiser le nombre et le détail des tickets qu'elle contient avant de l'ajouter
- Ajout d'un bouton pour rafraîchir le bloc "Synchronisations" sur la page de configuration du plugin Xsquash4Jira
- Optimisation de l'affichage des erreurs de syntaxe lors de la configuration des équivalences entre les valeurs des champs

### Corrections

- Un filtre avec un apostrophe mène la synchro en échec
- Ordre d'affichage des items de la liste déroulante de la colonne 'Champ Squash' + bouton de suppression non conforme

## 2.0.0.RELEASE

**Sortie le 01/07/2021**<br />
**Compatible avec Squash TM 2.0**

### Evolutions

- Compatibilité avec Squash TM 2.0
- Forcer la synchronisation en masse

### Corrections

- Les sprints ne sont pas récupérés lors d'une synchronisation tableau, sur les projets nouvelle génération avec Jira Cloud
