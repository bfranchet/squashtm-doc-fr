# Release notes du plugin Redmine Exigences

## 2.1.0.RELEASE

**Sortie le 26/08/2021**<br />
**Compatible avec Squash TM 2.1**

### Evolutions

- Optimisation de l'affichage des erreurs de syntaxe lors de la configuration des équivalences entre les valeurs des champs

### Corrections

- Non validation des champs Projet et Filtre dans la configuration
- Le message de l'infobulle de l'icône "?" n'est pas conforme dans le bloc Équivalences entre les champs

## 2.0.0.RELEASE

**Sortie le 01/07/2021**<br />
**Compatible avec Squash TM 2.0**

### Evolutions

- Compatibilité avec Squash TM 2.0

### Corrections

- Spinner en continu pour la popup 'Synchroniser avec Redmine' si l'option 'Utiliser les identifiants de Squash TM' est cochée mais que les identifiants n'ont pas été renseignés