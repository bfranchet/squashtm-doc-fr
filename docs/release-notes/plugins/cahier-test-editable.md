# Release notes du plugin Cahier de tests éditable

## 2.0.1.RELEASE

**Sortie le 26/08/2021**<br />
**Compatible avec Squash TM 2.0+**

### Évolutions

- Affichage des cas de test BDD dans les cahiers de test au format éditable

### Corrections

- [8560](https://ci.squashtest.org/mantis/view.php?id=8560) Génération du cahier de test KO si requêtes importées dans les steps

## 2.0.0.RELEASE

**Sortie le 01/07/2021**<br />
**Compatible avec Squash TM 2.0**

### Evolutions

- Compatibilité avec Squash TM 2.0