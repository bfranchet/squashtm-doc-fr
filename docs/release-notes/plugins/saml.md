# Release notes du plugin SAML

## 2.0.1.RELEASE

**Sortie le 20/10/2021**<br />
**Compatible avec Squash TM 2.0+**

### Corrections

- [8744](https://ci.squashtest.org/mantis/view.php?id=8744) Mauvaise redirection via SAML en 2.0
- Compatiblité du plugin SAML avec java 11

## 2.0.0.RELEASE

**Sortie le 01/07/2021**<br />
**Compatible avec Squash TM 2.0+**

### Evolutions

- Compatibilité avec Squash TM 2.0