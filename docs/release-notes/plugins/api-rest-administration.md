# Release notes du plugin API REST administration

## 2.0.1.RELEASE

**Sortie le 26/08/2021**<br />
**Compatible avec Squash TM 2.0+**

### Évolutions

- [8733](https://ci.squashtest.org/mantis/view.php?id=8733) Recherche d'équipes et d'utilisateurs par nom d'équipe et login
- [8734](https://ci.squashtest.org/mantis/view.php?id=8734) Association entre équipe et utilisateur par nom d’équipe et login

## 1.4.0.RELEASE

**Sortie le 26/08/2021**<br />
**Compatible avec Squash TM 1.22**

### Évolutions

- [8733](https://ci.squashtest.org/mantis/view.php?id=8733) Recherche d'équipes et d'utilisateurs par nom d'équipe et login
- [8734](https://ci.squashtest.org/mantis/view.php?id=8734) Association entre équipe et utilisateur par nom d’équipe et login

## 1.2.1.RELEASE

**Sortie le 26/08/2021**<br />
**Compatible avec Squash TM 1.21**

### Évolutions

- [8733](https://ci.squashtest.org/mantis/view.php?id=8733) Recherche d'équipes et d'utilisateurs par nom d'équipe et login
- [8734](https://ci.squashtest.org/mantis/view.php?id=8734) Association entre équipe et utilisateur par nom d’équipe et login

## 2.0.0.RELEASE

**Sortie le 01/07/2021**<br />
**Compatible avec Squash TM 2.0**

### Evolutions

- Compatibilité avec Squash TM 2.0