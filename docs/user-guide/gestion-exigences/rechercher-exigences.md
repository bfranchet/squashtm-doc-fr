# Rechercher des exigences 

La page de recherche des exigences se divise en 2 parties : à gauche le bloc des critères de recherche et à droite les résultats de recherche.

!!! tip "En savoir plus"
    Consulter la page "[Rechercher un objet](../presentation-generale/fonctionnalites-objet.md#rechercher-un-objet)" pour connaître le fonctionnement de la page de recherche.

Pour rappel, le périmètre reprend par défaut les projets présents dans le filtre projet. Il est également possible de faire une recherche sur un périmètre personnalisé en sélectionnant un ou plusieurs dossiers ou une sélection d'exigences.

![Page de recherche d'exigences](./resources/page-recherche-exigence.jpg){class="pleinepage"}

## Les critères de recherche des exigences 

Les critères de recherche correspondant aux exigences sont nombreux et peuvent être couplés les uns aux autres.

Le critère "Versions" est ajouté par défaut et ne peut être supprimé. Il permet de rechercher sur:

- Toutes les versions d'exigences
- Seulement la dernière version de chaque exigence
- Seulement la dernière version de chaque exigence non obsolète.

Les critères de recherche sont répartis en plusieurs catégories de critères:

- Les **informations** de l'exigence
- L'**historique** de l'exigence (date de création/modification ainsi login de la personne ayant fait cette action)
- Les **attributs** de l'exigence
- Les informations liées aux **jalons**
- Les informations liées au **contenu** (nombre de pièces jointes et présence d'une description)
- Les informations liées aux **associations** avec des cas de test et des exigences
- Les différents **champs personnalisés** associés aux exigences.

!!! info "Info"
    Les critères de recherche associés aux jalons ne sont visibles que si les jalons sont activés sur l'instance.

**Par exemple :**

Sélectionner dans la catégorie **Attributs**, le critère "Criticité" avec la valeur "Critique" et dans la catégorie **Associations**, le critère "Nombre de cas de test associés" avec la comparaison Egal à 0 : 
<br/>Les résultats de recherche retournent uniquement les exigences de criticité "Critique" n'ayant aucun cas de test associé.



## Les résultats de recherche 

Les résultats de recherche sont affichés sous forme de tableau comportant plusieurs colonnes reprenant les différentes informations des exigences. Les résultats de recherche peuvent être **exportés** dans un fichier excel sous deux formats :

- le format XLS 'Champs actuels'
- le format XLS 'Tous les champs'

!!! tip "En savoir plus"
    Consulter la page "[Rechercher un objet](../presentation-generale/fonctionnalites-objet.md#exporter)" pour en savoir plus sur l'export des résultats de recherche.

Directement dans le tableau des résultats de recherche, il est possible de **modifier unitairement** les attributs suivants de chaque exigence :

- La référence
- Le nom
- Le statut
- La criticité
- La catégorie

Il est également possible de **modifier en masse** les informations suivantes à l'aide des boutons d'actions présents en haut à droite du tableau :

- de modifier en masse les informations comme le statut, la criticité ou la catégorie
- d'associer ou dissocier en masse des jalons aux exigences (s'ils sont associés au projet) 

!!! danger "Attention"
    - Une exigence au statut "Approuvée" ou "Obsolète" ne peut pas être modifiée.
    - Une exigence associée à un jalon au statut "Verrouillé" ne peut pas être modifiée
