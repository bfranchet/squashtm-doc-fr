# Versionner les exigences

## Le versioning d’exigences

Le versioning permet de gérer l'évolution des exigences au fil des évolutions de l'application et des documents de spécification. Une nouvelle version d'une exigence est créée si la fonctionnalité qu'elle définit évolue dans la nouvelle version des spécifications. 

L'ensemble des versions coexiste en parallèle. Chaque version est indépendante des autres : contenu, cycle de vie, cas de test associés, etc. 

!!! info "Info"
    Pour un suivi optimal des versions d'exigences, ajouter un champ personnalisé afin de tagger la version des spécifications concernées. 

!!! warning "Focus"
    Deux versions d'une même exigence ne peuvent être associées à un même cas de test


## Créer une nouvelle version d’exigence

Pour créer une nouvelle version d'exigence, cliquer sur le sous menu 'Ajouter une nouvelle version' présent sous le bouton **[...]**.

![Nouvelle version d'exigence](./resources/creation-version-exigence.jpg){class="centre"}

Une popup de sélection s'affiche demandant de choisir les informations à reprendre :

- Lorsque la case "Reprendre les liens entre version d’exigences non-obsolètes" est cochée, les liens existants entre l'exigence et d’autres exigences sont conservés pour la nouvelle version de celle-ci.
- Lorsque la case "Reporter les cas de test attachés vers la nouvelle version de l’exigence" est cochée, les cas de test qui étaient attachés à la précédente version de l’exigence sont désormais attachés à la nouvelle version de l’exigence. Ils ne sont plus attachés à la précédente version à la suite de la création.

La nouvelle version créée est considérée comme la version active. Ainsi, lors de l’association d'exigence à un cas de test par exemple, la dernière version sera sélectionnée par défaut.

!!! info "Info"
    Si les versions antérieures à la version de l'exigence courante ne sont plus d'actualité, passer leur statut à "Obsolète".
 

## Consulter l’historique des versions d’une exigence

Le numéro de la version de l'exigence courante est indiquée :

- dans une capsule dans la partie fixe en haut de la page de l'exigence
- dans le bloc **Informations** dans le champ "N° version"

En cliquant sur le numéro de la version dans le bloc **Informations**, la page de gestion des versions de l'exigence s'affiche sous forme de tableau et répertorie l'ensemble des versions existantes pour cette exigence.
La page de consultation d'une version de l'exigence s'ouvre en cliquant sur la ligne correspondante dans le tableau.

![Page de gestion des versions de l'exigence](./resources/page-gestion-version-exi.jpg){class="pleinepage"}