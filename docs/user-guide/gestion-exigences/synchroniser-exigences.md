# Synchroniser des exigences depuis un outil tiers

Les plugins Redmine Exigences, Jira Exigences et Xsquash4jira permettent de synchroniser des tickets présents dans Redmine ou Jira sous forme d'exigences dans Squash TM. 
<br/>Si les synchronisations du plugin XSquash4jira sont gérées automatiquement ce n'est pas le cas pour les synchronisations des plugins Redmine Exigences et Jira Exigences. En effet, ces synchonisations doivent être déclenchées manuellement depuis l'espace Exigences.

Pour effectuer une synchronisation manuelle des tickets avec les plugins Redmine Exigences ou Jira Exigences, il faut :

- Sélectionner le nom du projet configuré avec le plugin
- Ouvrir le menu [Importer/Exporter] 
- Cliquer sur 'Synchroniser avec Redmine' ou 'Synchroniser avec Jira'
- Renseigner les identifiants de connexion à Redmine ou Jira s'ils ne sont pas déjà enregistrés dans l'espace 'Mon compte'
- Valider la demande de synchronisation

Les options "Synchroniser avec Redmine" et "Synchroniser avec Jira" ne sont actives que sur les projets pour lesquels le plugin correspondant est activé et configuré. En outre, seul un utilisateur disposant d'un profil administrateur ou chef de projet dans Squash TM peut déclencher la synchronisation. Et il est nécessaire d'avoir un compte Jira ou Redmine pour se connecter à l'outil et réaliser la synchronisation.

![Synchroniser des exigences manuellement](./resources/synchroniser-exigences.jpg){class="centre"}

Les tickets sont synchronisés sous forme d'exigences dans le dossier indiqué dans la configuration du plugin au niveau du projet. Si aucun dossier de destination n’est renseigné, ils sont synchronisés à la racine du projet dans l'espace Exigences.
Les exigences synchronisées apparaissent en grisé dans l’arborescence et tous leurs champs sont modifiables.
<br/>Les synchronisations se font uniquement dans le sens Redmine vers Squash TM et Jira vers Squash TM.

!!! tip "En savoir plus"
    Pour en savoir plus sur la configuration des plugins au niveau du projet, consulter les pages suivantes : 
    <br/>- [Configuration de Redmine Exigences](../../admin-guide/gestion-projets/configurer-plugins.md#configurer-le-plugin-redmine-req)
    <br/>- [Configuration de Jira Exigences](../../admin-guide/gestion-projets/configurer-plugins.md#configurer-le-plugin-jira-req)

## Synchroniser avec Redmine Exigences

![Synchronisation avec RedmineReq](./resources/synchroniser-redminereq.jpg){class="pleinepage"}

Le périmètre de la synchronisation de tickets Redmine réalisée par l'utilisateur dans l'espace Exigences est défini par : 

- Le ou les couples Identifiant projet/Identifiant filtre configurés au niveau du projet Squash TM
- Les droits d'accès sur Redmine dont dispose l'utilisateur réalisant la synchronisation 

En effet, si l'utilisateur ne dispose pas à minima de droits de lecture sur tous les projets Redmine indiqués dans la configuration, la synchronisation des tickets sera partielle. Seuls les tickets visibles par l'utilisateur seront synchronisés.

La synchronisation n'étant pas automatique, il est nécessaire de resynchroniser régulièrement pour récupérer :

- Les nouveaux tickets Redmine pris en compte dans le périmètre
- Les nouvelles valeurs des champs des tickets déjà synchronisés
- Les suppressions de tickets (selon l'option choisie lors de la configuration du plugin)
- Les ajouts et suppressions de liens entre exigences

## Synchroniser avec Jira Exigences

![Synchronisation avec JiraReq](./resources/synchro-jirareq.jpg){class="pleinepage"}

Le périmètre de la synchronisation de tickets Jira réalisée par l'utilisateur dans l'espace Exigences est défini par : 

- Le ou les couples Clé projet/Filtre configurés au niveau du projet Squash TM
- Les droits d'accès sur Jira dont dispose l'utilisateur réalisant la synchronisation 

En effet, si l'utilisateur ne dispose pas à minima de droits de lecture sur tous les projets Jira indiqués dans la configuration, la synchronisation des tickets sera partielle. Seuls les tickets visibles par l'utilisateur seront synchronisés.

La synchronisation n'étant pas automatique, il est nécessaire de resynchroniser régulièrement pour récupérer les nouveaux tickets et les modifications effectuées sur les tickets déjà synchronisés.

!!! tip "En savoir plus"
    Le plugin Jira Exigences est un ancien plugin qui a vocation à être remplacé progressivement par le plugin Xsquash4jira.
    <br/>Pour en savoir plus sur le plugin Xsquash4jira consulter les pages suivantes : 
    <br/>- [Configurer Xsquash4jira dans Squash TM](../../admin-guide/configuration-xsquash/configurer-xsquash4jira.md)
    <br/>- [Synchroniser des objets agiles Jira dans Squash TM](../xsquash/synchro-objets-agiles-jira.md)