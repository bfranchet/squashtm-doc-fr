# Couvrir les exigences par les tests

Le bloc **Cas de test vérifiant cette exigence**, permet d’associer à chaque exigence, un ou plusieurs cas de test. Cette fonction est capitale pour établir la couverture des exigences par les cas de test.
<br/>Il existe 2 façons de réaliser cette association :

## Associer des cas de test à une exigence en utilisant la bibliothèque

En cliquant sur le bouton ![Ajouter](resources/icone-add.png){class="icone"}, il est possible, via un glisser-déposer depuis le volet 'Référentiel des Cas de test' vers la page de consultation de l'exigence, d'ajouter un ou plusieurs cas de test dans la table 'Cas de test vérifiés par cette exigence'. 

![Associer des cas de test à une exigence](resources/associer-ct-a-exigence.png){class="pleinepage"}

## Associer des cas de test à une exigence en utilisant la recherche

Le bouton ![Rechercher](resources/icone-browse.png){class="icone"} permet d'ajouter à la table 'Cas de test vérifiés par cette exigence' un ou plusieurs cas de test via l'outil de recherche :

- Dans la partie gauche, il est possible d'ajouter des filtres et autres critères de recherche.
- Dans la partie droite, la liste des cas de test correspondants s'affiche. Il est alors possible de lier tous les cas de test recherchés via ![Tout associer](resources/icone-link-all.png){class="icone"} ou seulement une sélection via ![Associer la sélection](resources/icone-link-selection.png){class="icone"}.

![Recherche de cas de test](resources/recherche-cas-de-test-fr.png){class="pleinepage"}

Une fois lié à l'exigence, le cas de test et ses attributs apparaissent dans la table. Un lien cliquable sur le nom du cas de test permet d'accéder à la page de consultation de celui-ci.

![Cas de test vérifiés par l'exigence](resources/cas-de-test-verifie-fr.png){class="pleinepage"}

L'ancre du bloc 'Cas de test vérifiés par cette exigence' se met automatiquement à jour avec le nombre de cas de test liés :  ![Cas de test vérifiés par l'exigence](resources/ancre-cas-de-test-verifies-fr.png){class="icone"}.

Une fois liée, l'exigence apparaît également dans la table 'Exigences vérifiées par ce cas de test' des cas de test associés.
