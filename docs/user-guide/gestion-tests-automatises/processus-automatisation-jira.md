# Suivre le processus d’automatisation dans Jira

## Présentation du plugin Workflow d'Automatisation Jira

Le plugin Workflow Automatisation Jira (WAJ) est une externalisation dans Jira du workflow d'automatisation. 

Il permet de transmettre à l’automaticien, depuis Squash TM, les cas de test à automatiser. L'automaticien peut ensuite indiquer l'avancement de son travail via un workflow personnalisé et peut déclarer l’automatisation du cas de test sans avoir à modifier les données dans Squash TM.

Lorsque le ticket atteint son état final d’automatisation, l’exécution automatique du cas de test correspondant est possible dans un plan d'exécution Squash TM.

## Première transmission de cas de test 

Pour qu'un cas de test soit transmis à Jira pour automatisation, l'utilisateur doit le rendre *Éligible* en cliquant sur le bouton radio correspondant dans le bloc Automatisation du cas de test. 

Au clic, les champs spécifiques à l'automatisation et le bouton de transmission matérialisé par une flèche apparaissent. 

![Bloc Automatisation - Plugin WAJ](resources/blocAutom_Transmettre_FR.png){class="pleinepage"}

Si un serveur d'exécution **Squash AUTOM** est configuré sur le projet, alors les champs Technologie du test automatisé, URL du dépôt de code source et Référence du test automatisé sont à compléter.

Si un serveur d'exécution **Squash TF** est configuré sur le projet, alors le champ Script Auto est à compléter. 

Pour un **cas de test classique**, au premier clic sur le bouton de transmission, un ticket est créé dans Jira et les champs du bloc Automatisation sont automatiquement complétés.

Pour un **cas de test Gherkin ou BDD**, au premier clic sur le bouton de transmission, un fichier .feature ou .robot est créé sur le serveur de partage de code source (si il est paramétré sur le projet), un ticket est créé dans Jira et les champs du bloc Automatisation sont automatiquement complétés.

Il est possible de transmettre en masse plusieurs cas de test éligibles à l’automatisation à partir de la bibliothèque des cas de test, en cliquant sur le bouton Importer/Exporter puis sur le sous menu 'Transmettre pour automatisation'. Une popup informe du nombre de cas de test transmis.

Chaque ticket transmis avec succès donne lieu à la création d'un ticket dans Jira.


![Ticket Jira - Plugin WAJ](resources/ticketJira_WAJ_FR.png){class="pleinepage"}


!!! info "Info"
    Le champ 'Résumé' du ticket Jira, valorisé par la référence et le libellé du cas de test Squash TM, ne peut contenir que 255 caractères. Il est conseillé de convenir d’une nomenclature 'Référence' et 'Intitulé' de sorte que la somme des deux soit inférieure à 255 caractères.



## Synchronisation des données

Le libellé et la référence du cas de test ainsi que toutes les informations contenues 
dans le bloc Automatisation sont synchronisés automatiquement entre Squash TM et Jira.

Ainsi, en cas de **modification du cas de test côté Squash TM**, les données suivantes seront automatiquement mises à jour sur le ticket Jira à la prochaine synchronisation :

- le libellé du cas de test
- la référence du cas de test
- la priorité 
- le dépôt de code source

Toutes les autres modifications du cas de test n'impacteront pas le ticket jira à la prochaine synchronisation.

En cas de **modification du ticket côté Jira**, les données suivantes seront automatiquement mises à jour sur le cas de test Squash TM à la prochaine synchronisation :

- l'état
- l'assignation

Toutes les autres modifications du ticket Jira ne sont ni impactées ni impactantes lors de la prochaine synchronisation.

!!! warning "Focus"
    Toute modification du champ 'Résumé' sur le ticket Jira sera écrasée par les données de Squash TM à la synchronisation suivante.

## Retransmission de cas de test    

Lorsqu'une modification, (autre que l'une des modification synchronisée automatiquement listées précedemment) est effectuée sur un cas de test Squash TM, il est parfois nécessaire de retransmettre le cas de test pour mettre à jour les informations du ticket Jira. 

Le tableau ci-dessous résume les actions réalisées lors de la retransmission d'un cas de test: 

| Modification effectuée | Conséquence de la retransmission |
|-----|------------|
| Déplacement du cas de test Squash TM vers un projet dont la configuration du plugin est identique | Le cas de test et le ticket Jira sont mis à jour |
| Déplacement du cas de test Squash TM vers un projet dont la configuration du plugin est différente | La retransmission du cas de test n'est pas possible |
Suppression du cas de test Squash TM | Le ticket Jira associé n'est pas supprimé mais ne sera plus mis à jour par synchronisation |
| Déplacement du ticket Jira vers un autre projet | La retransmission du cas de test Squash TM n'est pas possible |
| Déplacement du ticket Jira vers un autre type de ticket | La retransmission du cas de test Squash TM n'est pas possible |
| Suppression du ticket Jira | Création d'un nouveau ticket Jira |
|

Lorsqu’aucune modification n'est faite sur le périmètre de synchronisation, le bouton de transmission a pour seule fonction de mettre à jour le fichier du script déposé sur le serveur de partage de code source.


!!! info "Info"
    Si un cas de test est modifié après son automatisation et qu’une mise à jour du script automatisé doit être effectué, il est possible de retransmettre le cas de test afin d’avertir l’automaticien de la modification. Cette retransmission déclenche la création d’un nouveau ticket Jira à l’état initial du workflow défini dans Jira.

## Exécution automatique

Une fois que le ticket Jira a atteint l’état final définit dans la configuration du plugin, Squash TM récupère l’information et rend le cas de test exécutable 
automatiquement (à la condition d’avoir paramétré un serveur d’exécution et un serveur de partage de code source pour un cas de test Gherkin ou BDD). 

Le cas de test automatisé peut maintenant être exécuté dans le plan d'exécution d'une itération ou d'une suite de test.



