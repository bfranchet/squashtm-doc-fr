# Exécuter un cas de test automatisé

## Lancement des tests automatisés

Les tests automatisés sont facilement identifiables dans le plan d'exécution d'une itération ou d'une suite de test grâce à l'icône ![Exécution d'un test automatisé](resources/icone_robot.png){class="icone"} affiché dans la colonne 'Mode'. 


Il est possible d'exécuter tous les cas de tests automatisés présents dans le plan d'exécution en cliquant sur le bouton "Lancer les tests automatisés" présent en haut à droite du plan d'exécution : 

![Exécution d'un test automatisé](resources/lancer_tests_auto2_FR.png){class="pleinepage"}

Il est également possible d'exécuter un seul cas de test automatisé présent dans le plan d'exécution en cliquant sur le sous-menu "Exécution automatique" :

![Exécution d'un test automatisé](resources/lancer_tests_auto1_FR.png){class="pleinepage"}

Lorsque l'exécution automatique démarre, une popup de Supervision de l'exécution automatique des tests s'affiche. Lorsque le test automatisé est exécuté avec Squash TF, une barre de progression affiche l'avancement de l'exécution des tests. Cette barre de progression n'est pas disponible lors d'une exécution avec Squash AUTOM.   


![Popup de supervision de l'exécution automatique des tests](resources/execution_tests_auto_popup_FR.png){class="pleinepage"}
 

!!! info "Info"
    Dans le cadre d'une exécution avec un serveur d'exécution automatisé Squash TF de type Jenkins, si ce dernier est configuré pour laisser le choix du serveur d’exécution, une popup permettant de choisir le serveur apparaît avant le démarrage de l'exécution. 

Une fois l'exécution des tests terminée, les statuts d'exécutions sont automatiquement mis à jour dans le plan d'exécution et une suite automatisée est créée.

## Les suites de tests automatisées

Une suite automatisée est systématiquement créée après chaque exécution automatique des tests. La suite est créée au niveau de l'itération et de la suite de test. 
Depuis cette suite automatisée, il est possible de consulter :

- le statut d’exécution global de la suite automatisée (qui dépend du statut des exécutions associées)
- le détail des exécutions
- les rapports de l’ensemble des exécutions de la suite automatisée

![Suite automatisée](resources/suite_auto_FR.png){class="pleinepage"}

## Consulter une exécution automatisée et ses rapports d’exécution

Il est possible de consulter une exécution et ses rapports **depuis le plan d'exécution**.

La page de consultation de l'exécution s'affiche en cliquant sur le sous-menu "Historique des exécutions" puis sur le numéro de l'exécution.

![Consulter l'historique des exécutions](resources/historique_executions1_FR.png){class="pleinepage"}

Depuis la page de consultation de l'exécution, les rapports d'exécution sont téléchargeables dans le bloc pièce-jointe.

![Consulter les rapports d'exécution](resources/rapport_pj_FR.png){class="pleinepage"}

Il est également possible de consulter une exécution et ses rapports **depuis la suite automatisée**.

La page de consultation de l'exécution s'affiche en cliquant sur le bouton "Détails des exécutions" puis sur le numéro de l'exécution.
![Consulter l'historique des exécutions](resources/detail_execution_FR.png){class="pleinepage"}

Depuis la page de consultation de l'exécution, les rapports d'exécution sont téléchargeables dans le bloc pièce-jointe.

Il est également possible de consulter les rapports d'exécutions depuis la suite automatisée en cliquant sur le bouton "Rapport des exécutions".

![Consulter les rapports d'exécution](resources/consulter_rapport_FR.png){class="pleinepage"}

