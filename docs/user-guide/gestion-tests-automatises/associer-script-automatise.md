# Associer un script automatisé

La méthode d'association d'un script automatisé à un cas de test Squash TM peut se faire de façon automatisée ou manuelle et dépend du serveur d'exécution automatisée utilisé ainsi que du workflow d'automatisation choisi.

## Association manuelle de scripts automatisés avec Squash Autom 

Le tableau ci-dessous récapitule la méthode d'association d'un script automatisé à un cas de test (Classique, Gherkin ou BDD) avec Squash Autom en fonction du workflow utilisé :

| Workflow d'automatisation | Type de serveur d'exécution | Champs à compléter |
|-----|------------|------|
| Squash TM | Squash Autom | Les 3 champs "*Techno*", "*URL SCM*" et "*RÉF. TEST AUTO*" depuis les tables "M'étant assignés" ou "Vue globale" de **l'espace Automatisation (Automaticien)**.|
| Autom Jira | Squash Autom | Les 3 champs "*Technologie du test automatisé*", "*URL du dépôt de code source*" et "*Référence du test automatisé*" depuis **l'espace Cas de test**, bloc Automatisation.  |
| Aucun workflow | Squash Autom | Les 3 champs "*Technologie du test automatisé*", "*URL du dépôt de code source*" et "*Référence du test automatisé*" depuis **l'espace Cas de test**, bloc Automatisation.  | 

- Dans le champ **Techno/Technologie du test automatisé**: sélectionner la technologie d'automatisation du cas de test parmi les options proposées dans la liste.
- Dans le champ **URL SCM/URL du dépôt de code source**: sélectionner l'URL du dépôt de code source contenant le script automatisé dans la liste des dépôts de code source enregistrés dans Squash TM.
- Dans le champ **RÉF. TEST AUTO/Référence du test automatisé**: renseigner le chemin du script automatisé au sein du dépôt de code source sous le format <nom du dépôt de code source\>/<chemin 
vers le script auto\>#<nom du cas de test au sein du script\>

*Exemple: association depuis la table M'étant assignés de l'espace Automatisation (Automaticien)*.

![Associer un script auto à un cas de test classique avec SquashAutom](resources/TableAutom-FR.png){class="pleinepage"}


*Exemple: association depuis le bloc Automatisation de l'espace Cas de test*.

![Associer un script auto à un cas de test classique avec SquashAutom](resources/BlocAutom-NoWF-FR.png){class="pleinepage"}


## Association manuelle de scripts automatisés avec Squash TF

Le tableau ci-dessous récapitule la méthode d'association d'un script automatisé à un **cas de test classique** avec Squash TF en fonction du workflow utilisé :

| Workflow d'automatisation | Type de serveur d'exécution | Champs à compléter |
|-----|------------|------|
| Squash TM | Jenkins | Le champ "Script Auto" disponible au clic sur le bouton **[Crayon]** depuis la colonne "Squash TF" des tables "M'étant assignés" et "Vue globale" de **l'espace Automatisation (Automaticien)**.  |
| Autom Jira | Jenkins | Le champ "Script Auto" depuis **l'espace Cas de test**, bloc Automatisation.|
| Aucun workflow | Jenkins | Le champ "Script Auto" depuis **l'espace Cas de test**, bloc Automatisation. | 

Le champ **Script Auto** peut être édité manuellement ou bien complété en cliquant sur le bouton **[Loupe]** pour sélectionner un script dans la liste des scripts des jobs associés au projet.


*Exemple: association depuis la table Vue globale de l'espace Automatisation - Vue Automaticien*.

![Associer un script auto à un cas de test classique avec Squash TF](resources/ScripAuto_UUID1-FR.png){class="pleinepage"}

*Exemple: association depuis le bloc Automatisation de l'espace Cas de test*.

![Associer un script auto à un cas de test classique avec Squash TF](resources/ScriptAuto_BlocAutom1-FR.png){class="pleinepage"}


!!! warning "Focus"
    **Cas particulier des Cas de test Gherkin et BDD :**
    <br/>Si un projet Squash TM est associé au dépôt d'un serveur de partage de code source et qu'un des jobs associés à ce même projet est également associé au même dépôt alors l'association de script automatisé à un cas de test Gherkin ou BDD se fait automatiquement lors de la transmission du cas de test pour automatisation.


## Association automatique de scripts automatisés

L’association automatique d'un cas de test Squash TM à un script automatisé se fait via son UUID. L'UUID du cas de test doit être renseigné dans les métadonnées d'un script automatisé se trouvant dans un job associé au projet Squash TM pour que cela focntionne.

Le lien se fait lorsque le cas de test est passé au statut 'Automatisé'. Un nom de script auto non modifiable s'affiche alors dans la colonne "Squash TF".

!!! warning "Focus"
    Si l'UUID d'un cas de test est inscrit dans les métadonnées de plus d'un script auto, un lien "scripts en conflit" apparaît lorsqu'un utilisateur tente de passer le cas de test au statut "Automatisé". La liste des scripts automatisés contenant l'UUID est consultable au clic sur le lien.


