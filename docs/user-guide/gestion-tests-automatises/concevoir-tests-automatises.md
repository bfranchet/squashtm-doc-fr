# Concevoir des cas de test automatisés 

## Rappels sur les cas de test BDD

Les cas de test BDD se différencient des cas de test classiques par la structure de leurs pas de test. Les pas de test BDD sont basés sur la syntaxe Gherkin.
Ils se composent de deux éléments :

- un Mot-clé : Given, When, Then, And, But
- une Action : phrase d’action qui suit le Mot-clé

!!! info "Info"
    Il est possible d'utiliser les cas de test au format Gherkin pour initier l'automatisation mais les cas de test au format BDD disposent de fonctionnalités plus poussées.

## La bibliothèque d'actions 

!!! info "Info"
    L'espace Bibliothèque d'actions est accessible via le plugin 'Bibliothèque d'actions'. Ce plugin fait partie de la licence  Squash AUTOM Premium.

La Bibliothèque d'actions offre deux fonctionnalités majeures :

-  La gestion des actions utilisées par les cas de test au format BDD
-  La suggestion des actions disponibles dans les projets auxquels est habilité l'utilisateur à la rédaction des pas de test BDD

Chaque projet dispose de sa bibliothèque d'actions. Mais une même action peut être utilisée par des cas de test issus de différents projets.

### La page de consultation d'une action 

![Page de consultation de l'action](./resources/page-consultation-action2.jpg){class="pleinepage"}

La page de consultation d’une action se compose de 4 blocs :

- Le bloc **'Informations'** contient le nom du projet et la description de l’action.
- Le bloc ***'Paramètres'*** permet de [gérer les paramètres de l’action](#gerer-les-parametres-dune-action) 
- Le bloc **'Cas de test utilisant l'action'** permet de lister les informations des cas de test utilisant l’action. Au clic sur le nom du cas de test, sa page de consultation s'affiche.
- Le bloc **'Implémentation'** affiche les informations relatives à l’implémentation de l’action. 
    - La colonne 'Technologie' indique la technologie d’implémentation paramétrée au niveau du projet Squash TM du cas de test utilisant l'action lorsque celui est passé au statut "Automatisé" pour la dernière fois.
    - La colonne 'Date de dernière implémentation' correspond à la date à laquelle un cas de test utilisant l'action a été passé au statut "Automatisé" pour la dernière fois.

### Consulter une action depuis l'espace Cas de test

Depuis l'onglet "Pas de test" d'un cas de test BDD, il est possible d'accéder directement à la page de consultation d'une action en cliquant sur le bouton **[...]** puis sur **"Consulter l'action"** :

![Consulter l'action](./resources/consultation-action.png){class="pleinepage"}

Cette option n'est cliquable que si l'utilisateur dispose d'une habilitation sur le projet contenant l'action.

Au clic, l'action s'affiche dans une page de niveau 2. Si l’utilisateur dispose des droits nécessaires, il peut modifier la description et le contenu du bloc 'Paramètres' de l'action.
Le bloc 'Informations' indique le projet contenant l’action.

![Page de consultation niveau 2 de l'action](./resources/page-consultation-action.jpg){class="pleinepage"}

## Gérer les paramètres d'une action

Le bloc 'Paramètres' présent sur la page de consultation d'une action permet de gérer le nom et la valeur par défaut des paramètres de celle-ci. La valorisation de ce bloc diffère selon la manière dont l'action est créée.

À la création d'une action avec paramètre depuis la Bibliothèque d'actions :

- le nom du paramètre est automatiquement renseigné dans le bloc 'Paramètres'
- la valeur par défaut est vide

Sauf si le nom du paramètre renseigné est un nombre. Dans ce cas, voir ci-dessous.

À la création d’une action avec paramètre valorisé par une valeur libre depuis l’Espace Cas de test :

- le nom du paramètre est automatiquement valorisé par une valeur générique « param1 », « param2 », …
- la valeur libre saisie au niveau du pas de test est automatiquement renseignée comme valeur par défaut du paramètre.

![Parametre de l'action](./resources/parametre-action.jpg){class="pleinepage"}

Il est possible de modifier le nom et la valeur par défaut du paramètre depuis le bloc 'Paramètres' :

- Pour le nom du paramètre, seuls les lettres majuscules, minuscules, les chiffres et les tirets **-** et **_** sont autorisés
- Pour la valeur par défaut, tous les caractères sont autorisés sauf **<**, **>** et **"**.

Ces deux champs sont limités à 255 caractères moins le nombre de caractères déjà présents dans les parties fixes de l’action.

## La suggestion d'actions

Lorsque le plugin Bibliothèque d'actions est installé sur Squash TM, une suggestion d'actions est activée à la création et à la modification des pas de test des cas de test BDD.

### Fonctionnement de la suggestion

À la création ou la modification d'un pas de test, les actions déjà présentes dans les projets auxquels est habilité l'utilisateur et qui correspondent à sa saisie lui sont suggérées. La correspondance se fait uniquement sur les parties fixes de l'action et l'ordre des mots n'a pas d'incidence sur la proposition de suggestions.

Il est possible de naviguer entre les différentes suggestions avec les flèches du clavier. Pour sélectionner une suggestion, cliquer dessus ou sur le bouton **[Entrée]** du clavier. Pour ajouter l'action sélectionnée au cas de test, il suffit de cliquer sur le bouton **[Entrée]** du clavier.

![Suggestion d'action pas de test BDD](./resources/suggestion-action-pastest.jpg){class="pleinepage"}

### Suggestion d'actions inter projet

Les actions suggérées lors de la saisie ou la modification d'un pas de test BDD sont toutes celles présentes dans les projets sur lesquels l'utilisateur dispose de droits de lecture.

Il existe trois cas de figure :

- Lorsque l'action suggérée est dans le projet **courant** et d'**autres** projets
- Lorsque l'action suggérée est uniquement dans un seul **autre** projet 
- Lorsque l'action suggérée est dans plusieurs **autres** projets

**Cas 1 :**
Lorsque l'action suggérée est à la fois dans le projet courant et dans d'autres projets, c'est l'action présente dans le projet courant qui sera rattachée au cas de test.

**Cas 2 :**
Lorsque l'action suggérée est dans un seul autre projet, c'est cette action qui sera rattachée au cas de test.

**Cas 3 :**
Lorsque l'action suggérée est dans plusieurs autres projets, une popup s'affiche pour permettre à l'utilisateur de choisir le projet de l'action qui sera rattachée au cas de test.

![Choix du projet pour l'action](./resources/selection-projet-action.jpg){class="pleinepage"}

### Valoriser le paramètre d'une action suggérée

Au sein des pas de test, le paramètre d'une action peut être valorisé par une **valeur libre**. Interprétée dans le code comme étant une variable, elle peut prendre n'importe quelle valeur et différer d'un cas de test à un autre.
<br/>Pour définir une valeur libre dans une action, elle doit être écrite entre guillemets : **"valeur"**.

Le paramètre d'une action peut également être valorisé par un **paramètre du cas de test**. Le paramètre ainsi renseigné dans l'action du pas de test peut prendre plusieurs valeurs définies dans le tableau des jeux de données du cas de test.
<br/>Pour définir un paramètre du cas de test dans une action, il doit être écrit entre les caractères <> : **<valeur\>**.

Dans la syntaxe Gherkin, l'utilisation de ces paramètres correspond à un Scenario Outline.

![Paramètre d'une action suggérée](./resources/parametre-action-suggere.jpg){class="pleinepage"}

Une fois qu’une action suggérée est sélectionnée, il est possible de modifier dans le champ de saisie son paramètre et de le remplacer soit par une valeur libre entre " " soit par un paramètre du cas de test entre < >.
Cette modification sera spécifique au cas de test. Ce dernier sera rattaché à l’action suggérée même si le paramètre est modifié.