# Suivre le processus d’automatisation dans Squash

## Présentation du workflow d’automatisation Squash 

Le schéma du workflow d’automatisation Squash est représenté ci-dessous.
Il s’applique uniquement aux cas de tests éligibles à l’automatisation contenus dans les projets pour lesquels le workflow d’automatisation Squash a été activé.
Il est composé de différentes étapes correspondant à des statuts d’automatisation. Les statuts d’automatisation sont accessibles en fonction du profil de l’utilisateur :

- Profils testeurs fonctionnels : statuts 'En cours de rédaction', 'Prêt pour transmission', 'Transmis' et 'Suspendu'
- Profil automaticien : statuts 'Rejeté', 'En cours d’automatisation' et 'Automatisé'

![Workflow d'automatisation](./resources/workflow-autom-fr.png){class="centre"}

Ce schéma est donné à titre de recommandation mais il ne comporte que deux étapes obligatoires : Transmis et Automatisé. 

Un cas de test doit obligatoirement avoir été Transmis, pour se voir attribuer les statuts réservé au profil automaticien, c’est-à-dire Rejeté, En cours d’automatisation et Automatisé.
Les autres étapes sont facultatives, le workflow peut ainsi s’adapter à différentes organisations d’équipes.

!!! info "Info"
    Il est possible d'externaliser le [workflow d'automatisation vers Jira](./processus-automatisation-jira.md) en installant le plugin "Workflow automatisation Jira" accessible avec la licence Squash AUTOM Premium.

## Éligibilité à l’automatisation 

Une fois le workflow d’automatisation activé, le bloc 'Automatisation' est mis à jour sur la page de consultation d’un cas de test.

Ce bloc contient un champ "Éligibilité à l’automatisation" qui peut être valorisé avec l’une des options suivantes : 

- À instruire (Par défaut)
- Éligible
- Non éligible

Ce champ permet au testeur d’indiquer si le cas de test est à automatiser ou non. Lorsque le testeur fonctionnel sélectionne l'option "Éligible", de nouveaux éléments s’affichent :

- Un champ "Priorité (numérique)" : Plus la valeur numérique renseignée est élevée plus la priorité est haute.
- Un champ "Statut d’automatisation" : liste déroulante avec les options 'En cours de rédaction' (par défaut), 'Prêt pour transmission' et 'Suspendu'.
- Un champ "UUID" : il s’agit d’un identifiant universel unique de type GUID généré automatiquement à la création du cas de test.
- Un bouton ![Transmettre](./resources/transmettre.svg){class="icone"} pour transmettre le cas de test en haut du bloc.

![Bloc automatisation d'un cas de test](./resources/bloc-automatisation-ct.jpg){class="pleinepage"}

L’éligibilité à l’automatisation est modifiable individuellement depuis la page de consultation du cas de test ou en masse depuis la page de recherche via le bouton ![Bouton Modification en masse](./resources/modification-masse.png){class="icone"}.

![Modification en masse de cas de test](./resources/masse-eligibilite-automatisation.jpg){class="pleinepage"}


!!! info "Info" 
    - Dans le cas où le workflow d’automatisation Squash est activé sur un projet contenant des cas de test déjà associés à des scripts automatisés, le champ "Éligibilité à l’automatisation" est automatiquement valorisé à 'Éligible' et le champ "Statut d’automatisation" à 'Automatisé'.
    - Pour un cas de test éligible à l’automatisation, le passage aux statuts 'À instruire' et 'Non éligible' ne supprime pas la priorité et le statut d’automatisation renseignés. Ces valeurs sont rétablies dès que le cas de test repasse au statut 'Éligible'.
    - De même, lorsque le workflow d’automatisation est désactivé puis réactivé sur un projet, l’éligibilité à l’automatisation, la priorité et le statut d’automatisation sont conservés.


## Espace Automatisation - Vue Testeur fonctionnel 

### Onglet "Prêts pour transmission"

Cet onglet contient un tableau avec l’ensemble des cas de test éligibles à l’automatisation qui sont au statut 'Prêt pour transmission'.
Depuis cet onglet, il est possible d’appliquer le statut 'Transmis' aux cas de test sélectionnés et de modifier leur priorité.

### Onglet "A valider avant transmission"

Cet onglet contient un tableau avec l’ensemble des cas de test éligibles à l’automatisation qui sont au statut 'En cours de rédaction', 'Rejeté' ou 'Suspendu'.
Depuis cet onglet, il est possible d’appliquer le statut 'Prêt pour transmission' aux cas de test sélectionnés et de modifier leur priorité.

### Onglet "Vue globale"

Cet onglet contient un tableau avec l’ensemble des cas de test éligibles à l’automatisation quel que soit leur statut d’automatisation.
Depuis cet onglet, il est possible d’appliquer les statuts 'Transmis', 'Suspendu', 'Prêt pour transmission' et 'En cours de rédaction' aux cas de test sélectionnés ainsi que de modifier leur priorité.

![Onglet Vue Globale - Testeur fonctionnel](./resources/vue-globale-testeur.jpg){class="pleinepage"}

## Espace Automatisation - Vue Automaticien

Le profil 'Automaticien' a accès aux espaces suivants :

- Espace Automatisation - Vue automaticien
- Mon compte

L’automaticien n’a pas accès à l’espace Cas de test mais peut consulter les cas de test à automatiser depuis l’espace Automatisation via le nom du cas de test.
Il peut modifier le statut d’automatisation des cas de test uniquement avec l’un des statuts suivants : 'Rejeté', 'En cours d’automatisation' et 'Automatisé'.
Il peut également s’assigner ou se désassigner des cas de test à automatiser.


### Onglet "M’étant assignés"

Cet onglet contient un tableau avec l’ensemble des cas de test éligibles à l’automatisation et assignés à l’utilisateur connecté à Squash TM.
Depuis cet onglet, il est possible d’appliquer les statuts 'Automatisé', 'En cours d’automatisation' et 'Rejeté' aux cas de test sélectionnés.

L’utilisateur peut également se désassigner des cas de test et associer un script automatisé aux cas de test via les champs 'Technologie du test automatisé', 'URL du dépôt de code source', 'Référence du test automatisé' (Squash AUTOM) ou via le champ 'Script auto' s’il s’agit d’un cas de test classique (Squash TF).

### Onglet "À traiter"

Cet onglet contient un tableau avec l’ensemble des cas de test éligibles à l’automatisation aux statuts 'Transmis' et 'En cours d’automatisation' mais sans assignation.
Depuis cet onglet, l’utilisateur peut s’assigner des cas de test à automatiser.

### Onglet "Vue globale"

Cet onglet contient un tableau avec l’ensemble des cas de test éligibles à l’automatisation, quels que soient leur statut et leur assignation.
Depuis cet onglet, il est possible d’appliquer les statuts 'Automatisé', 'En cours d’automatisation' et 'Rejeté' aux cas de test sélectionnés.

L’utilisateur peut également s’assigner et désassigner des cas de test et associer un script automatisé aux cas de test via les champs Squash AUTOM ou Squah TF.

![Onglet Vue Globale - Automaticien](./resources/vue-globale-automaticien.jpg){class="pleinepage"}

## Les statuts d'automatisation

### Le statut "Prêt pour transmission" (En tant que Testeur)

Une fois les cas de test déclarés éligibles à l’automatisation, les testeurs fonctionnels peuvent accéder à l’onglet "À valider avant transmission" de l’Espace Automatisation – Vue testeur fonctionnel, où figurent les cas de test ayant le statut d’automatisation 'En cours de rédaction'.

Depuis cet onglet, le testeur peut sélectionner des cas de test et les passer au statut 'Prêt pour transmission' à l’aide du bouton ![Prêt pour transmission](./resources/ready_for_transmission.svg){class="icone"}.
Cette action a pour conséquence de faire disparaître les cas de test de l’onglet "À valider avant transmission" pour les afficher dans l’onglet "Prêt pour transmission".

!!! info "Info"
    Le passage au statut 'Prêt pour transmission' peut également être effectué depuis le bloc "Automatisation" sur la page de consultation d’un cas de test.

### Le statut "Transmettre" (En tant que Testeur)

Les cas de test au statut 'Prêt pour transmission' peuvent ensuite être transmis à l’automaticien.
La transmission de cas de test peut se faire de différentes manières :

- Depuis l'onglet "Prêt pour transmission", en cliquant sur le bouton ![Transmettre](./resources/transmettre.svg){class="icone"}
- Depuis la page de consultation d’un cas de test, en cliquant sur le bouton ![Transmettre](./resources/transmettre.svg){class="icone"}
- Avec la transmission en masse, en cliquant sur l'option 'Transmettre pour automatisation' depuis le bouton ![Importer/Exporter…](./resources/import_export.svg){class="icone"} présent dans la bibliothèque de l'espace Cas de test.

!!! warning "Focus"
    Pour que la transmission en masse fonctionne, il faut que le workflow d’automatisation soit activé pour tous les projets qui contiennent les éléments sélectionnés. 
    <br/>Sinon, l’option est grisée.

Le testeur fonctionnel retrouve les cas de test transmis dans l’onglet "Vue globale" de l’Espace Automatisation. 

!!! info "Info" 
    Lorsqu’un cas de test est transmis, une première tentative d’association automatique du cas de test à un script automatisé est effectuée notamment si le projet Squash tM est lié à un serveur de partage de code source. Les champs Squash AUTOM ou Script auto sont alors renseignés automatiquement par Squash TM. 
    <br/>Une deuxième tentative est effectuée lors du passage au statut « Automatisé » (voir la partie [Association automatique](./associer-script-automatise.md)) notamment avec la liaison automatique via UUID.

### Le statut "Suspendu" (En tant que Testeur)

Une fois les cas de test transmis pour automatisation ou lorsqu’ils sont en cours d’automatisation, le testeur peut décider de suspendre leur automatisation en les passant au statut 'Suspendu' à l'aide de l'option **[Suspendu]** de l'onglet "Vue globale"

Les cas de test au statut 'Suspendu' repassent alors dans l’onglet "À valider avant transmission"

!!! info "Info"
    Le passage au statut 'Suspendu' peut également être effectué depuis le bloc "Automatisation" sur la page de consultation d’un cas de test.

### Le statut "Rejeté" (En tant qu'automaticien)

Après s’être assigné des cas de test, si l’automaticien considère que l’automatisation de ces cas de test n’est pas possible en l’état, depuis l’onglet "M’étant assignés", il peut rejeter leur automatisation en cliquant sur l'option **[Rejeté]**.

Une fois rejetés, les cas de test restent dans l’onglet "M’étant assignés" de l’automaticien. Ils apparaissent également dans l’onglet "À valider avant transmission" du testeur fonctionnel pour qu’il effectue les modifications nécessaires en vue de leur automatisation.

### Le statut "En cours d'automatisation" (En tant qu'automaticien)

Lorsque l’automaticien commence à automatiser les cas de test qu’il s’est assignés, depuis l’onglet "M’étant assignés", il peut les sélectionner puis les passer au statut 'En cours d’automatisation'.

Lorsque qu’un serveur d’automatisation est associé au projet contenant les cas de test, il peut également :

- Associer manuellement des scripts automatisés avec les champs Squash Autom
- Associer manuellement des scripts automatisés avec le champ Script auto. de Squash TF

!!! tip "En savoir plus"
    Pour en savoir plus sur l'association de scripts automatisés aux cas de test, consulter la page suivante : [Associer des scripts automatisés](./associer-script-automatise.md) 

### Le statut "Automatisé" (En tant qu'automaticien)

Une fois l’automatisation des tests terminée, l’automaticien peut les sélectionner et les passer au statut 'Automatisé' en cliquant sur le bouton ![Automatisé](./resources/automated.svg){class="icone"}.
Les tests au statut 'Automatisé' peuvent ensuite être exécutés automatiquement depuis l’Espace Campagnes.

!!! warning "Focus"
    Pour passer les cas de test au statut "Automatisé", ils doivent être associés à des scripts automatisés soit manuellement soit automatiquement (via la transmission ou via l’UUID) :
    <br/>Dans le cas d'une automatisation avec Squash AUTOM, il est impératif d'avoir renseigné les 3 champs Squash AUTOM.
    <br/>Dans le cas d'une automatisation avec Squash TF, il est nécessaire d'avoir un script automatisé associé aux cas de test.


!!! info "Info"
    Pour les cas de test déjà associés à des scripts automatisés avant l’activation du workflow d’automatisation, il n’est plus possible de consulter/modifier les champs AUTOM ou le script auto d'un cas de test depuis sa page de consultation. Cela se fait uniquement depuis l’Espace Automatisation – Vue automaticien.
