# Synchroniser des objets agiles Jira dans Squash

!!! danger "Attention"
    Page en cours de rédaction. 
    <br/>Pour accéder à l'ancienne version de la documentation, cliquer sur le lien suivant : [Wiki Squash TM](https://sites.google.com/a/henix.fr/wiki-squash-tm/).

## Principe

Une fois le plugin configuré correctement, les tickets Jira sont automatiquement synchronisés en tant qu'exigences dans Squash TM et sont maintenus à jour. A la première mise à jour, le répertoire de synchronisation ainsi que l'ensemble des exigences synchronisées contenues dans ce répertoire sont créés. 

Cette partie détaille l’organisation de ce patrimoine dans l’espace Exigences.

## Les exigences synchronisées

Les tickets Jira sont représentés dans Squash TM par des exigences synchronisées. Ces exigences apparaissent avec un nom en grisé dans l’arborescence des exigences et sont précédées d'une icône ![Sync](resources/sync.png){class="icone"}.

Cette icône indique non seulement qu'il s'agit d'une exigence synchronisée mais elle donne également des informations sur le statut de synchronisation du ticket Jira associé via le code couleur suivant :

- icône verte : l'exigence est toujours mise à jour par synchronisation
- icône jaune : l'exigence n'est plus mise à jour par synchronisation car le ticket Jira correspondant n'entre plus dans le périmètre de synchronisation défini au niveau de la configuration du plugin (par exemple, le ticket Jira a été passé dans un statut qui ne figure pas dans le filtre défini pour la synchronisation)
- icône rouge : l'exigence n'est plus mise à jour par synchronisation car le ticket Jira correspondant a été supprimé dans Jira ou a été déplacé dans un autre projet Jira (sa clé a été modifiée)
- icône noire : le statut de synchronisation du ticket est inconnu (par exemple, lorsque la synchronisation globale est en échec)

![Statut synchro arborescence](resources/fr_statut_synchro.png){class="pleinepage"}

De plus, les exigences synchronisées disposent de trois informations supplémentaires par rapport aux exigences natives :

- Un lien hypertexte vers le ticket Jira d’origine
- Le statut de synchronisation du ticket Jira qui indique si l'exigence est toujours mise jour par synchronisation et si le ticket Jira correspondant est toujours présent dans Jira
- Le statut de la synchronisation à laquelle appartient le ticket Jira, qui permet d’alerter l’utilisateur si la communication entre Squash TM et Jira est rompue ou qu’une erreur est survenue lors du dernier process de mise à jour


## Le répertoire cible

### Statut de la synchronisation

Les répertoires cibles sont également précédés d'une icône ![Sync](resources/sync.png){class="icone"} et adoptent un code couleur similaire aux exigences synchronisées :

- icône verte : la synchronisation correspondant au répertoire cible est en succès et toutes les exigences synchronisées qu'il contient sont mises à jour par synchronisation
- icône jaune : la synchronisation correspondant au répertoire cible est en succès mais il contient des exigences qui ne sont plus mises à jour par synchronisation parce qu'elles sont sorties du périmètre de synchronisation ou parce qu'elles n'existent plus dans Jira
- icône rouge : la synchronisation correspondant au répertoire cible est en échec (par exemple, lorsque la communication entre Squash TM et Jira est rompue ou qu’une erreur est survenue lors du dernier process de mise à jour)