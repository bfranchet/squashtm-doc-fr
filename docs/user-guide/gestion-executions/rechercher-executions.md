# Rechercher des exécutions

La page de recherche des exécutions se divise en 2 parties : à gauche le bloc des critères de recherche et à droite les résultats de recherche.

!!! tip "En savoir plus"
    Consulter la page "[Rechercher un objet](../presentation-generale/fonctionnalites-objet.md#rechercher-un-objet)" pour connaître le fonctionnement de la page de recherche.

Pour rappel, le périmètre reprend par défaut les projets présents dans le filtre projet. Il est également possible de faire une recherche sur un périmètre personnalisé en sélectionnant un ou plusieurs dossiers ou une sélection de campagne, itération et suite de test.

![Page de recherche d'items de plan d'exécution](./resources/page-recherche-executions.jpg){class="pleinepage"}

## Les critères de recherche des exécutions 

Les critères de recherche correspondant aux exécutions sont nombreux et peuvent être couplés les uns aux autres.

Les critères de recherche sont répartis en plusieurs catégories de critères :

- Les **informations** du cas de test
- Les informations liées à l'**automatisation** du cas de test
- Les informations liées aux **jalons** de la campagne
- Les **attributs** du cas de test
- Les informations liées à l'**exécution** 

!!! info "Info"
    Les critères de recherche associés aux jalons ne sont visibles que si les jalons sont activés sur l'instance.

**Par exemple :**

Sélectionner dans la catégorie **Attributs** le critère "Importance" en choisissant l'option "Haute" et dans la catégorie **Exécutions** pour le critère "Statut" choisir les options "Bloqué" et "Échec" : 
<br/>Les résultats de recherche retournent uniquement les exécutions au statut Bloqué et Échec liés à des cas de test avec une importance "Haute".


## Résultats de recherche 

Les résultats de recherche sont affichés sous forme de tableau comportant plusieurs colonnes reprenant les différentes informations des exécutions comme le jeu de données, le statut d'exécution et la date d'exécution. 

Depuis cette page de recherche il est possible de :

- Accéder à la page de consultation de l'itération contenant l'exécution 
- Lancer ou relancer l'exécution d'un cas de test
- Ajouter des cas de test au plan d'exécution d'une itération existante ou d'une nouvelle itération
- Modifier en masse le statut d'exécution des cas de test ([Exécution via fastpass](./executer-manuellement-tests.md#executer-un-cas-de-test))

!!! tip "En savoir plus"
    La recherche permet de filtrer les tests que l'on souhaite ajouter à un plan d'exécution pour réexécution. Pour en savoir plus consulter la page [Identifier les tests à rejouer avec les résultats d'une recherche](./creer-plan-execution.md#identifier-les-tests-a-rejouer-avec-les-resultats-dune-recherche).