# Tableau de bord par défaut


<!--##(PIL1 + Doc : Dossier/campagne/itération + intérêt + tableau inventaire)-->
Le tableau de bord de l’espace Campagnes facilite le suivi de la phase d’exécution des cas de test.
À la sélection d'un élément de la bibliothèque de l'espace Campagne, la première ancre affiche le Tableau de bord par défaut de l'objet et il s'adapte à la sélection simple d'un objet. (La sélection multiple d'objets de même niveau hiérachique ne permet pas de générer un tableau de bord).


|Entité/Élément du tableau de bord|Avancement cumulé|Statistiques|Inventaire des tests|Périmètre
|:----:|:------:|:------:|:-------:|:---:|
|**Dossier** ||X|X|toutes les campagnes contenues dans le dossier
|**Campagne**|X|X|X|toutes les itérations contenues la campagne
|**Itération**|X|X|X|tous les ITPI, associés ou non une suite, contenus dans l'itération

## L’avancement cumulé
Le graphique Avancement cumulé de la campagne est généré à partir des graphiques d’Avancement cumulé des itérations. Ils sont générés automatiquement à partir des dates renseignées dans le bloc **Planning** de l’itération.
Il est une représentation graphique de l’avancement cumulé de l'objet tenant compte des dates de la campagne, des itérations et du nombre de cas de test à réaliser. Le graphique propose une représentation permettant de comparer le réalisé (courbe violette) et le planifié (courbe bleue) et ainsi évaluer l’avancement cumulé de la campagne ou de l'itération.

!!! warning "Focus"
	Les dates de deux itérations ne doivent pas se chevaucher pour que l’Avancement cumulé de la campagne puisse se générer.

    Le périmètre de l'avancement cumulé d'une campagne est toutes les itérations que contient la campagne.
    Le périmètre de l'avancement cumulé d'une itération est tous les items de son plan d'exécution.
    

<!--##(PIL1 + explications boutons)-->


![Avancement cumulé de la campagne](resources/avancement-cumule-campagne-fr.png){class="pleinepage" }

![test](resources/test-avancement-campagne.png){class="pleinepage" }

![test](resources/test-avancement-ite.png){class="pleinepage" }

## Les Statistiques

Les graphiques du bloc **Statistiques** permettent de visualiser la répartition par statut d'exécution des tests, le taux de succès ou d'échec par importance des tests et la répartition par importance des tests jamais exécutés.

![Statistique de la campagne](resources/statistique-campagne-fr.png){class="pleinepage" }

Le bloc **Inventaire des test** d'une campagne est un tableau de répartition comptant le nombre de tests par statut d'exécution et par itération

![Inventaire des tests par itération](resources/inventaire-des-tests-par-iteration-fr.png){class="pleinepage" }


<!--## -->