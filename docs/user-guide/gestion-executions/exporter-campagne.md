# Exporter les données d'une campagne

Squash TM permet d'exporter les données d'une campagne au format .csv en cliquant sur le bouton ![Importer et exporter des campagnes](./resources/import-export.png){class="icone"}. Il existe trois exports de campagnes différents : 

- **L'export de campagne simple**
</br>Contient les données de la campagne, de l'itération, de la suite, des cas de test et des données de l'exécution.
</br>Le nom du fichier est par défaut : "EXPORT_CPG_L_<reference_campagne\>\_<date\>\_<heure\>" au format JJMMAAAA et HHMMSS.
- **L'export de campagne standard**
</br>Contient en plus le pourcentage d'exécution, la description du cas de test et son prérequis.
</br>Le nom du fichier est par défaut : "EXPORT_CPG_S_<reference_campagne\>\_<date\>\_<heure\>" au format JJMMAAAA et HHMMSS.
- **L'export de campagne complet**
</br>Contient les données de la campagne, de l'itération, de la suite, des cas de test et le détail de l'exécution de chaque pas de test.
</br>Le nom du fichier est par défaut : "EXPORT_CPG_F_<reference_campagne\>\_<date\>\_<heure\>" au format JJMMAAAA et HHMMSS.

!!! info "Info"
    Ces exports de campagne au format .csv peuvent servir comme données d'entrée à des documents de reporting.

Détails des colonnes présentes dans chaque export :

| Colonnes exportées     | Description                                        | Simple (Light) | Standard | Complet (Full) |
|------------------------|----------------------------------------------------|:--------------:|:--------:|:--------------:|
| CPG_SCHEDULED_START_ON | Date de début de campagne prévue                   |        X       |     X    |        X       |
| CPG_SCHEDULED_END_ON   | Date de fin de campagne prévue                     |        X       |     X    |        X       |
| CPG_ACTUAL_START_ON    | Date de début de campagne réelle                   |        X       |     X    |        X       |
| CPG_ACTUAL_END_ON      | Date de fin de campagne réelle                     |        X       |     X    |        X       |
| IT_ID                  | Identifiant de l'itération                         |        X       |          |        X       |
| IT_NUM                 | Numéro de l'itération                              |        X       |    X*    |        X       |
| IT_NAME                | Nom de l'itération                                 |        X       |    X*    |        X       |
| IT_MILESTONE           | Nom du jalon associé à la campagne contenant l'itération               |        X       |     X    |        X       |
| IT_SCHEDULED_START_ON  | Date de début d'itération prévue                   |        X       |     X    |        X       |
| IT_SCHEDULED_END_ON    | Date de fin d'itération prévue                     |        X       |     X    |        X       |
| IT_ACTUAL_START_ON     | Date de début d'itération réelle                   |        X       |     X    |        X       |
| IT_ACTUAL_END_ON       | Date de fin d'itération réelle                     |        X       |     X    |        X       |
| TC_ID                  | Identifiant du cas de test                         |        X       |          |        X       |
| TC_NAME                | Nom du cas de test                                 |        X       |    X**   |        X       |
| TC_PROJECT_ID          | Identifiant du projet                              |        X       |          |        X       |
| TC_PROJECT             | Nom du projet                                      |        X       |     X    |        X       |
| TC_MILESTONE           | Jalon(s) associé(s) au cas de test                 |        X       |     X    |        X       |
| TC_WEIGHT              | Importance du cas de test                          |        X       |     X    |        X       |
| TEST_SUITE             | Nom des suites de test où le cas de test est associé       |        X       |     X    |        X       |
| #_EXECUTIONS           | Numéro de l'exécution                              |        X       |     X    |        X       |
| #_REQUIREMENTS         | Nombre d'exigences associées                       |        X       |     X    |        X       |
| #_ISSUES               | Nombre d'anomalies associées                       |        X       |     X    |        X       |
| DATASET                | Nom du jeu de données                              |        X       |     X    |        X       |
| EXEC_STATUS            | Statut d'exécution de l'ITPI                       |        X       |     X    |        X       |
| EXEC_SUCCESS_RATE      | Pourcentage d'exécution des pas de test en succès  |                |     X    |                |
| EXEC_USER              | Login de l'utilisateur ayant exécuté le cas de test |        X       |     X    |        X       |
| EXEC_DATE              | Date d'exécution                                   |        X       |     X    |        X       |
| DESCRIPTION            | Description du cas de test                         |                |     X    |                |
| TC_REF                 | Référence du cas de test                           |        X       |     X    |        X       |
| TC_NATURE              | Nature d'ordre du cas de test                      |        X       |     X    |        X       |
| TC_TYPE                | Type du cas de test                                |        X       |     X    |        X       |
| TC_STATUS              | Statut de rédaction du cas de test                 |        X       |     X    |        X       |
| PREREQUISITE           | Prérequis du cas de test                           |                |     X    |                |
| STEP_ID                | Identifiant du pas de test                         |                |          |        X       |
| STEP_NUM               | Numéro du pas de test                              |                |          |        X       |
| STEP_#_REQ             | Nombre d'exigences associées au pas de test        |                |          |        X       |
| EXEC_STEP_STATUS       | Statut d'exécution du pas d'exécution               |                |          |        X       |
| EXEC_STEP_DATE         | Date d'exécution du pas de test                    |                |          |        X       |
| EXEC_STEP_USER         | Login de la personne ayant exécutée le pas         |                |          |        X       |
| EXEC_STEP_#_ISSUES     | Nombre d'anomalies associées au pas                |                |          |        X       |
| EXEC_STEP_COMMENT      | Commentaire associé au pas d'exécution             |                |          |        X       |
| CPG_CUF_<CUF_code\>     | Champ(s) personnalisé(s) associé(s) à la campagne  |        X       |     X    |        X       |
| IT_CUF_<CUF_code\>                  | Champ(s) personnalisé(s) associé(s) à l'itération  |        X       |     X    |        X       |
| TC_CUF_<CUF_code\>                  | Champ(s) personnalisé(s) associé(s) au cas de test |        X       |     X    |        X       |
| EXEC_CUF_<CUF_code\>                | Champ(s) personnalisé(s) associé(s) à l'exécution                                      |                |     X    |        X       |
| STEP_CUF_<CUF_code\>                | Champ(s) personnalisé(s) associé(s) au pas                                             |                |          |        X       |



X* : Dans l’export Standard, le numéro et le nom des itérations sont rassemblés dans une colonne appelée ITERATION.

X** : Dans l’export Standard, la colonne où figure le libellé du cas de test est appelée TEST_CASE.


!!! tip "En savoir plus"
    Il est également possible de créer des exports de campagne personnalisés avec les données de son choix. Pour en savoir plus, consulter la page ["Les exports de campagnes personnalisés"](../pilotage-recette/exports-campagne-perso.md)
     
