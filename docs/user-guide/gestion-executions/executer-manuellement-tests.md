# Exécuter manuellement les tests

## Exécuter un cas de test 

### Exécuter un test manuellement

Il est possible d'exécuter manuellement tous les formats de cas de test : Classique, Gherkin et BDD. L'exécution peut se déclencher depuis plusieurs emplacements :

- dans le plan d'exécution, avec le bouton ![Exécution](./resources/executions.png){class="icone"} et l'option *"Avec la popup"* 
- dans le plan d'exécution avec le bouton ![Exécution](./resources/executions.png){class="icone"} et l'option *"Nouvelle exécution"*
- depuis la page de consultation de l'exécution avec le bouton **[...]** et l'option *"Lancer avec la popup"*
- depuis la page de consultation de l'exécution depuis l'ancre **Scénario d'exécution** en modifiant les statuts d'exécution sur chaque pas de test, et en ajoutant des commentaires, des pièces jointes et en déclarant des anomalies.
- depuis la page de recherche d'items de plan d'exécution avec le bouton ![Exécution](./resources/executions.png){class="icone"} présent en bout de ligne dans les résultats de recherche

!!! info "Info"
    Quel que soit le mode de lancement, il est possible de réaliser plusieurs exécutions d'un item de plan de test (ITPI).
    </br>Le statut de l'ITPI pris en compte est celui de la dernière exécution.

!!! warning "Focus"
    Si le script d'un cas de test Gherkin est invalide et ne respecte pas la syntaxe Gherkin, un message d’erreur s’affiche au lancement de l’exécution.

**Marche à suivre pour exécuter un cas de test avec la popup :**

Au lancement de l'exécution d'un ITPI avec la popup d'exécution, les informations suivantes s'affichent à l'étape 0 :

- Un bloc 'Informations' avec le statut de rédaction, l'importance, la nature, le type, la description du cas de test ainsi que le jeu de données utilisé pour l'exécution. Les champs personnalisés associés aux entités Cas de test (identifiés par une astérisque, ils ne sont pas modifiables) ou Exécutions (modifiables) sont également présents
- Un bloc 'Prérequis' (pour les cas de test Classique) ou 'Contexte' (pour les cas de test Gherkin) qui reprend les pré-conditions à remplir avant l'exécution du test. 
- Un bloc 'Exigences vérifiées' avec les exigences associées au cas de test. Un lien cliquable permet de les consulter depuis le tableau.
- Un bloc 'Pièces jointes' avec les pièces jointes associées au cas de test. Ce bloc permet d'ajouter les pièces jointes qui sont associées à l'exécution

![Exécution Popup Etape 0](./resources/etape0.jpg){class="centre"} 

En cliquant sur le bouton **[Commencer]** ![Commencer l'exécution](./resources/commencer.png){class="icone"}, l'étape '1' du test s'affiche. Elle reprend comme tous les pas de test suivants :

- l'action et le résultat attendu du pas de test 'N' d'un cas de test classique, 
- le script du scénario de test 'N' d'un cas de test Gherkin,
- l'action du pas de test 'N' d'un cas de test BDD.

Sur chaque pas d'exécution :

- Un staut d'exécution peut être attribué
- Un commentaire et des pièces jointes peuvent être ajoutés
- Si des champs personnalisés ont été associés aux entités "Pas de test" ou "Pas d'exécution", un bloc 'Informations' s'affiche. Les champs personnalisés associés au pas de test indiqués par une astérisque sont non modifiables tant que ceux associés aux pas d'exécution sont modifiables.
- Si un bugtracker est activé sur le projet, un bloc 'Anomalies' apparaît pour assosier ou déclarer des anomalies
- Des flèches de navigation permettent de se déplacer d'un pas à l'autre. Il est également possible de modifier le numéro du pas courant pour accéder à un autre pas de test du cas de test.

![Exécution Popup Etape N](./resources/etapeN.jpg){class="centre"} 

### Les statuts d'exécution

L'exécution des tests consiste à attribuer un statut d'exécution à chaque pas de test, ou cas de test. Dans le cadre d'une exécution avec la popup, cliquer sur la pastille de couleur du statut déclenche l'affichage du pas de test suivant.

![Statut d'exécution](./resources/statut-execution.jpg){class="centre"}

!!! info "Info"
    Les statuts "Arbitré" et "Non testable" sont à activer sur la page de configuration du projet, dans le bloc "Exécutions".

Le plan d’exécution contient une colonne "% succès" qui correspond au pourcentage de pas de test en succès pour la dernière exécution manuelle.

Le statut attribué à un cas de test répond à un ordre de priorité donné à chacun des statuts.
Voici l'ordre de priorité des statuts principaux. Il suffit d'un seul pas de test au statut le plus élevé pour que l'ITPI prenne le statut de celui-ci :

- "Bloqué"
- "Échec"
- "Succès" : 
    - un pas de test en succès met l'ITPI au statut "En cours". 
    - un cas de test est en "Succès" seulement si tous ses pas de test sont en "Succès" ou "Arbitré".

**Par exemple** : pour ce cas de test, un seul pas de test est au statut "Échec", peu importe le statut des autres pas du test, le cas de test aura le statut "Échec".

![Exécution en Echec](./resources/execution-echec.png){class="pleinepage"}

Chaque suite de tests a un statut attribué en fonction des statuts des tests de la suite. La notion de priorité reste valable également pour les suites de tests.

**Par exemple** : pour cette suite, un seul test est au statut "Bloqué", peu importe le statut des autres tests, la suite aura le statut "Bloqué".

![Exécution en Echec](./resources/execution-suite.jpg){class="pleinepage"}

### Exécuter un test en mode Fastpass

Il est possible d'exécuter un test en "Fastpass", sans exécuter tous ses pas de test : pour cela il suffit de changer le statut d'exécution de l'ITPI dans la colonne "Statut" depuis le plan d'exécution.
Les précédentes exécutions de l'ITPI (sans Fastpass) sont conservées.

Le mode Fastpass n'est pas considéré comme une exécution en soit, il n'y aura donc pas de page de détails de l'exécution, comme c'est le cas avec la popup. Aucune ligne pour l'exécution ne sera donc présente dans la popup "Historique des exécutions".

![Exécution en mode fastpass](./resources/fastpass.jpg){class="pleinepage"} 

## Lancer une exécution en chaîne de plusieurs tests 

Le bloc **Plan d'exécution** d'une itération ou d'une suite de tests présente des boutons d'action permettant d'exécuter des tests à la chaîne.

### Le bouton [Lancer] 

Le bouton **[Lancer]** ![Lancer une exécution](./resources/play.png){class="icone"} est présent par défaut si aucune exécution n'a été lancée dans l'itération ou la suite de tests.
Lorsque l’utilisateur clique sur **[Lancer]**, il peut exécuter à la chaîne tous les tests présents dans le plan d'exécution via la popup d'exécution.

### Les boutons [Reprendre] et  [Relancer]

Lorsque l'utilisateur stoppe une exécution en chaîne alors qu'il lui reste des pas de test à exécuter, il peut la reprendre avec le bouton **[Reprendre]** ![Reprendre une exécution](./resources/pause.png){class="icone"} là où il s'est arrêté.

Si l'exécution de tous les tests présents dans le plan d'exécution a été réalisée au moins une fois, le bouton **[Relancer]** ![Relancer une exécution](./resources/restart.png){class="icone"} apparaît sur la page du plan d'exécution. Cela permet de relancer les tests du plan d'exécution en supprimant toutes les exécutions précédentes.

!!! danger "Attention"
    Le bouton **[Relancer]** supprime toutes les exécutions précédentes des ITPI contenus dans le plan d'exécution.

!!! info "Info"
    - Il est possible de passer directement au prochain cas de test en cliquant sur le bouton ![Suivant](./resources/suivant.png) présent sur les étapes N dans la popup.
    - Quand tous les pas de test ont été exécutés, une popup s'affiche pour informer l'utilisateur de la fin des tests.

## Modifier un cas de test en cours d’exécution 

!!! info "Info"
    - Pour modifier un cas de test en cours d'exécution, il faut activer cette fonctionnalité sur la page de configuration du projet, dans le bloc "Exécutions".
    - Seuls les cas de test au format Classique peuvent être modifiés pendant l'exécution.

La modification d'un cas de test en cours d'exécution est possible en cliquant sur le bouton ![Modifier un cas de test pendant son exécution](./resources/icone-pen.png){class="icone"} présent sur les étapes d'exécution d'un cas de test. Il est possible de modifier les informations générales et les pas de test du cas de test.

### Modification du cas de test à l'étape 0

A l'étape 0 dans la popup d'exécution, en cliquant sur le bouton ![Modifier un cas de test pendant son exécution](./resources/icone-pen.png){class="icone"}, la page de détails du cas de test s'affiche. Il est alors possible de modifier toutes les informations du cas de test.

Depuis cette page, cliquer sur le bouton **[Revenir à l'exécution]** ![Revenir à l'exécution](./resources/back.png){class="icone"} pour revenir à l'étape 0 de l'exécution. Les modification effectuées s'affichent dans la popup d'exécution.

### Modification du cas de test à l'étape N

À l'étape N dans la popup d'exécution, en cliquant sur le bouton ![Modifier un cas de test pendant son exécution](./resources/icone-pen.png){class="icone"}, la page de détails du pas de test s'affiche. L'ensemble des pas de test peuvent être modifiés ainsi que leurs associations aux exigences.
Il n'est pas possible depuis cette page d'ajouter ou de supprimer des pas de test.

Depuis cette page, cliquer sur le bouton **[Revenir à l'exécution]** ![Revenir à l'exécution](./resources/back.png){class="icone"} pour revenir à l'exécution.
La popup d’exécution s’ouvre au niveau de la première étape modifiée (première au sens ordre d'exécution). Le statut des pas de test déjà exécutés reprend le statut "À exécuter".

## Consulter l’historique des exécutions d’un test 

L'historique des exécutions d'un ITPI est accessible depuis le bouton ![Exécution](./resources/executions.png){class="icone"} et l'option *"Historique des exécutions"* pour chaque ITPI d'un plan d'exécution.

La popup "Historique des exécutions" s'affiche et répertorie l'ensemble des exécutions de l'ITPI, notamment avec leur pourcentage de pas de test en succès, leur statut d'exécution, l'utilisateur ayant exécuté le test et la date de dernière exécution. 

Il est également possible depuis cette popup de supprimer une exécution.

![Historique des exécutions d'un test](./resources/historique-executions.jpg){class="centre"}

Il est également possible depuis l'espace Cas de test, de visualiser l'ensemble des [exécutions d'un cas de test](../../gestion-cas-test/suivre-executions-cas-test/), en cliquant sur l'ancre **Exécutions** du cas de test souhaité. Pour rappel, un cas de test peut avoir plusieurs exécutions pour une de ces ITPI et peut être exécuté dans plusieurs itérations ou suite de tests différentes.

## La page de consultation d'une exécution

La page de détails d'une exécution est accessible au clic sur :

- le lien présent sur la date dans la colonne *"Dernière Exec."* dans le plan d'exécution
- le numéro de l'exécution (colonne *"N° Exec."*), dans la popup "Historique des exécutions"

La page de consultation d'une exécution comporte plusieurs ancres :

- l'ancre "Informations" qui reprend le statut de rédaction, l'importance, la description, la nature et le type du cas de test, ainsi que les champs personnalisés associés au cas de test (non modifiables) et à l'exécution (modifiables).
- l'ancre "Exigences vérifiées" qui reprend les exigences associées au cas de test.
- l'ancre "Commentaires" pour visualiser ou ajouter un commentaire concernant l'exécution du cas de test.
- l'ancre "Anomalies" pour visualiser ou associer des anomalies au cas de test durant son exécution.

!!! info "Info"
    L'ancre "Anomalies" ne s'affiche que si un serveur bugtracker a été associé au projet dans l'administration.

Ces 4 premières ancres sont communes quelque soit le format du cas de test exécuté.

![Ancre page de consultation d'une exécution](./resources/detail-execution.jpg){class="pleinepage"}

**Cas de test Classique :**

L'ancre "Scénario d'exécution" reprend le **Prérequis** du cas de test ainsi que l'ensemble des pas d'exécution avec les informations suivantes : 

- le statut d'exécution
- la date d'exécution et le login de l'utilisateur
- le contenu du bloc Action et Résultat attendu
- un encart Champs personnalisés associés au pas de test (non modifiables) et au pas d'exécution (modifiables)
- un encart Commentaires
- un encart Pièces jointes
- un encart Anomalies

![Scénario d'exécution d'un cas de test classique](./resources/scenario-execution-2.jpg){class="pleinepage"}


**Cas de test Gherkin :**

L'ancre "Script" reprend le **Contexte** ainsi que chaque scénario de test dans chaque pas d'exécution avec les informations suivantes :

- le statut d'exécution
- la date d'exécution et le login de l'utilisateur
- le script d'un scénario d'exécution
- un encart Commentaires
- un encart Pièces jointes
- un encart Anomalies

!!! info "Info"
    - Si le script Gherkin contient un plan de scénario, il sera traduit en autant de pas d’exécution qu’il y a de jeux de données dans la table d’exemples. Un cas de test Gherkin n’est donc jamais associé à un Jeu de données au sens de Squash TM.
    - Dans les pas d’exécution d’un plan de scénario, les paramètres Gherkin (qui sont entre <>) sont remplacés par leurs valeurs dans la ligne exemple considérée.
    - Si la balise de langue est correctement renseignée dans le script, l’exécution prend en compte la coloration syntaxique des mots clés.
    - Si balise de langue est invalide, le script est traité comme écrit en Anglais


**Cas de test BDD :**

L'ancre "Scénario d'exécution" reprend l'ensemble des pas d'exécution avec les informations suivantes :

- le statut d'exécution
- la date d'exécution et le login de l'utilisateur
- le contenu du bloc Action
- un encart Commentaires
- un encart Pièces jointes
- un encart Anomalies

La page de consultation d'une exécution permet de visualiser ou modifier une exécution déjà réalisée mais peut aussi servir à effectuer une nouvelle exécution depuis le plan d'exécution avec le bouton ![Exécution](./resources/executions.png){class="icone"} et l'option *"Nouvelle exécution"*.