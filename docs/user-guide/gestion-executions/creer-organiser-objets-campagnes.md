# Créer et organiser les objets de l’espace Campagnes

## Créer une campagne, une itération et une suite de tests

Depuis l'espace Campagnes, lorsqu'un projet est sélectionné, seul l'ajout d'un dossier ou d'une campagne est permis. L'itération ne peut être créée qu'à partir d'une campagne et la suite qu'à partir d'une itération.

!!! info "Info"
	 Il est possible de créer une suite de tests depuis le plan d'exécution d'une itération via le bouton ![Ajouter une suite de tests](resources/add_tests_suites.png){class="icone"}. Pour activer ce bouton, il suffit de sélectionner des items du plan de tests (ITPI). Suite à la création de la suite, l'ensemble des items sélectionnés sont automatiquement associés à la suite créée.

!!! danger "Attention"
    La suppression d'un dossier de campagnes, d'une campagne ou d'une itération entraine la suppression des ITPI contenus dans les plans d'exécutions qu'il contient ainsi que la perte irrémédiable de l'historique d'exécution associé.

## Les attributs des objets de l’espace Campagnes
Le bloc **Informations** recense les informations élémentaires quelque soit l'objet de l'espace Campagnes (Campagne, Itération ou Suite de tests) :

|Champs|<center>Définition</center>|Campagne|Itération|Suite
|:----:|------|:------:|:-------:|:---:|
|**ID** |ID technique nécessaire pour accéder à l'objet via son url ou pour le passage de requêtes API|X|X|X
|**Création  Modification**|les dates et le login de l'utilisateur sont déterminés automatiquement lors de la création ou de la modification de l'objet|X|X|X
|**État**|déterminé par l’utilisateur en cliquant sur le champ, les valeurs possibles sont :<br/>• Non défini<br/>• Planifié (par défaut)<br/>• En cours<br/>• Terminé<br/>• Archivé|X|X|
|**Statut d'avancement**|déterminé automatiquement en fonction de l’avancement de l’exécution des tests de la campagne, les valeurs possibles sont :<br/>• À exécuter : aucun test exécuté dans le(s) plans d'exécution<br/>• En cours : quelques tests exécutés dans le(s) plans d'exécution<br/>• Terminé : tous les tests exécutés dans le(s) plans d'exécution|X|X|X
|<align>**Jalon**</align>|champ éditable à partir duquel il est possible d'associer un seul des jalons portés par le projet|X
|**Description**|champ éditable libre|X|X|X
|**UUID**|déterminé automatiquement à la création de l'objet, l'UUID sert au lancement des tests automatisés contenus dans le plan d'exécution depuis un pipeline CI/CD||X|X
|**Statut d'exécution**|champ modifiable déterminé automatiquement en fonction du statut d’exécution des tests présents dans le plan d'exécution. Les valeurs possibles sont : <br/>• Arbitré<br/>• En cours<br/>• Échec<br/>• À exécuter<br/>• Bloqué<br/>• Non testable<br/>• Succès|||X
|**Champs personnalisés**|obligatoires ou facultatifs, ils servent à la personnalisation des objets. Ils peuvent également être transmis comme jeux de données dans le cadre des exécutions automatisées.|X|X|X

!!! tip "En savoir plus"
    Pour la gestion de l'exécution de tests automatisés par champs personnalisés, consulter la page [Exécuter un cas de test automatisé](../gestion-tests-automatises/executer-test-auto.md)

## Planning et Statistiques des objets de l’espace Campagnes

### Planning

Le planning d'une campagne ou d'une itération permet de renseigner des dates de début et de fin d'exécution afin de définir le planning prévisionnel et le planning réel de la phase d'exécution de la recette. Il permet également de générer les graphiques Avancement cumulé de l'itération et de la campagne.

Les champs *Début prévu le* et *Fin prévue le* sont déterminés par l'utilisateur depuis l'ancre Planning de la campagne ou de l'itération.

La *Date de début réelle*, lorsque l'option 'Auto' est cochée, est égale à la date d'exécution du premier ITPI exécuté de la campagne ou de l'itération. Cette date est modifiable manuellement. 
<br/>La *Date de fin réelle*, lorsque l'option 'Auto' est cochée, est égale à la date d'exécution du dernier ITPI exécuté de la campagne ou de l'itération. Cette date est également modifiable manuellement.

Depuis le bloc 'Avancement cumulé de la campagne' du tableau de bord d'une campagne, le bouton ![Gérer les dates d'itérations](resources/calendar.svg){class="icone"} permet de renseigner les dates de début et de fin prévisionnelles de ses itérations.

![Planning Campagne](resources/planning-FR.png){class="pleinepage"}


### Statistiques

L'ancre "Statistiques" d'une campagne, d'une itération ou d'une suite dénombre le nombre de tests exécutés sur le nombre total de cas de test associés au plan d'exécution (a/b) ainsi que le pourcentage d'avancement de ce ratio (a/b*100 %). Elle recense également dans le détail le nombre de tests en fonction de leur statut d'exécution dans le périmètre de l'objet consulté.


![Statistiques Campagnes](resources/statistiques-FR.png){class="pleinepage"}


## Organiser la bibliothèque des campagnes

### L'arborescence

La bibliothèque de l'espace Campagnes est régie par une hiérarchie stricte : 

- Les dossiers servent uniquement à organiser les campagnes
- Les campagnes ![Campagne](resources/campaign.svg){class="icone"} contiennent les itérations
- Les itérations ![Itération](resources/iteration.svg){class="icone"} peuvent être organisées par suites de tests
- Les suites de tests ![Suite de tests](resources/tests_suite.svg){class="icone"} sont le plus petit objet de l'espace Campagnes

Cette hiérarchie traduit les ordres de grandeur entre les différents objets de l'espace Campagnes.

Une campagne est la phase d'exécution qui s'étend de la première livraison en recette à la mise en production. Une campagne est donc lancée avant chaque mise en production d'un outil. Les dossiers peuvent être utilisés pour rassembler les campagnes de tests d'un outil ou d'un module de l'outil.

Une itération est définie par le laps de temps entre deux livraisons de développements au cours de la phase d'exécution qu'est la campagne. L'itération est par conséquent un sous objet de la campagne qui porte le plan d'exécution d'une version de recette de l'outil. Il y en a autant qu'il y a de livraisons avant la mise en production.

La suite de tests permet d'organiser le plan d'exécution d'une itération en sections afin de faciliter l'exécution des tests qu'il contient. Cet objet permet ainsi d'organiser le plan d'exécution par fonctionnalité, par US, par type de test, ou encore par testeur. 

Voici un exemple d'organisation possible de la bibliothèque de l'espace Campagnes pour un projet :

![Organisation de la bibliothèque Campagne](resources/organisation-bibliotheque-FR.png){class="pleinepage"}

### Les icônes et capsules

Dans l'arborescence, un onglet coloré sur le nom de la suite indique le statut d'exécution globale de celle-ci. Le statut attribué à une suite de tests répond à un ordre de priorité donné à chacun des statuts.
Voici l’ordre de priorité des statuts principaux. Il suffit d’un seul test au statut le plus élevé pour que la suite de tests prenne ce statut :

- Bloqué
- Échec
- Succès : un test en succès met la suite de tests au statut "En cours" et lorsque tous les tests sont en succès, la suite prend le statut "Succès"

Par exemple, pour une suite si un seul test est au statut "Bloqué", peu importe le statut des autres tests, la suite aura le statut "Bloqué".

![Statut d'une suite de tests](resources/icone-suite.png){class="pleinepage"}

Des capsules sont visibles sur la page de consultation d'une campagne, d'une itération ou d'une suite. Elles indiquent :

- Au niveau de la campagne et de l'itération : l'état et le statut d'avancement
- Au niveau de la suite de tests : le statut d'éxécution et le statut d'avancement

![Capsule](resources/capsule.png)
