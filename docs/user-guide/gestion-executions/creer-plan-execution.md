# Créer un plan d'exécution

Le plan d'exécution est l'ancre des objets de l'espace Campagnes dédié à l'exécution des cas de test. C'est dans ce tableau que ce sont ajoutés les cas de test afin d'être exécutés. Il existe plusieurs façons de constituer un plan d'exécution. 

## Ajouter des cas de test à un plan d’exécution en utilisant la bibliothèque

Au-dessus du plan d'exécution d'une campagne, d'une itération ou d'une suite de tests, le bouton ![Ajouter](resources/icone-ajouter.png){class="icone"} permet d'associer des cas de test au plan d'exécution. Il suffit de glisser/déposer une sélection de cas de test depuis le volet 'Référentiel des cas de test' dans le tableau du plan d'exécution pour les associer.

## Ajouter des cas de test à un plan d’exécution en utilisant la recherche 

Pour ajouter des cas de test à un plan d'exécution depuis la recherche de cas de test ou d'exigences associées, cliquer sur le bouton ![Recherche](resources/browse.png){class="icone"} dans le bloc **Plan d'exécution**. Rechercher les cas de test en utilisant les différents critères présents puis cliquer sur le bouton ![Associer la sélection](resources/link-selection.png){class="icone"} pour associer la sélection ou sur le bouton ![Tout associer](resources/link-all.png){class="icone"} pour associer tous les résultats de la page visible.

!!! info "Info"
     La recherche par exigences associées permet de lancer une recherche de cas de test en utilisant les critères des exigences. Dans la partie résultats, ce sont les cas de test associés aux exigences correspondant aux critères sélectionnés qui s'affichent.
     <br/>*Exemple* : il est possible via cette recherche d'ajouter au plan d'exécution tous les cas de test associés à des exigences de criticité "Critique" dont le statut est "Approuvée".

## Reprendre le plan d’exécution d’une campagne dans une itération 

Il est possible de reprendre le plan d'exécution d'une campagne avec l'ensemble des cas de test qu'il contient ainsi que leurs assignations dans le plan d'exécution d'une nouvelle itération. 

Pour cela, à la création de l'itération, il suffit de cocher la case "Voulez-vous copier le plan d'exécution de la campagne ?" présente dans la popup d'ajout.

![Reprendre le plan d'exécution d'une campagne dans une itération](./resources/popup-reprendre-plandexex.jpg){class="centre"}

!!! info "Info"
     Il n'est pas possible d'exécuter les cas de test ajoutés au plan d'exécution d'une campagne. Son seul objectif est de permettre à l'utilisateur de faire une préselection des tests à exécuter dans les itérations. En cela, il est idéal pour stocker la liste des tests de non régression qui doivent être exécutés à chaque itération. 


## Identifier les tests à rejouer selon les résultats d’exécution précédents  

### Identifier les tests à rejouer avec les résultats d'une recherche

Pour réexécuter des cas de test qui ont déjà été exécutés dans une itération précédente, il est possible d'utiliser la recherche de l'espace Campagnes pour réaliser un filtre. Pour ce faire cliquer sur le bouton ![Recherche](resources/browse.png){class="icone"} présent au-dessus de la **bibliothèque** de l'espace Campagnes. Rechercher les items de plan d'exécution (ITPI) à ajouter au plan d'exécution d'une itération en utilisant les différents critères présents, sélectionner les ITPIs à reprendre puis cliquer sur le bouton ![Ajouter](resources/icone-ajouter.png){class="icone"} au-dessus des résultats de recherche.
La popup 'Ajouter la sélection à un plan d'exécution' s'affiche et offre la possibilité : 

- d'ajouter les ITPIs sélectionnés au plan d'exécution d'une itération existante dans l'arborescence
- d'ajouter les ITPIs sélectionnés au plan d'exécution d'une nouvelle itération de la campagne sélectionnée. La nouvelle itération aura pour nom 'Résultats de recherche AAAA/MM/JJ HH:MM:SS"

![Ajouter la sélection à un plan d'exécution](./resources/selection-plan-exec.png){class="centre"}

La recherche de l'espace Campagnes permet ainsi d'ajouter des tests à une itération afin de les réexécuter notamment s'il ont été exécutés précédemment comme invalides avec les statuts d'exécution suivants : "Bloqué", "Echec" ou "Non testable".

!!! info "Info"
     Depuis la page de recherche, il est possible d'exécuter à nouveau un ITPI en cliquant sur le bouton ![Exécuter](resources/executions.png){class="icone"}.

### Identifier les tests à rejouer avec l'Assistant de campagne

#### Présentation

Le plugin Assistant de campagne présente une fonctionnalité de reprise d'itération sous forme d’assistant qui permet à l’utilisateur de créer plus simplement une nouvelle itération et son plan d’exécution à partir d'une itération existante, sur la base de différents critères.
Ces critères peuvent être simples (cas les plus fréquents de reprise) ou avancés (le plugin permet une sélection détaillée des tests à reprendre).

!!! info "Info"
     Il faut activer le plugin Assistant de campagne sur la page de configuration du projet (ancre **Plugins**) avant de pouvoir l'utiliser. Ce plugin fait partie de la licence Premium Squash TM.

#### Les différentes étapes de sélection

Pour accéder à l'Assistant de campagne, sélectionner une **itération** puis cliquer sur le bouton ![Assistant](./resources/wizard.png){class="icone"} puis sur l'option "Assistant campagne".

##### **Étape 1** : Choisir l'itération source

Par défaut, c’est l’itération à partir de laquelle l’assistant a été lancé qui est sélectionnée. La liste de sélection propose toutes les itérations de la campagne. Il est possible de conserver les suites de test et les affectations de l'itération source dans la nouvelle itération. Le champ 'Type de critères' permet de choisir entre les critères simples ou les critères avancés de sélection des ITPIs.

![Reprendre le plan d'exécution d'une campagne dans une itération](./resources/assistant-campagne.jpg){class="pleinepage"}

##### **Étape 2** : Sélectionner des critères simples/avancés

Dans le cas où plusieurs critères sont sélectionnés, tous les tests qui répondent à **au moins un** des critères sélectionnés sont repris.

**- Les critères simples**

![Critères simples de l'assistant de campagne](./resources/assistant-campagne-criteres-simples.jpg){class="pleinepage"}

La sélection simple permet conserver les tests de l'itération source ayant pour statut d'exécution "Echec" ou "Bloqué", les tests liés à des anomalies ou encore les cas de test de type "Non-regression".

**- Les critères avancés**

![Critères avancés de l'assistant de campagne](./resources/assistant-campagne-criteres-avances.jpg){class="pleinepage"}

Les critères avancés de l'Assistant de campagne peuvent porter sur les tests eux-mêmes ou sur les entités liées aux tests. Pour sélectionner des critères, faire un glisser/déposer du critère choisi dans le bloc "Critères sur les entités Squash" depuis l'espace de sélection des critères contenant les entités suivantes :

- "Items de plan d'exécution"
- "Cas de test"
- "Exigences"
- "Suites de tests" 

Les critères avancés sont idéaux pour identifier les cas de test à réexécuter car :

- ils n'ont pas été validés lors de l'itération précédente
- ils ont une importance "Très haute" ou "Haute"
- ils sont associés à des exigences de criticité "Critique" ou "Majeure"

Les critères dans le bloc "Critères sur les anomalies" sont différents suivant le bugtracker associé au projet : 

- Si le projet Squash TM n’est lié à aucun bugtracker : Le bloc "Critères sur les anomalies" n’est pas visible
- Si le projet Squash TM est lié à un bugtracker autre que Jira : Le bloc "Critères sur les anomalies" est visible uniquement avec le critère "Conserver les tests liés à des anomalies"
- Si le projet Squash TM est lié à un bugtracker Jira (de type Jira.rest) : Le bloc "Critères sur les anomalies" est visible avec le critère "Conserver les tests liés à des anomalies". Si la case est cochée, les critères "Statut", "Priorité", "Assigné à" et le mode de reprise des anomalies apparaissent.


##### **Étape 3** : Affiner la sélection des cas de test

La liste des cas de test s'affiche suivant les critères sélectionnés à l'étape précédente. Il est possible d'affiner la sélection en dissociant les cas de test non souhaités dans la nouvelle itération en cliquant sur le bouton ![Dissociation](resources/unlike.png){class="icone"}.


##### **Étape 4** : Créer une itération

Renseigner un nom, une référence ainsi qu'une description à la nouvelle itération puis cliquer sur le bouton **[Créer]**.
La nouvelle itération est visible dans la bibliothèque de l'espace Campagnes avec comme plan d'exécution, la liste des cas de test sélectionnés à l'étape 3.
