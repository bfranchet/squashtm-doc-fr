# Les objets de l'espace Campagnes

## Qu’est ce qu’une campagne, une itération et une suite de tests ?
La phase d’exécution ou « campagne » dans Squash est régie par certains principes :

 - une campagne démarre lors de la première livraison de développement et se termine au moment de la mise en production (MEP)
 - une campagne (ou phase d’exécution) est découpée en itérations (ou cycles)
 - chaque itération est définie par le laps de temps entre deux livraisons de développements (évolutions et/ou corrections).

### La campagne
Une campagne est définie par un libellé, une référence, une description et des dates de début et de fin d'exécution. Lorsque les cases "auto" sont cochées, les dates de première et dernière exécutions seront automatiquement renseignées.
Une campagne est un élément organisationnel : bien qu'il soit possible d'alimenter son plan d'exécution par des cas de test, il n'est en revanche pas possible d'exécuter des tests depuis une campagne.

![Qu'est-ce qu'une-campagne ?](resources/qu-est-ce-qu-une-campagne.png){class="pleinepage"}

### L'itération
L'itération est également définie par son libellé, sa référence, sa description, des dates de début et de fin d'exécution et un plan de test.
Elle est dite opérationnelle puisqu'elle permet, depuis son plan de tests, d'exécuter les tests associés.

Lors de la création d'une itération pour une campagne, les cas de test présents dans le plan d'exécution de la campagne pourront être ajoutés au plan d'exécution de l'itération.

![Qu'est-ce qu'une itération ?](resources/qu-est-ce-qu-une-iteration.png){class="pleinepage" }

### La suite
La suite permet d'organiser le plan d'exécution d'une itération. Elle sert à regrouper les tests par fonctionnalité ou par type.


![Qu'est-ce qu'une suite ?](resources/qu-est-qu-une-suite.png){class="pleinepage"}


## La page de consultation des objets de l’espace Campagnes 

Communes à la campagne, l'itération et la suite, les deux capsules présentes au sommet de la page sont des indicateurs visibles quelle que soit l'ancre consultée.
<br/>La capsule 'État' reporte la valeur du champ État, déterminée par l'utilisateur par sélection d'un état selon les besoins organisationnels du projet :

  - Non défini
  - Planifié (par défaut)
  - En cours
  - Terminé
  - Archivé
  
La capsule 'Statut d'avancement' indique la valeur du champ Statut d'avancement, déterminée automatiquement en fonction de l'avancement de l'exécution des tests du périmètre :

 - À exécuter : aucun test n'est exécuté dans le périmètre
 - En cours : tous les tests n'ont pas un statut final d'exécution
 - Terminé : tous les tests sont exécutés
 
Les différentes ancres d'une campagne, une itération ou une suite permettent d'accéder aux informations de gestion et de suivi d'exécutions des tests.

L'ancre **Tableau de bord** d'une campagne ou d'une itération présente 3 blocs de données chiffrées sur les exécutions, dont le périmètre est, pour la campagne toutes les itérations qu'elle contient, et pour l'itération, tous les tests contenus dans son plan d'exécution (ordonnées ou non en Suites) :

 - Avancement cumulé : graphique de type Évolution sur le nombre de tests planifiés et exécutés
 - Statistiques : graphiques de type Répartition sur le Statut d'exécution des tests, le Taux de succès/échec et l'Importance des tests jamais exécutés
 - Inventaires des tests : synthèse et taux des tests par entité du périmètre (itérations ou suites) en fonction de leur statut d'exécution.

L'ancre **Informations** fournit les informations relatives à l'objet consulté.

L'ancre **Planning** d'une campagne ou d'une itération permet de renseigner des dates de début et de fin d'exécution afin de générer le graphique de l'avancement cumulé.

L'ancre **Statistiques** recense le nombre de tests selon leur statut d'exécution dans le périmètre de l'objet consulté.

L'ancre **Plan d'exécution** affiche une table de l'ensemble des cas de test ajoutés.

!!! tip "En savoir plus"
	Pour en savoir plus sur la gestion du plan d'exécution d'une campagne, d'une itération et d'une suite, se référer à [Gérer un plan d'exécution](../gestion-executions/gerer-plan-execution.md)

L'ancre **Suites automatisées** d'une itération ou d'une suite affiche une table des tests exécutés automatiquement permettant d'accéder aux résultats d'exécution et aux différents rapports et pièces jointes liées.

L'ancre **Anomalies connues** fournit la liste des anomalies remontées au cours des exécutions de l'objet consulté.

![Page de consultation](resources/ancre-fr.png){class="pleinepage"}
