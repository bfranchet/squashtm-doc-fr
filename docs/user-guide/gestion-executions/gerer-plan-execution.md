# Gérer un plan d'exécution

## Organiser le plan d’exécution 

Dans le plan d'exécution d'une campagne, d'une itération ou d'une suite de tests, il est possible de modifier l'ordre des items de plan d'exécution (ITPI). Pour cela il suffit de glisser déposer les ITPI à l'emplacement souhaité dans le tableau du plan d'exécution. 

!!! info "Info"
    - L'utilisateur doit disposer des droits nécessaires pour réorganiser le Plan d’exécution.
    - La réorganisation du plan d’exécution à l'aide du glisser/déposer affecte tous les utilisateurs et est permanente.

 Pour une réorganisation non permanente, il est possible de trier et/ou filtrer certaines colonnes du plan d'exécution.
 
 ![Trier et filtrer le plan d'exécution](./resources/tri-filtre-colonne-plan-exec.jpg){class="pleinepage" }

Il est ainsi possible de trier le plan d'exécution par date de dernière exécution et par utilisateur ou encore de filtrer le plan d'exécution par projet et par statut d'exécution.

!!! info "Info"
    Le plan d'exécution de l'itération peut être filtré en fonction d'un attribut (référence, importance,...), pour faciliter l'identification des tests à ajouter à une suite de tests.

## Assigner les tests 

Il est possible d’assigner l'exécution des tests à des utilisateurs depuis le bloc **Plan d’exécution** d’une campagne, d'une itération et d'une suite de test.

Dans le plan d’exécution, la colonne "Utilisateur" se présente sous la forme d’une liste où l’utilisateur a le choix entre la valeur "Non Affecté" et la liste des utilisateurs (Prénom Nom (Login)) ayant une habilitation sur le projet.

L'assignation des tests à des utilisateurs permet de planifier en amont la phase d'exécution du projet.

!!! info "Info"
    Si un utilisateur a le profil "Testeur" sur un projet, il ne peut visualiser que les cas de test qui lui sont assignés dans le bloc **Plan d'exécution**. 

## Modifier les attributs des ITPI

### Modification simple des attributs

En cliquant sur la cellule correspondante dans le tableau du plan d'exécution, il est possible de modifier :

- Le jeu de données de l'ITPI
- L'utilisateur affecté pour l'exécution de l'ITPI
- Le statut d'exécution de ITPI en mode "Fastpass". 

!!! info "Info"
    La modification du statut d'un ITPI en mode Fastpass ne crée pas d'exécution. Ces ITPI n'apparaissent donc pas dans les exports personnalisés qui ciblent les exécutions.

### Modification multiple des attributs

Le bouton **[Modifier les attributs]** ![Modifier les attributs](./resources/bulk_edition.png){class="icone"} permet la modification en masse des attributs de plusieurs ITPI simultanément. 
<br/>Pour les ITPI sélectionnés, il est possible de modifier les informations suivantes :

- L'ajout ou la modification de l'association des ITPI à une suite de tests (si celle-ci est déjà créée)
- L'assignation d'un utilisateur à plusieurs ITPI
- Le statut d'exécution des ITPI en mode "Fastpass"

Le bouton **[Ajouter une suite]** ![Ajouter une suite](./resources/add_tests_suites.png){class="icone"} permet de créer une ou plusieurs suites de tests et d'associer les ITPI sélectionnés dans le tableau directement à cette/ces suite(s).

!!! tip "En savoir plus"
    Pour en savoir plus sur l'exécution d'un cas de test en mode "Fastpass", consulter la page ["Exécuter manuellement les tests"](./executer-manuellement-tests.md)
