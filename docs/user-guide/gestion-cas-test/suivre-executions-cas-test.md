# Suivre les exécutions du cas de test

L'historique des exécutions d'un cas de test est renseigné dans l'ancre "Exécutions" ![Ancre "Exécutions"](./resources/icone-execution.png){class="icone"} du cas de test. Le nombre d'exécutions est indiqué dans l'ancre.

Le tableau des exécutions est alimenté automatiquement et ne peut être modifié. 
En cliquant sur le lien d’une exécution dans la colonne "Exéc", l’utilisateur est renvoyé sur la page de consultation de l’exécution.


![Ensemble des exécutions du cas de test](./resources/execution-ct.jpg){class="pleinepage"}

Si le cas de test est exécuté plusieurs fois dans la même itération ou dans la même suite de tests, il sera présent autant de fois qu'il a été exécuté. La colonne "Exéc" affiche le numéro de l'exécution (#1, #2, #3).

Le statut de la dernière exécution du cas de test est repris dans la capsule en haut de la page.