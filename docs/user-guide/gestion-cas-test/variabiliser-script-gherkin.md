# Variabiliser un script Gherkin

Variabiliser un script Gherkin permet de rejouer un scénario autant de fois qu'il comporte de jeu de données.

L'introduction de variables dans un script Gherkin nécessite l'utilisation de différents mots-clés, paramètres et jeux de données directement intégrés dans le script. Le mot-clé 'Plan du Scénario' indique que le scénario de test contient des exemples, c'est-à-dire des paramètres et des jeux de données.

Dans le script, les paramètres sont définis en étant écrit entre les caractères < et >. Il est possible d'avoir autant de paramètres que souhaité dans un script. Ces paramètres doivent ensuite être insérés dans une table de données.

Avant l'insertion d'une table de données, le mot-clé 'Exemples' doit apparaître afin d'indiquer la présence de cette table. Celle-ci s'insère sous la forme d'un tableau comportant autant de colonnes que de paramètres renseignés dans le script. La ligne d'en-tête de ce tableau reprend pour chaque colonne les noms des paramètres tels qu'ils ont été écrits dans le script. Chaque ligne correspond à un jeu de données qui sera joué à l'exécution.

!!! warning "Focus"
    Le nom des paramètres dans l'en-tête du tableau de données doit être écrit de la même manière que dans le script. La casse doit être respectée, au quel cas les paramètres ne seront pas pris en compte et ne s'afficheront pas lors de l'exécution. 
    
![Script Gherkin variabilisé](resources/script-gherkhin-variabilise-fr.png){class="centre"}


À l'exécution, le script est rejoué automatiquement pour chaque jeu de données dans le cas d'une exécution automatique et dans le cas d'une exécution manuelle un pas de test est créé pour chaque jeu de données dans l'**exécution**. Sur le plan d'exécution, contrairement aux cas de test BDD, il n'y a pas une ITPI par jeu de données, mais un seul ITPI. Dans l'exécution de cet ITPI, le scénario d'exécution affiche autant de pas de test qu'il y a de jeux de données dans le tableau de données du script.


![Pas de test pour script Gherkin avec paramètres](resources/pas-de-test-gherkin-fr.png){class="pleinepage"}