# Rédiger le script d'un cas de test Gherkin

La page de consultation d'un cas de test Gherkin se différencie par la structure de sa page **Script** ![Ancre Script](resources/list.png){class="icone"} qui propose un éditeur de texte dédié offrant une coloration et une vérification syntaxique, sans autocomplétion. 

![Ancre Script d'un cas de test de Gherkin](resources/ancre-script-FR.png){class="pleinepage"}

Dans Squash TM, un cas de test Gherkin peut être exécuté manuellement comme un cas de test classique ou exporté pour une exécution automatisée externe.

## Script d’un cas de test Gherkin

La langue du script Gherkin est définie par la balise langue présente sur la première ligne du script. Elle se présente de cette façon : *#language: < locale >*. 
Il est possible de modifier la langue du script en substituant < locale > par les deux lettres identifiant la langue de l'utilisateur :

- **fr** pour le Français
- **en** pour l'Anglais
- **es** pour l'Espagnol
- **de** pour l'Allemand

Pour que la langue du script soit prise en compte, la balise langue doit être conforme aux spécifications Gherkin et être placée en premier dans le script. Le mot 'Fonctionnalité' doit également être traduit dans la langue souhaitée. Si la balise est absente ou invalide, Squash TM interprétera le script comme rédigé en Anglais.

La seconde ligne contient le mot 'Fonctionnalité' suivi du nom du cas de test.

Les scénarios s'écrivent selon la syntaxe suivante : 

- **Scénario** suivi du nom du scénario de test ou **Plan de scénario** suivi du nom du scénario de test porteur de jeux de données

- **Étant donné que** ou  **Soit** suivi du contexte dans lequel doit être réalisé le test 

- **Quand** suivi de l'action à réaliser par l'utilisateur

- **Alors** suivi de la description du résultat attendu suite à l'action effectuée

- **Exemples** suivi d'une table de données contenant les jeux de données d'un plan de scénario

Il est également possible d'ajouter des instructions complémentaires en utilisant des mots-clés de type continuation tels que **et** ou **mais**.

!!! tip "En savoir plus"
    Pour en savoir plus sur la variabilisation d'un scénario Gherkin, consulter la page [Variabiliser un script Gherkin](./variabiliser-script-gherkin.md)

Au clic sur le champ texte du script, celui-ci devient blanc et passe en mode éditable. Plusieurs boutons apparaissent, au dessus du script, permettant de :

- Confirmer les modifications effectuées. Il est également possible d'enregistrer en utilisant le raccourci clavier [CTRL]+[ALT]+[S].

- Annuler les modifications effectuées depuis la dernière sauvegarde et de repasser en mode lecture seule.

- Insérer des snippets dans le script.

- Vérifier la syntaxe du script.

![Script d'un cas de test de Gherkin](resources/GherkinEdition.png){class="pleinepage"}

!!! warning "Focus" 
    Si l’utilisateur n’a pas le droit de modifier le cas de test, ou qu’un des jalons liés au cas de test est au statut verrouillé, le script n'est pas modifiable. 

## Insérer des snippets

Les snippets sont des modèles de code réutilisables qu'il est possible d'insérer dans l’éditeur de script via le bouton ![Insérer](resources/insert.png){class="icone"} pour gagner du temps sur la rédaction. 

Dans Squash TM, les snippets suivants sont disponibles :

- **param** insert des <> pour la notation des paramètres
- **plan_scenario** insert un scénario avec les mots clés Plan du scénario, Soit, Quand, Alors, Exemples
- **scenario** insert un scénario avec les mots clés Scénario, Soit, Quand, Alors
- **tab4\*3, tab4\*2, tab3\*3, tab3\*2** insèrent des tables de données dont le nombre de colonnes et de lignes est indiqué dans le nom. Une ligne d’entête est également ajoutée.

![Insertion des snippets dans un script Gherkin](resources/inserer-snippetsFR.png){class="pleinepage"}

## Vérifier la syntaxe du script

Lorsque le champ de texte est en mode édition, un bouton ![Vérifier](resources/verify.png){class="icone"} est disponible et permet de vérifier que la syntaxe Gherkin est respectée avant de confirmer les modifications.

Si le script est invalide, une pop-up s'affiche avec un message d'erreur. Provenant directement du parser Gherkin, ce message n'est pas traduit. 

![Vérifier la syntaxe du script](resources/Verifier-ScriptFR.png){class="pleinepage"}

## Utiliser l’aide

Sur la page de consultation d'un cas de test Gherkin, le bouton ![Aide](resources/help.png){class="icone"} affiche un volet d'aide à la rédaction de cas de test Gherkin présentant :

- un lexique des mot-clés

- des exemples de scénarios de test différents pour une fonctionnalité donnée

Ce volet s'affiche à droite du script Gherkin dans la même langue que celle définie au niveau du script. 

![Aide à la rédaction des cas de test Gherkin](resources/aide-GherkinFR.png){class="pleinepage"}