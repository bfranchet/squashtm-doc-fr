# Variabiliser un cas de test BDD 

Une action se compose de parties fixes et de parties variables aussi appelées ‘paramètres’. Afin de variabiliser un cas de test BDD, ces parties variables peuvent être valorisées de deux manières différentes dans les pas de test : 

- Par des valeurs libres
- Par des paramètres du cas de test qui donnent lieu à des jeux de données

![Deux types de paramètres d'un cas de test BDD](resources/2-types-param-BDD-FR.png){class="pleinepage"}

## Avec des valeurs libres

Interprétées dans le code comme étant des variables, les valeurs libres peuvent prendre n’importe quelle valeur et différer d’un cas de test à un autre.<br /> Pour définir une valeur libre dans une action, elle doit être écrite entre guillemets : "valeur".

Une action doit obligatoirement contenir au moins une partie fixe, elle ne peut donc pas être composée uniquement d’une valeur libre. Tous les caractères sont autorisés sauf les caractères ", < et >. Les guillemets sont automatiquement refermés si le caractère " est oublié.
Les nombres renseignés dans une action sont considérés comme des valeurs libres, qu’ils soient saisis ou non entre guillemets. Ils sont automatiquement affichés en bleu dans les pas de test du cas de test BDD.

!!! info "Info"
    Si le plugin **Bibliothèque d'actions** est installé, dans l'espace **Actions**, l’action contenant le paramètre avec une valeur libre s’affiche avec un nom de paramètre par défaut : "param1", "param2", "param3" (s’il y a plusieurs paramètres dans l’action). La valeur libre qui a été renseignée à la création de l’action depuis le cas de test devient la valeur par défaut du paramètre. Le nom et la valeur par défaut du paramètre de l’action sont modifiables depuis l'espace **Actions** mais leur modification n’a aucune incidence sur les valeurs déjà renseignées dans les pas de test.<br />Pour en savoir plus, consulter la page [Concevoir des cas de test automatisés](../gestion-tests-automatises/concevoir-tests-automatises.md)


![Paramètre libre d'un pas de test BDD](resources/param-libre-BDD-FR.png){class="pleinepage"}


## Avec des paramètres et jeux de données 

Les parties variables d’une action peuvent également être valorisées par des paramètres du cas de test. Le paramètre renseigné dans l’action du pas de test peut ainsi prendre les valeurs définies dans les jeux de données du cas de test au moment de son exécution.<br />
Pour définir un paramètre du cas de test dans une action, il doit être écrit entre les caractères <>.
On parle de "paramètre porté" par le cas de test.

Un paramètre de cas de test ne peut être vide et seuls les lettres majuscules, minuscules, les chiffres et les tirets - et _ sont autorisés. 
Il est possible de modifier une action déjà présente dans le cas de test pour lui ajouter un paramètre avec une valeur libre ou un paramètre du cas de test. Cette modification donnera lieu à la création d'une nouvelle action.

!!! info "Info"
    Si le plugin **Bibliothèque d'actions** est installé, dans l'espace **Actions**, l’action contenant le paramètre du cas de test s’affiche avec un nom de paramètre par défaut : "param1", "param2", "param3" (s’il y a plusieurs paramètres dans l’action). 
    Le paramètre du cas de test entre <> qui a été renseigné à la création depuis le cas de test n’est pas repris comme valeur par défaut du paramètre.<br />Pour en savoir plus, consulter la page [Concevoir des cas de test automatisés](../gestion-tests-automatises/concevoir-tests-automatises.md)
    
Les paramètres renseignés <> sont automatiquement répertoriés dans l'ancre **Paramètres et jeux de données** du cas de test.
Ces paramètres ont pour vocation à être variabilisés par un ou plusieurs jeux de données de la même manière que pour un cas de test classique.

![Paramètre porté d'un pas de test BDD](resources/param-porte-BDD-FR.png){class="pleinepage"}

