# Rédiger le script d'un cas de test BDD

Les cas de test BDD se différencient des cas de test classiques par la structure de leurs pas de test au niveau de l'ancre **Pas de test** ![Ancre Prérequis et pas de test](resources/list.png){class="icone"}. Ils se basent sur la syntaxe Gherkin et sont composés d’un mot-clé suivi d’une action :

- Mot-clé : Étant donné que, Quand, Alors, Et, Mais
- Action : phrase d’action qui suit le mot-clé.

![Pas de test BDD](resources/pas-de-test-bddFR.png){class="pleinepage"}


## Pas de test d’un cas de test BDD

### Créer un pas de test BDD

Pour créer un pas de test de cas de test BDD il faut sélectionner un mot-clé dans la liste déroulante, saisir une action dans le champ texte puis appuyer sur la touche Entrée du clavier. 

Le champ 'Action' d’un pas de test BDD se compose d'une ou de plusieurs parties fixes (du texte) et d'une ou plusieurs variables (des paramètres). Une action doit obligatoirement contenir au moins une partie fixe et ne peut pas excéder 255 caractères.

Pour les **parties fixes** d’une action, tous les caractères sont autorisés sauf <, > et " qui sont interprétés comme des marqueurs de paramètres ainsi que les nombres qui sont interprétés automatiquement comme des variables.
Pour les **parties variables** d’une action, les caractères autorisées dépendent de la nature de la variable.


Lors de l’ajout d’un pas de test BDD : 

- soit une nouvelle action est créée dans le projet courant
- soit l’action du pas de test est rattachée à une action existante dans le projet courant ou l’un des projets auxquels est habilité l’utilisateur

!!! info "Info"
    La gestion des actions des pas de test BDD se fait via l’espace **Actions** porté par le plugin **Bibliothèque d'actions**. Ce plugin offre également à l'utilisateur une suggestion des actions présentes dans la bibliothèque lors de la saisie/modification des pas de test BDD.
    Si le plugin ‘Bibliothèque d’actions’ est installé, les actions créées, les cas de test auxquels elles sont rattachées ainsi que des informations sur leur implémentation sont visibles directement depuis l’espace **Actions**. <br />Pour en savoir plus, consulter la page [Concevoir des cas de test automatisés](../gestion-tests-automatises/concevoir-tests-automatises.md)

![Bibliothèque d'actions](resources/plugin-bibliotheque-actionsFR.png){class="pleinepage"} 

### Modifier et supprimer un pas de test BDD

La modification des pas de test BDD s’effectue en cliquant directement sur les champs des pas de test. Il est possible de modifier le mot-clé et l’action, les parties fixes et les parties variables. 
Si le plugin **Bibliothèque d'actions** est installé, lors de la modification d'une action, une liste de suggestions d'actions existantes s'affiche.

![Suggestion à la modification d'un pas de test BDD](resources/BDD-autocompletionFR.png){class="pleinepage"}


Suite à la modification d'une action, soit une nouvelle action est créée, soit un rattachement est fait à une action existante.

La suppression d’un pas de test BDD ne supprime pas l’action, elle supprime simplement le lien entre l’action et le cas de test. L’action sera toujours présente dans l’espace **Actions**.

## Editer les données complémentaires d’un pas de test BDD

Chaque pas de test BDD dispose d’un sous-menu permettant de renseigner des données complémentaires. 

![Ajout de données complémentaires d'un pas de test BD](resources/donnees-complementaires-BDD-FR.png){class="pleinepage"}

Ces champs permettent d’enrichir l’action avec des informations complémentaires. 

- Le champ **table de données** permet d’ajouter à l’action un tableau de valeurs. Les valeurs du tableau doivent être renseignées en respectant le formalisme de l'exemple fourni lors de l'ajout.
-   Le champ **docstring** permet d’ajouter à l’action un texte multiligne qui sera interprété comme un attribut de l’action lors de l’exécution automatisée.
- Le champ **commentaire** permet d’ajouter une ou plusieurs lignes de commentaires à l’action.

![Données complémentaires d'un pas de test BDD](resources/donnees-complementaires2-BDD-FR.png){class="pleinepage"}

## Générer l’aperçu du script d’un cas de test BDD

Un bouton [Aperçu] ![Visualiser l'aperçu](resources/icone-aperçu.png){class="icone"} permet de consulter le script qui sera généré par Squash TM lors de l’export ou de la transmission du cas de test BDD. 

La structure du script visible depuis l’aperçu dépend de la technologie et de la langue d’implémentation paramétrées au niveau de la configuration du projet. Pour un même cas de test BDD, le script ne sera donc pas le même en fonction de la technologie choisie : Cucumber ou Robot Framework.
