# Rechercher des cas de test 

La page de recherche des cas de test se divise en 2 parties : à gauche le bloc des critères de recherche et à droite les résultats de recherche.

!!! tip "En savoir plus"
    Consulter la page "[Rechercher un objet](../presentation-generale/fonctionnalites-objet.md#rechercher-un-objet)" pour connaître le fonctionnement de la page de recherche.

Pour rappel, le périmètre reprend par défaut les projets présents dans le filtre projet. Il est également possible de faire une recherche sur un périmètre personnalisé en sélectionnant un ou plusieurs dossiers ou une sélection de cas de test.

![Page de recherche de cas de test](./resources/page-recherche-casdetest.jpg){class="pleinepage"}

## Les critères de recherche des cas de test 

Les critères de recherche correspondant aux cas de test sont nombreux et peuvent être couplés les uns aux autres.

Les critères de recherche sont répartis en plusieurs catégories de critères :

- Les **informations** du cas de test
- L'**historique** du cas de test (date de création/modification ainsi que login de la personne ayant fait cette action)
- Les informations liées à l'**automatisation** du cas de test
- Les informations liées aux **jalons**
- Les **attributs** du cas de test
- Les informations liées au **contenu** (pas de test, paramètres, jeux de données, appels de cas de test, pièces jointes)
- Les informations liées aux **associations** avec les exigences, les itérations, les exécutions et les anomalies
- Les différents **champs personnalisés** associés aux cas de test.

!!! info "Info"
    Les critères de recherche associés aux jalons ne sont visibles que si les jalons sont activés sur l'instance.

**Par exemple :**

Sélectionner dans la catégorie **Attributs**, le critère "Statut" avec les options "En cours de rédaction" et "A approuver" et dans la catégorie **Associations** le critère "Nombre d'exigences' avec la comparaison Egal à 0 : 
<br/>Les résultats de recherche retournent uniquement les cas de test au statut "En cours de rédaction" et "A approuver" qui ne sont pas liés à des exigences.


## Les résultats de recherche 

Les résultats de recherche sont affichés sous forme de tableau comportant plusieurs colonnes reprenant les différentes informations des cas de test. Les résultats de recherche peuvent être **exportés** dans un fichier excel sous 2 formats : 

- le format XLS 'Champs actuels'
- le format XLS 'Tous les champs'

!!! tip "En savoir plus"
    Consulter la page "[Rechercher un objet](../presentation-generale/fonctionnalites-objet.md#exporter)" pour en savoir plus sur l'export des résultats de recherche.

Directement dans le tableau des résultats de recherche, il est possible de **modifier unitairement** les attributs suivants de chaque cas de test :

- La référence
- Le nom 
- Le statut 
- L'importance 
- La nature 
- Le type
- L'éligibilité à l'automatisation

Il est également possible de **modifier en masse** les informations suivantes à l'aide des boutons d'actions présents en haut à droite du tableau :

- de modifier en masse les informations comme l'importance, le statut, le type, la nature et l'éligibilité à l'automatisation
- d'associer ou dissocier en masse des jalons aux cas de test (s'ils sont associés au projet) 

!!! danger "Attention"
    Un cas de test associé à un jalon au statut "Verrouillé" ne peut pas être modifié



