# Les anomalies dans Squash

## Qu’est-ce qu’une anomalie ? 

Une anomalie est un dysfonctionnement détecté au niveau d’un système ou d’un produit. Il s'agit d'un comportement de l’application observé le plus souvent lors de l’exécution d’un test, qui ne correspond pas au comportement décrit dans les spécifications fonctionnelles ou la User Story. 

L’anomalie constitue l’un des principaux indicateurs d'évaluation de la qualité d’un produit/système. La correction des anomalies aboutit à l’obtention d’un système valide, conforme aux spécifications et au besoin client/utilisateur.

Dans toute équipe IT, les anomalies sont tracées dans un outil afin d'être corrigées par les développements.

## Squash TM et ses bugtrackers 

Squash TM ne dispose pas d'espace de gestion dédié aux anomalies. Mais il s'interface parfaitement avec plusieurs gestionnaires d'anomalies grâces à différents plugins. Les anomalies sont ainsi gérées dans un outil dédié à cette tâche tout en étant associées aux exécutions de tests dans Squash TM.

Squash TM s'interface avec les outils de bugtracking suivants : 

|Bugtracker|Plugin nécessaire|
|--|--|
| Mantis | Intégré au coeur de Squash TM |
| Jira Server | Jira Bugtracker Server et Data Center |
| Jira Datacenter | Jira Bugtracker Server et Data Center |
| Jira Cloud | Jira Bugtracker Cloud |
| Redmine | Redmine Bugtracker |
| Rational Team Concert | RTC Bugtracker |
| Tuleap | Tuleap Bugtracker |

Pour pouvoir déclarer, associer des anomalies depuis des exécutions et consulter des anomalies depuis Squash TM, il est nécessaire :

- D'avoir le [plugin](../../install-guide/installation-plugins/installer-plugins.md) correspondant au bugtracker d'installé sur Squash TM
- D'avoir le [serveur](../../admin-guide/gestion-serveurs/gerer-bugtracker-serveur-synchro.md) correspondant au bugtracker de déclaré sur Squash TM par un administrateur
- D'avoir le serveur et le projet du bugtracker de configurés sur le [projet](../../admin-guide/gestion-projets/configurer-projet.md#parametrer-un-bugtracker) Squash TM par un administrateur ou un chef de projet

!!! tip "En savoir plus"
    Consulter la page [Déclarer et suivre des anomalies](./declarer-suivre-anomalies.md) pour savoir comment gérer les anomalies dans Squash TM.