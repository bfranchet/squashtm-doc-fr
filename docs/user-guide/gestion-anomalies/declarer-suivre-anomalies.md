# Déclarer et suivre des anomalies

La déclaration des anomalies se fait dans l'espace Campagnes de Squash TM. Les anomalies sont identifiées et remontées lors de la phase d'exécution des cas de test. Les cas de test peuvent être exécutés un par un depuis le plan d'exécution ou à la chaîne depuis l'itération ou la suite de tests avec le bouton ![Exécuter](resources/executions.png){class="icone"} présent au-dessus du plan d'exécution.
<br/>Squash TM supporte les connexions avec les bugtrackers Mantis, Jira (Server, Datacenter, Cloud), Redmine, Bugzilla, RTC et Tuleap. 

## Déclarer une anomalie depuis un pas d’exécution/une exécution

Deux options sont proposées pour l'exécution des cas de test : 

- Avec la pop-up : une pop-up d'exécution s'affiche et permet d'exécuter les pas de test un à un.
- Nouvelle exécution : la page d'exécution du cas de test s'affiche et il est possible de définir, depuis l'ancre 'Scénario d'exécution', le statut de l'exécution de chaque pas de test.

![Options d'exécution des cas de test](./resources/execution-ct-fr.png){class="pleinepage"}


Sur la pop-up d'exécution, la déclaration de l'anomalie se fait depuis le bloc 'Anomalies'. Sur la page d'exécution, elle se fait depuis l'ancre 'Scénario d'exécution', via le bouton ![Ajouter](resources/icone-ajouter.png){class="icone"}.
<br/>Une nouvelle anomalie peut être déclarée an choisissant l'option 'Déclarer une nouvelle anomalie'.


![Déclarer une nouvelle anomalie](./resources/ajouter-nouvelle-ano-fr.png){class="centre"}

La popup 'Déclarer une nouvelle anomalie' s'affiche avec les champs à renseigner pour déclarer l'anomalie sur le bugtracker (exemple sur Mantis) :

- **Projet** : le nom du projet sur le bugtracker dans lequel sera déclarée l'anomalie. Si plusieurs projets du bugtracker sont associés au projet Squash TM, il faut choisir le projet de destination souhaité pour l'anomalie.

- **Catégorie** : le type de ticket sur le bugtracker

- **Sévérité** : le degré de gravité de l'anomalie 

- **Version du produit** : la version du produit sous lequel l'anomalie est rapportée 

- **Assigné à** : la personne à qui sera assignée l'anomalie 

- **Résumé** : le titre de l'anomalie (champ obligatoire)

- **Description** : Elle contient par défaut : le libellé du cas de test associé, l'URL de  l'exécution en cours et la description de l'anomalie qui est à compléter par l'utilisateur

- **Informations complémentaires** : les pas de test tels qu'ils ont été renseignés sur Squash TM jusqu'au pas de test en échec. 

À la création de l'anomalie, il est obligatoire de remplir à minima le champ 'Résumé'. Les champs remplis par défaut peuvent être modifiés.

Les champs du formulaire s'adaptent en fonction du projet et du type de ticket sélectionné par le testeur. Pour les bugtrackers autres que Mantis, les champs personnalisés sont également récupérés dans la popup de déclaration afin d'être renseignés. À la création d'une anomalie, tous les champs identifiés par une astérisque sont obligatoires.

![Déclarer une nouvelle anomalie](./resources/declarer-nouvelle-anomalie-fr.png){class="centre"}

Une fois l'anomalie ajoutée, le bloc 'Anomalies' se met à jour. L'anomalie apparaît alors dans le tableau et est automatiquement ajoutée au bugtracker associé au projet Squash TM. Il est possible de la consulter sur le bugtracker en cliquant sur son identifiant dans la colonne 'Clé' du tableau :

![Anomalie enregistrée](./resources/tableau-anomalie-fr.png){class="centre"}

## Associer une anomalie existante à un pas d’exécution/une exécution

Lorsque l'anomalie constatée a déjà été rapportée, il est possible de l'associer directement au pas d'exécution ou à l'exécution avec l'option 'Rattacher une anomalie existante' après avoir cliqué sur le bouton ![Ajouter](resources/icone-ajouter.png){class="icone"}. Renseigner l'identifiant de l'anomalie dans le champ puis valider la saisie.
Une fois l'anomalie trouvée, ses informations s'affichent et elle peut être ajoutée au pas d'exécution ou à l'exécution.

![Recherche une anomalie](./resources/associer-anomalie-existante-fr.png){class="centre"}

## Suivre les anomalies connues sur tous les objets de Squash TM

Les anomalies relevées depuis une exécution ou un pas d'exécution sont ajoutées à l'ancre 'Anomalies connues' dans la suite de tests, l'itération et la campagne dans laquelle l'anomalie a été relevée. Un tableau avec toutes les anomalies s'affiche et le nombre d'anomalies connues est mis à jour sous l'icône de l'ancre.

Ainsi, l'ancre "Anomalies connues" rassemble pour la suite de tests l'ensemble des anomalies déclarées durant l'exécution des tests contenus dans son plan d'exécution. Il en va de même pour les itérations et les campagnes.

Une ancre "Anomalies connues" est également présente sur le cas de test rassemblant toutes les anomalies déclarées au cours des diverses exécutions de ce dernier. Sur l'exigence l'ancre "Anomalies connues" répertorie la totalité des anomalies associées aux tests qui la vérifient.

Ce tableau permet notamment de suivre la résolution des tickets (colonne "Statut") car les informations sont récupérées en temps réel directement depuis le bugtracker. Il permet en outre, d'avoir un historique complet des défauts rencontrés au cours des phases d'exécution.

![Tableau des anomalies connues](./resources/ano-connues-fr.png){class="pleinepage"}


