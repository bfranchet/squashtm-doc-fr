# Les tableaux de bord personnalisés

## Créer un tableau de bord personnalisé 

Le tableau de bord personnalisé permet d'afficher sur la même page plusieurs graphiques personnalisés ainsi que des rapports.

![Création d'un tableau de bord](./resources/tableau-de-bord.jpg){class="pleinepage"}

La génération d'un tableau de bord personnalisé se fait depuis l'espace Pilotage via le bouton ![Ajouter](resources/icone-add.png){class="icone"} puis 'Ajouter un tableau de bord'.

Une fois le tableau de bord ajouté, les graphiques et rapports créés dans n'importe quel projet peuvent être placés dans le tableau de bord à l'aide d'un glisser déposer.

Les rapports ajoutés sont téléchargeables directement depuis le tableau de bord.

Un tableau de bord peut contenir au maximum 12 éléments. 
Par défaut, l'élément déposé a une taille qui fait 1/12ème de l'espace du tableau de bord et il est positionné à l'emplacement le plus haut à gauche disponible.

!!! info "Info"
    Pour une meilleure visibilité des graphiques notamment, il est recommandé d'agrandir leur taille pour arriver au minimum à 2/12ème de l'espace. 
    <br/>Il est préférable d'adapter la taille du graphique en fonction des données qu'il contient. 

Pour modifier la disposition des éléments dans un tableau de bord : 

- Glisser déposer l'élément à l'emplacement voulu dans le tableau de bord
- Utiliser l'icone grise ![Redimensionnement élément](./resources/redimensionnement.jpg){class="icone"} présente dans l'angle en bas à droite pour agrandir ou diminuer la taille d'un élément. 
- Si un élément (1) sélectionné dans l'arbre est déposé sur un élément (2) déjà présent dans le tableau de bord, alors l'élément (2) est remplacé par l'élement (1) et ne s'affiche plus dans le tableau de bord. 
- Il est possible d'intervertir 2 graphiques s'ils ont la même taille.

Un quadrillage apparaît pour aider au redimensionnement et au placement des éléments.

![Placement et redimensionnement des éléments dans un tableau de bord](./resources/tableau-bord-disposition.jpg){class="pleinepage"}

Pour supprimer un élément du tableau de bord, survoler celui-ci et cliquer sur le bouton ![Supprimer](resources/unlike.png){class="icone"} présent en haut à droite.

## Afficher un tableau de bord personnalisé en favori 

Sur la page de consultation d’un tableau de bord, le bouton ![Favori](./resources/wizard.png){class="icone"} permet d’afficher le tableau de bord dans les différents espaces de l’application :

- Page d’accueil
- Espace Exigences
- Espace Cas de test
- Espace Campagnes

Il est possible d'afficher le tableau de bord sur plusieurs espaces. Pour ne plus afficher le tableau de bord en favori, sélectionner l'option "Aucun espace".

![Mettre un tableau de bord en favori](./resources/tableau-de-bord-favori.jpg){class="pleinepage"}

!!! info "Info"
    Le tableau de bord favori se définit indépendamment d’un utilisateur à l’autre. 
    <br/>Une fois qu'un tableau de bord est affecté à un espace, il s’affiche depuis tous les projets de l’espace sélectionné.

**Affichage du tableau de bord favori dans les espaces**

Pour afficher le tableau de bord favori sur la page d'accueil de Squash TM, cliquer sur le bouton **[Tableau de bord]** présent en haut à droite de la page.

Sur la page d'accueil, le tableau de bord présente le même périmètre que dans l'espace pilotage.

![Tableau de bord sur la page d'accueil](./resources/accueil-tdb.jpg){class="pleinepage"}

Pour afficher le tableau de bord personnalisé dans les espaces Exigences, Cas de test et Campagnes, cliquer sur le bouton **[Favori]** en haut et à droite du bloc "Tableau de bord".
</br>Pour revenir au tableau de bord par défaut, cliquer sur le bouton **[Défaut]* pour l'afficher.

!!! info "Info"
    Les graphiques des tableaux de bord personnalisés s'adaptent à la sélection réalisée dans l'arbre de l'espace quel que soit le périmètre des graphiques dans l'espace Pilotage.

![Tableau de bord favori dans les espaces](./resources/espace-tdb.jpg){class="pleinepage"}
