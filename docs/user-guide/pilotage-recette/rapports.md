# Raports

## Gestion des rapports

La génération d'un rapport se fait depuis l'espace Pilotage via le bouton ![Ajouter](resources/icone-add.png){class="icone"} puis 'Ajouter un rapport'. Une page s'affiche proposant tous les types de rapports disponibles dans Squash TM. 
<br/>En cliquant sur le rapport souhaité, les blocs 'Informations' et 'Critères' s'affichent. Une fois les informations saisies et les critères sélectionnés, cliquer sur **[Télécharger]** pour générer le rapport, ou sur **[Ajouter]** pour l'enregistrer dans le projet. 

![Liste de rapports](resources/liste_rapports.png){class="pleinepage"}

Lorsque le rapport est enregistré, il apparaît dans l'arbre et les attributs à partir desquels il a été créé sont consultables depuis l'ancre 'Informations'. 
<br/>Le rapport est modifiable via le bouton ![Modifier](resources/edit1.png){class="icone"} et téléchargeable depuis le bouton ![Télécharger](resources/download.png){class="icone"}.

![Rapport créé dans l'arbre](resources/affichage-rapport-arbre.png){class="pleinepage"}

## Les rapports de la phase de préparation

### Les cahiers d'exigences

Le cahier des exigences peut être généré au format PDF ou Word et peut comprendre : 

- Toutes les exigences d'un ou plusieurs projets

- Une sélection simple ou multiple d'exigences d'un ou plusieurs projets

- Les exigences associées à un jalon


![Création cahier des exigences](resources/creation-cahier-des-exigences-fr.png){class="pleinepage"}

!!! warning "Focus"
    - La fonction de sélection des projets ou des exigences ne fonctionne que si l'utilisateur a bien cliqué sur l'option choisie. Squash TM affichera par défaut l'ensemble des exigences du 1er projet sélectionné par défaut.
    - Si l'option 'Imprimer seulement la dernière version d'exigence' est cochée, seule la dernière version de l'exigence sera présente dans le rapport. Sinon, toutes les versions de l'exigence seront présentes dans le rapport.
    - Sous Word, la table des matières et l'index des exigences doivent être mis à jour manuellement, en les sélectionnant et en appuyant sur F9.


### Les cahiers de test

Le cahier de test peut être généré au format PDF ou Word. Il peut comprendre les informations suivantes : 

- Les cas de test d'un ou plusieurs projets

- Une sélection simple ou multiple des cas de test d'un ou plusieurs projets

- Les cas de test associés à un jalon

- Les cas de test associés à un champ personnalisé de type 'tag'

Il peut également contenir des informations complémentaires telles que : 

- Les pas de test

- Les champs personnalisés associés aux pas de test

- Les exigences liées aux pas de test

- Le nombre de pièces jointes attachées aux pas de test

- Les pas de test des cas de test appelés 

- Les paramètres et les jeux de données

- Les exigences associées

![Création cahier de test (Format PDF)](resources/cahier-de-test-edit-fr.png){class="pleinepage"}

!!! info "Info"
    Pour le rapport **Cahier de test** (format PDF), les options d'impression 'Imprimer les exigences liées aux pas de test' et 'Imprimer le nombre de pièces jointes des pas de test' ne sont pas disponibles.


!!! warning "Focus"
    - La fonction de sélection des projets ou des cas de test ne fonctionne que si l'utilisateur a cliqué sur l'option choisie. Sinon, Squash TM affichera l'ensemble des cas de test du 1er projet sélectionné par défaut.
    - Les options d'impression sont cochées par défaut mais pas les sous-options.
    - Sous Word, la table des matières et l'index des exigences doivent être mis à jour manuellement, en les sélectionnant et en appuyant sur F9.


### Couverture des exigences

Le rapport de Couverture des exigences est un tableau de bord de suivi de la couverture fonctionnelle des exigences par les cas de test.
Généré à partir du périmètre d'un ou plusieurs projets ou d'un jalon, il propose deux tableaux de suivi :

- Le tableau de couverture des exigences
- La liste des exigences du projet

![Couverture des exigences](resources/couverture-des-exigences-fr.png){class="pleinepage"}

**Le tableau de couverture des exigences** contient le nombre total d'exigences par projet et leur répartition par niveau de criticité et par statut. Le pourcentage de couverture des exigences par au moins un cas de test est indiqué pour chaque catégorie.

**La liste des exigences par projet** affiche pour chaque exigence la version et la criticité ainsi que le nombre de cas de test couvrant l’exigence.

!!! info "Info"
    Les rapports générés peuvent être exportés sous différents formats : 

    - Le tableau de couverture des exigences peut être exporté au format : .pdf et .html
    - La liste des exigences par projet peut être exportée au format : .xsl, .csv, .pdf et .html

## Les rapports de la phase d'exécution

### Avancement de l'exécution

Le rapport d'avancement de l'exécution est un tableau de bord de suivi de l'exécution des tests.
La fenêtre 'Avancement de l'exécution' s'affiche avec les blocs 'Informations' et 'Critères' au clic sur **Avancement de l'exécution** sur la page de *Sélection des rapports*. 
Le bloc 'Critères' permet de déterminer le périmètre des campagnes prises en compte dans le rapport :

- **le périmètre du rapport** : par défaut, Squash TM génère le rapport d'avancement sur toutes les campagnes. Il est possible de modifier le périmètre en sélectionnant l'option voulue.

- **le planning** : l'utilisateur a la possibilité de renseigner les dates de début et de fin (prévues et/ou réelles). Ces champs sont facultatifs.

- **le statut de la campagne** :

    - **Toutes** (valeur sélectionnée par défaut)

    - **En cours** : les campagnes ayant au moins une itération avec au moins un cas de test au statut *A exécuter* ou *En cours*.

    - **Terminées** : les campagnes dont aucune des itérations n'a de cas de test au statut *A exécuter* ou *En cours*.

Deux tableaux sont générés pour ce rapport : 

**Le tableau de bord de suivi des campagnes** contient le planning des campagnes et des itérations ainsi que le détail des statuts d’exécution des cas de test par suite de test. 

![Suivi des campagnes](resources/suivi-des-campagnes-fr.png){class="pleinepage"}

**La liste des cas de test par campagne** affiche la liste détaillée des cas de test par campagne, itération et suite de test avec leur statut d’exécution.

![Liste cas de test](resources/cas-de-test-avancement-execution-fr.png){class="pleinepage"}

!!! info "Info"
    L'export des rapports d'avancement de l'exécution se fait dans les mêmes conditions que l'export du rapport de couverture des exigences.

### Avancement qualitatif

Le rapport sur l'avancement qualitatif permet d'avoir un aperçu rapide des exigences couvertes ou non par les tests exécutés. Ce rapport est généré à partir du périmètre d'un ou plusieurs projets. 
Deux tableaux sont générés pour ce rapport :

**Le tableau de bord d'avancement qualitatif** quantifie le nombre d’exigences testées et validées dans les différentes campagnes, itérations et suites de tests contenues dans le projet.

![Avancement qualitatif](resources/avancement-qualitatif-fr.png){class="pleinepage"}

**La liste des exigences avec leur statut d'exécution** affiche la liste détaillée des exigences avec le statut d’exécution des cas de test les couvrant par campagne, itération et suite de tests.

![Exigences avancement qualitatif](resources/exigences-avancement-qualitatif-fr.png){class="pleinepage"}

### Bilan de campagne

Le rapport Bilan de campagne permet de générer un rapport éditable contenant le détail des indicateurs d’une campagne : détail des exigences et cas de test testés dans les itétations de la campagne, taux de couverture des exigences, détail des anomalies avec les bugtrackers Jira et Redmine. Il est généré au format Word pour permettre à l'utilisateur d'y apporter des modifications. 

Il comprend, **sur une seule campagne d'un projet ainsi que ses itérations**, les informations suivantes :

- les informations de la campagne choisie

- les itérations qui lui sont associées

- le planning (si renseigné) de la campagne et de ses itérations

- le nombre de cas de test par statut d'exécution pour la campagne et pour chaque itération

- le nombre total de cas de test pour la campagne et pour chaque itération

- le poucentage d'avancement de l'exécution de la campagne et de chaque itération

- la couverture fonctionnelle des exigences en fonction de leur criticité pour la campagne

- la couverture fonctionnelle des exigences en fonction de leur criticité pour chaque itération<sup id="fnref:1"><a href="#fn:1" rel="footnote">1</a></sup>

- le nombre d'anomalies de la campagne par criticité et par statut selon leur type<sup id="fnref:2"><a  href="#fn:2" rel="footnote">2</a></sup>

- la liste de toutes les anomalies de la campagne<sup id="fnref:2"><a  href="#fn:2" rel="footnote">2</a></sup>

- le détail des exigences et des cas de test en fonction de leur criticité ou de leur importance pour chaque itération de la campagne<sup id="fnref:3"><a href="#fn:3" rel="footnote">3</a></sup>


!!! warning "Focus"
    - Pour récupérer le détail des anomalies Jira ou Redmine, il faut renseigner les identifiants de connexion au bugtracker dans l'espace « Bugtrackers ». Cette option ne fonctionne pas pour les autres types de bugtrackers.
    - Sur Word, la table des matières doit être mise à jour manuellement en appuyant sur F9.
    - Dans le bilan de campagne, le texte surligné en jaune est à compléter manuellement par l'utilisateur.
    - Le Bilan de Campagne ne présente un intérêt que si la campagne possède des itérations.


### Bilan d'itération

Comme le Bilan de campagne, le rapport Bilan d’itération permet de générer un rapport éditable contenant le détail des indicateurs d’une itération : détail des exigences et cas de test testés dans l’itération, taux de couverture des exigences, détail des anomalies avec les bugtrackers Jira et Redmine. Il est également généré au format Word pour permettre à l'utilisateur d'y apporter des modifications. 

Il comprend, **sur une seule itération d'un projet ainsi que ses suites de tests**, les informations suivantes :

- les informations de l'itération choisie

- les suites de test qui lui sont associées

- le planning (si renseigné) de l'itération

- le nombre de cas de test par statut d'exécution pour l'itération et pour chaque suite de test

- le nombre total de cas de test pour l'itération et pour chaque suite de test

- le pourcentage d'avancement de l'exécution de l'itération et de chaque suite de test

- la couverture fonctionnelle des exigences en fonction de leur criticité pour l'itération

- la couverture fonctionnelle des exigences en fonction de leur criticité pour chaque suite de tests<sup id="fnref:4"><a href="#fn:4" rel="footnote">4</a></sup>

- le nombre d'anomalies de l'itération par criticité et par statut selon leur type<sup id="fnref:2"><a href="#fn:2" rel="footnote">2</a></sup>

- la liste de toutes les anomalies de l'itération<sup id="fnref:2"><a href="#fn:2" rel="footnote">2</a></sup>

- le détail des exigences et des cas de test de l'itération en fonction de leur criticité ou de leur importance<sup id="fnref:3"><a href="#fn:3" rel="footnote">3</a></sup>

!!! warning "Focus"
    - Pour récupérer le détail des anomalies Jira ou Redmine, il faut renseigner les identifiants de connexion au bugtracker dans l'espace « Bugtrackers ». Cette option ne fonctionne pas pour les autres types de bugtrackers.
    - Sur Word, la table des matières doit être mise à jour manuellement en appuyant sur F9.
    - Dans le Bilan d'Itération, le texte surligné en jaune est à compléter manuellement par l'utilisateur.

<div class="footnotes">
<hr/>
<ol>
<li id="fn:1">
<p>Généré lorsque la case 'inclure le taux de couverture des exigences <b>par itération</b>' est cochée.<a href="#fnref:1" rev="footnote">&#8617;</a></p></li>
<li  id="fn:2">
<p>Généré lorsque la case 'inclure le détail des anomalies' est cochée.<a  href="#fnref:2"  rev="footnote">&#8617;</a></p></li>
<li  id="fn:3">
<p>Généré lorsque la case 'inclure le détail des exigences et des cas de test' est cochée.<a  href="#fnref:3"  rev="footnote">&#8617;</a></p></li>
<li id="fn:4">
<p>Généré lorsque la case 'inclure le taux de couverture des exigences <b>par suite</b>' est cochée.<a href="#fnref:4" rev="footnote">&#8617;</a></p></li>
</ol>
</div>