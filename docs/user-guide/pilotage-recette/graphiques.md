# Les graphiques

L'espace Pilotage de Squash TM permet de créer des graphiques personnalisés à partir des données présentes dans les projets. Ces graphiques sont un outil idéal pour le suivi de la recette dans toutes ses phases aussi bien pour le chef de projet et que pour les membres de l'équipe de Recette.

## La page de création de graphique

La page de création de graphique se divise en 3 parties : 

- À gauche, la partie de sélection du périmètre, du type de graphique, des axes et des filtres.
- Au centre, le nom du graphique ainsi que l'aperçu du graphique.
- À droite, la liste des attributs sélectionnables. La partie "Sélection des attributs" peut être cachée suivant la taille de l'écran.

![Page de création d'un graphique](./resources/creation-graphique.jpg){class="pleinepage"}

Pour générer et ajouter un graphique, il faut au préalable :

- donner un nom au graphique
- sélectionner son périmètre
- sélectionner un type de graphique
- renseigner les axes présents dans le bloc "Axes"

Une fois le graphique créé, sur la page de consultation du graphique, il est possible de le modifier en cliquant sur le bouton ![Modifier le graphique](./resources/pen.png){class="icone"} ou de le télécharger en cliquant sur le bouton ![Télécharger le graphique](./resources/download.png){class="icone"}.

## Le choix du périmètre 

Il existe 3 options de sélection du périmètre :

- **Projet courant** : lorsque cette option est sélectionnée, le périmètre pris en compte est l’ensemble du projet dans lequel est créé le graphique. Le périmètre par défaut est flottant, c’est-à-dire qu’il s’adapte au projet dans lequel se trouve le graphique c'est notammenet le cas si celui-ci est copié ou déplacé dans un autre projet.
- **Sélection par projet** : cette option permet de sélectionner un ou plusieurs projets. En cliquant sur cette option, la liste des projets pour lesquels l'utilisateur dispose d'une habilitation s'affiche. Dans ce cas, le périmètre est fixe, il ne s’adapte pas au projet dans lequel se trouve le graphique si ce dernier est copieé ou déplacé.
- **Sélection personnalisée** : cette option permet d’effectuer une sélection précise d’objets (exigences, cas de test, campagnes, dossiers) par espace (Exigences, Cas de test et Campagnes). Le périmètre reste fixe comme pour l'option "Sélection par projet".

## Le type de graphique 

Il est possible de créer 5 graphiques de type différents : 

- **Répartition** : (diagramme) avec choix de l’axe.
</br>Ce graphique n'accepte que des agrégations, chaque portion représente le nombre d'enregistrements correspondants à la valeur indiquée.
- **Histogramme** : avec choix de l’axe des abscisses et de l’axe des ordonnées.
</br>Ce graphique demande une agrégation ou un regroupement par date en axe horizontal (Par), et un numérique (Comptage, Min, Max, Moyenne ou Somme) en axe vertical (Mesurer).
- **Evolution** : avec choix de l’axe des abscisses et de l’axe des ordonnées.
</br>Ce graphique demande une agrégation ou un regroupement par date en axe horizontal (Par), et un numérique (Comptage, Min, Max, Moyenne ou Somme) en axe vertical (Mesurer). Les valeurs de l'axe vertical sont additionnées lorsqu'on avance sur l'axe horizontal, afin d'obtenir un cumul.
- **Tendances** : avec choix de l’axe des abscisses, de l’axe des ordonnées et de la série de données.
</br>Ce graphique demande une agrégation ou un regroupement par date en axe horizontal (Par) et pour les séries (Séries), et un numérique (Comptage, Min, Max, Moyenne ou Somme) en axe vertical (Mesurer).
- **Comparaison** : avec choix de l’axe des abscisses, de l’axe des ordonnées, et de la série de données.
</br>Ce graphique demande une agrégation ou un regroupement par date en axe vertical (Par) et pour les séries (Séries), et un numérique (Comptage, Min, Max, Moyenne ou Somme) en axe horizontal (Mesurer). Le graphique de type Comparaison est « couché » par rapport aux graphiques de type Tendances ou Histogramme.

| Axe \ Type  | Répartition | Histogramme                         | Evolution                           | Tendances                           | Comparaison                          |
|---|-------------|-------------------------------------|-------------------------------------|-------------------------------------|--------------------------------------|
| Par  | Agrégation  | Agrégation ou Regroupement par date | Agrégation ou Regroupement par date | Agrégation ou Regroupement par date | Agrégation ou Regroupement par date    |
| Mesurer  |             | Numérique                           | Numérique                           | Numérique                           |   Numérique                        |
| Séries  |             |                                     |                                     | Agrégation ou Regroupement par date | Agrégation ou regroupement par date   |


## La sélection des attributs pour les axes du graphique 


Les entités contenant les attributs sélectionnables sont les suivantes :

- Exigences
- Versions d’exigence
- Cas de test
- Campagnes
- Itérations
- Items de plan d’exécution
- Exécutions

Chaque attribut est sélectionnable indépendamment des autres.

!!! info "Info"
    Un item de plan d'exécution (ITPI) correspond à une ligne (soit un cas de test) dans le plan d'exécution d'une itération ou d'une suite de tests.
    Un item de plan d'exécution peut avoir plusieurs exécutions.
    C'est le statut de la dernière exécution qui est comptabilisé comme étant le statut d'exécution de l'ITPI.

### Sélection des attributs

Pour sélectionner un attribut, il faut : 

- depuis la 'Sélection des attributs' à droite : glisser/déposer l'attribut dans la zone d'aperçu du graphique. L'attribut sera alors automatiquement ajouté à l'axe correspondant dans le bloc "Axes"
- depuis la 'Sélection des attributs' à droite : glisser/déposer l'attribut dans l'axe souhaité (ne fonctionne que si l'axe n'est pas déjà renseigné)
- depuis le bloc "Axes" à gauche, cliquer sur l'axe à renseigner, un encart s'affiche avec l'ensemble des attributs par entités. Cliquer sur l'attribut pour le sélectionner. 

Une fois l'ensemble des axes renseignés, l'aperçu du graphique se met automatiquement à jour.

La liste des opérations possibles varie selon le type d’attribut choisi :

- Agrégation : indique qu'il y aura un élément de graphique par valeur rencontrée de cet attribut.
- Comptage : compte le nombre de valeurs distinctes obtenues.
- Maximum, Minimum, Moyenne et Somme : s'appliquent à des attributs de type numérique.
- Par jour, par semaine, par mois, par année (Regroupement par date): s'appliquent à des attributs de type date.

Lors de la sélection d'un attribut pour un axe, un opérateur par défaut s'affiche suivant le type d'attribut choisi, afin de faciliter la création du graphique. Cet opérateur peut être modifié pour correspond au mieux aux résultats attendus.

### Les champs personnalisés et listes personnalisées

Pour les entités "Versions d’exigences", "Cas de test", "Campagnes", "Itérations" et "Exécutions", il est possible d’avoir comme attributs les champs personnalisés qui leurs sont associés. La couleur de l'arrondi est alors légérement plus claire que pour les attributs natifs de Squash TM.
</br>Il est possible de générer un graphiques à partir des champs personnalisés de type "texte simple", "liste déroulante", "date", "case à cocher", "tag" et "numérique". Les champs personnalisés de type "texte riche" ne permettent pas de créer un graphique.

![Champs personnalisés dans les attributs](./resources/cuf-graphique.png){class="centre"}

!!! info "Info"
    L’ensemble des attributs des listes par défaut ont des couleurs prédéfinies dans les graphiques personnalisés. </br>Les graphiques réalisés à partir des champs personnalisés de type liste ainsi que des listes personnalisées arborent les couleurs prédéfinies par l'administrateur pour les options de ces listes dans l’espace Administration.

## La mise en place de filtres

Les filtres permettent d'épurer les données en ne sélectionnant que les éléments souhaités dans le graphique. 

La liste des filtres possibles varie selon le type d’attribut choisi : 

- Supérieur, Supérieur ou égal, Inférieur, Inférieur ou égal, Entre, Égal et Différent de : s'appliquent à des attributs de type numérique ou de type date.
- Égal et Contient : s'appliquent à des attributs de type texte libre (ex : Référence).
- Égal et Dans : s'appliquent à des attributs de type liste (ex : Importance, Statut). Le filtre 'Dans' permet notamment de sélectionner plusieurs valeurs d'une liste.

Il est possible d'appliquer des Filtres sur tout ou une partie des attributs sélectionnés.


!!! tip "En savoir plus"
    Les graphiques personnalisés peuvent être ajoutées à un tableau de bord personnalisé. Pour en savoir plus sur les tableau de bord personnalisés, consulter la page ["Les tableaux de bord personnalisés"](./tableaux-bord-perso.md)

## Exemples de graphiques 

### Graphique de type Répartition

Je souhaite avoir la répartition des mes exigences en fonction de leur criticité.

![Graphique de type Répartition](./resources/graphique-repartition.jpg){class="pleinepage"}

La criticité est une liste de valeurs qui est interprétée comme une agrégation.
Étant donné que l'on souhaite organiser les exigences avec une seule agrégation, il faut utiliser le graphique de Répartition.


### Graphique de type Histogramme

Je souhaite pour un périmètre fonctionnel donné (un dossier) avoir le nombre d'exigences fonctionnelles, métiers et le nombre de user stories (valeurs de la liste Catégorie d'exigence).

![Graphique de type Histogramme](./resources/graphique-histogramme.jpg){class="pleinepage"}

La catégorie est une liste de valeurs qui est interprétée comme une agrégation. Elle peut être croisée avec les ID d'exigences qui vont être comptés pour chaque valeur de la liste.
Un premier filtre est réalisé sur le périmètre pour n'avoir que les exigences du dossier et un second filtre est réalisé sur les 3 valeurs souhaitées.
Le graphique le plus approprié pour croiser une agrégation et un comptage est l'Histogramme.


### Graphique de type Evolution

Je souhaite suivre la création des exigences dans le temps pour mon projet.

![Graphique de type Evolution](./resources/graphique-evolution.jpg)
{class="pleinepage"}

La date de création des exigences permet de faire un regroupement par date pour suivre l'évolution de la conception.
Pour comptabiliser le nombre d'exigences créées par jour, il faut compter les ID d'exigences.
Pour restreindre les données à celles du projet, il faut filtrer le périmètre.
Le graphique le plus adapté est le graphique de type Évolution.

### Graphique de type Tendances

Je souhaite suivre la création des tests de mes 5 testeurs sur une période de 30 jours. Je ne souhaite avoir que les tests qui contiennent des pas de test.

![Graphique de type Tendances](./resources/graphique-tendances.jpg)
{class="pleinepage"}

La date de création des cas de test permet de faire un regroupement par date pour suivre l'évolution de la conception.
Il faut également le login des testeurs qui ont créé les tests pour faire une agrégation. Pour comptabiliser le nombre de cas de test créées par jour, il faut compter les ID de cas de test. Pour restreindre les données à une période, il faut filtrer sur la date de création. Et pour être sûre de n'avoir que des cas de test contenant des pas de test, il faut ajouter l'attribut ‘Nombre de pas de test' pour réaliser un filtre. 
Le graphique le plus adapté est le graphique de type Tendances.

### Graphique de type Comparaison

Je souhaite avoir le statut d'exécution des tests de ma campagne en fonction de mes testeurs.

![Graphique de type Comparaison](./resources/graphique-comparaison2.jpg)
{class="pleinepage"}

Le statut d'exécution des tests et le login du testeur sont les deux agrégations de ce graphique. Pour comptabiliser le nombre de cas de test par testeur, il faut compter l'ID des cas de test. Pour restreindre les données à une campagne, il faut filtrer sur le périmètre. Le graphique le plus adapté est le graphique de type Comparaison.