# Les exports de campagne personnalisés

Cet élément de reporting permet à l’utilisateur de générer un export personnalisé d’une campagne, d’une itération ou d’une suite de tests à l’aide d’un assistant qui permet de choisir le périmètre et les données à exporter.

Les exports personnalisés sont au format .csv.

La génération d'un export personnalisé se fait depuis l'espace Pilotage via le bouton ![Ajouter](resources/icone-add.png){class="icone"} puis 'Ajouter un export personnalisé'.

![Création d'un export personnalisé](resources/export-personnalise.jpg){class="pleinepage"}


## La page de création d’un export personnalisé 

La page de création d'un export personnalisé se divise en 2 parties :

- À gauche, le nom de l'export, le bloc de sélection du périmètre et le récapitulatif des attributs sélectionnés
- À droite, la liste des attributs sélectionnables organisés par entité.

Pour enregistrer un export personnalisé, il faut :

- Renseigner son nom
- Sélectionner son périmètre
- Sélectionner au moins 1 attribut.

Une fois l'export de campagne personnalisé enregistré, il est possible, depuis sa page de consultation, de le modifier en cliquant sur le bouton ![Modifier l'export personnalisé](./resources/pen.png){class="icone"} ou de le télécharger en cliquant sur le bouton ![Télécharger l'export personnalisé](./resources/download.png){class="icone"}.

### Le choix du périmètre

Pour les exports personnalisés, le périmètre sélectionnable est soit :

- une campagne
- une itération
- une suite de test

!!! info "Info"
    La sélection multiple ainsi que la sélection de projet ou dossier n'est pas possible.

### La sélection des attributs


Les entités contenant les attributs sélectionnables sont les suivantes :

- Campagnes
- Itérations
- Suites de test
- Cas de test
- Exécutions
- Pas d’exécution
- Anomalies

Chaque attribut est sélectionnable indépendamment des autres.

Pour sélectionner un attribut, il faut depuis la partie "Sélection des attributs" à droite, glisser/déposer l'attribut dans la partie "Attributs sélectionnés" à gauche.

Pour les entités "Campagnes", "Itérations", "Suites de tests", "Cas de test", "Exécutions" et "Pas d’exécutions", il est possible d’avoir comme attributs les champs personnalisés qui leurs sont associés. La couleur de l'arrondi est alors légérement plus claire que pour les attributs natifs de Squash TM. 
L'affichage des champs personnalisés s'adapte au périmètre choisi.

![Champs personnalisés](./resources/cuf-export.jpg){class="centre"}

## Structure du fichier de l'export personnalisé 

|Nom de la colonne dans l'export   |  Description |
|---|---|
| CPG - Libellé |  Libellé de la campagne |
| CPG - ID | Identifiant de la campagne  |
| CPG - Référence |  Référence de la campagne |
| CPG - Description | Description de la campagne   |
| CPG - Etat  |  État de la campagne |
| CPG - Statut d'avancement  |  Statut d'avancement de la campagne |
| CPG - Jalon | Jalon associé à la campagne  |
| CPG - Début prévu le  | Date de début de campagne prévu le  |
| CPG - Fin prévue le | Date de fin de campagne prévue le |
| CPG - Début réel le |  Date réelle de début de la campagne |
| CPG - Fin réelle le  | Date réelle de fin de la campagne |
| IT - Libellé | Libellé de l'itération  |
| IT - ID  | Identifiant de l'itération  |
| IT - Référence | Référence de l'itération   |
|IT - Description  |  Description de l'itération |
|IT - Etat | État de l'itération  |
| IT - Début prévu le | Date de début d'itération prévue le  |
| IT - Fin prévue le |  Date de fin d'itération prévue le |
|IT - Début réel le  | Date réelle de début de l'itération  |
| IT - Fin réelle le | Date réelle de fin de l'itération  |
| SUI - Libellé | Libellé de la suite  |
| SUI - ID  | Identifiant de la suite  |
|SUI - Description  |  Description de la suite |
| SUI - Statut d'exécution  |  Statut d'exécution de la suite |
| SUI - Statut d'avancement  | Statut d'avancement de la suite  |
| TC - Projet  |  Nom du projet du cas de test |
| TC - Jalons |  Jalon(s) associé(s) au cas de test |
|  TC - Libellé | Libellé du cas de test  |
| TC - ID |  Identifiant du cas de test |
| TC - Référence | Référence du cas de test  |
|TC - Description | Description du cas de test  |
|TC - Statut  | Statut du cas de test  |
| TC - Importance  | Importance du cas de test  |
| TC - Nature | Nature du cas de test  |
|  TC - Type|  Type du cas de test |
| TC - Jeu de données |  Jeu de données associés au cas de test |
| TC - Prérequis | Prérequis du cas de test   |
|TC - Nombre d'exigences liées  |  Nombre d'exigences liées au cas de test|
|TC - ID exigence(s) liée(s)| Identifiant des exigences liées au cas de test |
|EXEC - ID  | Identifiant de l'exécution  |
|  EXEC - Mode d'exécution| Mode d'exécution (manuel ou automatique)   |
|  EXEC - Statut |  Statut d'exécution |
| EXEC - % Succès | Pourcentage de succès pour l'exécution   |
| EXEC - Utilisateur | Login de l'utilisateur ayant exécuté le cas de test  |
|  EXEC - Date d'exécution| Date d'exécution  |
| EXEC - Commentaires | Commentaire lié à l'exécution  |
| EXEC_STEP - Numéro du pas d'exécution | Numéro du pas d'exécution  |
| EXEC_STEP - Action |  Action du pas d'exécution |
| EXEC_STEP - Résultat | Résultat attendu du pas d'exécution  |
| EXEC_STEP - Statut | Statut d'exécution du pas |
|EXEC_STEP - Utilisateur  | Login de l'utilisateur ayant exécuté le pas  |
| EXEC_STEP - Date d'exécution |  Date d'exécution du pas |
|  EXEC_STEP - Commentaires | Commentaire lié au pas d'exécution  |
| EXEC_STEP - Nombre d'exigences liées |  Nombre d'exigences liées au pas d'exécution |
|EXEC_STEP - ID exigence liée au pas de test  | Identifiant des exigences liées au pas d'exécution   |
| BUG - Nombre d'anomalies liées à l'exécution  | Nombre d'anomalies liées à l'exécution  |
| BUG - Exécution | Clé de(s) anomalie(s) liée(s) à l'exécution   |
| BUG - Nombre d'anomalies liées au pas d'exécution | Nombre d'anomalies liées au pas d'exécution   |
|BUG - Pas d'exécution  | Clé de(s) anomalie(s) liée(s) au pas d'exécution    |



