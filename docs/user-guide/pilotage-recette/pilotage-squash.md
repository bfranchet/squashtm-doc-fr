# Le pilotage dans Squash

## L’espace Pilotage de Squash

Cet espace permet de piloter l'activité de recette à l’aide d’éléments de reporting standards ou personnalisés.
Depuis l'espace Pilotage, il est possible de créer quatre types d’éléments de reporting :

- Graphique ![Graphique](./resources/chart.png){class="icone"} 
- Rapport ![Rapport](./resources/report.png){class="icone"} 
- Tableau de bord ![Tableau de bord](./resources/dashboard.png){class="icone"} 
- Export personnalisé ![Export personnalisé](./resources/custom_export.png){class="icone"}

![Espace pilotage](./resources/espace-pilotage.jpg){class="pleinepage"}


## La page de consultation des objets de l’espace Pilotage

La page de consultation d'un élément de reporting est constituée :

- d'un nom
- Pour un graphique : des blocs "Graphique" et "Informations"
-  Pour un rapport : d'un bloc "Informations"
-  Pour un tableau de bord : des blocs "Tableau de bord" et "Informations"
- Pour un export personnalisé : d'un bloc "Informations"

Depuis la page de consultation il est possible de : 

- Modifier le nom de l'élément
- Modifier un élément
- Télécharger un rapport, un export personnalisé et un graphique

La barre des ancres à gauche, permet au clic sur une ancre, d'accéder au bloc correspondant :

- Le bloc "Informations" affiche les attributs de l'élément
- Le bloc "Graphique" affiche le graphique généré
- Le bloc "Tableau de bord" affiche le tableau de bord généré

