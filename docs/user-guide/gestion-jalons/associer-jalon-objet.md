# Associer un jalon à un objet

Lorsqu'un jalon au statut 'En cours' ou 'Terminé est associé à un projet, les utilisateurs disposant des droits nécessaires sur le projet peuvent associer le jalon aux objets suivants : exigences, cas de test et campagnes.

!!! tip "En savoir plus"
    Pour associer des objets à un jalon, il est nécessaire que le jalon ait été associé préalable au projet.
    Pour savoir comment associer un jalon à un projet consulter la page suivante : [Associer des jalons au projet](../../admin-guide/gestion-projets/configurer-projet.md#associer-des-jalons-au-projet)

## Associer un jalon à un objet 

Pour associer une exigence, un cas de test ou une campagne à un jalon, il faut se rendre sur le bloc "Informations" de la page de consultation de l'objet puis cliquer sur le bouton ![Ajouter](./resources/add.png){class="icone"} présent dans le champ 'Jalons'.

Les jalons disponibles à l'association s'affichent dans une popup. Il suffit de cocher le jalon à associer puis de cliquer sur **[Confirmer]**.

![Associer un jalon](./resources/associer-jalon-objet.png){class="centre"}

Plusieurs versions d'une même exigence ne peuvent pas être associer au même jalon. Lorsqu'un jalon du projet est déjà associé à une autre version de l'exigence, il n'est pas proposé dans la popup d'association.

Il est possible d'associer plusieurs jalons à une version d'exigence et à un cas de test. Le comportement défini au sein de l'exigence et le cas de test vérifiant ce comportement peuvent être valides sur plusieurs versions (jalons) du système testé.

Par contre, une campagne ne peut être associée qu'à un seul jalon. En effet, la campagne est la représentation de la phase d'exécution sur la version tagguée par le jalon, le lien est donc unique.

![Info Jalons](./resources/info-jalon.png){class="centre"}

Les informations du jalon s'affichent au survol dans une infobulle. Pour dissocier un jalon d'un objet il suffit de cliquer sur la **[X]**.

!!! danger "Attention"
    Le statut d'un jalon peut être modifié à 'Verrouillé' après qu'il ait été lié à des objets.
    <br/>Dans ce cas tous les objets liés à ce jalon verouillé ne peuvent plus être dissociés du jalon, modifiés ou supprimés.
    <br/>Dans le cas d'une campagne associée à ce jalon, l'ensemble des élements qu'elle contient, tels que ses itérations, ses suites de tests et ses exécutions, sont bloqués en modification et ne peuvent être supprimés.

## Les jalons dans la recherche

Il est possible d’effectuer une recherche d’exigences, de cas de test et de campagnes en filtrant sur les critères relatifs aux jalons : 'Nom du jalon', 'Statut du jalon' et 'Date d'échéance'.

![Critères recherche jalons](./resources/criteres-recherche.png){class="centre"}

La colonne 'NB JAL' qui s'affiche dans le tableau des résultats de recherche de l'espace Exigences et Cas de test indique le nombre de jalons auxquels est associé l'objet.

Dans l'espace Exigences et Cas de test, la modification en masse des jalons est disponible. Pour ce faire, sélectionner des exigences ou des cas de test dans le tableau des résultats de recherche puis cliquer sur le bouton ![Jalons](./resources/milestone.png){class="icone"} pour sélectionner les jalons à associer ou dissocier.

Si plusieurs versions d'une même exigence sont sélectionnées, la modification en masse des jalons est impossible.