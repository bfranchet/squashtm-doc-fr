# Les jalons dans Squash TM

## Qu’est qu’un jalon ? 

En développement informatique, une version d’application contient un ensemble d’évolutions et/ou de corrections. Ces évolutions (fonctionnelles) sont issues des besoins (métiers) et décrites sous la forme d’exigences. In fine, la version d’une application est associée un ensemble d’exigences, elles-mêmes couvertes par un ensemble de cas de test et vérifiées par un ensemble d’exécutions réalisées dans le cadre d’une campagne de tests visant à contrôler ladite version.
<br/>Cet ensemble d’objets cohérents (exigences, cas de test, exécutions, campagne...) peut donc être logiquement regroupé et marqué (« tagué ») comme étant le référentiel de test cohérent correspondant à une version donnée de l’application : le versioning du référentiel de test.

La notion de versioning dont il est question ici est une vision simplifiée de la notion de versioning appliquée aux sources de code : il n'est pas question de fonctionnalités de gestion de branches ou de merge. Pour éviter toute confusion ce versionning dans Squash TM est appelé "Jalons" plutôt que "Versions".
<br/>Un jalon est donc une étiquette (ex : « 1.11 », « 2013-2 », « Projet SEPA v4.1 »...) affectée aux différents objets de l’application et permettant de les regrouper sous une entité commune. Un même objet peut se voir affecter différents jalons. Ainsi, une exigence qui n’évolue pas entre deux versions de l’application se traduit par un même objet exigence affecté à deux jalons différents celui de la version n-1 et de celui de la version n.

Seuls les utilisateurs avec un profil 'Administrateur' ou 'Chef de projet' peuvent créer des jalons depuis l'espace Administration de Squash TM.

!!! tip "En savoir plus"
    Les fonctionnalités liées aux jalons sont par défaut désactivés sur Squash TM. Pour utiliser ces fonctions, il est nécessaire d’activer les jalons depuis l'espace Administration de Squash TM. <br/>Pour en savoir plus sur l'activation et l'adminsitration des jalons dans Squash TM consulter la partie dédiée dans le guide administrateur : [Gestion des jalons](../../admin-guide/gestion-jalons/activer-jalons.md)

Un jalon se définit par un libellé, une date d'échéance et un statut. 
<br/>Le libellé reprend communément le nom de la version de l'application. La date d'échéance correspond à la date prévisionnelle de fin de la recette ou à la date prévue de mise en production de l'application. Le statut du jalon est quant à lui défini par utilisateur en fonction du cycle de recette de la version :

- version pas encore en cours de recette : Planifié
- version en cours de recette : En cours
- version dont la recette est terminée : Terminé
- version dont la recette est terminée et pour laquelle on souhaite bloquer toutes modifications du référentiel de test : Verrouillé

## Le cycle de vie d’un jalon 

Le cycle de vie d'un jalon se compose des quatre statuts suivants : 'Planifié', 'En cours', 'Terminé' et 'Verrouillé'.

Chacun de ces statuts confère des droits différents en matière :

- d’association du jalon à un projet ou un objet
- de création/suppression/modification des objets associés au jalon

Avec un jalon au statut 'Planifié' :

- il est possible de l’associer/le dissocier des projets
- il n’est pas possible de l’associer/le dissocier des objets (le jalon ne figure pas dans les popups d’association d’un jalon aux objets)

Avec un jalon au statut 'En cours' :

- il est possible de l’associer/le dissocier des projets
- il est possible de l’associer/le dissocier des objets (le jalon figure dans les popups d’association d’un jalon aux objets)
- il est possible de créer/modifier/supprimer les objets associés au jalon

Avec un jalon au statut 'Terminé', le comportement est en tout point similaire au statut 'En cours'. Il est juste présent à titre indicatif afin d'informer l'utilisateur que la recette de cette version est terminée.

Avec un jalon au statut 'Verrouillé' :

- il n’est pas possible d’associer/dissocier le jalon des projets. (le jalon ne figure plus dans la popup d’association d’un jalon à un projet)
- il n’est pas possible d’associer/dissocier le jalon des objets (le jalon ne figure plus dans la popup d’association d’un jalon à un projet)
- il n’est pas possible de modifier/supprimer les objets associés au jalon.

Ci-dessous un tableau récapitulatif recense les actions possibles pour chacun des 4 statuts :

| Statut | Associer/dissocier un jalon à un projet | Associer/dissocier un jalon à un objet | Autorise la création d'objets | Autorise la suppression d'objets | Autorise la modification d'objets |
|--|--|--|--|--|--|
|Planifié|oui|non|non|non|non|
|En cours|oui|oui|oui|oui|oui|
|Terminé|oui|oui|oui|oui|oui|
|Verrouillé|non|non|non|non|non|

!!! danger "Attention"
    Tout objet associé à un jalon au statut "Verouillé" ne peut être modifié ou supprimé. Dans le cas d'une campagne associée à un jalon "Verouillé", l'ensemble des élements qu'elle contient, tel que ses itérations, ses suites de tests et ses exécutions, sont bloquées en modification et ne peuvent être supprimés.