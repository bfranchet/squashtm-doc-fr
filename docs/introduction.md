# Introduction

## Documentation de l'outil Squash TM

- [Guide d'installation](install-guide/installation-squash/config-mini-prerequis.md)
- [Guide administrateur](admin-guide/presentation-generale/presentation-administration-squash.md)
- [Guide utilisateur](user-guide/presentation-generale/espaces-squash.md)
- [Documentation Legacy](https://sites.google.com/a/henix.fr/wiki-squash-tm/)

## Documentation Squash AUTOM et DEVOPS

- [Documentation AUTOM et DEVOPS](https://autom-devops-fr.doc.squashtest.com)

## Terminologie

Présentation des notions et concepts abordés dans cette documentation :

|Notion|Définition métier
|--|--|
|Exigence|Extraites des documents de conception et des règles de gestion dont elles découlent, les exigences décrivent les comportements attendus de l’application.
|Cas de test|Chemin fonctionnel à mettre en œuvre pour vérifier la conformité d’une fonctionnalité. Le cas de test se définit par le jeu de données à constituer, le scénario de test à exécuter et les résultats attendus.
|Pas de test|Étape du chemin fonctionnel mis en place dans le cadre d'un cas de test. Chaque pas de test permet de vérifier un résultat attendu.
|Campagne|Établie de manière à vérifier un nombre fini de fonctionnalités, une campagne de tests compte plusieurs étapes : <br/>- définition des objectifs de test (choix des cas de test et des jeux de tests rattachés)<br/>- détermination du nombre d'itérations et de leur contenu en fonction des objectifs de test <br/>- exécution des tests<br/>- identification, analyse et suivi d’anomalies <br/>- exploitation des résultats <br/>- conclusion sur le comportement des fonctionnalités vérifiées.
|Itération|Sélection de cas de tests exécutés de manière successive dans le cadre d'une campagne de test. Chaque itération est un cycle de la campagne qui est définie par un laps de temps entre deux livraisons de développements.
|Suite de tests |Manière d’organiser les cas de test, de les regrouper afin de créer une image d'une partie du plan de test d'une itération. Fonctionnalité permettant d’enchaîner l’exécution des cas de test.
|Plan d'exécution |Table permettant l’organisation et le pilotage des tests de la campagne. Vision proposant différentes informations sur les cas de test comme leur mode d'exécution (automatique ou manuel), leur importance, leur statut, ou encore l’utilisateur assigné à leur exécution.
|Item de plan d'exécution (ITPI)|Un ITPI est une ligne de la table « plan d'exécution » des campagnes, itérations et suites de tests. Il est différent d’une exécution qui est une instanciation d’un cas de test ayant un statut d’exécution, un exécutant et une date d’exécution.
|Exécution|Il s’agit de la phase de déroulement des cas de test sur le système testé. C'est lors de cette phase que sont identifiées et tracées les anomalies rencontrées.