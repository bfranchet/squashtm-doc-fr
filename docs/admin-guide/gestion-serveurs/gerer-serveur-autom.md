# Gérer les serveurs d'exécution automatisée

## Ajouter, modifier, supprimer un serveur d’exécution automatisée

Squash TM peut se connecter avec deux serveurs d'exécution automatisée différents : l'[orchestrateur Squash](https://autom-devops-fr.doc.squashtest.com/2021-09/autom/autom.html) pour l'automatisation des tests avec Squash AUTOM (type SquashAUTOM)  et Jenkins pour l'automatisation des tests avec Squash TF (type Jenkins).

L'ajout et la configuration d'un serveur d'exécution se fait depuis le menu "Serveurs>Serveurs d'exécution automatisée" de l'espace Administration.

![Gestion des serveurs d'exécution automatisée](resources/administration_serveurs2_FR.png){class="pleinepage"}

À la création d'un serveur, le nom, le type et l'URL du serveur (la plus courte possible) doivent obligatoirement être renseignés. Pour que le serveur soit fonctionnel, il faut également y ajouter des identifiants : un login et mot de passe valides pour Jenkins et un token d'authentification pour Squash AUTOM.

![Ajout d'un serveur d'exécution automatisée](resources/nouveau_serveur-FR.png){class="pleinepage"}

!!! info "Info" 
    Avec Squash AUTOM, la présence d'un token est obligatoire pour la bonne exécution de tests automatisés depuis Squash TM. Dans le cas où l'authentification au serveur d'automatisation n'est pas requise par ce dernier, il faut quand même renseigner une valeur quelconque de token dans Squash TM.

*Page de consultation d'un serveur d'exécution automatisée de type Squash AUTOM*
![Page de consultation d'un serveur d'exécution automatisée](resources/configuration_serveurs_autom3_FR.png){class="pleinepage"}

*Page de consultation d'un serveur d'exécution automatisée de type Jenkins*
![Page de consultation d'un serveur d'exécution automatisée](resources/configuration_serveurs_FR.png){class="pleinepage"}

Lors de l'ajout d'un serveur d'exécution automatisée de type Jenkins, la case "Autoriser le choix manuel du serveur d'exécution au lancement des tests automatisés (Implique que ce serveur est un serveur maître)" permet de choisir le serveur d'exécution lors du lancement des tests dans le cas de l'utilisation d'un serveur maître avec des serveurs secondaires.


!!! danger "Attention" 
    La suppression d'un serveur d'exécution automatisée associé à au moins un projet ou une exécution entraîne la suppression des liens vers les rapports de résultat et d’exécution mais aussi l’association des scripts d'automatisation aux cas de test. Les exécutions des cas de test automatisés restent néanmoins accessibles. **Attention, cette action n'est pas réversible**.


