# Configurer le protocole d'authentification OAuth 1A

Le protocole d'authentification OAuth 1A n’est pour l’instant exploité que par les connecteurs Jira (Jira Bugtracker et Xsquash4Jira). Cela nécessite de configurer les URLs impliquées dans l’échange de jetons et les modalités de signature des requêtes. 

!!! tip "En savoir plus"
    Avant de réaliser la configuration du protocole d'authentification OAuth 1A dans Squash TM, il est nécessaire de configurer un lien d'application côté Jira.
    <br/> Pour ce faire, consulter la page dédiée : [Configuration de Oauth pour Jira](../../install-guide/installation-plugins/configurer-plugins-bt-outils-tiers.md#configuration-de-oauth-pour-jira).

La configuration du protocole d'authentification OAuth 1A se fait depuis la page de consultation du connecteur dans l'espace 'Bugtrackers et serveurs de synchronisation' :

![Protocole d'authentification Oauth1](./resources/protocole-oauth1.png){class="centre"}

Le formulaire présente les champs suivants :

- Consumer key : le nom du service dédié à Squash TM sur le fournisseur OAuth
- URL jetons temporaires : adresse du service d’obtention des jetons temporaires, et le protocole à utiliser (GET ou POST)
- URL jetons d’accès : adresse du service d’obtention des jetons définitifs, et le protocole à utiliser.
- URL d’autorisation : adresse du service d’autorisation par l’utilisateur.
- Secret : quel secret utiliser pour la signature des messages.
- Méthode de signature : de quelle façon signer les messages avec le secret. Les choix possibles sont HMAC-SHA1 et RSA-SHA1.

Dans le formulaire vierge, les champs URLs sont préremplis avec l'URL de Squash TM définie dans la configuration générale. Une fois la configuration effectuée, ne pas oublier de la sauvegarder avec le bouton **[Enregistrer]**. 
<br/>Il n'est pas possible de sauvegarder le formulaire avant d’avoir fourni toutes les informations demandées. Les informations manquantes sont mises en évidence avec un message adapté.

!!! warning "Focus"
    **Note sur le mode RSA-SHA1 :** la clé privée doit être fournie au format PKCS8 non encrypté. Si la clé se présente sous forme de PEM encodé en base 64, il faut l’entrer dans Squash TM sans les en-têtes/pieds (pas de -----BEGIN PRIVATE KEY-----) et sans retour chariot (tout doit tenir sur une seule ligne).

!!! info "Info"
    **Note sur le callback OAuth :** La séquence d’autorisation des jetons implique que le fournisseur OAuth redirige le client vers Squash TM. L’URL de redirection est renseignée par Squash TM qui a besoin de connaître sa propre adresse. 
    <br/>Pour fixer l’URL publique de Squash TM, renseigner le champ 'URL publique de Squash' dans la section 'Paramètres système' du sous-menu 'Système' de l'administration.
