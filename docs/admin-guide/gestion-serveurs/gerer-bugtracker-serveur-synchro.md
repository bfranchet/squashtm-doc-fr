# Gérer les bugtrackers et serveurs de synchronisation

## Bugtrackers vs serveurs de synchronisation 

Dans Squash TM, il faut distinguer les plugins de bugtracking des plugins dédiés aux synchronisations.
<br/>Les plugins de bugtracking servent à la déclaration d'anomalies depuis Squash TM vers des outils tiers de type Bugtracker. 
<br/>Les plugins de synchronisation servent à synchroniser dans Squash TM des tickets présents dans des outils tiers dédiés au suivi de projets IT. 

Voici la liste des outils tiers avec lesquels peut s'interfaçer Squash TM :

| Nom du plugin | Catégorie| Type dans l'espace Serveurs | Outils tiers | 
|--|--|--|--|
| Intégré dans le coeur de Squash TM | Bugtracking | mantis | Mantis |
| Bugzilla Bugtracker | Bugtracking | bugzilla | Bugzilla |
| Jira Bugtracker Server et Data Center | Bugtracking | jira.rest | Jira Server, Jira Data Center |
| Jira Bugtracker Cloud | Bugtracking | jira.cloud | Jira Cloud |
| Xsquash4jira | Synchronisation | jira.xsquash | Jira Server, Jira Data Center, Jira Cloud |
| Workflow Automatisation Jira | Synchronisation | jira.rest ou jira.cloud | Jira Server, Jira Data Center, Jira Cloud |
| Redmine Bugtracker | Bugtracking | redmine3.rest | Redmine |
| Redmine Exigences | Synchronisation | redmine3.rest | Redmine |
| RTC Bugtracker | Bugtracking | rtc | Rational Team Concert |
| Tuleap Bugtracker | Bugtracking | tuleap | Tuleap |

!!! tip "En savoir plus"
    Mis à part Mantis intégré nativement dans le coeur de Squash TM, il nécessaire d'installer le ou les plugins correspondant à l'outil pour accéder aux fonctionnalités de bugtracking ou de synchronisation.
    <br/>Pour en savoir plus sur les plugins, consulter les pages suivantes : [Les plugins de Squash TM](../../install-guide/installation-plugins/plugins-squash.md), [Installer les plugins Squash TM](../../install-guide/installation-plugins/installer-plugins.md).

## Ajouter, modifier, supprimer un bugtracker et un serveur de synchronisation

La gestion des bugtrackers et serveurs de synchronisation est accessible depuis l'espace Administration, sous-menu "Serveurs", via l'ancre **Bugtrackers et serveurs de synchronisation** ![Bugtrackers et serveurs de synchronisation](./resources/bug.png){class="icone"}.

Depuis le tableau de gestion des bugtrackers et serveurs de synchronisation, il est possible d'ajouter ![icone ajouter](./resources/icone-ajouter.png){class="icone"} ou de supprimer ![icone suppression](./resources/delete.png){class="icone"} des serveurs de façon unitaire ou en masse. Il est également possible de supprimer un bugtracker ou un serveur de synchronisation depuis sa page de consultation en cliquant sur le bouton **[...]**.

![Tableau de gestion des bugtrackers et serveurs de synchronisation](./resources/tableau-bt-serveur2.jpg){class="pleinepage"}

Lors de la création d'un bugtracker ou d'un serveur de synchronisation, il faut renseigner un nom (libre), choisir le type de serveur dans la liste déroulante et renseigner l'URL de l'outil tiers. L'URL doit être la plus courte possible car elle sert de base à toutes les requêtes API utilisées par Squash TM pour communiquer avec l'outil tiers. Tous ces champs sont obligatoires.

![Popup création BT ou serveur de synchro](./resources/popup-creation-bt-serveur.jpg){class="centre"}

En cliquant sur le numéro de ligne (#) ou le nom, la page de consultation du serveur s'affiche pour permettre sa modification. La barre des ancres permet de naviguer entre les différents blocs. 

![Page de consultation BT ou serveur de synchro](./resources/page-consult-bt-serveur.jpg){class="pleinepage"}

!!! danger "Attention"
    Lorsqu'un serveur de bugtracking est supprimé, il est automatiquement supprimé du projet auquel il était associé. La valeur "Pas de bugtracker" s'affiche sur la page de consultation du projet. 
    <br/>Toutes les associations avec les anomalies présentes dans les blocs 'Anomalies connues' sont également supprimées.

## Paramètrer un bugtracker ou un serveur de synchronisation

Pour qu'un bugtracker accepte d'échanger des informations avec Squash TM, ce dernier doit s'authentifier. La gestion de l'authentification permet de définir de quelle façon les utilisateurs, ou Squash TM lui-même, vont s'authentifier sur le serveur. Elle se décompose en deux blocs :

- Le bloc "Protocole d’authentification" qui définit le protocole de sécurité utilisé pour authentifier la connexion
- Le bloc "Politique d’authentification" qui détermine de quelle façon les utilisateurs et/ou Squash TM sont authentifiés.

### Protocole d'authentification

Le bloc "Protocole d'authentification" permet de définir le protocole d'authentification. Les protocoles supportés par Squash TM sont *Basic Authentication* et *OAuth 1a* (uniquement pour les connecteurs Jira). Les protocoles proposés à la configuration dépendent du type de bugtracker. 

!!! warning "Focus"
    Lorsque l'on change le protocole d'authentification, l’effet est appliqué immédiatement. La configuration précédente est rendue obsolète et supprimée du serveur. Cependant les informations restent présentes dans la page tant que la page n'est pas ferméeß ou rafraîchie : si le protocole est modifié par erreur, il est possible de rétablir le protocole précédent et de sauvegarder à nouveau la configuration.

#### Basic Authentification

Basic Authentification est le protocole par défaut. L'authentification s'appuie sur la saisie du login et du mot de passe de l'utilisateur. Il est supporté par tous les serveurs. Il ne requiert pas de configuration supplémentaire. L'usage de Basic Authentication n’est considéré comme sécurisé qu’en conjonction avec SSL/TLS (https).

#### OAuth 1a

Ce protocole n’est pour l’instant exploité que par les connecteurs Jira. Il nécessite de configurer les URLs impliquées dans l’échange de jetons et les modalités de signature des requêtes. 
Les champs à renseigner dans le bloc "Politique d'authentification" sont un jeton et le secret du jeton.

!!! tip "En savoir plus"
    Pour en savoir plus sur la configuration du protocole OAuth 1a, consulter la partie [Configurer le protocole d'authentification OAuth 1a](./configurer-oauth.md).


### Politique d'authentification

Le bloc "Politique d'authentification" permet de configurer le choix d'authentification des utilisateurs pour le bugtracker ou le serveur de synchronisation. 

![Politique d'authentification d'un serveur/bt](./resources/bugtracker-authentification.jpg){class="centre"}

- *Les utilisateurs s'authentifient eux-mêmes* : chaque utilisateur doit s'identifier personnellement à l'outil tiers. La procédure varie suivant le protocole choisi. Dans le cas d'une Basic Authentication, il doit saisir son login et mot de passe de connexion à l'outil tiers dans Squash TM, alors que pour OAuth 1a, l'authentification est déléguée au serveur tiers dans une fenêtre distincte.

- *Utiliser les identifiants de Squash TM* : les utilisateurs n'ont plus à s'authentifier et le serveur utilise automatiquement les identifiants d'accès à l'outil tiers renseignés dans le champ "Identifiants de Squash TM". Quelque soit l'utilisateur connecté dans Squash TM, les données sont remontées vers l'outil tiers avec les identifiants de l'utilisateur renseignés dans le bloc. 

!!! info "Info"
    L'option "Les utilisateurs s'authentifient eux-mêmes" est conseillée pour la configuration des bugtrackers. Le nom de l'utilisateur est ainsi renseigné comme rapporteur de l'anomalie, à la place d'un utilisateur commun. Cela permet d'avoir une traçabilité des échanges entre l'outil tiers et Squash TM.
    <br/>L'option "Utiliser les identifiants de Squash TM" est à utiliser lors de la configuration d'un serveur de synchronisation, notamment pour Xsquash4jira et le plugin Workflow Automatisation Jira.

Le champ "Identifiants de Squash TM" est à renseigner uniquement si l'option "Utiliser les identifiants de Squash TM" est cochée.

Pour que les champs soient enregistrés en succès, il faut que :

- l'outil tiers soit joignable
- les identifiants de connexion à l'outil tiers soient corrects
- le compte associé à ces identifiants dispose des permissions nécessaires sur l'outil tiers
- la configuration du serveur soit correcte

!!! warning "Focus"
    Jira Cloud nécessite un API Token à la place du mot de passe habituel pour se connecter. Dans le champ "Login", il faut renseigner l'adresse mail de l'utilisateur, et dans le champ "Mot de passe", renseigner l'API token généré pour le compte sur Jira Cloud. La procédure à suivre pour générer un token est détaillée sur cette page : [Compte technique Jira](../configuration-xsquash/configurer-xsquash4jira.md#compte-technique-jira).

!!! info "Info"
    Squash TM persiste en base de données certaines informations concernant l'authentification. Ceci inclut la configuration de protocole, les identifiants applications de Squash TM, et les jetons OAuth des utilisateurs le cas échéant. Les logins/mots de passe personnels des utilisateurs (Basic Authentication) sont sauvegardés encryptés dans la base de données.
