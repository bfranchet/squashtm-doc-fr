# Définir un lien avec serveur d'exécution Squash TF

Il est possible depuis Squash TM de lancer l'exécution de tests automatisés sur le serveur d'automatisation Jenkins via Squash TF.

L’objectif du lien Squash TM-Squash TF est de fournir un moyen simple de gérer les tests automatisés de Squash TF depuis Squash TM. Pour ce faire, Squash TM envoie une requête au serveur Squash TF afin de lancer le test, puis Squash TF renvoie les résultats à Squash TM.

## Déclarer un serveur d'exécution automatisée 

Il est nécessaire de déclarer un serveur d'exécution automatisée de type 'Jenkins' en suivant la procédure d'[ajout d'un serveur d'exécution automatisée](../gestion-serveurs/gerer-serveur-autom.md) pour permettre à Squash TM de se connecter à Jenkins.

Une fois le serveur enregistré, il faut l'associer au projet Squash TM depuis le bloc Automatisation de la page de configuration du projet. 

![Configuration du serveur d'exécution automatisée sur le projet](resources/ajout_serveurJenkins_projet_FR.png){class="pleinepage"}

Une fois le serveur d'exécution automatisée associé au projet, il faut sélectionner les jobs Jenkins qui seront utilisés pour l'exécution des tests automatisés. 

![Ajouter un job](resources/ajout_jobs_FR.png){class="pleinepage"}


## Ajouter un serveur de code source (Facultatif)

Squash TM permet de créer des cas de test scriptés (Gherkin et BDD). Ces cas de test peuvent ensuite être transmis pour automatisation aux formats .feature ou .robot dans un dépôt de code source hébergé sur Github, Gitlab ou Bitbucket.

La page de gestion des serveurs de partage de code source accessible depuis l'espace Administration permet d'[ajouter un serveur de partage de code source](../gestion-automatisation-tests/serveur-partage-code-source.md) et de déclarer les dépôts concernés. 

Une fois le dépôt ajouté, il peut être [associé au projet](../gestion-projets/configurer-projet.md#associer-un-serveur-de-partage-de-code-source) contenant les cas de test scriptés à pousser dans le dépôt.

!!! tip "En savoir plus"
    Pour en savoir plus sur la transmission de cas de test scriptés, consulter la page suivante [Transmettre un cas de test scripté sur un SCM](../../user-guide/gestion-tests-automatises/transmettre-script-scm.md).


## Définir l'URL publique de Squash TM 

L'URL publique de Squash TM doit impérativement être renseignée dans les Paramètres Système de l'espace Administration pour que les résultats d'exécution des tests automatisés exécutés avec Squash TF remontent dans Squash TM.

Cette URL prévaut sur la valeur de la propriété *tm.test.automation.server.callbackurl* présente dans le fichier *squash.tm.cfg.properties*.

![URL publique de Squash TM](resources/url-publique.png){class="pleinepage"}

## Créer un utilisateur pour le serveur d'exécution automatisée

Pour exécuter les tests automatisés avec Squash TF, il est impératif de créer un utilisateur appartenant au groupe "Serveur d'automatisation de tests" dans Squash TM.

![Ajout d'un serveur d'exécution automatisée](resources/ajout_tfserver_FR.png){class="pleinepage"}

## Configurer Squash TF

### Configurer les paramètres de sécurité.

Afin que Squash TM puisse envoyer l'ordre à Squash TF d’exécuter un job, un utilisateur avec les droits d'exécution doit avoir été créé sur le serveur Squash TF.

Configurer la sécurité du serveur Squash TF équivaut à configurer la sécurité du serveur Jenkins embarqué. Pour configurer la sécurité standard de Jenkins, il est préférable de choisir la meilleure option en fonction de l'environnement en se référent à la [documentation Jenkins](https://www.jenkins.io/doc/book/security/). 

### Configurer le rétro-appel TM

Après que le serveur Squash TF ait exécuté une suite de tests à la demande du serveur Squash TM, Squash TF renvoie des informations sur l’exécution au serveur Squash TM. Cette fonctionnalité fonctionne de la manière suivante :

- Quand Squash TM envoie une requête d’exécution au serveur Squash TF, il fournit à Squash TF son URL de rétro-appel.
- Squash TF utilise cette URL de rétro-appel pour récupérer le login/mot de passe à utiliser, et envoie à Squash TM l’évènement d’exécution.

Du côté du serveur Squash TF, l’association de l’URL de rétro-appel et du couple login/mot de passe est fait dans un fichier properties. Le fichier est modifiable depuis le menu *Configuration files* de l'espace *Administrer Jenkins*. 
Dans le fichier *taLinkConf.properties*, il faut définir pour chaque serveur Squash TM utilisant ce serveur Squash TF, une URL de rétro-appel et le login/mot de passe associé de l'utilisateur serveur d'exécution automatisée créé sur Squash TM.

*Exemple:*
<br/>*Pour un serveur Squash TM ayant comme URL de rétro-appel http://myServeur:8080/squash et comme login/mot de passe de l'utilisateur serveur d'exécution automatisée, tfserver/tfserver, il est nécessaire de définir les propriétés suivantes dans le fichier taLinkConf.properties* :

    endpoint.1=http://myServer:8080/squash 
    endpoint.1.login=tfserver
    endpoint.1.password=tfserver

### Configurer l'URL de Jenkins

Depuis le framework Squash TF 1.8.0, lorsque le framework envoie à Squash TM les informations d’exécution, il fournit l’URL du rapport HTML. Pour fabriquer l’URL du rapport HTML, la variable JENKINS_URL est utilisée. Il est nécessaire de paramétrer cette variable dans Jenkins à partir du menu *Configurer le système* de l'espace *Administrer Jenkins*, champ "URL de Jenkins".

!!! warning "Focus"
    Au premier lancement de Jenkins, même si la propriété "URL de Jenkins" contient la bonne valeur, il est impératif de cliquer sur **Enregistrer** ou **Appliquer**, sinon la variable JENKINS_URL ne sera pas correctement paramétrée.










