# Définir un lien avec un serveur d'exécution Squash AUTOM

Il est possible depuis Squash TM de lancer l'exécution de tests automatisés sur l'Orchestrateur Squash. Cet outil composé d’un ensemble de micro-services exploitables via l’envoi d’un plan d’exécution sous un formalisme bien précis, le PEAC (Plan d’Exécution «as code») permet d’orchestrer des exécutions de tests automatisés scriptés dans différentes technologies d'automatisation.

Pour s'interfacer avec l'Orchestrateur Squash, il est nécessaire d'avoir installé le plugin Squash AUTOM sur l'instance Squash TM ([Installation des plugins Squash TM](https://tm-fr.doc.squashtest.com/install-guide/installation-plugins/installer-plugins/)). 

!!! info "Info"
    Le plugin Squash AUTOM existe en deux versions : community téléchargeable depuis le site [Squashtest.com](https://www.squashtest.com/community-download) et premium disponible via la licence Squash AUTOM Premium.

Pour exécuter les tests automatisés depuis Squash TM avec l'Orchestrateur Squash, il est impératif de définir le lien entre Squash TM et le serveur d'exécution en suivant les étapes détaillées ci-dessous.

## Installer et configurer l'Orchestrateur Squash

Pour l'installation et la configuration de l'Orchestrateur Squash, consulter le [guide d'installation de l'Orchestrateur Squash](https://squashtest.gitlab.io/doc/squash-autom-devops-doc-fr/1.1.0/autom/install.html).

## Déclarer un serveur d'exécution automatisée 

Il est nécessaire de déclarer un serveur d'exécution automatisée de type 'SquashAUTOM' en suivant la procédure d'[ajout d'un serveur d'exécution automatisée](../gestion-serveurs/gerer-serveur-autom.md) pour permettre à Squash TM de se connecter à l'Orchestrateur Squash.

Une fois le serveur enregistré, il faut l'associer au projet Squash TM depuis le bloc Automatisation de la page de configuration du projet. 

![Configuration du serveur d'exécution automatisée sur le projet](resources/ajout_serveur_execu-FR.png){class="pleinepage"}

## Ajouter un serveur de code source

Pour que les scripts automatisés, exécutables par l'Orchestrateur Squash, soient accessibles depuis Squash TM, le dépôt de code source dans lequel ils sont contenus doit être déclaré sur l'instance.

La page de gestion des serveurs de partage de code source accessible depuis l'espace Administration permet d'[ajouter un serveur de partage de code source](../gestion-automatisation-tests/serveur-partage-code-source.md) et de déclarer les dépôts concernés. 

Une fois le dépôt ajouté, il peut être sélectionné dans le champ 'URL du dépôt de code source' de n'importe quel cas de test ou depuis les tables de l'espace Automatisation afin d'y associer un script automatisé.

## Définir l'URL publique de Squash TM 

L'URL publique de Squash TM doit impérativement être renseignée dans les Paramètres Système de l'espace Administration pour que les résultats d'exécution des tests automatisés exécutés dans l'Orchestrateur Squash remontent dans Squash TM.

Cette Url prévaut sur la valeur de la propriété *tm.test.automation.server.callbackurl* présente dans le fichier *squash.tm.cfg.properties*.

![URL publique de Squash TM](resources/url-publique.png){class="pleinepage"}


!!! warning "Focus"
    Dans le cadre de **Squash DEVOPS**, il est impératif de créer un utilisateur apartenant au groupe "Serveur d'automatisation de tests" dans Squash TM pour pouvoir récupérer un plan d'exécution Squash TM via un PEAC envoyé depuis un pipeline. Plus de détails sur le fonctionnement de Squash Devops avec Squash TM sont disponibles dans [le guide de récupération d'un plan d'exécution Squash TM depuis un PEAC](https://squashtest.gitlab.io/doc/squash-autom-devops-doc-fr/1.1.0/devops/callWithPeac.html). 
    <br/>La création de l'utilisateur technique n'est pas obligatoire pour l'exécution des tests automatisés avec Squash AUTOM.  