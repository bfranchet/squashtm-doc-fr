# Définir un serveur de partage de code source 

Dans Squash TM, il est possible de déclarer des serveurs de partage de code source Github, Gitlab et Bitbucket. Ces serveurs jouent un rôle essentiel dans l'automatisation des tests dans Squash TM :

- Ils permettent de transmettre les cas de test scriptés par les testeurs fonctionnels dans Squash TM vers des dépôts hébergés sur Github, Gitlab ou Bitbucket pour automatisation par un automaticien.
- Ils permettent également d'associer aux cas de test Squash TM des scripts automatisés hébergés sur des dépôts Github, Gitlab ou Bitbucket grâce aux champs Squash AUTOM. 

L'ajout des serveurs de partage de code source se fait depuis l'espace 'Serveurs>Serveurs de partage de code source' dans l'Administration de Squash TM. 

!!! tip "En savoir plus"
    Pour en savoir plus sur la déclaration d'un serveur de patage de code source et de ses dépôts, consulter la page suivante : [Serveurs de partage de code source](../gestion-serveurs/gerer-serveur-scm.md)

Il y a deux modes de déclaration pour les dépôts sur les serveurs de partage de code source dans Squash TM :

- les dépôts clonés en local
- les dépôts non clonés

Le choix se fait via la case à cocher 'Cloner le dépôt' dans la popup d'ajout d'un dépôt. 

Il est nécessaire de cloner localement les dépôts qui accueilleront les cas de test BDD et Gherkin transmis par les testeurs fonctionnels pour automatisation. 
<br/>Une fois déclaré, un dépôt peut être associé à un projet depuis le bloc 'Automatisation' de sa page de configuration. Par la suite, tous les cas de test BDD et Gherkin contenus dans ce projet transmis par les utilisateurs Squash TM seront récupérés au format .feature ou .robot dans le dépôt associé.

!!! tip "En savoir plus"
    Pour associer un serveur de partage de code source à un projet consulter la page suivante : [Associer un serveur de partage de code source](../gestion-projets/configurer-projet.md#associer-un-serveur-de-partage-de-code-source).
    <br/>Pour en savoir plus sur le focntionnement de la transmission des cas de test scriptés, consulter cette page : [Transmettre un cas de test scripté sur un SCM](../../user-guide/gestion-tests-automatises/transmettre-script-scm.md)

Les dépôts contenant les scripts automatisés qui seront exécutés par l'orchestrateur Squash doivent être déclarés dans Squash TM sans être clonés. Il est possible de les cloner si ces dépôts ont également vocation à accueillir les cas de test scriptés dans Squash TM mais sinon cela n'est pas nécessaire.
<br/>Une fois déclaré, le dépôt peut être sélectionné dans le champ 'URL du dépôt de code source' de n'importe quel cas de test Squash TM. Ce champ fait partie des 3 champs Squash AUTOM qui permettent d'associer un cas de test à un script automatisé exécutable avec l'orchestrateur Squash AUTOM.

!!! tip "En savoir plus"
    Pour plus d'informations sur l'association d'un script automatisé avec Squash AUTOM à un cas de test, consulter la page suivante : [Associer un script automatisé](../../user-guide/gestion-tests-automatises/associer-script-automatise.md).