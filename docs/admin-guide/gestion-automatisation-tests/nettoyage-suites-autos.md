# Nettoyage des suites automatisées 

Une instance Squash TM depuis laquelle sont exécutés des tests automatisés quotidiennenment voit sa volumétrie d'exécutions augmenter de manière exponentielle. Une fonctionnalité de nettoyage des exécutions automatisées a été mise en place pour contrôler cette volumétrie et supprimer les exécutions automatisées trop anciennes.

Cette fonctionnalité se compose de deux parties : 

- Au niveau de la configuration projet, les chefs de projet indiquent la durée de conservation des exécutions automatisées de leurs projets.
- Au niveau des paramètres système, les administrateurs peuvent déclencher la suppression des exécutions automatisées présentes hors du périmètre de conservation enregistré par projet.

La durée de conservation des suites de tests automatisées se fixe en jours depuis le bloc 'Automatisation' de la page de configuration du projet. Si aucune durée n'est définie, la durée de vie des suites est considérée comme infinie. Par défaut, aucune valeur n’est renseignée.

Un administrateur peut déclencher la suppression des suites automatisées depuis l'ancre **Nettoyage** ![Nettoyage](./resources/broom.svg){class="icone"} du sous-menu 'Système'

Toutes les suites automatisées et exécutions automatisées dont la date d’exécution est antérieure à la durée de conservation renseignée au niveau de la configuration de chaque projet sont supprimées. 
<br/>Toutes les suites et exécutions automatisées entrant dans la durée de conservation renseignée dans leurs projets respectifs sont quant à elles conservées.

Cette suppression concerne à la fois les suites automatisées lancées depuis Squash TM et les suites automatisées déclenchées depuis un pipeline CI/CD.

Pour effectuer la suppression, il suffit de cliquer sur le bouton **[Supprimer]**. Le nombre de suites et d'exécutions automatisées comptabilisées pour la suppression est indiqué dans la popup d'information.

![Nettoyage des suites autos](./resources/nettoyage-suites-autos.png){class="pleinepage"}

!!! danger "Attention"
    Cette opération peut supprimer un très grand nombre de suites et d'exécutions automatisées. 
    <br/>L'application peut temporairement être fortement ralentie. Il est préférable d'effectuer cette opération lorsque l'application n'est pas utilisée.