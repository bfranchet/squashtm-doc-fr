# Paramétrer l’automatisation du projet

Sur Squash TM, l'automatisation se configure à l'échelle de chaque projet par un adminsitrateur ou un chef de projet.

La majeure partie de la configuration est gérée depuis le bloc 'Automatisation' de la page de configuration du projet.

## Configurer les paramètres des cas de test BDD du projet

Il est possible pour chaque projet de définir la technologie et la langue des scripts exportés ou transmis à partir des cas de test BDD rédigés au sein du projet.

!!! tip "En savoir plus"
	Pour en savoir plus sur ces deux paramètres, consulter la page suivante : [Technologie d'implémentation et langue des scripts](../gestion-projets/configurer-projet.md#technologie-dimplementation-et-langue-des-scripts).

## Associer un serveur d’exécution automatisée au projet

En fonction de l'outil utilisé pour l'exécution des cas de test automatisé (l'orchestrateur Squash ou Jenkins), la configuration du serveur d'exécution automatisée sur le projet est légérement différente.
<br/>Dans le cas de l'orchestrateur Squash (SquashAUTOM), il suffit d'associer le serveur créé au projet.
<br/>Avec Jenkins, il faut lier le serveur d'exécution créé au projet mais aussi les jobs contenant les scripts à exécuter depuis Squash TM.

!!! tip "En savoir plus"
	Pour d'information sur la configuration du serveur d'exécution automatisée sur les projets, consulter la page suivante : [Associer un serveur d'exécution automatisée](../gestion-projets/configurer-projet.md#associer-un-serveur-dexecution-automatisee).

## Activer le workflow d’automatisation

Afin de suivre l'automatisation des cas de test Squash TM, l'outil propose deux workflows d'automatisation. Un workflow natif porté par l'espace Automatisation de Squash TM et un workflow distant géré dans Jira qui permet de mettre en place un flux de travail personnalisé et un suivi de l'avancement grâce aux tickets Jira.

Le workflow d'automatisation [s'active](../gestion-projets/configurer-projet.md#workflow-dautomatisation) depuis le bloc Automatisation de la page de configuration d'un projet.

!!! tip "En savoir plus"
	Pour en savoir plus sur le fonctionnement des deux workflows d'automatisation, consulter les pages suivantes :
	
	- [Suivre le processus d’automatisation dans Squash](../../user-guide/gestion-tests-automatises/processus-automatisation-squash.md).
	- [Suivre le processus d’automatisation dans Jira](../../user-guide/gestion-tests-automatises/processus-automatisation-jira.md).

## Associer un serveur de partage de code source et un dépôt

Squash TM permet de transmettre des cas de test scriptés (BDD et Gherkin) rédigés dans Squash TM dans un dépôt de partage de code source Github, Gitlab ou Bitbucket. 

Pour ce faire, il faut associer un serveur de partage de code source et un dépôt au projet Squash TM.

!!! tip "En savoir plus"
	Pour configurer un dépôt de code source sur un projet, consulter la page suivante : [Associer un serveur de partage de code source](../gestion-projets/configurer-projet.md#associer-un-serveur-de-partage-de-code-source).

!!! info "Info"
	Pour un projet configuré avec un serveur Jenkins, si le dépôt du serveur de partage de code source configuré sur le projet est identique à celui configuré dans le job associé au projet Squash TM et que l'option "Peut exécuter BDD" est cochée, l’association de scripts automatisés aux cas de test scriptés (Gherkin ou BDD) se fait de manière automatique. 
	<br/>Attention, dans ce cas, il est recommandé de n’avoir qu’un seul job pouvant exécuter les scripts BDD dans le tableau Jobs.

	Pour un projet configuré avec un serveur Squash Autom, si un dépôt de serveur de partage de code source est configuré sur le projet, les champs Squash Autom "Technologie du test automatisé", "URL du dépôt de code source", "Référence du test automatisé" sont complétés automatiquement lors de la transmission des cas de test.

	
## Activer le plugin Result Publisher (Squash Autom)

Le plugin Result publisher permet de récupérer les résultats et le détail des exécutions ainsi que les rapports d'exécution des tests automatisés exécutés avec l'orchestrateur Squash.

Pour le configurer, il suffit de l'activer sur le projet depuis l'onglet dédié aux plugins. Lorsque le plugin est installé dans sa version Premium, il est possible de récupérer plus d'informations sur l'exécution. Pour ce faire, il faut activer la remontée d'information complète.

!!! tip "En savoir plus"
	Pour configurer le plugin Result publisher sur un projet, se référer à la page [Configurer le plugin Result publisher](../gestion-projets/configurer-plugins.md#configurer-le-plugin-result-publisher).
