# Les paramètres système

Les paramètres système de Squash TM se gèrent depuis l'ancre **Paramètres système** ![Paramètres](./resources/settings.svg){class="icone"} du sous-menu "Système" de l'Administration Squash TM.

![Page Paramètres système](./resources/page-system.png){class="pleinepage"}

## Pièces jointes

Le bloc "Pièces jointes" permet de gérer les paramètres d'import pour les pièces jointes et les fichiers d'import.

Dans le champ "Extensions autorisées", il suffit de renseigner les extensions de pièces jointes à autoriser séparées par des virgules pour permettre leur téléchargement depuis les espaces dédiés dans l'application.  

La "Taille limite des pièces jointes en Bytes" permet de renseigner la taille maximum autorisée pour les pièces jointes. Au-delà de cette taille, les pièces jointes ne sont pas téléchargeables dans Squash TM. 

La "Taille limite des imports en Bytes" permet de renseigner la taille maximum autorisée pour les fichiers d'import d'exigences et de cas de test. Au-delà de cette taille, les fichiers d'import ne sont pas pris en compte par Squash TM. 

!!! warning "Focus"
    La modification des champs "Taille limite" peut nécessiter une modification des paramètres en base de données pour être cohérent avec les valeurs renseignées. 

## URL publique de Squash

Le bloc "URL publique de Squash" permet de déclarer l’url publique de Squash TM. Celle-ci doit s'arrêter à */squash*. Pour exemple, l'url publique de l'instance de démo Squash TM est : [https://demo.squashtest.org/squash](https://demo.squashtest.org/squash)

Cette valeur est utilisée par l'outil pour fixer les urls présentes dans les fichiers *.json* de réponse de l'API Squash, dans les descriptions d'anomalies déclarées depuis Squash TM et comme callbackurl pour les échanges bidirectionnels Squash TM/Jenkins, Squash TM/OFT ou encore Squash TM/Jira.

La valeur renseignée dans ce bloc prévaut sur la valeur de la propriété *tm.test.automation.server.callbackurl* présente dans le fichier *squash.tm.cfg.properties*.

## Gestion des erreurs

Il est possible d'activer ou désactiver l’affichage du détail des erreurs dans Squash TM.

Si l'option est activée, le détail de l'erreur (stack trace) s’affiche pour l'utilisateur dans une popup lorsqu'il rencontre une erreur à l'utilisation de l'outil.

Si l'option est désactivée, en cas d’erreur, la pop-up indique à l'utilisateur de prendre contact avec un administrateur pour en savoir plus sur le détail de l'erreur.

!!! tip "En savoir plus"
    Le bloc "Gestion des erreurs" est caché si la propriété *squashtm.stack.trace.control.panel.visible* du fichier de configuration de Squash TM est à *false*. Pour en savoir plus sur les propriétés de Squash TM, consulter la page suivante : [Gestion du fichier de configuration Squash TM](../../install-guide/parametrage-squash/config-generale.md#gestion-du-fichier-de-configuration-squash-tm)

## Authentification automatique au bugtracker à la connexion

Cette option permet d’activer ou de désactiver l’authentification automatique de l’utilisateur au bugtracker lorsqu’il se connecte à Squash TM.

Si cette option est activée, lorsque l'utilisateur s'identifie, Squash TM tente de se connecter au bugtracker avec les identifiants utilisés pour se connecter à Squash TM, sans tenir compte des informations renseignées dans "Mon compte". Cette fonctionnalité est utile dans un environnement où l'authentification à Squash TM et au bugtracker associé est délégué au même annuaire LDAP ou AD.

Si cette option est désactivée, ce sont les identifiants renseignés dans "Mon compte" qui sont pris en compte pour se connecter au bugtracker.

## Sensibilité à la casse des logins

Il est possible d’activer ou désactiver l'insensibilité à la casse pour tous les logins des utilisateurs de Squash TM :

Quand l'insensibilité à la casse est activée, il n'est plus possible pour les administrateurs de créer des doublons de logins en faisant varier majuscules et minuscules. Les utilisateurs peuvent quant à eux renseigner leur login sans respecter la casse pour se connecter à l’application. Cette option est utile lorsque l'authentification à Squash TM est déléguée à un outil tiers tel que LDAP ou AD.

Si l'insensibilité à la casse est désactivée, les utilisateurs doivent saisir leur login en respectant la casse pour se connecter à l’application et les adminsitrateurs peuvent créer des doublons de login en variant majuscules et minuscules.

## Utilisation des jalons

Cette option permet d'activer ou de désactiver les fonctionnalités relatives aux jalons dans Squash TM. 

!!! danger "Attention"
    La désactivation des jalons signifie que tous les jalons crées sont supprimés et les fonctionnalités liées désactivées. Tous les éléments associés aux jalons ne le sont plus.
    <br/>À la réactivation, les jalons et leurs associations ne sont pas conservés.