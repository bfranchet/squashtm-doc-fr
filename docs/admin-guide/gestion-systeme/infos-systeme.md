# Les informations système

Depuis le sous-menu 'Système' de l'espace Administration, ancre Informations ![Informations](./resources/information.svg){class="icone"}, il est possible de visualiser les statistiques de l'application ainsi que la liste des plugins installés.

Le bloc "Statistiques" présente la version applicative en cours d'utilisation, le nombre de projets et d’utilisateurs créés dans l’application, la somme de tous les éléments créés par type d’éléments (exigences, cas de test, campagnes, itérations, exécutions) ainsi que la taille de la base de données.

Le bloc "Plugins installés" liste la totalité des plugins déposés dans le répertoire 'plugins' de Squash TM au niveau du serveur. Il permet de contrôler la bonne installation d'un plugin ainsi que sa compatibilité grâce à son numéro. 

!!! tip " En savoir plus"
    Pour en savoir plus sur le système de numérotation des versions de Squash TM et de ses plugins consulter la page suivante : [Numérotation Squash](../../faq/faq-numerotation-squash.md)


![Page Informations](./resources/page-informations.png){class="pleinepage"}