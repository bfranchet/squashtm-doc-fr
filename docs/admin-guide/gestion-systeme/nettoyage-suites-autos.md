# Le nettoyage des suites automatisées  

Depuis le sous-menu 'Système' de l'espace Administration, il est possible de déclencher la suppression des suites automatisées via l'ancre **Nettoyage** ![Nettoyage](./resources/broom.svg){class="icone"}.

Cette fonctionnalité permet aux administrateurs de lancer la suppression de toutes les suites automatisées et exécutions automatisées dont la date d’exécution est antérieure à la durée de conservation renseignée au niveau de la configuration de chaque projet. 
<br/>Toutes les suites et exécutions automatisées entrants dans la durée de conservation renseignée dans leurs projets respectifs sont quant à elles conservées.

Cette suppression concerne à la fois les suites automatisées lancées depuis Squash TM et les suites automatisées déclenchées depuis un pipeline CI/CD.

!!! tip "En savoir plus"
    Pour en savoir plus sur la configuration de la durée de conservation des suites automatisées consulter cette page : [Durée de conservation des suites automatisées](../gestion-projets/configurer-projet.md#duree-de-conservation-des-suites-automatisees).


Pour effectuer la suppression, il suffit de cliquer sur le bouton **[Supprimer]**. Le nombre de suites et d'exécutions automatisées comptabilisées pour la suppression est indiqué dans la popup d'information.

![Nettoyage des suites autos](./resources/nettoyage-suites-autos.png){class="pleinepage"}

!!! danger "Attention"
    Cette opération peut supprimer un très grand nombre de suites et d'exécutions automatisées. <br/>L'application peut temporairement être fortement ralentie. Il est préférable d'effectuer cette opération lorsque l'application n'est pas utilisée.