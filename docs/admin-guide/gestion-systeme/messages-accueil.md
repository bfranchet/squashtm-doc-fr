# Les messages d’accueil 

L'ancre **Messages** ![Messages](./resources/edit_message.svg){class="icone"} du sous-menu "Système" de l'Administration permet d'enregistrer les messages d'accueil qui s'affichent sur la page de login et la page d'accueil de Squash TM pour tous les utilisateurs.

L'éditeur de texte riche des deux messages permet d'insérer le texte et les éléments nécessaires dans la limite des fonctionnalités à disposition dans la barre d’outils. 

![Page Messages](./resources/page-messages.png){class="pleinepage"}