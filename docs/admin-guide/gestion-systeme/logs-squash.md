# Les logs de l’application 

Il est possible de télécharger les fichiers de logs de Squash TM directement depuis le sous-menu 'Système' de l'espace Administration, ancre **Téléchargements** ![Téléchargements](./resources/download.svg){class="icone"}.

Le fichier de logs du jour s'affiche sous le nom **squash-tm.log**. Le fichiers de logs des jours précédents s'affichent quant à eux avec un nom respectant la forme suivante : **squash-tm.log.AAAAMMJJ**. Il suffit de cliquer sur le nom du fichier souhaité pour le télécharger.

![Logs](./resources/logs.png){class="pleinepage"}

!!! info "Info"
    Pour ouvrir les fichiers de logs finissant par une date, faire clic droit puis ouvrir avec Notepad ou Bloc-notes.