# Activer et configurer les plugins pour un projet

L'activation et la configuration des plugins pour un projet s'effectue depuis l'ancre "Plugins" ![Plugin](./resources/plugin.png){class="icone"} sur la page de consultation du projet.

![Tableau des plugins](./resources/ancre-plugin.jpg){class="pleinepage"} 

Pour configurer un plugin, cliquer sur le bouton ![Configurer](./resources/settings.png){class="icone"} présent dans la dernière colonne du tableau.

## Activation et désactivation des plugins

Avant de pouvoir être configuré, le plugin doit être activé.
Pour activer le plugin, il suffit de cliquer sur le bouton présent dans la colonne "Activé". 

Pour désactiver un plugin, cliquer une nouvelle fois sur le bouton "Activé". Une popup apparaîtra vous demandant de conserver ou non la configuration sur le plugin, en vue d'une future activation. 

Si la configuration n'est pas sauvegardée, l'ensemble des données renseignées sur le plugin seront définitivement supprimées (Synchronisation, Equivalences entre les champs etc...)

La pastille de statut est :

- grise si le plugin est désactivé
- jaune si le plugin est activé mais sans configuration
- verte si le plugin est activé avec une configuration. Les plugins ne nécessitant pas de configuration particulière s'affiche avec une pastille verte également.


!!! info "Info" 
    Le plugin "**Assistant campagne**" ne nécessite pas de configuration. Dès son activation, le bouton ![Assistant](./resources/wizard.png){class="icone"} s'affiche dans l'espace Campagnes en haut de la bibliothèque, et l'Assistant campagne peut être utilisé.
    </br>Pour en savoir plus sur l'Assistant campagne, consulter la page [Identifier les tests à rejouer avec l'assistant de campagne](./../../../user-guide/gestion-executions/creer-plan-execution/#identifier-les-tests-a-rejouer-avec-lassistant-de-campagne)


##  Configurer le plugin Result publisher

!!! info "Info"
    Ce plugin est disponible en version Community ou Premium (avec une licence Autom ou Devops).
    </br>Le plugin doit être préalablement installé pour pouvoir le paramétrer.

Le plugin Result publisher permet de récupérer les résultats et le détail des exécutions ainsi que les rapports d'exécution des tests automatisés exécutés avec l'orchestrateur Squash.

![Configuration plugin result publisher](./resources/plugin-result-publisher.jpg){class="pleinepage"}


- Remontée d'information légère : à utiliser si vous lancez très souvent vos tests automatisés et qu'une augmentation importante de la taille de votre base de données peut être un désagrément. Lors de l'exécution automatisée d'un plan de test, une suite automatisée est créée mais aucune exécution n'est ajoutée aux items du plan de test.
- Remontée d'information complète : à utiliser si vous ne lancez pas très souvent vos tests automatisés ou si une augmentation importante de la taille de votre base de données n'est pas un désagrément. Lors de l'exécution automatisée d'un plan de test , une suite automatisée est créée ainsi qu'une exécution pour chaque item de plan de test exécuté. 

La remontée d'information légère est par défaut sélectionnée. 


##  Configurer le plugin Xsquash4jira

!!! info "Info"
    Ce plugin est disponible en version Community.
    </br>Le plugin doit être préalablement installé pour pouvoir le paramétrer.

Le plugin Xsquash4jira permet de synchroniser des objets agiles Jira sous forme d'exigences dans Squash TM.

![Configuration plugin Xsquash4jira](./resources/plugin-Xsquash4jira2.jpg){class="pleinepage"}

La page de configuration se compose de 4 blocs :

- Le bloc "**Synchronisations**" qui permet de configurer le périmètre de synchronisation.
- Le bloc "**Champs de reporting Squash vers JIRA**" qui permet de configurer le reporting de l’avancement de la recette directement dans JIRA
- Le bloc "**Equivalences entre les champs**" qui permet de configurer les équivalences de champs Squash et Jira
- Le bloc "**Equivalences entre les valeurs des champs**" qui permet de configurer les équivalences de valeurs pour les champs configurés dans le bloc "Equivalences" qui le nécessitent. Il s’agit notamment des champs de type liste de valeur "Criticité", "Catégorie" et "Statut".
- Le bloc "**Equivalences des liens entre exigences**" qui permet de configurer les équivalences des liens entre tickets Jira en liens entre exigences dans Squash.

!!! tip "En savoir plus"
    Pour en savoir plus sur la configuration du plugin Xsquash4jira, consulter la page détaillée [Configurer Xsquash4jira dans Squash TM](../../configuration-xsquash/configurer-xsquash4jira/)

##  Configurer le plugin Redmine Req

!!! info "Info"
    Ce plugin fait partie de la licence Premium Squash TM.
    </br>Le plugin doit être préalablement installé pour pouvoir le paramétrer. Consulter la partie "[Redmine Bugtracker et Redmine Exigences](./../../../install-guide/installation-plugins/configurer-plugins-bt-outils-tiers/#redmine-bugtracker-et-redmine-exigences)"

Le plugin RedmineReq permet de synchroniser manuellement des demandes Redmine sous forme d'exigences dans Squash TM.

![Configuration plugin RedmineReq](./resources/plugin-redminereq.jpg){class="pleinepage"}

La page de configuration du plugin Redmine Exigences se divisent en plusieurs blocs :

- Un bloc "**Information**" permettant de sélectionner un serveur de type "redmine3.rest"
- Un bloc "**Projets**" permettant d’ajouter et de supprimer des projets Redmine à synchroniser en renseignant leur identifiant ainsi que l’identifiant d’un filtre associé et éventuellement un chemin de synchronisation
- Un bloc "**Options de synchronisation**" : Si cette option est activée, les exigences synchronisées et présentes dans le projet Squash qui ne sont pas retrouvées lors d’une nouvelle synchronisation seront supprimées.</br>Si cette option est désactivée, les exigences ne seront pas supprimées mais passeront au statut "obsolète".
- Un bloc "**Equivalences entre les champs**" permettant d’ajouter et de supprimer des correspondances entre les champs Redmine et les champs Squash. Les équivalences pour les champs principaux sont déjà effectuées, et ne sont pas éditables : Référence, Libellé, Criticité, Description, Catégorie et Statut.</br>Pour ajouter de nouvelles équivalences, il faut au préalable créer des champs personnalisés dans Squash et les associer aux exigences du projet.
- Un bloc "**Equivalences entre les valeurs des champs**" qui comporte un champ zone de texte ainsi qu’un bloc d’aide à la configuration permettant pour les champs "Criticité", "Catégorie" et "Statut" d’effectuer les correspondances entres les valeurs de la liste du champ Redmine et les valeurs de la liste du champ Squash
- Un bloc "**Equivalences des liens entre exigences**" permettant via une liste déroulante d’effectuer les correspondances entre les liens entre exigences dans Redmine et dans Squash.

**Ajouter une synchronisation**

Dans le bloc "Information" sélectionner le bugtracker de type redmine3.rest préalablement créé dans l'espace Administration puis Serveurs.

En cliquant sur le bouton ![Ajouter](./resources/icone-ajouter.png){class="icone"} du bloc "Projets", une popup s'ouvre pour paramétrer un projet Redmine à synchroniser. Renseigner les champs : 

- Identifiant du projet : L’identifiant projet se trouve dans l’URL qui permet d’accéder au projet.
- Idenfifiant filtre : L’identifiant filtre se trouve dans l’URL qui permet d’accéder au filtre. Il est de type numérique.
- Chemin cible : Nom du dossier dans l'espace Exigences qui contiendra les exigences synchronisées depuis Redmine.

![Explications identifiant du projet et du filtre](./resources/explication-identifiant.jpg){class="centre"}

![Popup synchronisation](./resources/popup-synchro-redmine.jpg){class="centre"}

L'identifiant du projet et du filtre sont obligatoires pour valider la saisie. Si aucun chemin cible n'est renseigné, les exigences seront créées à la racine du projet.

Il est possible d'ajouter plusieurs couples projet/filtre.

!!! warning "Focus"
    Les champs Redmine de type ‘Liste clé/valeur’ ne sont pas pris en compte lors de la synchronisation.

!!! tip "En savoir plus"
    Pour en savoir plus sur la synchronisation du référentiel d'exigences avec RedmineReq, consulter la page [Synchroniser avec RedmineReq](../../../user-guide/gestion-exigences/synchroniser-exigences/#synchroniser-avec-redminereq)


##  Configurer le plugin Jira Req

!!! info "Info"
    Ce plugin fait partie de la licence Premium Squash TM.
    </br>Le plugin doit être préalablement installé pour pouvoir le paramétrer.

Le plugin JiraReq permet de synchroniser manuellement des tickets Jira sous forme d'exigences dans Squash TM. 

!!! warning "Focus"
    Ce plugin a été remplacé par le plugin Xsquash4jira. Nous vous conseillons d'installer à la place de JiraReq le plugin Xsquash4jira qui présente plus de fonctionnalités.

![Configuration plugin JiraReq](./resources/plugin-jirareq.jpg){class="pleinepage"}

La page de configuration du plugin Jira Exigences se divise en plusieurs blocs :

- Un bloc "**Information**" permettant de sélectionner un serveur de type "jira.rest" ou "jira.cloud".
- Un bloc "**Projets**" permettant d’ajouter et de supprimer des projets Jira à synchroniser en renseignant la clé du projet et éventuellement le nom d'un filtre ainsi qu'un chemin de synchronisation
- Un bloc "**Equivalences entre les champs**" permettant d’ajouter et de supprimer des correspondances entre les champs Jira et les champs Squash. Les équivalences pour les champs principaux sont déjà effectuées, et ne sont pas éditables : Référence, Libellé, Criticité, Description.</br>Pour ajouter de nouvelles équivalences, il faut au préalable créer des champs personnalisés dans Squash et les associer aux exigences du projet. </br>La valeur à saisir pour le "Champ JIRA" correspond à l’id du champ dans JIRA.
- Un bloc "**Equivalences entre les valeurs des champs**" qui comporte un champ zone de texte ainsi qu’un bloc d’aide à la configuration permettant pour les champs "Criticité", "Catégorie" et "Statut" d’effectuer les correspondances entres les valeurs de la liste du champ Jira et les valeurs de la liste du champ Squash
- Un bloc "**Equivalences des liens entre exigences**" permettant via une liste déroulante d’effectuer les correspondances entre les liens entre exigences dans Jira et dans Squash.

!!! tip "En savoir plus"
    Pour en savoir plus sur la synchronisation du référentiel d'exigences avec Jira Req, consulter la page [Synchroniser avec JiraReq](../../../user-guide/gestion-exigences/synchroniser-exigences/#synchroniser-avec-jirareq)

##  Configurer le plugin Workflow Automatisation Jira

!!! info "Info"
    Ce plugin fait partie de la licence Autom.
    </br>Le plugin doit être préalablement installé pour pouvoir le paramétrer. Consulter la partie "[Xsquash4jira et Workflow Automatisation Jira](./../../../install-guide/installation-plugins/configurer-plugins-bt-outils-tiers/#xsquash4jira-et-workflow-automatisation-jira)"

Lorsque le plugin est activé, le champ "Workflow d’automatisation" présent dans le bloc Automatisation de la page de consultation projet, est automatiquement prérempli par la valeur "Serveur distant".

### Prérequis nécessaires

- Le plugin Jira Bugtracker (Serveur ou Cloud) doit être également installé pour que le plugin Workflow Automatisation Jira (WAJ) puisse être utilisé. 
- Un bugtracker Jira doit être créé dans Squash (dans l'espace Administration - Serveurs puis "Bugtrackers et Serveurs de synchronisation").</br>Si un bugtracker Jira déjà présent sur l’instance de Squash, il est vivement recommandé de [créer un nouveau bugtracker](../gestion-serveurs/gerer-bugtracker-serveur-synchro.md) pointant vers le même serveur Jira. Ce nouveau bugtracker sera dédié uniquement au plugin Workflow Automatisation Jira et ne devrait pas être utilisé en tant que bugtracker classique dans les projets Squash.
- Pour fonctionner, le plugin doit pouvoir se connecter à un compte utilisateur Jira ayant des droits en lecture et en écriture sur l’ensemble des projets concernés par les synchronisations du plugin. La bonne pratique conseillée est de créer un compte technique dédié au plugin dans Jira et disposant des droits nécessaires uniquement sur les projets Jira souhaités.

### Fonctionnement du plugin

Le plugin Workflow Automatisation Jira permet à l’automaticien d’être informé des cas de test à automatiser dans Jira. Il peut alors indiquer l’évolution de son travail via un workflow personnalisé et peut ainsi déclarer quand l’automatisation est terminée sans avoir à modifier les données du cas de test dans Squash.
Le passage du ticket à l’état final d’automatisation rend possible l’exécution automatique d’un cas de test dans un plan de test (à la condition qu’un serveur d’exécution automatisée ait été configuré pour le projet).

Une partie des fonctionnalités du plugin sont automatiques et se déclenchent seules une fois la configuration complète effectuée.

| Nom | Déclenchement | Sens de circulation |
|---| ---| ---|
|Création ticket Jira| Manuel depuis le bouton **[Transmettre]**|De Squash vers Jira|
|Mise à jour d'un cas de test| Automatique une fois la configuration faite |De Jira  vers Squash|
|Mise à jour du ticket| Automatique une fois la configuration faite|De Squash vers Jira|
|Exécution automatisable | Automatique lors du passage du ticket à son état final |De Jira vers Squash puis de Squash vers Squash|

!!! info "Info" 
    Lors des opérations automatiques, le plugin n’effacera jamais les cas de test dans Squash ou les tickets synchronisés dans Jira. Si des cas de test venaient à disparaitre de Squash, les tickets correspondants resteront dans Jira et devront éventuellement être supprimés manuellement par les utilisateurs de Jira si nécessaire.

### Créer une configuration

![Configuration plugin WAJ](./resources/plugin-waj.jpg){class="pleinepage"}

Les éléments de configurations sont les suivants :

- Serveur : choisir dans la liste déroulante le bugtracker Jira préalablement créé
- Projet : projet Jira qui accueillera les tickets créés
- Type de ticket : type du ticket créé dans Jira et pour lequel un workflow a été paramétré.
- L’état final du ticket : état du workflow pour lequel l’automatisation est terminée et pour lequel un cas de test devient exécutable automatiquement dans un plan de test Squash. (Il est préconisé d’utiliser un état final du workflow Jira).

Si des champs obligatoires ont été paramétrés sur le type de ticket sélectionné, ceux-ci apparaitront sur la page de configuration et devront être renseignés.

!!! tip "En savoir plus"
    Pour en savoir plus sur la transmission de cas de test à Jira, consulter la page [Suivre le processus d'automatisation dans Jira](../../../user-guide/gestion-tests-automatises/processus-automatisation-jira/)
