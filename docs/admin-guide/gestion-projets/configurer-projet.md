# Configurer un projet

La configuration d'un projet est accessible depuis l'espace Administration, au clic sur le sous-menu "Projets". Cliquer sur le projet désiré pour afficher la page de consultation du projet.

!!! info "Info"
    Seul un administrateur ou un chef de projet peut configurer un projet.

L'ancre "Informations" ![Informations](./resources/information.png){class="icone"} permet de renseigner et de consulter les données générales du projet.

![Informations Projet](./resources/info-projet.png){class="pleinepage"}

## Paramétrer un bugtracker
    
La configuration du lien avec le bugtracker s’effectue depuis l'ancre **Bugtracker** ![Bugtracker](./resources/bug.png){class="icone"}.

Pour configurer un bugtracker sur un projet, le serveur correspondant doit avoir été au préablable déclaré par un administrateur dans le sous-menu "Serveurs". Le bugtracker peut ensuite être sélectionné via une liste déroulante pour être associé au projet.

Dans le champ "Nom du projet", il faut renseigner l’intitulé du projet configuré dans l'outil tiers. Il est indispensable de respecter scrupuleusement l’orthographe et la casse du nom du projet. Il est possible d'associer plusieurs projets dans ce champ. Au moment de la remontée d'anomalies, il est possible de choisir le projet dans lequel l'anomalie doit être déclarée.

![Paramétrer un bugtracker sur Squash](./resources/ancre-bugtracker.jpg){class="pleinepage"}

!!! warning "Focus"
    Au moment de l'exécution, si le nom du projet renseigné n'existe pas, ou s'il est mal écrit, un message d'erreur apparaît lors de la déclaration d'une nouvelle anomalie.

!!! tip "En savoir plus"
	Pour en savoir plus sur la création d'un serveur de type bugtracking, consulter la page [Gérer les bugtrackers et serveurs de synchronisation](../gestion-serveurs/gerer-bugtracker-serveur-synchro.md)


## Gérer les habilitations du projet

La gestion des habilitations sur le projet s’effectue depuis l'ancre **Habilitations** ![Habilitations projet](./resources/user.png){class="icone"}.
Il est possible d'habiliter des utilisateurs ou des équipes sur le projet. 

![Gestion des habilitations projet](./resources/ancre-habilitations-projet.jpg){class="pleinepage"}

Le bouton ![Supprimer l'associer](./resources/iconeSupprimer.png){class="icone"} permet de supprimer des habilitations en masse ou unitairement sur le projet. 

Le profil de l'équipe ou de l'utilisateur peut être modifié directement dans le tableau au clic sur le profil.

![Modification des habilitations projet](./resources/modification-habilitations.jpg){class="pleinepage"}

Un utilisateur ou une équipe peut avoir des profils différents sur des projets différents.

!!! tip "En savoir plus"
	Pour en savoir plus sur les différents profils et les habilitations dans les espaces Squash TM, consulter la page [Attribuer des habilitations](../gestion-utilisateurs/attribuer-habilitations.md)

## Définir les préférences d’exécution

L'activation de certaines fonctionnalités concernant l'exécution des cas de test s’effectue depuis l'ancre **Exécutions** ![Préférences d'exécution](./resources/play.png){class="icone"} : 

- Autoriser la modification des cas de test pendant leur exécution
- Activation/Désactivation du statut d'exécution 'Non testable'
- Activation/Désactivation du statut d'exécution 'Arbitré'

Seul le statut 'Non testable' est activé par défaut.

![Préférences d'exécution](./resources/preferences-executions.jpg){class="pleinepage"}

## Configurer l’automatisation du projet

L'ancre **Automatisation** ![Automatisation projet](./resources/automation.png){class="icone"} permet de configurer l'automatisation du projet.

Il est possible depuis ce bloc de :

- Sélectionner la technologie d'implémentation des cas de test BDD
- Sélectionner la langue des scripts BDD
- Activer un workflow d'automatisation (Squash ou Jira)
- Paramétrer une durée de conservation des suites automatisées
- Associer un serveur de partage de code source ainsi qu'un dépôt (si un workflow d'automatisation est activé)
- Associer un serveur d'exécution automatisée ainsi que des jobs

![Configurer l'automatisation sur un projet](./resources/bloc-automatisation.jpg){class="pleinepage"}

### Technologie d'implémentation et langue des scripts

Il est possible pour chaque projet de configurer la technologie d’implémentation des cas de test BDD ainsi que la langue des scripts BDD. Le chef de projet a le choix entre Cucumber et Robot Framework pour la technologie d’implémentation. 

Si la technologie Cucumber est sélectionnée, il est possible de choisir parmi les 4 langues suivantes pour le script : anglais, français, espagnol et allemand. 
</br>Pour la technologie Robot Framework, l’anglais est la seule langue disponible.

### Workflow d'automatisation

L’activation du workflow d’automatisation pour un projet se fait via une liste déroulante. 

Par défaut l’option "Aucun" est sélectionnée. 
</br>L’option "Squash" déclenche l’activation du [workflow natif Squash](../../user-guide/gestion-tests-automatises/processus-automatisation-squash.md) tandis que "Serveur distant" déclenche l’activation d’un workflow distant si le plugin [Worflow Automatisation Jira](../../user-guide/gestion-tests-automatises/processus-automatisation-jira.md) est activé sur le projet.

L'activation d'un workflow d'automatisation entraîne l’apparition de nouveaux champs dans le bloc "Automatisation" d'un cas de test. 
Dans le cas du workflow natif, les tests à automatiser sont suivis dans l’espace Automatisation à la fois par les testeurs et par les automaticiens.
Si c'est le workflow Jira qui est activé, le suivi des tests à automatiser se fait directement depuis le bloc "Automatisation" des cas de test.

L’activation d’un workflow d’automatisation entraîne également l’apparition d’une partie 'Serveur de gestion de code source' dans le bloc "Automatisation" d’un projet.

### Durée de conservation des suites automatisées

La durée de conservation des suites de tests automatisées du projet se fixe en jours. Lors d'un nettoyage des suites automatisées par un administrateur (Espace Administration, sous-menu Système), toutes les suites automatisées incluses dans le périmètre de cette durée de vie sont conservés tandis que les autres sont supprimées. 

Si aucune durée n'est définie, la durée de vie des suites est considérée comme infinie. Par défaut, aucune valeur n’est renseignée.

### Associer un serveur de partage de code source

Lorsque le workflow d’automatisation est activé, une partie "Serveur de gestion de code source" apparaît dans le bloc "Automatisation".
Il faut au préalable avoir créé un serveur de partage de code source dans l'espace Administration>Serveurs pour pouvoir le sélectionner et l'associer à un projet.
Une fois le serveur sélectionné, les noms de ses dépôts ainsi que leur branche s'affichent dans une liste déroulante.

Il est possible de reprendre l’arborescence de Squash TM dans le dépôt lors de la transmission des cas de test à l'outil de partage de code source afin qu’ils soient organisés de la même manière des deux côtés. L'activation de cette fonctionnalité se fait au niveau du champ "Utiliser l’arborescence de TM dans le dépôt".

!!! tip "En savoir plus"
	Pour en savoir plus sur la création d'un serveur de partage de code source, consulter la page [Gérer les serveurs de partage de code source](../gestion-serveurs/gerer-serveur-scm.md)

### Associer un serveur d'exécution automatisée

Il faut au préalable avoir créé un serveur d'exécution automatisée dans l'espace Administration>Serveurs pour pouvoir le sélectionner et l'associer à un projet.
<br/>Une fois le serveur sélectionné, un ou des jobs peuvent être associés au projet dans le cas d'un serveur Jenkins. Dans le cas d'un serveur SquashAUTOM, il y a rien de plus à faire que d'associer le serveur au projet.

!!! info "Info"
    Pour un projet configuré avec un serveur Jenkins, si le dépôt du serveur de partage de code source configuré sur le projet est identique à celui configuré dans le job associé au projet Squash TM et que l'option "Peut exécuter BDD" est cochée, l’association de scripts automatisés aux cas de test scriptés (Gherkin ou BDD) se fait de manière automatique. 
	<br/>Attention, dans ce cas, il est recommandé de n’avoir qu’un seul job pouvant exécuter les scripts BDD dans le tableau Jobs.
	<br/>Pour un projet configuré avec un serveur Squash Autom, si un dépôt de serveur de partage de code source est configuré sur le projet, les champs Squash Autom "Technologie du test automatisé", "URL du dépôt de code source", "Référence du test automatisé" sont complétés automatiquement lors de la transmission des cas de test.

Il est possible de modifier un job en cliquant sur le bouton ![Crayon](./resources/edit.png){class="icone"}. Dans le champ "Serveur(s) d’exécution possibles (séparées par des points-virgules)", il est possible de choisir les serveurs secondaires qui pourront être utilisés pour exécuter les cas de test automatisés. Il faut remplir le champ avec le nom des serveurs séparés par des points virgules (ex: SecondaryServer1; SecondaryServer2).

!!! tip "En savoir plus"
	Pour en savoir plus sur la création d'un serveur d'exécution automatisée, consulter la page [Gérer les serveurs d'exécution automatisée](../gestion-serveurs/gerer-serveur-autom.md)

## Personnaliser le projet

### Les champs personnalisés

La gestion des champs personnalisés se fait depuis l'ancre **Champs personnalisés** ![Champs personnalisés](./resources/custom_field.png){class="icone"}.

![Tableau d'association champs personnalisés](./resources/champs-personnalises.jpg){class="pleinepage"}

Les champs personnalisés doivent être créés dans l'espace Administration>Entités avant de pouvoir être associés à un projet.

Le tableau reprend le nom du champ personnalisé, son type, s'il est facultatif ou obligatoire ainsi que l'entité à laquelle il est associé. 

Pour associer un champ personnalisé au projet, cliquer sur le bouton ![Ajouter](./resources/icone-ajouter.png){class="icone"}. Dans la popup, il faut sélectionner, l'entité à laquelle l'associer et le champ personnalisé à associer. Depuis la popup, il est possible d'associer plusieurs champs personnalisés à la même entité.

 ![Associer champ perso](./resources/associer-cuf-2.jpg){class="centre"}

Un même champ personnalisé peut être associé à plusieurs entités. 
Les champs personnalisés peuvent être associés : 

 - Dans l'espace Exigences, aux : 
    - Dossiers d'exigences
    - Exigences
- Dans l'espace Cas de test, aux :
    - Dossiers de cas de test
    - Cas de test
    - Pas de test
- Dans l'espace Campagnes, aux :
    - Dossiers de campagnes
    - Campagnes
    - Itérations
    - Suites de test
    - Exécutions
    - Pas d'exécution

Pour dissocier un ou plusieurs champs personnalisés d'un projet, cliquer sur le bouton ![Dissocier](./resources/iconeSupprimer.png){class="icone"}.

!!! tip "En savoir plus"
	Pour en savoir plus sur la création d'un champ personnalisé, consulter la page [Gérer les champs personnalisés](../personnalisation-entites/gerer-champs-persos.md).

### Les listes personnalisées

La gestion des listes personnalisées se fait depuis l'ancre **Listes personnalisées** ![Listes personnalisées](./resources/list.png){class="icone"}.

![Bloc listes personnalisées](./resources/liste-perso.jpg){class="pleinepage"}

La ou les listes personnalisées doivent être créées dans l'espace Administration>Entités avant de pouvoir être associées à un projet.

Une liste personnalisée peut être associée au champ "Catégorie" des exigences, et aux champs "Nature" et "Type" des cas de test d'un projet.
Une fois la liste associée au projet, dans l'espace désiré, le champ choisi prend les valeurs de la liste personnalisée. Si des icônes ont été attribuées pour les valeurs de la liste, celles-ci s'affichent dans le bloc "Informations" de l'objet. 

![liste personnalisée Exigences](./resources/liste-perso-exigence.jpg){class="pleinepage"}

Lorsqu'une liste personnalisée est associée à un projet, les valeurs renseignées auparavant ne sont pas sauvegardées. La valeur par défaut de la liste sélectionnée s'affichera dans le champ pour tous les objets concernés.

!!! tip "En savoir plus"
	Pour en savoir plus sur la création de listes personnalisées, consulter la page [Gérer les listes personnalisées](../personnalisation-entites/gerer-listes-persos.md)

## Associer des jalons au projet

L'association de jalons au projet s'effectue depuis l'ancre **Jalons** ![Jalons](./resources/milestone.png){class="icone"}.

![Tableau d'association de jalons](./resources/jalons.jpg){class="pleinepage"}

!!! info "Info"
    Pour pouvoir associer un jalon à un projet, celui-ci doit être au statut "Planifié", "En cours" ou "Terminé". 

### Associer des jalons avec le bouton [+]

Il est possible d'associer un ou plusieurs jalons déjà créés via le bouton ![Ajouter](./resources/icone-ajouter.png){class="icone"}. Depuis la popup d'association, une recherche est possible. Les jalons sont organisés par portée.

![Popup association de jalons](./resources/popup-association-jalon.jpg){class="centre"}

### Associer des jalons avec le bouton [Créer]

Si le jalon n'a pas été préalablement créé, il est possible de le créer directement depuis le bloc grâce au bouton **[Créer]** présent au-dessus du tableau. Depuis la popup de création, le jalon créé peut être directement associer au projet en cliquant sur le bouton **[Créer et associer]**. Le jalon créé a automatiquement pour statut 'En cours'.

![Création et association d'un jalon](./resources/creer-associer-jalon.jpg){class="centre"}

### Dissocier des jalons

Pour dissocier un ou plusieurs jalons d'un projet, cliquer sur le bouton ![Dissocier](./resources/iconeSupprimer.png){class="icone"} présent en bout de ligne ou en haut du tableau.

Après la dissociation du jalon, celui-ci ne est plus associé ni au projet ni aux objets du projet. La colonne "Utilisé" du tableau indique si le jalon est associé à des objets du projet.

!!! tip "En savoir plus"
	Pour en savoir plus sur la création de jalons, consulter la page [Gérer un jalon](../gestion-jalons/gerer-jalon.md).
    <br/>Pour plus d'informations sur l'association d'un jalon à un objet, consulter la page [Associer un jalon à un objet](../../user-guide/gestion-jalons/associer-jalon-objet.md).