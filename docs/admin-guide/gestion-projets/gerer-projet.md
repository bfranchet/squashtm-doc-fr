# Gérer un projet

<!--Ajouter, modifier, suppriemr un projet
(ajout projet, présenter un projet squash, ancres, recherche, suppression, infos intéressantes )-->

La gestion des projets est accessible depuis l'espace Administration. Au clic sur le sous-menu "Projets", le tableau de gestion des projets s'affiche.

## Ajouter et modifier un projet

Depuis le tableau de gestion des projets, il est possible d'ajouter ![icone ajouter](./resources/icone-ajouter.png){class="icone"} un projet. 

![Tableau de gestion des projets](./resources/tableau-projet.jpg){class="pleinepage"}

Lors de la création d'un projet, il faut renseigner au minimum le champ Nom mais il est également possible d'y ajouter une description, une étiquette et un [modèle à associer au projet](../gerer-modele-projet/#associer-un-projet-a-un-modele).

![Tableau de gestion des projets](./resources/popup-ajout-projet.jpg){class="centre"}

Pour créer plusieurs projets à la suite, il suffit de cliquer sur le bouton **[Ajouter un autre]**.

Une fois créés, les projets s'affichent dans la bibliothèque des différents espaces.

Depuis le tableau de gestion des projets, en cliquant sur le numéro de ligne (#) ou le nom du projet, la page de consultation s'affiche, permettant sa modification. La barre des ancres permet de naviguer entre les différents blocs et pages.

![Tableau de gestion des projets](./resources/page-consultation-projet.jpg){class="pleinepage"}

Depuis la page de consultation, il est possible de [configurer un projet](./configurer-projet.md). Les actions suivantes peuvent être effectuées :

- Modifier les informations du projet
- Ajouter des pièces jointes au projet
- Associer le projet à un modèle ou transformer le projet en modèle
- Associer le projet à un bugtracker
- Gérer les habilitations des utilisateurs et/ou des équipes au projet
- Autoriser des statuts d'exécution optionnels ainsi que la modification de cas de test pendant l'exécution
- Activer un workflow d'automatisation et gérer les informations liés à l'automatisation et aux cas de test BDD.
- Associer des champs et listes personnalisées
- Associer des jalons
- Activer et configurer des plugins

## Supprimer un projet

Il n'est pas possible de supprimer des projets en masse. La suppression doit se faire unitairement depuis la page de consultation du projet au clic sur le bouton **[...]** puis "Supprimer le projet".


![Supprimer un projet](./resources/supprimer-projet.jpg){class="pleinepage"}

!!! info "Info"
    Un projet ne peut pas être supprimé s'il contient des données : Exigences, Cas de test, Campagnes, Exécutions...