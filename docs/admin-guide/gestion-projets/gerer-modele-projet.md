# Gérer un modèle de projet

La gestion des modèles de projet est accessible dans l'espace Administration, depuis le tableau de gestion des projets. 

Un modèle de projet permet de créer des projets avec des configurations identiques : Habilitations, Bugtrackers, Champs et listes personnalisés, Activation des plugins etc. En effet, un projet créé à partir d'un modèle reprend la configuration de ce dernier en fonction des options choisies par l'adminsitrateur.

Il est recommandé d'utiliser les modèles de projet pour gérer de façon mutualisée la configuration de plusieurs projets. Lorsqu'un modèle de projet est **associé** à un projet, ce dernier délègue une partie de sa configuration au modèle de projet.

Les modèles de projets se distinguent dans le tableau des projets à l'aide de l'icône ![Modèle de projet](./resources/template.png){class="icone"} présente dans la colonne "Mod."

![Tableau gestion modèle de projet](./resources/gestion-modele-projet.jpg){class="pleinepage"}

## Ajouter, modifier, supprimer un modèle 

Pour ajouter un modèle de projet, cliquer sur le bouton ![Ajouter](./resources/icone-ajouter.png){class="icone"}, depuis la page de gestion des projets, puis sur le sous-menu "Ajouter un modèle".

Lors de la création d'un modèle de projet, seul le nom est obligatoire.

En cliquant sur le numéro de ligne (#) ou sur le nom, la page de consultation du modèle de projet s'affiche pour permettre sa modification. La page de consultation d'un modèle de projet est identique à celle d'un projet Squash TM. La configuration d'un modèle se fait donc de la manière que pour un projet. 

!!! tip "En savoir plus"
    Se référer à la page suivante pour configurer un modèle de projet : [Configurer un projet](./configurer-projet.md).

![Page de consultation modèle de projet](./resources/page-consultation-modele-projet.jpg){class="pleinepage"}

Pour supprimer un modèle de projet, cliquer sur le bouton **[...]** en haut de la page puis sur le sous-menu "Supprimer le modèle". À la suppression d'un modèle associé à un projet, la configuration du projet n'est pas modifiée et les données du projet ne sont pas supprimées.

## Créer un projet à partir d'un modèle

Il est possible de créer un projet à partir d'un modèle pour reprendre des élements de sa configuration. Pour ce faire, il suffit de sélectionner un modèle dans la popup de création du projet puis de sélectionner les paramètres à reprendre. 

![Popup créer un projet à partir d'un modèle](./resources/creer-projet-modele.png){class="centre"}

Lors de la création d’un projet, lorsqu’un modèle est sélectionné dans le champ "Modèle", il est possible de conserver le lien entre le modèle et le projet qui va être créé en cochant la case "Conserver le lien avec le modèle".

Dans le cas où l'administrateur souhaite créer un projet à partir d'un modèle sans le lier à celui-ci, il lui est possible de choisir dans la liste des paramètres ceux à reprendre dans le projet indépendamment les uns des autres.

Mais si l'administrateur souhaite conserver le lien avec le modèle ou lier la configuration de Xquash4jira, les éléments de configuration suivants sont obligatoirement repris à la création du projet : 

- Configuration de Xsquash4jira
- Champs personnalisés
- Listes personnalisées
- Modification en cours d'écxécution
- Statut d'exécution optionnels

La reprise des autres paramètres est aux choix de l'administrateur.

!!! tip "En savoir plus"
    Pour en savoir plus sur la reprise de la configuration Xsquash4jira d'un modèle dans un projet, consulter la page suivante : [Configuration de Xsquash4Jira dans les modèles de projet](../configuration-xsquash/configurer-xsquash4jira.md#configuration-de-xsquash4jira-dans-les-modeles-de-projet).


## Associer un projet à un modèle 

L'association d'un projet à un modèle peut se faire :

- à la création d'un projet, depuis la popup de création d'un projet, en sélectionnant un modèle.
- depuis la page de consultation d'un projet non associé, en cliquant sur le bouton [**...**] en haut de la page, puis sur le sous-menu "Associer à un modèle"

![Popup associer le projet à un modèle](./resources/associer-projet-modele.jpg){class="centre"}

L'association d'un projet à un modèle entraîne la copie de plusieurs paramètres du modèle :

-  **Bugtracker** :
    - Si aucun bugtracker n'est configuré dans le projet, le bugtracker du modèle est utilisé.
    - Si un bugtracker est déjà configuré, il n'est pas modifié.
-  **Options d'exécution** : la valeur du modèle est reportée dans le projet rattaché.
-  **Options de statut d'exécution** : les valeurs du modèle sont reportées dans le projet rattaché.
- **Automatisation** :
    - La technologie et la langue d'implémentation des cas de test BDD sont reprises dans le projet associé.
    - Si aucun serveur d'automatisation n'est configuré dans le projet, la configuration du serveur d'automatisation du modèle est utilisée.
    - Si un serveur d'automatisation est déjà configuré dans le projet, aucune modification n'est apportée.
    - Si aucun workflow d'automatisation n'est configuré dans le projet, la configuration du workflow d'automatisation du modèle est utilisée.
    - Si un workflow d'automatisation est déjà configuré dans le projet, aucune modification n'est apportée.
-  **Champs personnalisés** : les champs présents sur le modèle et inexistants dans le projet sont ajoutés et valorisés avec la valeur par défaut.
-  **Listes personnalisées** : les listes personnalisées sont copiées depuis le modèle et valorisées avec la valeur par défaut.
-  **Jalons** : Aucune information relative aux jalons n'est modifiée par l'association d'un projet à un modèle.
-  **Plugins** : les plugins activés dans le modèle sont repris dans le projet. Si d'autres plugins étaient initialement activés pour le projet, ils sont toujours activés après association au modèle.

## La propagation des modifications d'un modèle vers ses projets associés 

Les modèles permettent de gérer en masse les projets qui leurs sont associés grâce à la propagation.
Les modifications suivantes apportées à un modèle sont propagées systématiquement aux projets auxquels il est associé :

-  Options d'exécution : toute modification est propagée aux projets associés
-  Options de statut : toute modification est propagée aux projets associés
-  Champs personnalisés : tout ajout et suppression d'une association entre un champ personnalisé et un modèle est propagé aux projets associés
-  Listes personnalisées : toute modification est propagée aux projets enfants

Par conséquent, il est impossible de modifier ces éléments directement dans les projets associés à un modèle.

Lors de la dissociation d'un modèle à un projet, le projet devient indépendant. La configuration du projet est sauvegardée et elle peut être complétement modifiée.


!!! tip "En savoir plus"
    La configuration du plugin Xsquash4jira peut également être gérée au niveau des modèles par propagation. Pour en savoir plus, consulter la partie [Configurer Xsquash4jira dans les modèles de projet](../configuration-xsquash/configurer-xsquash4jira.md#configuration-de-xsquash4jira-dans-les-modeles-de-projet) 


## Créer un modèle à partir d'un projet

Pour créer un modèle à partir d'un projet, sélectionner le projet source et cliquer sur le bouton ![Ajouter](./resources/icone-ajouter.png){class="icone"} puis sur le sous-menu "Ajouter un modèle à partir d'un projet".

Depuis la popup, nommer le modèle et sélectionner les paramètres du projet qui seront repris dans le modèle de projet.

![Popup de création d'un modèle à partir d'un projet](./resources/creer-modele-projet.jpg){class="centre"}


## Transformer un projet en modèle

!!! info "Info"
    Pour transformer un projet en modèle, le projet ne doit pas contenir de données (exigences, cas de test, exécutions, etc...).

Depuis la page de consultation du projet à transformer, cliquer sur le bouton **[...]** en haut de la page, puis sur le sous-menu "Transformer en modèle".
Une fois la transformation réalisée, l'opération est irréversible. L'ancien projet s'affiche comme les autres modèles avec l'icône ![Modèle de projet](./resources/template.png){class="icone"} dans le tableau.