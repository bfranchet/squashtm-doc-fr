# Configurer Xsquash dans Jira

Xsquash et Xsquash Cloud sont deux outils permettant de renforcer l’intégration entre Squash TM et Jira Server, Datacenter et Cloud. Ils récupèrent les cas de tests et les exécutions associés aux exigences synchronisées avec Xsquash4jira dans Squash TM afin de les afficher directement dans les demandes Jira.

!!! warning "Focus"
    Il est impératif d'utiliser le plugin Xsquash4jira sur Squash TM pour pouvoir utiliser Xsquash ou Xquash Cloud côté Jira.
    <br/>Pour en savoir plus sur l'utilisation du plugin Xsquash4jira, consulter la page suivante : [Configurer Xsquash4Jira dans Squash TM](./configurer-xsquash4jira.md)


!!! danger "Attention"
    Les versions 2.0.0 du plugin Xsquash et de Xsquash Cloud ne sont compatibles qu'avec les versions 2.X+ de Squash TM.
    <br/>Pour les versions antérieures de Squash TM, il faut utiliser une version inférieure compatible de Xsquash et de Xsquash Cloud.

## Configurer Xsquash sur Jira Server et Datacenter

Xsquash est un composant additionnel pour Jira Server et Jira Datacenter disponible sur l’Atlassian Marketplace.

Le plugin Xsquash requiert :

- un Jira Server ou un Jira Datacenter en version 8.X+
- un Squash TM en version 2.X+ avec les plugins Squash TM API Rest en version 2.X+ et Xsquash4jira en version 2.X+

### Installation de Xsquash

Pour installer Xsquash sur Jira Server ou Jira Datacenter : 

1. Se connecter à l’instance Jira en tant qu’administrateur
2. Aller sur l’Administration Jira et choisir l'option "Gérer les apps". L'Atlassian Marketplace pour Jira s'affiche
3. Rechercher "Xsquash" sur la Marketplace
4. Cliquer sur le bouton **[Installer]** pour télécharger et installer le plugin.

!!! info "Info"
    Par défaut, c'est la dernière version de Xsquash compatible avec la version du Jira qui est installée. 

Pour installer une précédente version du plugin Xsquash sur Jira :

1. Se rendre sur la page [Historique des versions](https://marketplace.atlassian.com/apps/1219060/xsquash-agile-testing-with-oss-squash/version-history) de Xsquash sur la marketplace 
2. Télécharger la version souhaitée sur le poste utilisateur
3. Se connecter à l’instance Jira en tant qu’administrateur
4. Importer manuellement le plugin en utilisant la fonctionnalité "Importer l'app" de la page "Gérer les apps".

### Déclarer un serveur dans Xsquash

La configuration du plugin Xsquash se fait depuis l'Administration Jira, section "Gérer les apps". Dans le menu gauche de l’écran, rechercher "Xsquash" puis cliquer sur "Configuration". 
Xsquash permet de configurer plusieurs serveurs Xquash4Jira issus de différentes instances Squash TM :

![Serveurs Xsquash](./resources/serveurs-xsquash.png){class="pleinepage"}

Pour ajouter un nouveau serveur, cliquer sur le bouton **[Ajouter]** et remplir le formulaire avec les informations suivantes : 

- Le nom du serveur (nom libre)
- L’url de base de Squash TM (s’arrêter à /squash)
- Le nom du serveur de type jira.xsquash déclaré dans Squash TM qui correspond à l'instance Jira (bien respecter la casse)
- Le login et le mot de passe d'un compte Squash TM. Ce compte sera utilisé pour toutes les requêtes API envoyées à Squash TM par le plugin.

![Ajout Serveur](./resources/ajout-serveur1.png){class="centre"}

Il est recommandé d'utiliser un compte administrateur Squash TM pour cette configuration car celui-ci a accès à tous les projets de l'instance. Mais il est tout à fait possible d'utiliser un compte ne disposant que de droits de lecture (profil Invité) sur les projets Squash TM contenant les données de recette (cas de test et exécutions) à synchroniser côté Jira.

### Configurer Xsquash sur un projet

Le plugin Xsquash se configure indépendamment pour chaque projet Jira. 

Pour configurer Xsquash sur un projet, accéder aux "Paramètres du projet", rechercher "Xsquash" dans le menu vertical puis cliquer sur "Configuration". Activer Xsquash à l’aide du bouton radio puis sélectionner le serveur souhaité dans la liste. 

![Activer Serveur](./resources/conf-projet-xsquash.png){class="centre"}

Par défaut, les onglets de synchronisation des données de recette Squash TM sont nommés « Cas de test Squash TM » et « Exécutions Squash TM ». La popup de configuration permet de renommer ces onglets affichés sur les demandes Jira du projet. Une fois la configuration terminée, cliquer sur **[Sauvegarder]**.

!!! warning "Focus"
    Il est impératif de disposer des droits d'administration sur le projet Jira pour activer le plugin Xsquash.

## Configurer Xsquash Cloud sur Jira Cloud

Xsquash Cloud est une application hébergée sur un serveur interne à l’entreprise Henix. 

!!! danger "Attention"
    Xsquash Cloud n'est disponible que pour les clients disposant d'une licence Premium Squash TM.

L'application Xsquash Cloud requiert : 

- un Jira Cloud
- un Squash TM en version 2.X+ avec les plugins Squash TM API Rest en version 2.X+ et Xsquash4jira en version 2.X+

### Installation de Xsquash Cloud

Il existe deux façons d’installer l’application Xsquash Cloud sur Jira Cloud :

**Installer manuellement Xsquash Cloud**

Pour installer manuellement Xsquash Cloud :

1. Se connecter à l’instance Jira Cloud en tant qu’administrateur.
2. Cliquer sur le bouton **[Paramètres]** et choisir "Apps" puis l’option "Gérer les apps".
3. Cliquer sur "Paramètres" pour ouvrir la fenêtre "Settings".
4. Cocher l’option "Enable development mode" puis cliquer sur le bouton **[Apply]**.
5. Sur la page "Gérer les apps", cliquer sur "App importable" pour ouvrir la fenêtre "Upload app".
6. Renseigner l’url qui aura été fournie par l’Équipe Support Squash puis cliquer le bouton **[Upload]**.
7. L’application Xsquash Cloud apparait ensuite dans la liste des applications installées.

![Installer Xsquash Cloud](./resources/installer-xsquash-cloud.png){class="centre"}

**Installer Xsquash Cloud via l’API**

Avant d’installer l’application Xsquash Cloud dans Jira Cloud par API, il faut au préalable un compte administrateur disposant d’un jeton d’API. 

!!! tip "En savoir plus"
    Pour générer un jeton d’API, une documentation est disponible à cette [adresse](https://support.atlassian.com/atlassian-account/docs/manage-api-tokens-for-your-atlassian-account/).

Voici la procédure à suivre pour l’installation de l’application Xsquash Cloud via l’API :

1. Faire une requête GET en BASIC AUTH à l’URL suivante : 

        https://<UrldeJira>.atlassian.net/rest/plugins/1.0/?os_authType=basic
    avec dans le header, l’information suivante :

        Accept: application/vnd.atl.plugins.installed+json
    et pour l’authentification, utiliser le Username de l’administrateur et son jeton d’API.

2. Récupérer la valeur du header « upm-token » dans la réponse
3. Faire une requête POST en BASIC AUTH à l’URL suivante :

        https://<UrldeJira>.atlassian.net/rest/plugins/1.0/?token=upm-token
    avec dans le header, l’information suivante :
    
        Content-type: application/vnd.atl.plugins.uri+json
        Accept: application/json
    et dans le body la commande suivante avec l’url qui vous aura été fournie par l’Équipe Support Squash,
    
        {
        "pluginUri": "https://<UrlXsquashCloud>.com/atlassian-connect.json",
        "pluginName": "Xsquash Cloud"
        }
    et pour l’authentification, utiliser le Username de l’administrateur et son jeton d’API.

### Déclarer un serveur dans Xsquash Cloud

Pour configurer Xsquash Cloud, cliquer sur **[Paramètres] > Apps** puis dans le menu à gauche de l’écran, rechercher "XSQUASH" puis cliquer sur "Configuration". 
Xsquash permet de configurer plusieurs serveurs Xquash4Jira issus de différentes instances Squash TM :

![Liste Serveurs](./resources/liste-serveurs-cloud.png){class="pleinepage"}

!!! info "Info"
    Une coche en bout ligne du serveur indique que la communication entre le serveur Squash TM et Jira Cloud est opérationnelle.
    <br/>Il est possible de télécharger les logs du plugin Xsquash Cloud depuis l'option' "Logs Xsquash".

Cliquer sur le bouton **[Ajouter un serveur Squash TM]** pour ajouter un nouveau serveur Squash TM et remplir le formulaire avec les informations suivantes : 

- Le nom du serveur (nom libre)
- L’url de base de Squash TM (s’arrêter à /squash)
- Le nom du serveur de type jira.xsquash déclaré dans Squash TM qui correspond à l'instance Jira (bien respecter la casse)
- Le login et le mot de passe d'un compte Squash TM. Ce compte sera utilisé pour toutes les requêtes API envoyées à Squash TM par le plugin.

![Ajout Serveur](./resources/ajouter-serveur-cloud.png){class="centre"}

Il est recommandé d'utiliser un compte administrateur Squash TM pour cette configuration car celui-ci a accès à tous les projets de l'instance. Mais il est tout à fait possible d'utiliser un compte ne disposant que de droits de lecture (profil Invité) sur les projets Squash TM contenant les données de recette (cas de test et exécutions) à synchroniser côté Jira.

### Configurer Xsquash Cloud sur un projet

Le plugin Xsquash Cloud se configure indépendamment pour chaque projet. 

Pour configurer Xsquash Cloud sur un projet, accéder aux "Paramètres du projet", puis cliquer sur "Configuration Xsquash" dans le menu vertical. Activer Xsquash à l’aide du bouton radio puis sélectionner le serveur souhaité dans la liste. 

![Activer Serveur](./resources/activer-xsquash-cloud.png){class="centre"}

Par défaut, les onglets de synchronisation des données de recette Squash TM sont nommés « Cas de test Squash TM » et « Exécutions Squash TM ». La popup de configuration permet de renommer ces onglets affichés sur les demandes Jira du projet. Une fois la configuration terminée, cliquer sur **[Sauvegarder]**.

## Aperçu d’une demande Jira

Sur Jira Server et Jira Datacenter, les onglets ajoutés par le plugin Xsquash s'affichent dans le bloc "Activité" des demandes du projet configuré.
<br/>Sur Jira Cloud, un bloc "Xsquash" composé de deux onglets est ajouté dans l'écran des demandes Jira du projet configuré. 
<br/>Le premier onglet dédié aux cas de test et le second aux exécutions permettent de suivre, depuis Jira, l'avancée de la recette dans Squash TM.

### L’onglet "Cas de test Squash TM"

L’onglet "Cas de test Squash TM" affiche un tableau avec diverses informations relatives aux cas de test liés à la demande Jira quelque soit leur format (Classique, Gherkin, BDD). Ce tableau est trié par importance, puis référence, et enfin libellé du cas de test.

Dans le tableau, au clic sur le bouton présent en bout de ligne d'un cas de test, des informations additionnelles sur ce dernier s’affichent comme sa description ou encore ses pas de test.

Il est également possible de cliquer sur :

- le libellé du cas de test pour l’afficher dans Squash TM 
- la pastille indiquant le statut de la dernière exécution pour l’afficher dans Squash TM.

*Aperçu de l'onglet "Cas de test squash TM" sur une demande Jira Serveur :*

![Cas de test Squash TM](./resources/ct-squash-xsquash.png){class="pleinepage"}

### L’onglet "Exécution Squash TM"

L’onglet "Exécution Squash TM" affiche un tableau avec les informations relatives à la dernière exécution de chaque item de plan d'exécution. 
Ainsi, si le cas de test est ajouté à x plans d'exécution, alors l'onglet affichera la dernière exécution des x occurrences.

Dans le tableau, au clic sur le bouton présent en bout de ligne, des informations additionnelles sur la dernière exécution s’affichent comme les pas de test exécutés et les commentaires des pas d’exécution.

Il est également possible de cliquer sur :

- Le nom de la campagne pour afficher la campagne sur Squash TM
- Le nom de l’itération pour afficher l'itération sur Squash TM
- Le libellé du test pour afficher le cas de test dans Squash TM
- La dernière date d’exécution pour afficher la dernière exécution de l'ITPI sur Squash TM

*Aperçu de l'onglet "Exécution Squash TM" sur une demande Jira Cloud :*

![Exécution Squash TM](./resources/execution-squash-xsquash-cloud.png){class="pleinepage"}

### Aperçu rapide depuis un tableau 

Sur Jira server et Jira Datacenter, depuis les tableaux Scrum et les tableaux Kanban, il est possible d’avoir une vue rapide de chaque demande au clic sur son nom.

Le plugin Xsquash ajoute deux blocs :

- Cas de Test Squash TM : affiche le nombre de tests liés à la demande.
- Exécutions Squash TM : affiche le nombre d’exécutions (planifiées ou terminées) des cas de test liés à la demande.

![Visualisation rapide](./resources/visualisation-rapide.png){class="pleinepage"}
