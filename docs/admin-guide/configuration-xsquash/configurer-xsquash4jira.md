# Configurer Xsquash4Jira dans Squash TM

Le plugin Xsquash4Jira est un plugin Squash TM dédié à la synchronisation de tickets depuis Jira Server, Jira Datacenter et Jira Cloud. Il s'articule autour de trois fonctionnalités principales :

- Un module de synchronisation automatique qui permet à Squash TM d’importer et de maintenir automatiquement à jour des tickets Jira (Server, Datacenter et Cloud) sous forme d’exigences synchronisées dans l’espace Exigences.
- Un module de reporting qui permet d’afficher sur les tickets Jira des informations concernant le déroulement de la recette dans Squash TM. Ces informations prennent la forme de champs personnalisés à ajouter sur
les tickets Jira qui sont ensuite complétés par Squash TM.
- Un module de création de plan de test à partir des objets Jira accessible depuis l’espace Campagnes. Cet assistant permet de créer des plans de test à partir d'un filtre sur les versions livrées, les sprints ou des requêtes JQL de Jira. Tous les cas de test associés aux tickets compris dans le périmètre choisi sont proposés à l'utilisateur afin d'être ajoutés au plan d'exécution.

Une partie des fonctionnalités du plugin sont automatiques et se déclenchent seules une fois la configuration complète effectuée :

| Nom | Déclenchement | Sens de circulation |
|--|--|--|
| Synchronisation des tickets Jira | Automatique une fois la configuration faite | De Jira vers Squash TM |
| Reporting des données de recette Squash TM | Automatique une fois la configuration faite | De Squash TM vers Jira |
| Création d’un plan de test dans Squash TM à partir d’objets Jira | Manuel depuis l’espace Campagnes de Squash TM | De Jira vers Squash TM |

!!! danger "Attention"
    Lors des opérations automatiques le plugin n’effacera jamais d’exigences synchronisées dans Squash TM ou de tickets dans Jira. Si des tickets venaient à être supprimés ou déplacés dans Jira, les exigences correspondantes resteront dans Squash TM et devront éventuellement être supprimées manuellement par les utilisateurs de Squash TM si nécessaire.

!!! tip "En savoir plus"
    Cette page est dédiée à la configuration du plugin Xsquash4jira depuis l'Administration de Squash TM.
    <br/>Pour en savoir plus sur les exigences synchronisées et le concepteur de plan d'exécution Jira, consulter la page suivante : [Synchroniser des objets agiles Jira dans Squash](../../user-guide/xsquash/synchro-objets-agiles-jira.md)

## Installation et prérequis

!!! info "Info"
    Le plugin Xsquash4jira est embarqué par défaut dans la distribution de Squash TM Freeware. Il est donc déjà installé sur Squash TM.
    <br/>Consulter la page suivante pour en savoir plus : [Les plugins de Squash TM](../../install-guide/installation-plugins/plugins-squash.md)

### Communication Squash TM/Jira

Le plugin Xsquash4Jira est un plugin reposant sur une communication bidirectionnelle entre Squash TM et Jira. Il s’agit d’une communication de serveur à serveur, passant essentiellement par l’API Rest de Jira. Pour que le plugin puisse fonctionner il faut que la communication soit possible ce qui nécessite souvent des interventions sur l’infrastructure réseau : ouverture de flux dans les firewalls, ajout de certificats dans les JVM, etc.

Si le même serveur Jira est déjà utilisé avec succès en tant que bugtracker au moyen du plugin Jira Bugtracker la communication est déjà possible.

En cas de problème, il faut effectuer un test directement sur le serveur Squash TM pour vérifier si Jira est accessible : essayer de passer une requête vers l’API Jira directement depuis le serveur hébergeant Squash TM au moyen d’un curl ou un wget. Si la requête ne passe pas, c’est un problème réseau.

### Compte technique Jira

Pour fonctionner le plugin doit pouvoir communiquer avec Jira au travers d’un utilisateur Jira. Cet utilisateur doit avoir à minima des droits en lecture par l’API sur l’ensemble des tickets à synchroniser. Si le reporting de Squash vers Jira est activé, l’utilisateur doit impérativement avoir des droits en écriture par l’API sur l’ensemble de ces tickets concernés. 

La bonne pratique conseillée est de créer un compte technique dédié au plugin dans Jira et disposant des droits nécessaires uniquement sur les projets Jira souhaités.

Si le serveur Jira est un Jira Cloud, il faut générer un jeton d'API à l'utilisateur pour qu'il puisse se connecter à l'API. Pour ce faire :

1. Être connecté avec le compte technique puis dans "Votre profil et vos paramètres", cliquer sur "Profil"
2. Cliquer sur "Gérer votre compte"
3. Cliquer sur "Sécurité"
4. Dans Jetons d'API, cliquer sur "Créer et gérer des jetons d'API"
5. Cliquer sur "Create API token" puis saisir un libellé
6. Saisir un libellé puis cliquer sur "Create"
7. Copier le jeton

La génération du jeton d'API n'est pas à faire pour Jira Server ou Jira Datacenter, l'utilisateur peut utiliser son mot de passe habituel pour se connecter à l'API.

## Création du serveur Xsquash4jira dans Squash TM

Pour utiliser le plugin Xsquash4jira, il faut déclarer un serveur de synchronisation de type *jira.xsquash* dans l'espace **Serveurs>Bugtrackers et serveurs de synchronisation** de l'Administration Squash TM.

Dans la popup de création d'un nouveau serveur, renseigner le nom du serveur (nom libre), le type 'jira.xsquash' et l'url du Jira la plus simple possible puis ajouter. Le plugin Xsquash4jira est opérationnel avec Jira Serveur, Jira Datacenter et Jira Cloud.

![Ajouter un serveur Xsquash4jira](./resources/ajout-serveur-x4j.png){class="centre"}

Le plugin utilise l'API Jira pour récupérer les données avec une authentifiation de type 'Basic Authentication' avec les identifiants du compte technique Jira.
<br/>Sur la page de configuration du serveur de synchronisation, il est donc nécessaire de choisir l’option **'Utiliser les identifiants de Squash TM'** et de renseigner les identifiants du compte technique Jira : couple login/mot de passe si Jira Server ou Datacenter ou login/jeton d'API si Jira Cloud. 

![Page serveur Xsquash4jira](./resources/page-conf-x4j.png){class="pleinepage"}

!!! tip "En savoir plus"
    Pour des raisons de sécurité, les login et mot de passe saisis pour la connexion aux outils tiers sont encryptés dans la base de données avec une clé de cryptage.
    Le plugin Xsquash4jira possède également deux propriétés à configurer directement dans le fichier de configuration de Squash TM squash.tm.cfg.properties.
    Pour en savoir plus, consulter la page suivante : [Gestion du fichier de configuration Squash TM](../../install-guide/parametrage-squash/config-generale.md#gestion-du-fichier-de-configuration-squash-tm)


!!! danger "Attention"
    Un serveur de synchronisation Xsquash4jira ne peut être supprimé s'il est utilisé pour réaliser des synchronisations. Il faut supprimer au préalable les synchronisations avant de pouvoir supprimer le serveur.

## Configuration de Xsquash4jira sur un projet

Les synchronisations de tickets Jira se configurent individuellement pour chaque projet Squash TM. Cette configuration se fait au niveau de l'ancre **Plugins** ![Plugins](./resources/plugin.png){class="icone"} de la page de consultation d'un projet.

Il faut commencer par activer le plugin **Connecteur Xsquash4Jira** pour pouvoir accéder à sa page de configuration spécifique depuis le bouton ![Configurer](./resources/settings.png){class="icone"}.

Pour désactiver l'utilisation du plugin sur le projet, il suffit de cliquer sur le même bouton radio que pour l'activer. L’option "Conserver la configuration du plugin" permet de sauvegarder durant la désactivation du plugin la configuration et les synchronisations du projet lorsqu'elle est cochée. Si l’option est décochée, la configuration et les synchronisations du projet sont définitivement supprimées à la désactivation du plugin.

![Désactiver Xsquash4jira](./resources/desactiver-x4j.png){class="centre"}

La page de configuration de Xsquash4jira est accessible au clic sur le bouton ![Configurer](./resources/settings.png){class="icone"} présent en bout de ligne du **Connecteur Xsquash4Jira**.

Elle se compose de 5 blocs :

- Le bloc **Synchronisations** qui permet de configurer le périmètre des synchronisations.
- Le bloc **Champs de reporting Squash vers Jira** qui permet de configurer le reporting de l’avancement de la recette sur les tickets Jira
- Le bloc **Équivalences entre les champs** qui permet de configurer les équivalences entre les champs Jira et les champs Squash TM
- Le bloc **Équivalences entre les valeurs des champs** qui permet de configurer les équivalences de valeurs pour les champs de type liste : "Criticité", "Catégorie" et "Statut".
- Le bloc **Équivalences des liens entre exigences** qui permet de configurer les équivalences des liens entre tickets Jira en liens entre exigences dans Squash TM.

### Configuration des synchronisations

Une synchronisation est l’unité de travail du plugin Xsquash4Jira. Elle représente l’ensemble des tickets Jira définis par un périmètre dynamique qui sont récupérés dans Squash TM. Ce périmètre dynamique est recalculé à chaque processus de mise à jour. Par défaut, le délai est de 5 minutes entre chaque processus de synchronisation.

C'est à partir du "Type de synchronisation" que l'utilisateur peut définir ce périmètre qui est soit un "Tableau", un "Filtre" ou une "Requête JQL". Pour les tableaux, il est possible d'affiner le filtre avec le champ "JQL additionnel" et l'option "Restreindre au sprint actif".

Voici le périmètre récupéré en fonction du type de synchronisation choisi :

| Type | Périmètre | Note |
|--|--|--|
| Tableau | L’ensemble des tickets inclus dans le filtre qui sert à la construction du tableau côté Jira. | Le contrôle du périmètre est délégué à un filtre existant dans Jira, qui est en fait le filtre associé au tableau ciblé. Pour rappel, un tableau dans Jira est avant tout un filtre, auquel sont rajoutés des filtrages et des présentations supplémentaires. |
| Filtre | L’ensemble des tickets renvoyés par l’appel de ce filtre dans Jira | Le contrôle du périmètre est délégué à un filtre existant dans Jira. Si le filtre est modifié côté Jira, le périmètre dans Squash TM est modifié, sans préavis ni message. Si le filtre est supprimé côté Jira, la synchronisation échoue avec message d’erreur dans les logs. |
| JQL | L’ensemble des tickets renvoyés par le passage de cette requête dans l’API recherche de Jira | Le contrôle du périmètre est effectué côté Squash TM. Utile s’il n’est pas possible de créer les filtres appropriés dans Jira. |

#### Créer une synchronisation

Pour créer une synchronisation, cliquer sur le bouton ![Ajouter](./resources/icone-ajouter.png){class="icone"} présent au sommet du bloc **Synchronisations**.

![Ajout synchro](./resources/ajout-synchro.png){class="centre"}

Le tableau suivant détaille le fonctionnement des champs de la popup et comment les renseigner : 

| Nom | Obligatoire | Modifiable | Commentaire |
|--|--|--|--|
| Nom | Oui | Oui | Nom libre. Il doit être unique sur l'ensemble de l'instance Squash TM. |
| Chemin Cible | Oui | Non | Chemin initial du répertoire cible de la synchronisation. Ce répertoire ne doit pas exister dans l’espace Exigences, il est créé par Xsquash4jira à la première mise à jour. Ce chemin ne doit pas commencer par un / (ex : dossier 1/sous-dossier 1). |
| Serveur | Oui | Non | Serveur Xsquash4jira utilisé pour cette synchronisation, à choisir dans la liste des serveurs paramétrés dans l’écran d’administration des Bugtrackers et serveurs de synchronisation. |
| Sélection par | Oui | Non | Type de périmètre. Trois options possibles : Tableau, Filtre ou Requête JQL. En fonction de l'option choisie les champs suivants de la popup changent. |
| Nom du tableau | Oui | Oui | Nom du tableau à synchroniser tel qu’il apparaît dans les écrans de Jira. Il faut respecter la casse. |
| JQL additionnel | Non | Oui | Permet d’affiner le périmètre au moyen d’une clause JQL additionnelle. La clause en question doit être nue, c’est-à-dire ne pas comporter d’opérateur (AND, OR…) en début ou fin de clause et ne comporter que des critères de sélection (pas de ORDER BY ni autres clauses…). |
| Restreindre au sprint actif | - | Non | Permet de garder synchronisés uniquement les tickets qui sont dans un sprint au statut « Actif » dans Jira. N’a de sens que pour une synchronisation de type "Tableau" qui pointe vers un tableau de type SCRUM, les autres tableaux n’ayant pas de sprints. |
| Nom du filtre | Oui | Oui | Nom du filtre à synchroniser tel qu’il apparait dans les écrans de Jira en respactant la casse. |
| Requête JQL | Oui | Oui | Requête JQL qui définit le périmètre de la synchronisation. Elle ne doit pas comporter d’opérateur (AND, OR…) en début ou fin de clause et ne comporter que des critères de sélection (pas de ORDER BY ni autres clauses…). |

Lors de l'ajout d'une nouvelle synchronisation, il est possible d'effectuer une simulation de la synchronisation avant de confirmer sa création. 

Pour cela, un bouton **[Simuler]** figure dans la fenêtre d'ajout de synchronisation. Il permet de visualiser le nombre et le détail des tickets que contient la synchronisation pour vérifier que cela correspond au périmètre voulu.

![Simulation synchro](./resources/fr_simulation_synchro.png){class="centre"}

#### Suivi des synchronisations

Une fois la synchronisation configurée, une ligne supplémentaire apparaît dans la table des synchronisations. À la prochaine mise à jour, l’ensemble des exigences synchronisées qui correspondent au périmètre sont créées. Si la synchronisation échoue sur une erreur, rien n'est créé dans Squash TM. Le statut de chaque synchronisation est affiché dans la table des synchronisations.

Le bouton ![Rafraîchir](./resources/refresh.svg){class="icone"} permet de rafraîchir la page de configuration pour voir si le statut des synchronisations a été mis à jour suite au passage du processus de mise à jour.

#### Forcer une synchronisation complète

Par défaut, le plugin ne met à jour que les exigences synchronisées qui ont été modifiées dans Jira depuis le dernier processus de mise à jour connu et en succès.
<br/>Dans le cas d’un changement d’équivalences de champ ou de valeur coté Squash TM, il est nécessaire de resynchroniser l’ensemble des exigences coté Squash TM, même si rien n’a changé coté Jira.
Le bouton ![Forcer la synchronisation](./resources/synchronize.svg){class="icone"} permet de forcer la mise à jour de l’ensemble des tickets synchronisés qui sont dans le périmètre actuel d’une ou plusieurs synchronisations.

!!! info "Info"
    Afin d’éviter des processus de mise à jour inutiles, il est conseillé de faire les équivalences avant de déclarer les synchronisations.
    <br/>Si les équivalences doivent évoluer par la suite, il est conseillé de faire toutes les évolutions d’équivalences de champs, de valeurs et de liens, puis de forcer une seule fois toutes les synchronisations du projet.

#### Désactiver une synchronisation

Le bouton radio présent en début de ligne permet de désactiver ou de réactiver une synchronisation sans la supprimer.
Lorsqu’une synchronisation est désactivée, les tickets présents dans son périmètre ne sont plus synchronisés. La ligne est grisée et n’est plus modifiable mais il est toujours possible de supprimer la synchronisation.
<br/>Lorsqu’elle est réactivée, les tickets sont de nouveau synchronisés dans Squash TM et reprennent les attributs de synchronisation ainsi que les équivalences et le reporting définis dans les autres blocs.

#### Supprimer une synchronisation

Le bouton ![Supprimer](./resources/delete.png){class="icone"} permet de supprimer une ou plusieurs synchronisations. En accord avec la politique de non-suppression, les exigences synchronisées ne sont pas supprimées mais transformées en exigences natives de Squash TM (affichées en noir). Elles ne sont plus mises à jour depuis Jira et se comportent comme n’importe quelle autre exigence de Squash TM. Il en va de même pour tous les répertoires synchronisés (répertoires cibles et répertoires de sprint).
<br/>Si l’utilisateur désire supprimer ces exigences, il doit le faire manuellement comme pour n’importe quelle exigence de Squash TM.

### Configurer le reporting de Squash vers Jira

Le plugin Xsquash4Jira est capable de remonter vers Jira des informations relatives à la recette réalisée dans Squash TM. Ces informations sont reportées directement au niveau des tickets Jira dans des champs personnalisés configurés à cet effet.

Sept informations sont disponibles : 3 taux d’avancement de la recette (Rédaction des cas de test, Vérification des tickets Jira, Validation des tickets Jira), 3 ratios reprenant ces taux d’avancement ainsi que le nombre de tests concernés, et enfin un champ qui présente une analyse synthétique de ces 3 taux : le champ "Statut de la recette".

Ce reporting est optionnel. Il est possible de configurer le plugin de sorte à ne rien récupérer dans Jira ou ne récupérer qu'une partie de ces informations. 

!!! warning "Focus"
    Les champs taux et ratios contiennent des informations similaires. Il est donc inutile de tous les synchroniser et plus intéressant de faire un choix entre les taux et les ratios.
    <br/>La différence se fait sur la forme. Les champs "taux" renvoient un nombre simple compris entre 0 et 100.
    <br/>Les champs "ratio" reprennent dans une chaîne de caractères le nombre remonté dans le taux avec le symbole % suivi du nombre de tests concernés (X/Y tests).

#### Création des champs de reporting dans Jira

Le reporting dans Jira s’effectue au moyen de champs personnalisés à rajouter directement dans les tickets Jira concernés par la synchronisation : 

| Information de recette | Champs personnalisés à créer côté Jira |
|--|--|
| Taux d'avancement (Rédaction, Vérification, Validation) | Dans Jira, il faut créer un champ personnalisé de type numérique pour chacun des trois taux proposés par le plugin que l’on souhaite récupérer dans Jira. Le nom est libre. |
| Ratios d'avancement (Rédaction, Vérification, Validation) | Dans Jira, il faut créer un champ personnalisé de type texte (mono-ligne) pour chacun des trois ratios proposés par le plugin que l’on souhaite récupérer dans Jira. Le nom est libre. |
| Statut de la recette | Dans Jira, il faut créer un champ personnalisé de type texte (mono-ligne) pour le champ "Statut de la recette". Le type de moteur de rendu pour le champ doit être "Générateur de rendu de style Wiki" pour permettre d’afficher les icônes. Le nom est libre. |

Les champs personnalisés Jira créés doivent ensuite :

- Être affectés à tous les types de tickets qui sont susceptibles d’être synchronisés dans Squash TM
- Être accessibles dans les écrans de modification des types de tickets et des projets concernés.
- Être modifiables par l’utilisateur technique déclaré dans Squash TM

Le nom des champs déclaré dans Jira est libre. Attention néanmoins à ce que tous les tickets Jira concernés par les synchronisations d’un même projet Squash aient les mêmes champs puisque que dans Squash TM la configuration se fait au niveau du projet.

Ces champs sont ensuite alimentés par Squash TM en fonction de l’avancement de la recette et remontés dans Jira à chaque cycle de mise à jour si nécessaire.

#### Configuration des champs de reporting dans Squash TM

Dans Squash TM, la configuration du reporting s’effectue dans le second bloc de l’écran de configuration du plugin Xsquash4Jira : "Champs de reporting Squash vers Jira".
Pour les configurer, il faut renseigner en face des données que l'utilisateur souhaite récupérer, les noms des champs personnalisés correspondants créés dans Jira en respectant bien la casse.
<br/>Une fois la configuration du bloc terminée, il ne faut pas oublier de bien forcer la synchronisation pour que la modification soit prise en compte.
<br/>Si l’utilisateur ne désire aucun reporting dans Jira il suffit de laisser les champs du bloc vides.

![Champs de reporting Squash vers Jira](./resources/conf-reporting.png){class="centre"}

#### Affichage des données de reporting dans Jira

Lorsque les champs de reporting sont correctement configurés côté Jira et qu'ils ont bien été renseignés dans Squash TM, Xsquash4jira renseigné automatiquement les valeurs dans ces champs lors du processus de synchronisation et les met à jour si besoin.

![Champs de reporting Squash dans Jira](./resources/reporting-jira.png){class="centre"}

**Taux de Rédaction :**
<br/>Ce taux permet de suivre l’avancement de la rédaction des cas de test. Pour une exigence synchronisée donnée ce taux est égal à :
<br/>Nombre de cas de test couvrant l’exigence ou l’une de ses descendantes et au statut « à Valider » ou « Validé » **/** Nombre de cas de test couvrant l’exigence ou l’une de ses descendantes quel que soit le statut
<br/>Si une exigence n’est pas couverte, le taux vaut 0.

**Ratio de Rédaction :**
<br/>Ce champ reprend la valeur du taux de rédaction suivi du nombre de cas de tests concernés (X/Y cas de test).

**Taux de Vérification :**
<br/>Ce taux permet de suivre l’avancement de l’exécution des cas de test liés à une exigence ou à l’une de ses filles. Pour une exigence synchronisée donnée ce taux est égal à :
<br/>Nombre d’ITPI liés à un CT couvrant l’exigence synchronisée ou l’une de ses descendantes ayant un statut d’exécution final (Bloqué, Echec, Non testable, Succès, Arbitré), en ne tenant compte que de la dernière exécution (ou du dernier fast pass) **/** Nombre d’ITPI liés à des cas de test couvrant l’exigence synchronisée ou l’une de ses descendantes

**Ratio de Vérification :**
Ce champ reprend la valeur du taux de vérification suivi du nombre de cas de tests concernés (X/Y cas de test).

**Taux de Validation :**
Ce taux permet de suivre la validation d’une exigence. Pour une exigence synchronisée donnée ce taux est égal à :
<br/>Nombre d’ITPI liés à un CT couvrant l’exigence synchronisée ou l’une de ses descendantes ayant un statut d’exécution validé (Succès ou Arbitré), en ne tenant compte que de la dernière exécution (ou du dernier fast pass) **/** Nombre d’ITPI liés à un CT couvrant l’exigence synchronisée ou l’une de ses descendantes ayant un statut d’exécution final, en ne tenant compte que de la dernière exécution (ou du dernier fast pass)

**Ratio de Validation :**
<br/>Ce champ reprend la valeur du taux de validation suivi du nombre de cas de tests concernés (X/Y cas de test).

**Statut de la recette :**
Il s’agit d’un champ synthétique qui présente un résumé des différents états de recette possible pour une exigence donnée.
<br/>Le mode de calcul est le suivant :

|Taux de Rédaction|Taux de Vérification|Taux de Validation|Statut de la Recette|
|--|--|--|--|
|0|0|0|Non initialisé|
|0 < Taux de Rédaction < 100|0|0|Conception en cours|
|100|0|0|À exécuter|
|Tous|0 < Taux de Vérification < 100|100|En cours d’exécution|
|Tous|0 < Taux de Vérification|Taux de Validation < 100|Non Validé|
|Tous|100|100|Validé|

Il est possible de n’afficher que ce champ dans Jira s'il est configuré seul. Les taux sont calculés mais non transmis à Jira.

### Configurer les équivalences de champs

Afin de permettre à Squash TM d’afficher les informations contenues dans les champs des tickets Jira, il faut configurer des équivalences de champs. Cette configuration se fait dans le troisième bloc de l'écran de configuration Xsquash4jira.

Le bouton ![Ajouter](./resources/icone-ajouter.png){class="icone"} présent au sommet du bloc permet d'ajouter une nouvelle équivalence. 

![Equivalence entre les champs](./resources/equivalences-champs.png){class="pleinepage"}

Les champs Squash TM disponibles sont présentés dans une liste déroulante. Il est possible d'enrichir cette liste en ajoutant des champs personnalisés aux exigences du projet. Il est recommandé d'utiliser des champs personnalisés Squash TM de type 'Texte simple' pour récupérer les données des tickets Jira dans la majeure partie des cas. Pour mapper les champs 'Texte multi-lignes' Jira, il est préférable d'utiliser un champ personnalisé Squash TM de type 'Texte riche'.

!!! tip "En savoir plus"
    Pour en savoir plus sur les champs personnalisés, consulter la page suivante : [Gérer les champs personnalisés](../personnalisation-entites/gerer-champs-persos.md).

Les champs Jira doivent être renseignés par leur nom tel qu'il s'affiche dans les requêtes JQL pour les champs natifs ou dans les tickets Jira pour les champs personnalisés. Les champs natifs Jira comme par exemple 'Type', 'Etat', et 'Priorité' doivent donc être renseignés en anglais en minuscule dans le tableau d'équivalences : issuetype, status, priority.

Une fois la configuration du bloc terminée, il ne faut pas oublier de bien forcer les synchronisations du projet pour que la modification soit prise en compte. Il est également nécessaire de forcer la synchronisation après une suppression d'équivalence dans le tableau sans quoi des erreurs apparaissent dans les logs.

!!! info "Info"
    Si une équivalence ne fonctionne pas, vérifier l'affichage du nom du champ Jira dans les requêtes JQL et essayer avec cette valeur.

### Configurer les équivalences des valeurs de champs

Si une équivalence de champs a été configurée pour les champs Squash TM 'Criticité', 'Catégorie' ou 'Statut', il faut réaliser une configuration des équivalences des valeurs de champs pour avoir un mapping complet. Celle-ci s’effectue dans le quatrième bloc de l’écran.

Cette configuration utilise la syntaxe YAML. Il est donc indispensable de bien respecter le format indiqué dans l'aide à la configuration.

Dans l'aide à la configuration :

- champsquash correspond au nom du champ Squash TM en anglais en minuscule comme affiché dans la liste des champs et valeurs disponibles à la fin de l'aide
- valeurjira est la valeur du champ Jira tel qu'affichée dans les requêtes JQL sur Jira ou dans les tickets Jira.
- valeursquash est la valeur du champ Squash TM tel qu'affichée dans la liste des champs et valeurs disponibles à la fin de l'aide

Voici un exemple de configuration pour ces trois champs :

![Equivalence entre les valeurs de champs](./resources/valeurs-champs.png){class="pleinepage"}

Une fois la configuration du bloc terminée, il ne faut pas oublier de bien forcer les synchronisations du projet pour que la modification soit prise en compte. Il est également nécessaire de forcer la synchronisation après une suppression d'équivalence.

!!! tip "En savoir plus"
    Il est possible d'associer une liste personnalisée au champ 'Catégorie' des exigences du projet pour récupérer la valeur des types de tickets Jira dans Squash TM comme dans l'exemple ci-dessus.
    <br/>Pour en savoir plus sur les listes personnalisées, consulter la page suivante : [Gérer les listes personnalisées](../personnalisation-entites/gerer-listes-persos.md).

!!! danger "Attention"
    Les statuts 'Approuvé' et 'Obsolète' bloquent toutes modifications sur une exigence. 
    <br/>Si un mapping est fait avec ces deux statuts, les exigences qui prendront ces statuts automatiquement ne seront plus mises à jour par la suite.
    <br/>Il faudra modifier le statut manuellement à 'En cours de rédaction' ou 'À approuver' pour réactiver la mise à jour de l'exigence.

### Configurer les équivalences des liens entre exigences

Xsquash4jira permet également de récupérer les liens entre tickets Jira sous forme de liens entre exigences. Cette configuration se fait dans le cinquième bloc de la page de configuration de Xsquash4jira.

![Equivalences des liens entre exigences](./resources/liens-exigences.png){class="centre"}

L’ensemble des liens entre exigences Squash s’affichent automatiquement dans le tableau d’équivalences. Il s’agit des liens paramétrés dans l’espace "Entités>Liens entre exigences" dans l’administration de Squash TM. 

!!! tip "En savoir plus"
    Il est possible d'ajouter de nouveaux liens entre exigences depuis l'espace "Entités>Liens entre exigences".
    <br/>Pour en savoir plus sur les liens entre exigences, consulter la page suivante : [Gérer les liens entre exigences](../personnalisation-entites/gerer-liens-exigences.md).

Le lien entre exigences Jira doit être désigné par son **Nom**, tel qu’il s’affiche dans la partie "Création de liens entre des demandes" dans l’administration de Jira Sever/Datacenter ou dans l'espace "Liaison de tickets" dans Jira cloud :

![Liens Jira](./resources/liens-jira.png){class="pleinepage"}

Une fois la configuration du bloc terminée, il ne faut pas oublier de bien forcer les synchronisations du projet pour que la modification soit prise en compte. Il est également nécessaire de forcer la synchronisation après une modification d'équivalence.

## Configuration de Xsquash4Jira dans les modèles de projet

Il est possible de définir la configuration de Xsquash4Jira dans un modèle de projet afin de la partager avec les projets associés au modèle ou créés à partir du modèle. La configuration au niveau des projets peut ensuite être déléguée au modèle ou évoluer indépendamment de celle du modèle.

Cette fonctionnalité est utile lorsque l'on souhaite configurer le plugin Xsquash4Jira pour plusieurs projets Squash et que les tickets Jira à synchroniser sont issus de projets Jira qui partagent le même paramétrage (champs personnalisés, types de tickets, priorités, workflow...).

### Configurer Xsquash4Jira dans les modèles de projet

La page de configuration du plugin Xsquash4Jira des modèles est similaire à celle des projets, à l'exception du bloc **Synchronisations** qui est absent pour les modèles de projet. 
Pour les modèles, il est donc possible de paramétrer les équivalences entre les champs Squash et Jira, à savoir :

- Champs de reporting Squash vers Jira
- Équivalences entre les champs
- Équivalences entre les valeurs des champs
- Équivalences des liens entre exigences

### Créer un projet en reprenant la configuration de Xsquash4Jira du modèle

Lors de la création d'un projet à partir d'un modèle, l'option "Configuration de Xsquash4jira' permet à l'administrateur de reprendre la configuration de Xsquash4Jira définie au niveau du modèle. 

![Reprendre conf Xsquash4Jira](resources/reprendre_conf_xsquash4jira_modele.png){class="centre"}

Ainsi, dans le projet créé, le plugin Xsquash4Jira est déjà préconfiguré à l'exception des synchronisations qui seront à ajouter par le chef de projet ou l'administrateur. 
La configuration de Xsquash4Jira au niveau des projets peut ensuite être déléguée au modèle ou évoluer indépendamment de celle du modèle.

### Lier la configuration de Xsquash4Jira du modèle aux projets liées

#### À la création d'un nouveau projet

Lors de la création d'un projet à partir d'un modèle, une option permet à l'administrateur de :

- Lier la configuration de Xsquash4Jira du projet à celle du modèle (option cochée): 
    - la configuration au niveau du projet est déléguée au modèle, en dehors des synchronisations
    - toutes les modifications effectuées au niveau du modèle seront répercutées dans les projets liés
- Ne pas lier la configuration de Xsquash4Jira du projet à celle du modèle (option décochée) : 
    - la configuration au niveau du projet peut évoluer indépendamment de celle du modèle lié
    - les modifications de la configuration effectuées au niveau du modèle ne seront pas répercutées dans les projets liés

![Lier conf Xsquash4Jira](resources/lier_conf_xsquash4jira_modele.png){class="centre"}

#### À l'association d'un projet existant à un modèle

Il est également possible de lier la configuration de Xsquash4Jira lors de l'association d'un projet existant à un modèle. 

Dans ce cas, si le plugin Xsquash4Jira était déjà configuré au niveau du projet, cette configuration est écrasée par celle du modèle. Seuls les paramétrages d'équivalences sont impactés, les synchronisations ne sont pas supprimées. 

![Associer conf Xsquash4Jira](resources/associer_conf_xsquash4jira_modele.png){class="centre"}

#### Désactiver Xsquash4Jira dans les modèles

La désactivation ou la suppression de la configuration dans le modèle entraîne la suppression de la configuration dans les projets liés, à l'exception des synchronisations.
