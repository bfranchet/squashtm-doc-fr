# Gérer un jalon

Les jalons permettent de versionner le référentiel de test d'un ou plusieurs projets et de filtrer au niveau des bibliothèques sur les éléments qui y sont associés (exigences, cas de test, campagnes).

## Ajouter, modifier, supprimer un jalon

La page de gestion des jalons est accessible depuis l'espace Administration via le sous-menu "Jalons". 
Depuis le tableau de gestion des jalons, il est possible d'ajouter ![Ajouter](./resources/iconeAjouter.png){class="icone"} ou de supprimer ![Ajouter](./resources/iconePoubelle.png){class="icone"} des jalons de façon unitaire ou en masse.

![page jalons admin](./resources/pageJalonsFR.png)

En cliquant sur le numéro de ligne '#' ou le nom d'un jalon, sa page de consultation s'affiche pour permettre sa modification. 

![page consultation jalon admin](./resources/consultationJalonFR.png)

Depuis la page de consultation d'un jalon, plusieurs actions sont possibles :

- Modifier le nom et la description du jalon
- Modifier la date d'échéance du jalon
- Changer le statut du jalon
- Choisir entre une portée globale ou restreinte
- Associer des projets aux jalons
- Supprimer un jalon

!!! tip "En savoir plus"
    Pour en savoir plus sur les attributs d'un jalon, consulter la page suivante : [Les jalons dans Squash TM](../../user-guide/gestion-jalons/jalons-squash.md)

!!! warning "Focus"
    Lors de la suppression d'un jalon, tous les projets et objets auxquels il était associé se verront dissociés de celui-ci.


## Droits associés 

À chaque jalon est attribué une portée. La portée d’un jalon est définie en fonction du profil qui crée le jalon :

- les jalons créés par un Chef de projet ont pour périmètre les projets sur lesquels il a des droits de chef de projet
- les jalons créés par un Administrateur ont une portée globale par défaut et peuvent par conséquent être utilisés sur tous les projets.

![portée jalon admin](./resources/porteeJalonFR.png)

!!! tip "En savoir plus"
    La portée d'un jalon a des impacts sur la duplication ou la synchronisation d'un jalon. 
    <br/>Pour en savoir plus, consulter la page : [Dupliquer et synchroniser un jalon](./dupliquer-synchroniser-jalon.md)


## Associer un jalon à un projet 


Depuis la page de consultation du jalon, il est possible d'associer un ou plusieurs projets au jalon en cliquant sur le bouton ![Ajouter](./resources/iconeAjouter.png){class="icone"} présent au dessus du bloc 'Projets'.

La popup d'association est composée d'une liste de projets précédés d'une case à cocher.

![popup associer admin](./resources/popupAssocierFR.png){class="centre"}


## Statut des jalons

Le cycle de vie d'un jalon se compose des quatre statuts suivants : 'Planifié', 'En cours', 'Terminé' et 'Verrouillé'.

Chacun de ces statuts confère des droits différents en matière :

- d’association du jalon à un projet ou un objet
- de création/suppression/modification des objets associés au jalon

Ci-dessous un tableau récapitulatif recensant les actions possibles pour chacun des 4 statuts :

| Statut | Associer/dissocier un jalon à un projet | Associer/dissocier un jalon à un objet | Autorise la création d'objets | Autorise la suppression d'objets | Autorise la modification d'objets |
|--|--|--|--|--|--|
|Planifié|oui|non|non|non|non|
|En cours|oui|oui|oui|oui|oui|
|Terminé|oui|oui|oui|oui|oui|
|Verrouillé|non|non|non|non|non|

!!! danger "Attention"
    Tout objet associé à un jalon au statut "Verouillé" ne peut être modifié ou supprimé. Dans le cas d'une campagne associée à un jalon "Verouillé", l'ensemble des élements qu'elle contient, tels que ses itérations, ses suites de tests et ses exécutions, sont bloqués en modification et ne peuvent être supprimés.