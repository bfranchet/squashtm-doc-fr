# Dupliquer et synchroniser un jalon

Les administrateurs et chefs de projet ont la possibilité depuis le sous-menu 'Jalons' de dupliquer un jalon et son référentiel et de synchroniser plusieurs jalons et leurs référentiels.

## Dupliquer un jalon

L’espace de gestion des jalons permet de dupliquer le référentiel d’un jalon : les exigences et les cas de test associés au jalon dupliqué sont automatiquement associés au nouveau jalon.
<br/>Cette fonctionnalité est très utile lorsqu’une nouvelle version de l’application représentée par un nouveau jalon Squash TM s’appuie sur le même référentiel qu'une précédente version de l'application représentée par un précédent jalon.

Pour dupliquer un jalon, il faut le sélectionner dans le tableau puis cliquer sur le bouton ![Dupliquer](./resources/duplicate.svg){class="icone"}.

![Dupliquer jalon](./resources/dupliquerJalonFR.png){class="pleinepage"}

Une popup permet de renseigner les informations du nouveau jalon et de choisir les éléments du référentiel associés au jalon dupliqué qui seront associés au nouveau jalon.

![Popup dupliquer jalon](./resources/popupduplicationFR.png){class="centre"}

En fonction des choix réalisés lors de la création du nouveau jalon par duplication, le nouveau jalon est associé :

- aux projets, exigences et cas de test du jalon original 
- aux projets et aux exigences ou cas de test du jalon original 
- uniquement aux projets du jalon original 

!!! warning "Focus"
    Un jalon doit être au statut 'En cours' ou 'Terminé' pour être dupliqué.

**La notion de portée dans la duplication d'un jalon**

La fonction de duplication d'un jalon est impactée par la notion de portée des jalons. La duplication d'un jalon n'aura pas le même effet en fonction de la portée du jalon original (jalon source) et du profil de l'utilisateur qui va créer le nouveau jalon par duplication (jalon cible). 

!!! info "Info"
    Pour rappel, lorsqu’un administrateur crée un jalon, celui-ci est par défaut de portée 'Globale' tandis que lorsqu’un chef de projet crée un jalon, ce jalon a une portée 'Restreinte' (comme son nom l’indique celui-ci est restreint au périmètre des projets pour lequel le chef de projet a une habilitation de chef de projet). 
    <br/>À la création d'un jalon de portée restreinte les projets auxquels est habilité le chef de projet s'affichent dans le bloc Projets. Cette liste de projets est le périmètre du jalon.

Lorsqu'un administrateur duplique un jalon de portée globale ou restreinte, le jalon cible créé de portée globale est associé à l'ensemble des projets, des exigences et des cas de test du jalon source. Ce jalon peut ensuite être associé à n'importe quel projet par un administrateur.

Lorsqu'un chef de projet duplique un des jalons dont il est le propriétaire, le jalon cible créé de portée restreinte est associé à l'ensemble des projets, des exigences et des cas de test du jalon source.
<br/>Lorsqu'un chef de projet duplique un jalon de portée globale ou de portée restreinte appartenant à un autre chef de projet, le jalon cible créé de portée restreinte est associé uniquement aux projets, exigences et cas de test du jalon source sur lesquels il a des droits de chef de projet. 
<br/>Le chef de projet peut associer les jalons qu'il a créés par duplication à tous les projets sur lesquels il est chef de projet.


## Synchroniser deux jalons 

L'espace de gestion des jalons offre également la possibilité de synchroniser plusieurs jalons entre eux. 
<br/>Cela permet en fonction de l’option choisie d’associer automatiquement à un jalon les objets (projets, exigences et cas de test associés) d’un autre jalon.

Pour synchroniser deux jalons, il faut les sélectionner tous les deux dans le tableau puis cliquer sur le bouton ![Synchroniser](./resources/synchronize.svg){class="icone"}.

![Synchro jalons](./resources/synchroJalonFR.png){class="pleinepage"}

Il existe trois modes de synchronisation :

- A -> B : Le jalon cible 'B' est associé aux objets du jalon source 'A'.
- B -> A : Le jalon cible 'A' est associé aux objets du jalon source 'B'.
- Union : Les jalons synchronisés en union partagent réciproquement leurs objets associés.

!!! warning "Focus"
    Dans le cas des deux premiers modes de synchronisation, le jalon cible doit être au statut 'En cours' ou 'Terminé' pour que la synchronisation s’opère. Pour une union, les deux jalons sélectionnés doivent être au statut 'En cours' ou 'Terminé'. 

![Popup synchro jalons](./resources/popupSynchroFR.png){class="centre"}

**La notion de portée dans la synchronisation de jalons**

De même que pour la duplication, la notion de portée a un impact sur la synchronisation des jalons. La portée des jalons sélectionnés et le profil de l'utilisateur réalisant la synchronisation influencent le résultat d'une synchronisation.

Lorsqu'un administrateur synchronise deux jalons quelque soit leur portée ou le mode de synchronisation, il y a aucune restriction et l'ensemble des objets sont associés comme attendu.

Lorsqu'un chef de projet synchronise deux jalons dont il est propriétaire, il peut utiliser les 3 modes de synchronisation et l'ensemble des objets sont associés s'il coche la case *'Inclure dans le périmètre du jalon cible, les projets du jalon source (s'ils n'y sont pas déjà)'*. Si la case n'est pas cochée, seuls les objets du jalon source inclus dans le périmètre des deux jalons sont impactés. 

Lorsqu'un chef de projet synchronise un jalon de portée globale avec un jalon de portée restreinte, il ne lui est possible que d'associer les objets du jalon global sur lesquels il a des droits de chef de projet au jalon resteint.

Lorsqu'un chef de projet synchronise deux jalons de portées restreintes dont il n'est pas propriétaire, il lui est possible d'utiliser les 3 modes de synchronisation mais seuls les objets sur lesquels il a des droits de chef de projet sont associés. Mais il est impératif que les deux jalons partagent le même périmètre (associés aux mêmes projets) pour que cela fonctionne.

Un chef de projet n'a par contre pas les droits pour :

- réaliser des synchronisations avec pour cible des jalons de portée globale
- réaliser une union sur deux jalons de portée différente
- associer les objets de deux jalons de portée restreinte dont il n'est pas propriétaire si ceux-ci ne partagent pas le même périmètre (associés à des projets différents)
 