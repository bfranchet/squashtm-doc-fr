# Gérer les champs personnalisés

La gestion des champs personnalisés est accessible depuis l'espace Administration, sous-menu "Entités", puis au clic sur l'ancre ![Listes personnalisées](./resources/custom-field.png){class="icone"}

Les champs personnalisés (CUF) sont des éléments personnalisables qui sont ajoutés au bloc "Informations" d'un dossier, d'une exigence, d'un cas de test, d'une campagne, d'une itération ou encore d'une suite de tests. 


## Les types de champs personnalisés 

Il existe 7 types de champs personnalisés (CUF) 

| Type de champ personnalisé | Description
|----|----|
|Texte simple |Le champ peut contenir au maximum 255 caractères|
|Texte riche |Le champ peut contenir du texte mis en page, des images, des liens ou encore des tableaux|
|Numérique |Le champ peut contenir des nombres entiers, négatifs et décimaux|
|Date |La date est sélectionnable depuis un calendrier|
|Case à cocher |----|
|Liste déroulante |Liste déroulante avec plusieurs options personnalisables|
|Tag |Le champ peut contenir plusieurs étiquettes|


Un champ personnalisé peut par exemple, prendre comme valeur la version de votre application, le choix du navigateur lors de l'exécution, un complément d'informations etc.

## Ajouter, modifier, supprimer un champ personnalisé 

Depuis le tableau de gestion des champs personnalisés, il est possible d'ajouter ![Icône ajouter](./resources/icone-ajouter.png){class="icone"} ou de supprimer ![Icône supprimer](./resources/delete.png){class="icone"} des champs personnalisés de façon unitaire ou en masse.

![Tableau de gestion des champs personnalisés](./resources/tableau-cuf.jpg){class="pleinepage"}

Lors de la création d'un champ personnalisé, il faut renseigner :

- le champ "Nom" 
	<br/>Le nom apparaît au moment de l'association à un projet et à une entité.
- le champ "Libellé"
	<br/>Le libellé s'affiche sur la page de consultation de l'entité choisie.
- le champ "Code" (ne peut contenir que des lettres, chiffres ou underscores)
	<br/>Ce code doit être unique, il sera utilisé pour faire des imports/exports de données.
- le type de champ personnalisé (par défaut, Texte simple est sélectionné)

Chaque champ personnalisé :

- peut être obligatoire ou facultatif (sauf pour le type "Case à cocher"). Par défaut à la création, l'option "Facultatif" est cochée. 
- peut contenir une valeur par défaut. Cette valeur par défaut s'affiche au moment de la création de l'objet. <br/> 
Si le champ personnalisé est obligatoire, il est obligatoire d'entrer une valeur par défaut.

![Popup création CUF](./resources/popup-creation-cuf-3.jpg){class="centre"}

Pour les champs personnalisés de type "Liste déroulante", il est possible d'attribuer une couleur à chaque option de la liste. Ces couleurs seront reprises lors de la création de graphiques portant sur le champ personnalisé dans l’espace Pilotage.

En cliquant sur le numéro de ligne (#) ou le nom du champ personnalisé, la page de consultation du champ personnalisé s'affiche pour permettre sa modification. 

![Page de consultation d'un champ personnalisé](./resources/cuf-liste-page-consult.jpg){class="pleinepage"}

!!! warning "Focus"
	Les champs personnalisés une fois créés doivent être associés à un projet et à une entité (Exigences, Cas de test, Pas de test, Campagne, Exécution, etc.)
	</br>Pour associer des champs personnalisés à un projet, consulter la page [Configurer un projet - Personnaliser le projet](../../gestion-projets/configurer-projet/#les-champs-personnalises.md)