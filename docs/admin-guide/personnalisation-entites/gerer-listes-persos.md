# Gérer les listes personnalisées

La gestion des listes personnalisées est accessible depuis l'espace Administration, sous-menu "Entités",  puis au clic sur l'ancre ![Listes personnalisées](./resources/list.png){class="icone"}

Les listes personnalisées remplacent les listes par défaut du champ "Catégorie" pour les exigences, ou des champs "Nature" et "Type" pour les cas de test.


## Ajouter, modifier, supprimer une liste personnalisée 

L'ajout ![icone ajouter](./resources/icone-ajouter.png){class="icone"} et la suppression ![icone suppression](./resources/delete.png){class="icone"} de listes personnalisées, de façon unitaire ou en masse, se fait depuis le tableau de gestion des listes personnalisées.

![Tableau des listes personnalisées](./resources/tableau-liste-perso.jpg){class="pleinepage"}

Lors de la création, il est impératif de renseigner :

- le champ "Nom". 
- le champ "Code" (il ne peut contenir que des lettres, chiffres ou underscores). Ce code doit être unique, il sera utilisé pour les imports/exports de données. 
- Au moins une option contenant un nom et un code.

Lors de la création d’une liste personnalisée, il est possible d’associer des icônes et des couleurs aux options ajoutées. Ces couleurs seront utilisées lors de la création de graphique portant sur la liste personnalisée dans l’espace Pilotage.

![Popup d'ajout de liste personnalisée](./resources/popup-liste-perso.jpg){class="centre"}

En cliquant sur le numéro de ligne (#) ou le nom de la liste personnalisée, la page de consultation de la liste s'affiche pour permettre sa modification. 

![Page de consultation d'une liste personnalisée](./resources/page-liste-perso.jpg){class="pleinepage"}


!!! warning "Focus"
	Les listes personnalisées doivent être associées à un projet et à une entité (Exigences ou Cas de test)
	</br>Pour associer des listes personnalisées à un projet, consulter la page [Configurer un projet - Personnaliser le projet](../../gestion-projets/configurer-projet/#les-listes-personnalisees.md)

Si une liste personnalisée associée à un projet est supprimée, la liste par défaut correspondant au champ s'affichera et les données de la liste personnalisée seront perdues.