# Gérer les liens entre exigences

La gestion des liens entre les exigences est accessible depuis l'espace Administration, sous-menu "Entités",  puis au clic sur l'ancre ![Listes personnalisées](./resources/icone_linked_requierement.png){class="icone"}

Les liens entre les exigences sont utilisés dans le bloc "Exigences liées" d'une exigence. Il existe par défaut 3 types de liens: 

- Relatif / Relatif
- Parent / Enfant
- Doublon / Doublon

!!! warning "Focus"
    Attention, le type de lien Parent/Enfant ne correspond pas aux [exigences mères/filles](../../../user-guide/gestion-exigences/creer-organiser-referentiel-exigences/#hierarchie-dexigences) telles que définies dans une arborescence. 

## Ajouter, modifier, supprimer un lien entre exigences 

Depuis le tableau de gestion des liens entre exigences, il est possible d'ajouter ![icone ajouter](./resources/icone-ajouter.png){class="icone"} ou de supprimer ![icone suppression](./resources/delete.png){class="icone"} des types de liens, de façon unitaire ou en masse.


![Tableau des types de lien entre exigences](./resources/tableau-lien-exigence.jpg){class="pleinepage"}

La colonne "Défaut" du tableau des types de liens entre exigences permet de définir le type de lien par défaut. Le lien par défaut s'affiche dans la popup d'association, lors de la liaison entre deux exigences. Si le rôle 1 est différent du rôle 2, alors au moment de l'association, les types "Rôle 1/Rôle 2" ainsi que "Rôle 2/Rôle 1" seront disponibles.

Lors de la création d'une liste personnalisée, les 4 champs sont obligatoires :

- le champ "Rôle 1" ; 
- le champ "Code rôle 1" (ne peut contenir que des lettres, chiffres ou underscore). Le code doit être unique ;
- le champ "Rôle 2" ;
- le champ "Code rôle 2" (ne peut contenir que des lettres, chiffres ou underscore). Le code doit être unique.

 ![Tableau des types de lien entre exigences](./resources/popup-ajout-lien-exi.jpg){class="centre"}   

Les champs "Code rôle 1" et "Code rôle 2" sont modifiables directement dans le tableau des liens entre exigences. Ces codes sont utilisés pour faire des imports/exports de données. 

Si des exigences sont liées par le type de lien supprimé, il sera remplacé par le type de lien par défaut. Il n’est pas possible de supprimer le type de lien défini "par défaut".