# Présentation de l’espace Administration de Squash

## La barre de navigation de l’espace Administration

L'espace Administration est accessible en cliquant sur un de ses sous-menus du menu Administration depuis la barre de navigation.

![sous-menus admin](./resources/accueilSquashFR.png){class="pleinepage"}

Au clic, la page de gestion correspondant au sous-menu s'affiche et la barre de navigation est remplacée par celle de l'espace Administration : 

![barre de navigation admin](./resources/userAdminFR.png){class="pleinepage"}

La croix présente au bas de la barre de navigation permet de quitter l'espace Administration.

## Les sous-menus de l'espace Administration

**Sous-menu Utilisateurs**

Espace consacré à la gestion des utilisateurs, des équipes d'utilisateurs ainsi qu'à la consultaton de l'historique des connexions.

**Sous-menu Projets**

Espace dédié à la gestion des projets et des modèles de projets.

**Sous-menu Jalons**

Espace destiné à la gestion des jalons.

**Sous-menu Entités**

Espace consacré à la gestion des champs personnalisés, des listes personnalisées et des liens entre exigences.

**Sous-menu Serveurs**

Espace dédié à la gestion des bugtrackers, serveurs de synchronisation, serveurs d'exécution automatisée et serveurs de partage de code source.

**Sous-menu Système**

Espace regroupant les informations, les paramètres système, le nettoyage des suites automatisées ainsi que le téléchargement des logs de l'outil.

!!! warning "Focus"
    Le menu Administration est accessible uniquement aux utilisateurs avec un profil 'Administrateur' ou 'Chef de projet'. 
    <br/>Un profil 'Chef de projet' n'accède qu'aux sous-menus Projets et Jalons. Les autres sous-menus sont réservés aux administrateurs.

## Les tableaux de l’Administration

Chaque page de gestion affiche un tableau :

![page de consultation espace admin](./resources/tableauAdminFR.png){class="pleinepage"}

En plus des informations présentes dans le tableau les éléments suivants s'affichent : 

- Une barre de recherche
- Des boutons de gestion (Ajouter, Supprimer...)
- Une pagination

Seule la page de gestion du sous-menu Système ne contient pas de tableau et se compose de plusieurs blocs.

## Les pages de consultation

Sur chaque tableau, au clic sur le nom, le login ou le numéro présent dans la première colonne, la page de consultation de l'élément sélectionné s'ouvre sur la droite de l'écran. 

![page de consultation espace admin](./resources/pageConsultationAdminFR.png){class="pleinepage"}


### Ancres et blocs

Au sein de la page de consultation, les informations de l'objet sélectionné sont affichées dans différents blocs escamotables, accessibles via des ancres. Les ancres d'un objet sont rangées par groupe fonctionnel. Elles permettent de naviguer soit vers un bloc présent dans la page, soit vers un bloc présent sur une autre page. Au survol d'une ancre, son infobulle affiche le titre du bloc associé. 

### Affichage étendu ou réduit

Par défaut, l'affichage est en mode réduit, c'est-à-dire que le tableau de gestion et la page de consultation s'affichent sur la même page. Il est possible de masquer le tableau afin d'étendre l'affichage de la page de consultation en cliquant sur le bouton **[<<]** ou de revenir à l'affichage réduit en cliquant sur le bouton **[>>]**.

!!! info "Info"
    Pour fermer la page de consultation d'un élément, cliquer sur le bouton [X] ou cliquer une seconde fois sur l'élément dans le tableau. 



