# Fonctionnalités transverses de l’administration

Les fonctionnalités décrites ci-dessous sont effectives sur la totalité des tableaux de gestion de l'espace Administration.

## Créer un objet

Au clic sur le bouton ![Ajouter](./resources/icone-ajouter.png){class="icone"}, situé au-dessus du tableau, il est possible d'ajouter un objet spécifique à la page de gestion consultée.

![bouton ajouter admin](./resources/ajouterObjetAdminFR.png){class="pleinepage"}

Le clic ouvre une popup de création. Il sera alors nécessaire de renseigner les champs obligatoires afin d'ajouter l'objet.

![popup ajouter admin](./resources/popupAjouterFR.png){class="pleintepage"}

Cette popup de création offre la possibilité d'ajouter des objets à la chaîne. En effet, réinitialisée au clic sur le bouton **[Ajouter un autre]** elle permet l'ajout rapide de plusieurs objets. À chaque ajout, le tableau se met à jour automatiquement.

## Modifier les champs d’un objet

Chaque objet de Squash TM est constitué de divers attributs. Au survol de la valeur d'un attribut, un indicateur visuel signale à l'utilisateur qu'elle est modifiable via une valeur soulignée, une icône crayon ou un cadre de couleur.

Pour modifier un attribut, il suffit de cliquer sur la valeur du champ, de renseigner une nouvelle valeur, puis de valider (bouton **[Confirmer]** ou touche **[Entrée]** du clavier).

Les différents types de champs présents dans l'application sont les suivants :

- Texte simple : texte sans mise en forme, sur une seule ligne
- Case à cocher
- Date : permet de sélectionner une date dans un calendrier ou de la saisir manuellement au format jj/mm/aaaa
- Liste déroulante
- Numérique
- Tag : permet de saisir une ou plusieurs valeurs sous forme d'étiquettes avec de l'autocomplétion
- Texte riche : texte multilignes avec de la mise en forme


![modifier champ admin](./resources/modifierChampAdminFR.png){class="pleinepage"}

## Supprimer un objet

La suppression d'objets dans les tableaux se fait de deux manières, soit en cliquant sur le bouton ![Supprimer](./resources/delete.png){class="icone"} présent en bout de ligne soit en utilisant le bouton de suppression massive présent au-dessus des tableaux, permetttant ainsi de supprimer simultanément plusieurs objets sélectionnés. 

![bouton suppression admin](./resources/boutonSuppressionFR.png){class="pleinepage"}

## Rechercher un objet

Les pages de gestion de l'espace Administration intègrent tous un champ permettant la recherche d'éléments dans les tableaux. Le contenu du tableau se met à jour automatiquement en fonction de la saisie dans le champ de recherche.

![champ recherche admin](./resources/rechercheAdminFR3.png){class="pleinepage"}

Il est possible de vider le contenu du champ de recherche en cliquant sur le bouton **[X]**.


## Fonctionnement des tables

Par défaut, un tri est appliqué aux tables et il est possible, en cliquant sur l'en-tête de la colonne, de modifier ce tri : la flèche orientée vers le haut indique un tri alphabétique ou chronologique ; la flèche orientée vers le bas indique un tri inverse.

![tri tableaux admin](./resources/triTableauAdminFR.png){class="pleinepage"}