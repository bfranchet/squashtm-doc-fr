# L'historique des connexions 

## Consulter l’historique des connexions 

L'historique de connexion est accessible depuis l'espace Administration, sous-menu "Utilisateurs", puis au clic sur l'ancre ![Historique](./resources/connection_history.png){class="icone"}

![historique connexion admin](./resources/pageHistoriqueFR.png){class="pleinepage"}

Des filtres peuvent être appliqués au niveau de l'en-tête des colonnes 'Login' et 'Date de connexion' pour permettre de cibler la recherche. Un tri est également possible sur chaque colonne.

![filtre historique connexion admin](./resources/filtreHistoriqueFR.png){class="pleinepage"}

Pour effacer les filtres, il suffit de cliquer sur le bouton **[Effacer les filtres]** présent au dessus du tableau.

## Exporter l’historique des connexions 

Au clic sur le bouton ![Exporter l'historiques de connexions](./resources/export.png){class="icone"} situé au-dessus du tableau, une popup s'ouvre dans laquelle on peut renseigner le nom du fichier à exporter. Ce champ est pré-rempli par défaut. Le fichier téléchargé est au format .CSV. 

![filtre historique connexion admin](./resources/popupHistorique.png){class="centre"}
