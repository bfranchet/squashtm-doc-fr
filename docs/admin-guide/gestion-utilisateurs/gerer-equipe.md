# Gérer une équipe

La gestion des équipes est accessible depuis l'espace Administration, sous-menu "Equipes", au clic sur l'ancre ![Ajouter](./resources/team.svg){class="icone"}.

## Ajouter, modifier, supprimer une équipe

La création d'une équipe permet d'attribuer une même habilitation à tous les membres qui la composent sur un ou plusieurs projets.

Depuis le tableau de gestion des équipes, il est possible d'ajouter, en cliquant sur le bouton ![Ajouter](./resources/iconeAjouter.png){class="icone"}, une ou plusieurs équipes en complétant au minimum le champ Nom dans la popup de création.


![popup création équipe admin](./resources/popupEquipeFR.png){class="centre"}

La suppression d'équipe peut se faire de façon unitaire ou en masse à l'aide des boutons ![Ajouter](./resources/iconePoubelle.png){class="icone"} présents en bout de ligne ou au dessus de chaque bloc. 

![suppression équipe admin](./resources/supprimerEquipesFR.png){class="pleinepage"}

En cliquant sur le numéro de ligne (#) ou le nom d'une équipe, sa page de consultation s'affiche, permettant sa modification. 

![page consultation équipe admin](./resources/consultationEquipeFR.png){class="pleinepage"}


Depuis la page de consultation d'une équipe, plusieurs actions sont possibles telles que:

- Mettre à jour les informations
- Gérer les membres de l'équipe
- Attribuer à l'équipe une habilitation sur un ou plusieurs projets
- Supprimer une équipe


Pour ajouter une habilitation ou un utilisateur à une équipe, il suffit de cliquer sur le bouton ![Ajouter](./resources/iconeAjouter.png){class="icone"} présent au dessus du tableau du bloc correspondant et de compléter les champs de la popup.

![ajouter habilitation equipe admin](./resources/ajouterHabilitationFR.png){class="pleinepage"}

La popup 'Ajouter des habilitations' est composée d'un champ 'Projets' permettant de sélectionner un ou plusieurs projets et d'un champ 'Profil' permettant de définir le niveau d'habilitation des membres de l'équipe sur les projets sélectionnés. 
Un utilisateur peut avoir des profils différents sur des projets différents. 

![popup habilitation equipe admin](./resources/popupHabilitationFR.png){class="centre"}

Un utilisateur peut appartenir à plusieurs équipes qui, elles-mêmes, peuvent avoir des droits différents sur un même projet. Dans ce cas, l’utilisateur possèdera pour ce projet le droit le plus permissif. 

*Exemple* :

- Un utilisateur appartient à l’équipe A ainsi qu’à l’équipe B.
- L’équipe A a la permission « invité » pour le projet Z.
- L’équipe B a la permission « testeur avancé » sur le même projet Z.

Lorsqu’il se connecte, cet utilisateur possède donc la permission « testeur avancé » pour le projet 


Il est possible de supprimer des habilitations ou des membres d'une équipe de facon unitaire ou en masse en cliquant sur le bouton ![Supprimer](./resources/iconeSupprimer.png){class="icone"}.

![supprimer habilitation équipe admin](./resources/supprimerHabilitationFR.png){class="pleinepage"}


!!! info "Info"
    La suppression d'une équipe n'entraîne pas la suppression des membres qui la composent. Néanmoins ces derniers perdront leurs habilitations sur les projets qui étaient associés à l'équipe.

