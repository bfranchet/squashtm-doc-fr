# Attribuer des habilitations

Un profil est attribué à un utilisateur ou une équipe d'utilisateurs pour un projet donné. C'est ce profil qui va définir les droits de l'utilisateur sur le projet auquel il a été habilité.

## Les profils utilisateurs dans Squash TM

Il existe 8 profils d'utilisateurs dans Squash TM qui disposent tous de droits différents :

- Chef de projet : Il s’agit de l’utilisateur en charge de la gestion du projet. Il a accès à l’administration du projet et des jalons.
- Testeur référent : Il a tous les droits de création/modification/suppression sur les espaces fonctionnels du projet.
- Designer de tests : C’est le profil dédié à la rédaction de cas de test, il ne peut pas exécuter les cas de test.
- Testeur avancé : Il ne peut pas créer d'exigences mais il peut modifier des cas de test existants et les exécuter.
- Testeur : Il ne peut pas créer ou modifier d’exigences ou de cas de test. Il ne peut exécuter que les tests qui lui sont assignés.
- Valideur : Il peut modifier les attributs des exigences et des cas de test afin de les valider et consulter l’exécution des tests.
- Invité : Il ne dispose que de droits de lecture sur le projet.
- Automaticien : Il n’a des droits que sur l’espace Automatisation dédié à l'automaticien.


## Ajouter une habilitation à un utilisateur ou une équipe

Pour ajouter une habilitation à un utilisateur ou une équipe, il suffit de cliquer sur le bouton ![Bouton Ajouter](./resources/iconeAjouter.png){class="icone"} présent dans le bloc 'Habilitations' sur la page de consultation d'un utilisateur ou d'une équipe.

![Ajouter habilitation](./resources/ajouterHabilitationFR2.png){class="pleinepage"}

Dans la popup 'Ajouter des habilitations', il est possible de sélectionner un ou plusieurs projets puis de définir un profil à l'utilisateur ou l'équipe sur l'ensemble des projets choisis. 
<br/>Pour retirer une ou plusieurs habilitations à un utilisateur ou une équipe, cliquer les boutons **[-]** présents en bout ligne ou au-dessus du bloc 'Habilitations'.

!!! info "Info"
    Si un utilisateur est membre de plusieurs équipes avec des habilitations différentes sur plusieurs projets, alors l'utilisateur en question se voit attribuer un cumul de droits sur ces différents projets.

!!! warning "Focus"
    Si un utilisateur dispose de profils différents sur le même projet dû au cumul de ses habilitations et de celles des équipes auxquelles il est associé, le profil disposant du plus de droits sur le projet prévaut sur les autres.