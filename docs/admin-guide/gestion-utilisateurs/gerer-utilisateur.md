# Gérer un utilisateur

La gestion des utilisateurs est accessible depuis l'espace Administration, sous-menu "Utillisateurs", puis au clic sur l'ancre ![Ajouter](./resources/ancreUtilisateursAdmin.png){class="icone"}


![page utilisateur admin](./resources/pageUtilisateursAdminFR.png){class="centre"}


## Ajouter, modifier, supprimer un utilisateur

Depuis le tableau de gestion des utilisateurs, il est possible d'ajouter ![Ajouter](./resources/iconeAjouter.png){class="icone"} ou de supprimer ![Ajouter](./resources/iconePoubelle.png){class="icone"} des utilisateurs de façon unitaire ou en masse.

Lors de la création d'un nouvel utilisateur, le choix du groupe définit le niveau d'habilitation de ce dernier :

- L'Administrateur : dispose de tous les droits sur tous les projets de l'instance.
- Le Serveur d'automatisation de tests : sert uniquement à l'exécution des tests automatisés depuis Squash.
- L'Utilisateur : dispose de droits restreints, ils sont limités par projet et par espace en fonction des habilitations qui lui sont accordées.

![champ groupe popup admin](./resources/choixUtilisateursAdminFR.png){class="centre"}

Pour créer un utilisateur, les champs suivants doivent obligatoirement être renseignés :

- Login
- Nom
- Mot de passe (au moins 6 caractères)
- Confirmation du mot de passe

En cliquant sur le numéro de ligne (#) ou le nom d'un utilisateur, sa page de consultation s'affiche, permettant sa modification. 

![modifier champ utilisateur admin](./resources/modifierChampAdminFR.png){class="pleinepage"}

Depuis la page de consultation d'un utilisateur, plusieurs actions sont possibles:

- Activer/Désactiver l'utilisateur
- Réinitialiser le mot de passe
- Mettre à jour les données de l'utilisateur
- Changer le groupe de l'utilisateur
- Attribuer des habilitations sur les projets
- Affecter l'utilisateur à des équipes
- Supprimer un utilisateur

Pour ajouter une habilitation ou une équipe à un utilisateur, il suffit de cliquer sur le bouton ![Ajouter](./resources/iconeAjouter.png){class="icone"} présent au dessus du tableau du bloc correspondant et de compléter les champs de la popup.

![ajouter equipe admin](./resources/ajouterEquipeFR.png){class="pleinepage"}


La suppression d'un utilisateur entraîne sa suppression de la liste des membres d'une équipe et empêche ce dernier de se connecter sur Squash TM. En revanche, les objets créés par un utilisateur supprimé sont conservés.

![supprimer utilisateur admin](./resources/supprimerUtilisateur.png){class="pleinepage"}

## Activer/Désactiver un utilisateur

Pour désactiver un utilisateur, il suffit de cliquer sur le bouton **[Désactiver]**. 
L'utilisateur non actif n'est pas supprimé mais il n'est plus en mesure de se connecter.

![désactiver utilisateur admin](./resources/desactiverUtilisateurFR.png){class="pleinepage"}

Pour le réactiver, il suffit de cliquer sur le bouton **[Activer]** ou sur le bouton switch 'Etat' du bloc 'Informations' de la page de consultation de l'utilisateur.