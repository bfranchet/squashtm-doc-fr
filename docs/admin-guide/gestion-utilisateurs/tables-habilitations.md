# Les tables des habilitations 

Ci-dessous les tables des habilitations par espace et par profil : 

## Espace Exigences

![Habilitations de l'espace Exigences](./resources/hab-exigences.png){class="centre"}

## Espace Cas de test

![Habilitations de l'espace Cas de test](./resources/hab-ct.png){class="centre"}

## Espace Campagnes

![Habilitations de l'espace Campagnes](./resources/hab-campagnes.png){class="centre"}

## Espace Pilotage

![Habilitations de l'espace Pilotage](./resources/pilotage-hab.png){class="centre"}

## Espace Automatisation

![Habilitations de l'espace Automatisation](./resources/hab-automatisation.png){class="centre"}

## Espace Bibliothèque d'actions

![Habilitations de l'espace Bibliothèque d'actions](./resources/hab-bibli-actions.png){class="centre"}

## Espace Administration

![Habilitations de l'espace Administration](./resources/administration-hab.png){class="centre"}





