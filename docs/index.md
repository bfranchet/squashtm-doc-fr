# Bienvenue sur la documentation de Squash TM

## A propos de Squash TM
Squash est une suite d’outils pour concevoir, automatiser, exécuter et industrialiser vos tests.
Basée sur un socle open source, la solution est modulable et facilement intégrable.

Au sein de cette suite, Squash TM est une application full web qui permet de gérer et piloter les tests en contexte agile et traditionnel.

![La suite Squash](resources/suite_squash.png){class=pleinepage}

## Contenu de ce site

Ce site contient les ressources suivantes :

<!-- - [Par où commencer](getting-started/getting-started.md) : donne la marche à suivre pour déployer et utiliser Squash TM dans des contextes précis -->
- [Guides de références](introduction.md) : documentation de l'outil (guide d'installation, guide administrateur et guide utilisateur)
<!-- - [Tutoriaux](tuto/tuto.md) : guide l'utilisateur étape par étape pour utiliser certaines fonctionnalités -->
- [Release notes](release-notes/squash/squash-tm-2.x.md) : liste des évolutions et des corrections de chaque version de Squash TM et de ses plugins
- [FAQ](faq/faq-squash-tm-2.0.md)