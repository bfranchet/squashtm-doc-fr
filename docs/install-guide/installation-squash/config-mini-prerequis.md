# Configuration minimale et prérequis 

## Dimensionnement minimal et recommandé

Pour l'installation et le déploiement de Squash TM, le tableau ci-dessous indique le dimensionnement minimal et celui recommandé :

|  | CPU | RAM | HDD | OS | Navigateur |
|--|--|--|--|--|--|
| Minimum (pour essai) | Mono-core | 1 Go dédié | 1 Go | Linux, Windows, Mac | Firefox, Chrome |
 Recommandé | Bi-core | 2 Go dédiés | 5 Go | Linux | Firefox ou Chrome |

L’espace disque (HDD) servira à stocker les journaux applicatifs (logs) et la base de données si cette dernière est sur le même serveur.
L’application en elle-même et ses fichiers de configuration pèsent 130 Mo. Pour l'espace disque minimum, il faut compter 200 Mo si Squash TM est stocké avec ses logs. Mais si la machine contient également le java et la base de données Squash TM, il faut compter 1 Go pour l'espace disque à minima. 

!!! warning "Focus"
    Ces éléments sont donnés à titre indicatif et ne sauraient remplacer une étude complète prenant en compte le contexte cible.

## Prérequis

Tableau des versions des intergiciels compatibles avec Squash TM : 

|  | Minimal pour environnement de production | Recommandé |
|--|--|--|
| Système d’exploitation | Tout système Linux ou Windows | Debian 11, Centos 7 |
| Java | JVM version 8 LTS | JVM version 11 |
| Base de données | Postgresql 9.6+, Mariadb 10.2 | Potgresql 13, Mariadb 10.5 |

Précisions sur les intergiciels.

- Système d’exploitation : tout système en mesure d’exécuter une JVM.
- Mise en service/daemon de l’application : un fichier de service systemd est fourni pour les systèmes Linux compatibles et un script Windows créant le service est fourni également.
- Machine virtuelle : Une JVM Hotspot est suffisante pour exécuter Squash TM. La version 8 est compatible mais du fait de sa fin de vie proche, une version 11 est recommandée.
Squash TM fonctionne très bien avec une JDK.
<br/>OpenJ9 fonctionne avec Squash TM et consomme moins de mémoire vive, cependant la compatibilité avec certains plugins n’est pas assurée. **L’usage d’une JVM OpenJ9 n’est pas supporté par Henix bien que compatible dans certains cas.**
<br/>Henix utilise principalement la JVM Hotspot distribuée par Oracle.
- Serveur applicatif : 
Aucun serveur applicatif n’est nécessaire, Squash TM embarquant son propre Apache Tomcat.
Il est possible de déployer Squash TM dans un serveur applicatif propre mais Henix ne recommande pas cette méthode.
- Base de données : 
    - Henix recommande l’usage de PostgreSQL 11. La version minimale de postgresql compatible avec Squash TM est la version 9.6 minimum.
    - Henix préconise aussi l’usage de Mariadb. **Mariadb 10.1 et les versions antérieures ne sont pas compatibles avec Squash TM, c’est pourquoi il est nécessaire d’utiliser Mariadb 10.2 à minima.**
    - **A partir de la version 1.21 de Squash TM, MySQL n’est plus officiellement supportée. Pour les versions antérieures de Squash TM, MySQL est supportée dans les versions 5.7.17 à 5.7.x (les versions antérieures et postérieures ne sont pas ou plus supportées par l’applicatif).**

!!! warning "Focus"
    Squash TM est livré avec une base embarquée (H2) utilisable à des fins d’évaluation. Son utilisation est déconseillée dans un contexte de production.
    <br/>Consulter la page [Installation de la base de données](./installation-bdd.md) pour savoir comment utiliser une base de données autre que H2.

- Autres composants recommandés :
    - Mantis 2.x+ ou Jira 8
    - Apache HTTPD (frontal web) dans sa dernière version


## Architecture possible

Pour des faibles volumétries et un nombre contenu d’utilisateurs (moins de 50), Henix préconise une architecture « trois-tiers » telle que sur une machine virtuelle / conteneur, on installe les trois services :

-	Application
-	SGBD
-	Frontal web

Pour un déploiement dans une infrastructure mutualisée, il est tout à fait possible d’avoir les trois services sur trois systèmes différents et de les interconnecter via le réseau.

Le mandataire inverse (reverse proxy) est facultatif mais nous recommandons son usage afin de séparer les logs d’accès des logs applicatifs d’une part, et pour confier le chiffrement TLS à un autre processus/service que l’application Squash TM bien que le serveur Tomcat embarqué en soit capable.

Il est fortement recommandé quelque soit la volumétrie d'avoir la base de données et l'application Squash TM sur la même machine pour ne pas ralentir les performances avec des processus supplémentaires.

## Recommandations si utilisation de Squash Autom

L'installation et le déploiement de Squash TM ne nécessitent pas de prérequis particulier dans le cas d'une utilisation avec l'orchestrateur Squash Autom :

- Pour utiliser l'orchestrateur Squash Autom avec Squash TM, il suffit d'installer les plugins nécessaires

!!! tip "En savoir plus"
    Pour en savoir plus sur les plugins Autom consulter la page [Les plugins de Squash TM](../installation-plugins/plugins-squash.md)

- Concernant le dimensionnement notamment de la base de données, cela va dépendre de la quantité et de la fréquence des tests automatisés remontés dans Squash TM. Une fonctionnalité est présente dans Squash TM pour réguler le volume des exécutions automatisées : [le nettoyage des suites automatisées](../../admin-guide/gestion-systeme/nettoyage-suites-autos.md)
- L'orchestrateur est packagé sous la forme d'une image Docker mais les échanges avec Squash TM se font via des appels REST il n'est donc pas nécessaire que Squash TM soit également installé en Docker.
