# Installation de la base de données 

Squash TM est livré avec une base de données H2 par défaut. Cette base utilisable à des fins d'évaluation est fortement déconseillée dans un contexte de production. 

Squash TM est compatible avec deux SGBD Open Source : PostgreSQL et MariaDB. 
<br/>Voici les versions préconnisées :

- PostgreSQL 11. La version minimale de PostgreSQL compatible avec Squash TM est la version 9.6.
- MariaDB 10.5. La version minimale de MariaBD compatible avec Squash TM est la version 10.2.

## PostgreSQL

Pour créer une **nouvelle** base de données en UTF8 pour l'application Squash TM passer les requêtes suivantes avec un utilisateur ayant les pleins pouvoirs :

    CREATE DATABASE squashtm WITH ENCODING='UTF8';
    CREATE USER "squash-tm" WITH PASSWORD 'password';
    GRANT ALL PRIVILEGES ON DATABASE squashtm TO "squash-tm";
    GRANT CONNECT ON DATABASE squashtm TO postgres;

Modifier le rôle de l'utilisateur "squash-tm" car la base de données de Squash TM nécessite l'installation de l'extension "uuid-ossp", action possible uniquement pour un superuser :

    Alter ROLE "squash-tm" with SUPERUSER;

Puis exécuter le script de peuplement de la base de données avec l'utilisateur "squash-tm". Ce script se trouve dans l'archive Squash TM dans le répertoire *database-scripts* : 

- *postgresql-full-install-version-2.X.X.RELEASE.sql*

Une fois le script exécuté, il faut retirer à l'utilisateur "squash-tm" le rôle de superuser :

    Alter ROLE "squash-tm" with NOSUPERUSER;

!!! warning "Focus"
    Pour les bases de données PostgreSQL, les actions sur la base de données telles que les sauvegardes, les restaurations et les passages de scripts sont à effectuer avec l’utilisateur 'squash-tm' et non avec l’utilisateur 'postgres'. <br/>S'assurer que l'utilisateur 'squash-tm' dispose des droits superutilisateur lors du passage des scripts et des sauvegardes.

!!! info "Info"
    La suppression de large object (comme les pièces jointes) dans PostgreSQL ne supprime pas les données (seulement le lien vers les données). 
    <br/>Pour supprimer les données, il est nécessaire d’utiliser la fonction vacuumlo (voir la [documentation](https://docs.postgresql.fr/10/vacuumlo.html) de PostgreSQL) pour nettoyer la base de données. 

## MariaDB

Pour créer une **nouvelle** base de données en UTF8 pour l'application Squash TM, passer les requêtes suivantes avec un utilisateur ayant les pleins pouvoirs :

    CREATE DATABASE IF NOT EXISTS squashtm CHARACTER SET utf8 COLLATE utf8_bin;
    CREATE USER 'squash-tm'@'localhost' IDENTIFIED BY 'password';
    GRANT ALL ON squashtm.* TO 'squash-tm'@'localhost';
    FLUSH PRIVILEGES;

Puis exécuter le script de peuplement de la base de données avec l'utilisateur 'squash-tm'@'localhost'. Ce script se trouve dans l'archive Squash TM dans le répertoire *database-scripts* : 

- *mysql-full-install-version-2.X.X.RELEASE.sql*

!!! danger "Attention"
    Utiliser impérativement **InnoDB**. Le moteur de la base de données doit impérativement être un moteur transactionnel. 
    <br/>Le moteur MyISAM par exemple n’est pas transactionnel et risque de corrompre vos données. Son utilisation est déconseillée et Henix ne saurait être responsable des anomalies engendrées par son utilisation.

!!! info "Info"
    Configuration de MariaDB

    La variable @@sql_mode doit contenir les tags suivants :

        NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES

    Et ne doit pas contenir les éléments suivants :
    
        ONLY_FULL_GROUP_BY
        EMPTY_STRING_IS_NULL
        NO_BACKSLASH_ESCAPES
        
    Les variables doivent être définies dans le fichier de configuration my.ini, dans la section [mysqld].
