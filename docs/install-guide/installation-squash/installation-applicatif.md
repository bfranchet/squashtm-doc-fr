# Installation de l'applicatif

L'application Squash TM est livrée sous différents packages :

- un installeur Windows (.jar) qui permet une installation rapide à des fins de démonstration
- un package universel compatible Windows, Mac, Linux (.zip ou .tar.gz)
- une image Docker

En version 1.X de Squash TM un paquet Debian (.deb) et Redhat (.rpm) étaient disponibles mais ces derniers sont aujourd'hui dépréciés. Une [procédure](#passage-dune-installation-packagee-1x-en-tarball-targz) indique comment basculer en tarball (tar.gz). 

!!! danger "Attention"
    Il est **IMPERATIF** d’installer une JRE version 8 ou 11 avant toute installation de Squash TM quel que soit l’environnement et quel que soit l’installeur.

## Installation avec le tarball Linux

Pour installer Squash TM avec le tarball Linux :

1. Décompresser l’archive .tar.gz de Squash TM dans */opt*

        tar -zxvf archivexsquashtm.tar.gz

2. Créer un utilisateur et un groupe dédié à Squash TM

        adduser --system --group --home /opt/squash-tm squash-tm

3. Définir squash-tm propriétaire de son dossier et ses fichiers

        chown -R squash-tm:squash-tm /opt/squash-tm

4. Peupler la base de données grâce aux scripts présents dans : 
  
        /opt/squash-tm/database-scripts 

    S'il s'agit d'une [première installation](./installation-bdd.md) passer le script *<database\>-full-install-version-2.X.X.RELEASE.sql* correspondant au SGBD portant le numéro de version de Squash TM. 
    <br/>S'il s'agit d'une [montée de version](../montee-version/procedure-montee-version.md), passer les scripts d'upgrade *<database\>-upgrade-to-X.X.X.sql* correspondants au SGBD dans l'ordre des versions jusqu'à atteindre la version cible.

5. Rendre *startup.sh* exécutable

        chmod +x /opt/squash-tm/bin/startup.sh

6. Renseigner dans le fichier *bin/startup.sh* les informations de connexion à la base de données :

        DB_URL="jdbc:mysql://localhost:3306/squashtm" ou "jdbc:postgresql://localhost:5432/squashtm"
        DB_TYPE="mysql" ou "postgresql"
        DB_USERNAME="squash-tm"
        DB_PASSWORD="password"

7.  Démarrer squash-tm via
        
        cd /opt/squash-tm/bin
        nohup ./startup.sh &

8. Faire Ctrl + C pour reprendre le contrôle sur le terminal.

## Installation du service systemd sous Debian

Pour installer Squash TM comme service systemd sous Debian :

1. Copier le fichier de service systemd qui est dans /opt/squash-tm.

        cp /opt/squash-tm/squash-tm.service /etc/systemd/system/

2. Puis recharger les services :

        systemctl daemon-reload

3. Faire en sorte que Squash-tm démarre avec le système :

        systemctl enable squash-tm

4. Et enfin démarrer le service :
        
        systemctl start squash-tm

Il n’y a pas de fichier de service dans *squash-tm-2.X.X.RELEASE.tar.gz*. Il faut donc créer le fichier de service squash-tm.service avec le contenu qui suit :

    [Unit]
    Description=Squash-tm daemon
    After=systemd-user-sessions.service time-sync.target

    [Service]
    WorkingDirectory=/opt/squash-tm/bin
    ExecStart=/opt/squash-tm/bin/startup.sh
    ExecStop=/bin/kill $MAINPID
    KillMode=process
    Type=simple
    User=squash-tm
    Group=squash-tm
    Restart=on-failure
    RestartSec=10
    StartLimitInterval=120
    StartLimitBurst=3
    StandardOutput=null
    StandardError=null

    [Install]
    WantedBy=multi-user.target

## Installation standard via le fichier zip en tant que service Windows 

Pour installer Squash TM en tant que service Windows :

1. Installer Squash TM : décompresser l’archive .zip et déplacer le contenu à l’emplacement souhaité (pour la suite *c:\<rep\>* sera utilisé). Attention : le processus Windows associé à Squash TM doit posséder les droits de lecture et d’écriture sur l’emplacement d’installation.
2. Peupler la base de données grâce aux scripts présents dans : 

        c:\<rep>\squash-tm\database-scripts 

    S'il s'agit d'une [première installation](./installation-bdd.md) passer le script *<database\>-full-install-version-2.X.X.RELEASE.sql* correspondant au SGBD portant le numéro de version de Squash TM. 
    <br/>S'il s'agit d'une [montée de version](../montee-version/procedure-montee-version.md), passer les scripts d'upgrade *<database\>-upgrade-to-X.X.X.sql* correspondants au SGBD dans l'ordre des versions jusqu'à atteindre la version cible.

3. Renseigner dans le fichier *startup.bat* les informations de connexion à la base de données

        DB_URL="jdbc:mysql://localhost:3306/squashtm" ou "jdbc:postgresql://localhost:5432/squashtm"
        DB_TYPE="mysql" ou "postgresql"
        DB_USERNAME="squash-tm"
        DB_PASSWORD="password"

4. Installer le service Squash TM via :

        c:\<rep>\Squash-TM\bin\squash-tm.exe install

!!! info "Info"
    L’installeur Windows est livré à des fins d’évaluation et son utilisation dans un contexte de production est déconseillée.
    Son installation étant guidée via un installeur, il ne sera pas décrit ici.


## Installation avec l'image Docker

Pour installer Squash TM avec l'image Docker consulter la documentation spécifique sur [https://hub.docker.com/r/squashtest/squash-tm](https://hub.docker.com/r/squashtest/squash-tm).

## Passage d'une installation packagée (1.X) en tarball (tar.gz)

Les distributions packagées Redhat et Debian de Squash TM ne sont plus supportées en 2.X+. Cette section décrit la procédure à suivre pour basculer d'une installation packagée en version 1.X de Squash TM à une installation en tarball en version 2.X.

!!! warning "Focus"
    Avant de commencer la procédure, bien sauvegarder tous les binaires, fichiers de configuration, plugins, licences, base de données de Squash TM

Cette documentation est valable pour du Debian/Ubuntu ou Red Hat/CentOS, s'il y a des différences, celles-ci seront clairement notées.

### Sauvegarde des éléments avant migration

Avant de procéder au transfert, il faut d'abord créer un répertoire de backup

```sh
mkdir -p /opt/squash-bckp/plugins
mkdir -p /opt/squash-bckp/conf
mkdir -p /opt/squash-bckp/ident
```

Pour Debian/Ubuntu :
  
```sh
cp -r /usr/share/squash-tm/plugins/*  /opt/squash-bckp/plugins
cp -r /etc/squash-tm/* /opt/squash-bckp/conf
cp /etc/default/squash-tm /opt/squash-bckp/ident
```

Pour Red Hat/CentOS :

```sh
cp -r /usr/lib/squash-tm/plugins/*  /opt/squash-bckp/plugins
cp -r /etc/squash-tm/* /opt/squash-bckp/conf
cp /etc/sysconfig/squash-tm /opt/squash-bckp/ident
```

### Suppression de l'ancienne installation

!!! danger "Attention"
    Si la désinstallation de la base de données est proposée, choisir "No".

Pour Debian/Ubuntu :

```sh
apt purge squash-tm
```

Pour Red Hat/CentOS :

```sh
yum remove squash-tm
```

### Installation

!!! info "Info"
    Dans ce qui suit, il est supposé que l'archive de Squash TM de la 2.X a été téléchargée dans */opt*

```sh
cd /opt
tar -zxvf squash-tmXXXX.tar.gz
chmod +x squash-tm/bin/startup.sh
```

### Reprise de l'ancienne installation

#### Configuration

Si les fichiers de configuration n'ont pas été modifiés, cette section peut être passée, sinon il faut éditer chaque fichier qui se trouve dans */opt/squash-bckp/conf*, reprendre les anciens paramètres de configuration et les reporter dans les fichiers se trouvant dans */opt/squash-tm/conf/*

#### Connexion à la base de données

Pour récupérer la connexion à la base de données :

* Éditer */opt/squash-bckp/ident/squash-tm*
* Reprendre les valeurs des variables :
    * DB_TYPE
    * DB_URL
    * DB_USERNAME
    * DB_PASSWORD
* Les reporter dans le fichier */opt/squash-tm/bin/startup.sh*

!!! warning "Focus"
    Ne pas oublier de passer les scripts d'upgrade sur la base de données pour faire la montée de version du schéma en 2.X.

#### Licence

Pour récupérer le fichier de licence de Squash TM : 

```sh
cp -ar /opt/squash-bckp/plugins/license /opt/squash-tm/plugins
```

#### Plugins

Les plugins évoluent entre la V1 et la V2 de Squash TM, il n'est donc pas possible de reprendre les anciens plugins en faisant un simple copier/coller :

* Lister les plugins/jar se trouvant dans */opt/squash-bckp/plugins/*
* En cas de plugin inclus dans la distrubution de Squash TM, la mise à jour est déjà faite dans */opt/squash-tm/plugins/*
* En cas de plugins gratuits, télécharger la mise à jour sur le [site](https://www.squashtest.com/community-download) de Squash TM
* En cas de plugins commerciaux, se rapprocher du Support Squash

### Service sous systemd

* Créer le fichier de service dans */etc/systemd/system/squash-tm.service*
* Le remplir avec cet exemple (à modifier si l'installation ne se fait pas dans */opt*)

```sh
[Unit]
Description=Squash-TM daemon
After=systemd-user-sessions.service time-sync.target

[Service]
WorkingDirectory=/opt/squash-tm/bin
ExecStart=/opt/squash-tm/bin/startup.sh
ExecStop=/bin/kill $MAINPID
KillMode=process
Type=simple
#Restart=on-failure
User=squash-tm
Group=squash-tm

[Install]
WantedBy=multi-user.target
```

* Recharger les services

```sh
systemctl daemon-reload
```

* Mettre à jour la base de données

Les scripts de mise à jour se trouvent dans */opt/squash-tm/database-scripts/*

* Changer l'utilisateur de Squash-TM

```sh
useradd squash-tm
chown -R squash-tm: /opt/squash-tm/
```

* Supprimer le repository

    * Pour Debian/Ubuntu
  
    ```sh
    rm /etc/apt/sources.list.d/squash-tm.list*
    ```

    * Pour Red Hat/CentOS

    ```sh
    rm /etc/yum.repos.d/squashtest.repo*
    ```

* Lancer Squash-tm

```sh
systemctl enable squash-tm
systemctl start squash-tm
```

Pour vérifier le bon déroulement du démarrage de Squash TM regarder les logs dans */opt/squash-tm/logs/squash-tm.log*.
