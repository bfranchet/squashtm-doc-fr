# Externalisation des fichiers de langues

Cette partie présente le fonctionnement de l’externalisation des fichiers de langues utilisés pour l’internationalisation de Squash TM et de ses différents plugins.

## Fonctionnement des fichiers de langues

Historiquement les fichiers de langues utilisés pour l’internationalisation de Squash TM sont des fichiers avec l’extension .properties.
Il y a un fichier 'principal' *nom_du_fichier.properties* puis un fichier pour chaque langue qui est nommé *nom_du_fichier_locale.properties*. 

La valeur « locale » doit être remplacée par le code correspondant à la langue, par exemple *en* pour l’anglais ou *fr* pour le français.
Ces fichiers sont composés d’associations clé/valeurs pour chaque traduction à effectuer. 

_Exemple de fichier :_

    report.name = SampleReport
    report.name = ThisisaSampleReport,thatissuppliedwiththearchetypeasanexampleforyourowndevelopments.

Ces fichiers de langues sont utilisés pour que Squash TM s’affiche dans la langue sélectionnée depuis le navigateur. 
Depuis la version 1.11 de Squash TM , il est possible d’externaliser la gestion des fichiers de langues. Il est donc possible d’ajouter de nouvelles langues et de modifier les langues existantes.

## Fichiers de langues externalisés

Pour faciliter le changement de tout ou partie des fichiers de langues, il est possible de redéfinir les fichiers de langue dans le répertoire conf/lang de squash-tm.

_Exemple d’arborescence :_

![Fichiers de langues externalisés](resources/fichiers-langue-externes.png){class="centre"}

Ces fichiers doivent être placés dans un sous-répertoire ayant le nom du plugin et le nom des fichiers de langues seront dépendants du plugin. Dans le tableau ci-dessous est récapitulé le nom des répertoires et des fichiers de langues à utiliser :

| Plugin | Nom du sous-répertoire | Nom du fichier de langue |
|--|--|--|
| Redmine | plugin.bugtracker.redmine | messages |
| Bugzilla | plugin.bugtracker.bugzilla | bugzilla |
| Jira BT Server | henix-tm-jira-rest-connector | jira-rest |
| Report Qualitative coverage | squash-tm-report-qualitativecoverage | messages |
| Report requirements | reports.books.requirements | messages |
| Report testcases | reports.books.testcases | messages |
| Squash TM | plugin.bugtracker.mantis | mantis-bugmessages |
| Report std | Report std | messages |
| Report test | plugin.report.test | messages |
| Workspacewizard test | plugin.workspacewizard.test | messages |

- Ces fichiers de langues externalisés sont optionnels. En l’absence de ces fichiers, ce sont les fichiers de langues définis en interne qui seront utilisés.
- De plus, il n’est pas nécessaire de redéfinir toutes les clés, si des clés sont absentes du fichier externe, ce sera la clé du fichier interne qui sera utilisée.
- Il est possible d’ajouter de nouvelles langues à Squash TM ou à ses plugins, pour ce faire il suffit d’ajouter le fichier nom_du_fichier_locale.properties. Par exemple, pour traduire Squash TM en italien, il suffit de rajouter dans le dossier conf/lang/core le fichier messages_it.properties et bien sûr de traduire chaque clé en italien.

!!! info "Info"
    Contacter le référent Squash TM de votre organisation pour obtenir la liste des fichiers de langues à modifier ou à traduire.


## Les fichiers de langues en 2.0


En 2.0, les fichiers de langues .properties existent encore mais sont dorénavant uniquement utilisés pour des messages assez spécifiques (certains messages backend). En effet, le front ayant changé avec Angular, il y a de nouveaux fichiers au **format json** pour les messages du front, ce qui change complètement la syntaxe. Il y a déjà des fichiers custom (vides) pour chacune des langues supportées (de, es, en, fr) dans le dossier *conf/lang.*

_Voici un exemple de message dans *conf/lang/core/messages_fr.properties* avec l'ancien fichier :_

    label.Actions

_Voici un exemple de message dans *conf/custom_translations_fr.json* avec le nouveau fichier :_

    {
        "sqtm-core": {
            "action-word-workspace": {
                "label": {
                    "short": "Actions"
                }
            }
        }
    }

!!! info "Info"
    Il n'y a malheureusement aucun moyen de migrer des clés de l'ancien format au nouveau format car les clés des messages ont changé.
