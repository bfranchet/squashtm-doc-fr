# Utilisation du HTTPS

La mise en place d’une connexion sécurisée HTTPS sur Squash TM peut se faire de deux façons : d’une part par la mise en place d’un reverse proxy ou d’autre part via un paramétrage dans Squash TM. L’option recommandée par Henix est la mise en place d’un reverse proxy.

## Mandataire inverse (Reverse proxy) 

Pour utiliser une connexion HTTPS, Henix préconise l'utilisation d'un reverse proxy Apache HTTPD installé sur le serveur qui héberge l'application Squash TM. Henix recommande la branche 2.4 du serveur apache avec mod_proxy et mod_rewrite configurés pour forcer la connexion https.

Voici un exemple à adapter : 

    <VirtualHost *:443>
    SSLEngine on
    SSLProxyEngine on
    ServerName myhost.mydomain.com
    ErrorLog ${APACHE_LOG_DIR}/myhost_error.log
    DocumentRoot /var/www
                # Possible values include: debug, info, notice, warn, error, crit,
                # alert, emerg.
    LogLevel warn
    CustomLog ${APACHE_LOG_DIR}/myhost_access.log combined
    SSLCertificateFile    /etc/ssl/mon-certificat-serveur.crt
    SSLCertificateKeyFile /etc/ssl/private/ma-clef-privee.key

    <IfModulemod_proxy_http.c>
    ProxyPreserveHost On
    ProxyPass /squash http://localhost:8080/squash
    ProxyPassReverse /squash http://localhost:8080/squash
    </ifModule>
    </VirtualHost>

Si certaines URL de Squash persistent en http (Espace Exigences, URL dans les API ou dans le champ description des anomalies), il faut forcer leur réecriture en suivant l'exemple ci-dessous : 

    <IfModulemod_rewrite.c>
    RewriteLog rewrite.log
    RewriteLogLevel 0
    <IfModulemod_ssl.c>
    <Location />
    RewriteEngine on
    RewriteCond %{HTTPS} !^on$ [NC]
    RewriteCond %{HTTP_HOST} (^.*)$ [NC]
    RewriteRule . https://%{HTTP_HOST}%{REQUEST_URI}  [L]
    </Location>
    </IfModule>
    </IfModule>


!!! info "Info"
    Si un plugin est installé sur Squash TM pour réaliser une connexion avec un outil configuré en https, il faut autoriser les flux https entre le serveur Squash TM et le serveur de l'outil tiers. Pour ce faire, il faut enregistrer le certificat de l'outil tiers dans le truststore de la JVM de Squash TM. Consulter la page [Connexion à un outil tiers en https](../installation-plugins/configurer-plugins-bt-outils-tiers.md#connexion-a-un-outil-tiers-en-https) pour en savoir plus.
    
## Activer le https sur Squash TM sans Reverse Proxy

Pour activer le https directement dans le tomcat embarqué de Squash TM, il faut ajouter les informations suivantes dans le fichier *‘conf/squash.tm.cfg.properties’* :

    server.ssl.key-store=<chemin du keystore>
    server.ssl.key-store-password=<mot de passe du keystore>
    server.ssl.key-password=<mot de passe du certificat serveur>
    server.ssl.key-alias=<mot de passe>

Dans le fichier *bin\startup.sh*, il faut positionner la variable HTTP_PORT de la manière suivante :

    HTTP_PORT=8443

!!! warning "Focus"
    Une fois ce paramétrage effectué, l'application ne fonctionne qu'en https c'est-à-dire que si l'utilisateur saisit http://..., l’URL ne sera pas redirigée automatiquement.

!!! danger "Attention"
    Il faut au préalable créer un keystore.
    Pour générer/manipuler un keystore au format JKS (Java KeyStore), les commandes se trouvent [ici](http://www.lmhproductions.com/37/common-java-keytool-commands/).

## Connecter Squash à une BDD PostgreSQL en mode SSL

Pour que Squash TM se connecte en mode SSL à une base de données PostgreSQL, il faut ajouter la mention *?sslmode=require* à la fin de la ligne *DB_URL* du fichier de démarrage *bin/startup* de Squash TM quelle que soit l'installation.

    DB_URL="jdbc:postgresql://localhost:5432/squashtm?sslmode=require"
    DB_TYPE="postgresql"
    DB_USERNAME="squash-tm"
    DB_PASSWORD="password"
