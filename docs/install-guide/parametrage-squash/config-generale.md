# Configuration générale de Squash TM

Cette section traite de la configuration générale de Squash TM et plus particulièrement de la gestion du port d'accès à Squash TM et des propriétés contenues dans le fichier de configuration *squash.tm.cfg.properties*.

## Changer le port d’accès à Squash TM

Le port par défaut du serveur est modifiable dans le fichier *\bin\startup*. Rechercher la ligne suivante et remplacer le port 8080 par celui souhaité :

    [...]
    set HTTP_PORT=8080 
    [...]

!!! info "Info"
    Noter que le système ne vérifiera pas si le port choisi est disponible, il faut donc s'assurer auprès de l'administrateur que c'est bien le cas.

## Gestion du fichier de configuration Squash TM

Dans le fichier de configuration de Squash TM *conf/squash.tm.cfg.properties*, il est possible de configurer un certain nombre de propriétés.

!!! warning "Focus"
    Toutes les modifications réalisées dans le fichier de configuration *squash.tm.cfg.properties* doivent être enregistrées et l'application doit être redémarrée pour qu'elles soient prises en compte.

Le timeout de session de Squash est configuré par défaut sur 1h. En effet, si un utilisateur connecté à Squash TM n'effectue aucune action, il est déconnecté au bout d'une heure. Pour allonger ou diminuer cette durée, modifier la propriété suivante en renseignant une valeur en secondes :

    server.servlet.session.timeout=3600


Il est possible de modifier le context path de Squash en ajoutant la propriété suivante avec le nouveau context :

    server.servlet.context-path=/test

Suite à cette modification les utilisateurs Squash TM peuvent se connecter en renseignant cette url dans leur navigateur : *<url-squash\>/test*


Le pool size de connexion à la base de données est par défaut configuré à 20. Il est possible de l'augmenter en décommentant la ligne après avoir modifié la valeur :

    spring.datasource.hikari.maximumPoolSize = 20

Le timeout de connexion au bugtracker est par défaut configuré à 15 secondes. Pour le modifier, renseigner une nouvelle valeur puis enregistrer le fichier : 

    squashtm.bugtracker.timeout=15

L’affichage du panneau de configuration pour activer/désactiver le détail des erreurs est par défaut masqué dans la page ['Paramètres système'](../../admin-guide/gestion-systeme/parametres-systeme.md) dans l'Administration de Squash TM. 
<br/>Pour le rendre visible, il faut modifier la propriété suivante dans le fichier avec la valeur *true* :
    
    stack.trace.control.panel.visible=true

Les login et mot de passe saisis pour la connexion aux outils tiers par les utilisateurs sont encryptés dans la base de données avec une clé de cryptage qu’il est possible de modifier via la propriété suivante dans le fichier de configuration de Squash TM :

    squash.crypto.secret = *******
L’application est livrée avec une clé de cryptage par défaut pour des raisons pratiques, il est fortement conseillé de la changer. Si la clé de cryptage est modifié, les identifiants saisis avant la modification sont inutilisable avec la nouvelle clé. Ils devront être saisi à nouveau pour prendre en compte le nouvel encryptage.

Le plugin Xsquash4jira possède deux propriétés à configurer directement dans le fichier de configuration de Squash TM :

La première règle le délai entre deux processus de mise à jour (mise à jour de Squash et de Jira) exprimé en secondes. Si cette propriété est absente ou incorrecte, le plugin se régle par défaut sur 5 minutes (300 secondes) et émet un avertissement dans les logs de Squash TM au démarrage de l’application. Plus le délai est court, plus le plugin consomme de ressources coté Squash TM afin de maintenir à jour les informations. Pour la plupart des usages courants, un temps compris entre 5 et 15 minutes est largement suffisant.

    squash.external.synchronisation.delay = 300

La seconde règle la taille du batch de mise à jour des informations depuis Jira. La valeur par défaut est de 50. Cette taille doit être inférieure ou égale à la valeur *jira.search.views.default.max* définie dans le fichier de propriété de *jira-config.properties*. La valeur par défaut, 50, fonctionne très bien avec Jira Server, Datacenter et Cloud, si aucune modification de configuration n’a été apportée à l’instance Jira.

    plugin.synchronisation.jira.batchSize = 50