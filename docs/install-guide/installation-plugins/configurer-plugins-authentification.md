# Configurer les plugins d'authentification

Les plugins Squash TM relatifs à l'authentification nécessitent une configuration complémentaire au niveau du serveur. Cette page détaille toutes les configurations nécessaires pour les plugins suivants : LDAP, Active Directory et SAML.

## LDAP et Active Directory

Les connecteurs LDAP et Active Directory (AD) permettent d’externaliser la gestion des autorisations pour les connexions à Squash. La gestion des habilitations reste interne à Squash.

### Configuration de LDAP et AD

Les plugins LDAP et AD supportent à la fois des configurations simple et multi-domaines.
Des exemples de configurations standards se trouvent dans les fichiers suivants, présents dans le dossier 'config' du plugin :

-	Simple domaine : squash.tm.cfg.properties
-	Multi-domaines : multi-ldap(ou ad).squash.tm.cfg.properties 

Suite à l'installation des fichiers .jar du plugin LDAP ou AD dans le répertoire 'plugins' de Squash TM, il faut copier/coller cette configuration standard dans le fichier **squash.tm.cfg.properties** présent dans le dossier 'conf' de Squash TM afin de la compléter.

#### Propriétés du connecteur LDAP

Le connecteur LDAP permet une configuration plus poussée à travers les propriétés suivantes :

- authentication.ldap.server.url : l’URL de l’annuaire. Il peut être de type ldap:// ou ldaps:// dans le cas d’une connexion sécurisée.
- authentication.ldap.server.managerDn : identifiant qui a les droits pour parcourir l’annuaire lorsque celui-ci ne peut pas être parcouru en mode anonyme
- authentication.ldap.server.managerPassword : mot de passe de l'utilisateur qui a les droits pour parcourir l’annuaire
- authentication.ldap.user.searchBase : endroit où se trouvent les utilisateurs qui pourront se connecter à Squash
- authentication.ldap.user.searchFilter : permet de chercher l'attribut de l’utilisateur qui sera son login dans Squash

#### Utilisation du connecteur LDAP avec un annuaire AD

Il est possible d’utiliser le connecteur LDAP pour le connecter à un annuaire de type Active Directory et ainsi bénéficier de ses possibilités plus importantes de configuration.
La configuration à renseigner est sensiblement la même, que l’annuaire soit de type AD ou LDAP.
<br/>La principale différence se situe au niveau de la propriété authentication.ldap.user.searchFilter :

- Pour un annuaire de type AD, il s’agit généralement de l’attribut samAccountName ou UserPrincipalName
- Pour un annuaire de type LDAP, il y a plus de possibilités, il peut s’agir de uid, id, uniqueMember ou autre

### Fonctionnement de la connexion

Pour se connecter à Squash TM, il faut au préalable créer au moins un utilisateur dans l’annuaire LDAP ou AD avec un login identique à celui de l’administrateur Squash, par défaut : « admin ». Cet utilisateur permet de se connecter au logiciel Squash TM avec un profil d’administrateur. L’administrateur pourra par la suite ajouter des habilitations aux autres utilisateurs.

!!! info "Info"
    Un compte administrateur est créé lors de la première utilisation de Squash, le login est « admin ».

Pour que les autres utilisateurs puissent se connecter à Squash TM avec le connecteur LDAP ou AD, il est nécessaire de les ajouter dans l’annuaire :

- Si l’utilisateur est absent de l’annuaire LDAP ou AD, il ne peut pas se connecter à Squash TM. La connexion est impossible.
- Si l’utilisateur est présent dans l’annuaire LDAP ou AD, il peut se connecter à Squash TM. Le mot de passe est géré par l’annuaire. Tandis que les habilitations pour les projets sont gérées dans Squash. Deux cas sont alors possibles : 
    - L’utilisateur a déjà un compte utilisateur dans Squash TM : lors de sa connexion, il aura les habilitations correspondantes à son compte.
    - L’utilisateur n’a pas de compte utilisateur dans Squash TM : lors de sa première connexion, un compte utilisateur est automatiquement créé dans Squash TM mais aucune habilitation ne lui est affectée. Par la suite, des habilitations peuvent lui être ajoutées par un administrateur dans Squash TM.

!!! warning "Focus"
    Lorsque l’authentification est déléguée à un annuaire, les mots de passe ne sont plus administrables dans Squash TM. Les options **[Réinitialiser]** le mot de passe (administrateur) et modifier le **[Mot de passe local]** (utilisateur) sont désactivées.

### Authentification multi-sources

L’authentification multi-sources (annuaire + local) à Squash TM est possible.

Pour cela :

- Effectuer l’installation et la configuration du plugin LDAP ou AD
- Dans le fichier 'squash.tm.cfg.properties', compléter la propriété suivante en ajoutant 'internal' comme source d’authentification : 
    
        authentication.provider=ldap,internal

Les utilisateurs pourront alors se connecter à l'outil avec des comptes présents dans l’annuaire, et des comptes locaux de Squash TM.

### Autoconnect aux bugtrackers

Dans le cas précis où les identifiants de connexion à Squash TM et aux bugtrackers sont gérés par le même service d’annuaire, il est possible d’activer l’autoconnect aux bugtrackers dans Squash TM.
Dans les 'Paramètres Systèmes' accessibles depuis l’administration de Squash TM, il faut activer le bouton radio présent dans le bloc 'Authentification automatique au bugtracker à la connexion'.

![Autoconnect](resources/autoconnect-bugtrackers.png){class="pleinepage"}

Une fois l’option activée, l'autoconnect se déclenche à chaque connexion d’un utilisateur et tente l'identification sur les différents bugtrackers auxquels l'utilisateur est associé via les projets sur lesquels il est habilité. Les informations enregistrées dans l’espace "Mon Compte" seront alors ignorées.
Si cette option est désactivée, il n'y aura pas de tentative de connexion automatique au bugtracker, les identifiants renseignés dans "Mon compte" seront pris en compte

### Désinstallation de LDAP et AD

Pour désinstaller les plugins LDAP et AD, il faut :

1. Arrêter Squash TM
2. Supprimer ou commenter avec un # les lignes de configuration spécificiques au plugins dans le fichier 'squash.tm.cfg.properties'
3. Retirer les fichiers .jar du plugin du dossier 'plugins' de Squash TM

## SAML

### Présentation

Le plugin SAML ajoute un nouveau point d’entrée pour l’authentification sur Squash TM.
Il s’accompagne de nombreux avantages, tels qu’un service d’authentification sécurisé et centralisé, et un service SSO. Ce plugin ne remplace pas le point d’entrée par défaut (formulaire de connexion). Les utilisateurs et les administrateurs peuvent continuer d'utiliser l’ancienne interface (qui peut d’ailleurs toujours bénéficier des plugins de sécurité AD/LDAP) s'ils le souhaitent.

Le protocole SAML est en version 2.0.

#### Prérequis

Le plugin SAML nécessite d’avoir Squash TM en version 1.19 minimum et Java 8 ou 11. Les autres versions de Java ne sont pas testées.

#### Fonctionnalités

Les fonctionnalités prises en charge sont les suivantes : 

- SSO Web
- SSO Web avec un profil holder-of-key
- SSO initié par SP ou IDP
- Profils HTTP-POST, HTTP Redirect, SOAP, PAOS et Artifact Binding
- Vérification de confiance à l’aide de Metadata Interoperability ou ICP (PKIX)
- Prise en charge du scoping et des contextes d'authentification
- Publication de métadonnées SP

#### Points de Terminaison

Le plugin SAML présente de nouveaux point de terminaison : 

- /auth/saml/login : point d'entrée SAML pour l’authentification de l’utilisateur 
- /auth/saml/SSO : points de terminaison pour les profils SSO, également connus sous le nom de service ACS (Assertion Consumer Service)
- /auth/saml/SSOHoK : point de terminaison pour le profil SSO Holder-of-Key
- /auth/saml/metadata : l’URI des métadonnées 
- /auth/saml/SingleLogout : URL de déconnexion simple

Pour rappel, le chemin de contexte de l’application est toujours requis. Par exemple, pour le déploiement final de Squash TM, l’URL serait semblable à : *http://someserver:8080/squash/auth/saml/login*

#### Limites

Le plugin SAML ne prend en charge les métadonnées que pour un seul fournisseur d’identité (IdP) et un seul SP. Pour cette raison, il n’y a pas de services de découverte d’IDP : les utilisateurs sont redirigés vers le seul IDP connu. Le plugin ne comporte pas d’outils intégrés pour la génération de métadonnées SP. Pour cela, il faudra utiliser des outils tiers. 

#### Intégrer Squash et le plugin SAML à son environnement 

Le schéma ci-dessous illustre les acteurs et les ressources impliquées dans les interactions SAML. 

![Interactions](resources/saml-interactions.png){class="pleinepage"}

**Utilisateurs :** Les utilisateurs veulent avoir accès aux ressources détenues par le Fournisseur de service, après avoir réussi à s’authentifier auprès du Fournisseur d’identité.

**Fournisseur d’identité :** Le fournisseur d’identité (ou IDP, de l’anglais Identity Provider) détient l’annuaire des utilisateurs. C’est lui qui se charge de l’authentification des utilisateurs, et, si elle est réussie, de fournir des certificats d’authentification s’il s’agit d’un scénario SSO. Les services SAML pris en charge par l’IDP sont listés et publiés dans un fichier intitulé Métadonnées IDP qui définit quelles données seront échangées et comment elles le seront. 

**Fournisseur de service :** Le fournisseur de services (SP, de l’anglais *Service Provider*) est le serveur avec lequel l’utilisateur souhaite interagir. Dans notre cas, il s’agit de Squash TM. Ce fournisseur délègue les problèmes d’authentification à l’IDP, mais c’est lui qui détient les autorisations. Comme l’IDP, il publie les fonctionnalités SAML qu’il prend en charge dans des métadonnées SP.  

**SP/IDP Metadata:** Ces fichiers publient les fonctionnalités prises en charge par le serveur qu’elles décrivent. Ils comportent les listes de point de terminaisons, de type de lien SSO préférés (HTTP-POST, redirect, etc.), d’informations attendues sur les utilisateurs (et sous quel format), de certificats utilisés pour le chiffrement, et de signatures.

**Keystore:** Le plugin SAML utilise son propre keystore, qui est un keystore JKS (PKCS#12) qui se distingue du keystore JVM. Ce keystore contient d’une part les clés privées pour l’utilisation de Squash TM et d’autre part les clés publiques pour l’utilisation de tous les autres outils tiers : IDPs, proxys (offloading SSL), etc. Il contient également le certificat racine et l'autorité de certification nécessaire à la vérification des clefs publiques.

À son lancement, Squash accède d’abord au keystore et aux métadonnées SP et IDP. Les métadonnées SP et IDP peuvent toutes deux être récupérées soit localement ou soit à distance via une connexion HTTP(S). Dans notre exemple, les métadonnées SP sont disponibles localement tandis que les métadonnées IDP sont téléchargeables directement depuis l’IDP. Quant au keystore, il n’est accessible que localement. L’idéal est d’avoir le keystore et les métadonnées sur la même machine. 

L’étape suivante est la validation des métadonnées, qui inclut une validation syntaxique et une vérification de la signature des métadonnées. Les certificats peuvent éventuellement être vérifiés, notamment au niveau de leur chaîne de confiance et de leur révocation (ou l’on peut simplement assumer leur validité si la source est de confiance).

Lorsque toutes les vérifications ont été effectuées, Squash signe ces métadonnées et les publie pour qu’elles puissent être utilisées par des outils tiers. C’est alors que l’on peut configurer l’IDP et déclarer l’instance Squash TM comme une source de demandes d’authentification valide, et lui fournir les métadonnées SP si nécessaire.

À ce stade, le plugin est opérationnel. Squash TM interagit avec l’IDP directement ou indirectement (via le navigateur Internet de l’utilisateur), selon le profil de liens SSO choisi. Squash TM peut également être configuré pour fonctionner avec des serveurs mandataires (proxy) si l’un d’entre eux se trouve entre deux serveurs et qu’une communication directe est requise (non-illustré). 

#### Utilisation

Les utilisateurs peuvent désormais s’authentifier sur Squash TM en utilisant soit l’interface avec le formulaire d’authentification, soit l’interface SAML SSO (/auth/saml/login). Chaque interface peut être sélectionnée en tant qu’interface principale. Par défaut, les utilisateurs seront automatiquement redirigés vers cette interface. Si un utilisateur souhaite s’authentifier en utilisant l’autre interface, il peut le faire en accédant directement à son URL. 

Une fois que l’utilisateur a réussi à s’identifier via SAML, le contexte utilisateur est créé. Le NameID sera le même que l’identifiant de connexion utilisé et transmis lors de l’authentification (l'identifiant est défini dans les métadonnées). Le compte utilisateur correspondant y sera alors lié, sans quoi l'utilisateur sera authentifié mais ne pourra accéder à aucun projet sur Squash TM.

Un utilisateur s’étant authentifié sur Squash TM via SAML peut changer son mot de passe local sur sa page de Préférences de compte. Par ailleurs, un administrateur peut lui définir un mot de passe via la page d’administration des utilisateurs. Un utilisateur s’étant authentifié via le formulaire de connexion peut changer son mot de passe normalement. 
En revanche un utilisateur ne peut pas changer son mot de passe SAML via Squash TM.

### Configuration

Cette partie détaille les étapes de configuration nécessaires à l’installation du plugin SAML. Ici seront abordées les principales actions à réaliser, ainsi que les options pouvant être choisies. Pour la partie sur la configuration avancée, rendez-vous à la prochaine partie.

Les étapes nécessaires à l’installation du plugin sont les suivantes : 

- Déploiement du binaire ;
- Création du keystore et des métadonnées SP ;
- Configuration de l’application.

#### Déploiement du binaire 

À partir du paquet .zip contenant le plugin, copiez le fichier .jar du dossiers **plugins** et collez le dans dossier du même nom dans votre application Squash TM *$SQUASH_HOME/plugins/*.

#### Création d’un keystore

Le plugin SAML utilise son propre keystore, qui centralise tout le matériel de chiffrement utilisé dans les cas d’utilisation SAML<sup id="fnref:1"><a  href="#fn:1" rel="footnote">1</a></sup>. Le keystore doit contenir au moins un couple clé publique / clé privée pour Squash TM. Selon les besoins de l'ICP, les certificats des outils tiers et/ou de CA intermédiaires et racines peuvent être utilisés. 

Pour créer ce keystore, il vous faut le keytool qui vient avec le JDK (c.-à-d $JAVA_HOME/bin/keytool). Dans cette partie, nous allons partons du principe que votre Java est de type Oracle Hotspot. Pour accéder à la documentation complète, rendez-vous sur *https://docs.oracle.com/javase/8/docs/technotes/tools/unix/keytool.html*.

NB : Les alias des clés de certificats à installer ou créer ne doivent ni être vides, ni contenir d’espace. Ils doivent contenir une succession de lettres ou chiffres, et peuvent contenir des traits d’union ou des underscores en tant que séparateurs. En résumé, ils peuvent contenir : [a-zA-Z0-9-]+.

##### Créer un couple clé publique / clé privée pour Squash TM

La commande suivante permet de créer un couple de clés RSA pour Squash ™ avec l’algorithme de signature SHA256withRSA valable un an :

    keytool -genkeypair -keyalg rsa -keysize 2048 -sigalg SHA256withRSA -alias squashtm -keystore keystore.jks -validity 365

où “squashtm” est l’alias de la clé et “keystore.jks” est le nom du keystore. S’il n’existe pas déjà, le keystore sera créé dans la foulée et dans ce cas, vous devrez répondre aux questions nécessaires à sa création. Veillez à bien noter les mots de passe pour le keystore et la clé. 

!!! info "Info"
    Vous pouvez définir plus d’un couple de clés. Le plugin permet l’utilisation de différentes clés pour diférents usages (par exemple, la signature et le chiffrement).

À ce stade, le certificat associé est toujours autosigné. Si cela vous convient, vous pouvez alors [Exporter un certificat](#exporter-un-certificat).

Si vous avez besoin de la permission d’une autorité de certification (CA), vous pouvez émettre une nouvelle demande de signature de certificat (CSR) avec la commande suivante : 

    keytool -certreq -alias squashtm -keystore keystore.jks -file squashtm-csr.csr

Puis, transférez cette demande à l’autorité compétente. Une fois que votre demande est traitée, installez le certificat signé comme indiqué dans [Importer un certificat](#importer-un-certificat).

##### Facultatif (Installer une clé privée externe)

Si la clé privée devant être utilisée par Squash TM a été générée ailleurs, il faut l’importer. La clé doit être un fichier .p12 ou .pfx. 

    keytool -importkeystore \
        -srckeystore imported.p12 -srcstoretype PKCS12 -srcstorepass imported_pass \
        -srcalias key -srckeypass key_pass \
        –destkeystore keystore.jks -deststorepass dest_store_pwd \
        -destalias squashtm -destkeypass squashtmpassword

##### Exporter un certificat

Vous pouvez exporter un certificat à partir du keystore en utilisant la commande suivante :

    keytool -export -rfc -keystore keystore.jks -alias squashtm -file squashtm.cer

La plupart du temps, cela est nécessaire pour générer des métadonnées SP (voir ci-dessous) pour que Squash puisse les utiliser en tant que certificat lors d’une signature ou d’un chiffrement. L’option -rfc permet de générer le certificat aur format PEM (un fichier encodé en base64 de texte simple dont vous pouvez copier/coller le contenu).

##### Importer un certificat

Les certificats de tiers peuvent être importés en tant que fichiers .cer/.crt, notamment des certificats intermédiaires et racines, ou encore un certificat signé par une autorité de certification si vous en avez une. Cela fonctionne aussi pour les certificats PCKS#7 (fichiers .p7b). 

    keytool -importcert -alias certname -file certificate.cer -keystore keystore.jks

#### Métadonnées de fournisseurs de service

Le plugin SAML n’embarque pas de programme utilitaire de Métadonnées de Fournisseurs de service. Il vous faudra les générer en utilisant des outils externes. Vous pouvez aussi éditer celui founi en exemple.

##### Configuration de base

L’exemple qui suit utilise l’outil en ligne OneLogin (*https://www.samltool.com/sp_metadata.php*), qui offre les options de base pour la génération de métadonnées. Cet outil peut vous fournir un modèle qui pourra vous servir de base par la suite : changement de service consommateur d'assertion, ajout de formats de noms d'IDs, etc. 

Voici un formulaire simple de génération de métadonnées SP :

![Sample Metadata Generation](resources/sample-metadata-generation.png){class="pleinepage"}

Dans l’exemple, nous avons fourni les informations suivantes : 

- **EntityID :** l'identifiant de votre instance Squash ; doit être unique parmi tous les fournisseurs de service connus par l’IDP ;
- **Consumer Service endpoint :** le point de terminaison où tous les messages liés au SSO (assertions) doivent être envoyés. Le chemin de ce point de terminaison est */auth/saml/SSO* ;
- **Single Logout Service endpoint :** le point de terminaison où l’IDP peut initier une déconnexion à distance. Le chemin de ce point de terminaison est */auth/saml/SingleLogout* ;
- **NameID format :** le format préféré pour les noms d’utilisateur.
- **SP X.509 Cert** (pour la signature et le chiffrement) : le certificat Squash TM pouvant être obtenu comme expliqué dans [Exporter un certificat](#exporter-un-certificat).

Il n’est pas nécessaire de remplir les autres données. 

Vous pouvez maintenant cliquer sur le bouton **Build SP METADATA** au bas de la page et obtenir les métadonnées dans un document XML. Les services SAML publiés sont minimes mais fonctionnels et peuvent être utilisés dans la plupart des environnements. 

!!! warning "Focus"
    Ces outils vous offrent également la possibilité de signer les métadonnées en leur fournissant une clé publique et une clé privée. Il n’est pas recommandé de faire cela dans l’immédiat car : 
	
    - Cela implique d'envoyer une clé privée depuis Internet ;
    - Vous ne pouvez pas changer les métadonnées sauf si vous les re-signez après les avoir modifiées ;
    - Squash TM peut les resigner lors de son exécution avant de les exposer publiquement.

##### Configuration avancée

L’outil présenté ci-dessus a des limites. Par exemple, vous êtes obligé·e d’utiliser le profil de liaison HTTP-POST pour le point de terminaison SSO. Les détails sur des métadonnées SAML plus précises sont en dehors du périmètre de cette documentation. Si vous avez besoin de produire un fichier de métadonnées plus spécifique, voici quelques détails qui pourraient vous intéresser :

- La liste des points de terminaisons est donnée dans [Points de terminaison](#points-de-terminaisons)
- Le NameID est le nom de l’identifiant de connexion dans Squash TM
- Squash TM ne vérifie pas d’informations complémentaires (adresse e-mail...) en plus du nom  (<RequestedAttribute\> ignorés).

!!! tip "For more information"
    Pour plus d’informations, veuillez vous référer aux  [Spécifications SAML](https://docs.oasis-open.org/security/saml/v2.0/saml-metadata-2.0-os.pdf) 

#### Configuration

##### Utilisation d'un fichier dédié 

Le plugin SAML propose de nombreuses propriétés de configuration et le fichier de configuration est plutôt lourd, surtout par rapport aux autres plugins de Squash TM. À partir de Squash TM 1.18, vous pouvez configurer le plugin en utilisant son propre fichier de configuration dédié, ou bien y faire référence dans le fichier de configuration principal plutôt que d'y intégrer le contenu comme cela était requis pour les anciens plugins.

Pour cela, copiez le fichier *config/squash.tm.cfg-saml.properties* et collez-le dans le répertoire de configuration home. Dans la distribution de Squash TM en tant qu’application autonome, ce répertoire est en général *SQUASH_HOME/conf*. Puis, éditez le fichier de configuration principal *SQUASH_HOME/conf/squash.tm.cfg.properties* et associez-lui le fichier de configuration du plugin en lui ajoutant/éditant la propriété suivante : 

    spring.profiles.include=saml

Si cette propriété était déjà présente, vous pouvez juste ajouter “saml” à la liste, séparé par une virgule “,”.

##### (Alternative) Intégration des propriétés

Si cela est plus pratique pour vous, vous pouvez également simplement copier et coller le contenu du fichier de configuration du plugin dans le fichier de configuration principal, sans avoir besoin d’ajouter SAML à la liste des profils.

##### Configuration minimale

Le fichier de configuration propose de nombreuses propriétés, mais seulement certaines d’entre elles sont obligatoires. Squash TM ne démarrera pas / ne fonctionnera pas correctement si ces propriétés sont laissées vides ou ne sont pas renseignées correctement.

Les propriétés listées ci-dessous sont requises. Vous trouverez plus de détails sur elles (valeurs autorisées, etc.) dans les commentaires du fichier *squash.tm.cfg-saml.properties*, et plus loin dans cette même documentation. 

**saml.enabled:** interrupteur principal. Cette propriété vous permet d’activer ou de désactiver le plugin sans commentaire et sans avoir à supprimer le fichier jar. 

**saml.idp.metadata.url:**  l'emplacement des métadonnées du Fournisseur d'identité. Les *protocoles file://*, *http://* et *https://* sont autorisés. 

**saml.sp.metadata.url:** : l'emplacement des métadonnées du Fournisseur de service. Les *protocoles file://*, *http://* et *https://* sont autorisés. 

**saml.keystore.url:** l'emplacement du keystore. Seul le protocole *file://* est autorisé. 

**saml.keystore.password:** le mot de passe du keystore. 

**saml.keystore.credentials.<?>:** l'alias d’un couple clé publique / clé privée pour les besoins de Squash TM en terme de chiffrement. Le point d’interrogation “?” représente un alias. La valeur de la propriété est le mot de passe de cette clé. Cette propriété peut être répétée une fois pour chaque clé. 

**saml.keystore.default-key:** l’alias de la clé que Squash TM doit utiliser à chaque fois qu’une clé privée est requise, sauf si une autre clé est spécifiée pour cette situation dans le fichier de configuration. La clé par défaut doit évidemment être présente dans la liste des identifiants. 

#### Autres actions

L’instance de Squash TM doit être enregistrée auprès de l’IdP avec les métadonnées SP pour débuter la relation de confiance. Les détails de cette étape dépendent de votre fournisseur SAML ; veuillez vous référer à sa documentation dédiée. 

![Suggestion of configuration layout](resources/configuration-layout.png){class="centre"}

### Configuration avancée

Le chapitre précédent présentait les préoccupations générales en ce qui concerne la configuration de Squash-SAML. Dans la plupart des cas, cette configuration devrait bien s’intégrer à votre environnement de travail. Cependant, le plugin propose plusieurs options pour une personnalisation plus poussée comme par exemple des exigences avancées pour le chiffrement RSA, ou encore pour prendre en compte l’organisation de votre réseau. Ce chapitre traite de ces options supplémentaires et revient sur quelques éléments présentés dans le chapitre précédent. 

!!! info "Info"
    Si certaines choses vous semblent peu claires, vous pourrez trouver des indices utiles dans les [Appendices](#a-propos-de-la-cryptographie).

#### Conventions

##### Format des chemins de fichiers

Les métadonnées et le keystore peuvent être récupérées à la lecture en tant que fichiers locaux ou à distance via des appels HTTP(s) en utilisant les propriétés *saml.idp.metadata.url*, *saml.sp.metadata.url* et *saml.keystore.url*. 

**Via HTTP(s) :** le chemin du fichier est l’URL à partir de laquelle il peut être téléchargé. Le protocole est soit http://, soit https://. Exemple : *https://votre.idp/metadonnees.xml*


**Via un chemin absolu :**  URL menant à un fichier local qui fonctionne bien en tant que chemin absolu. Dans ce cas, le protocole est file://. Exemple : *file:///dev/squashtm/saml/idp.xml* (sous Linux), *file://C:/dossierappli/squashtm/conf/idp.xml* (sous Windows).

**Via un chemin relatif vers le dossier de configuration :** Tout chemin qui n’a pas de protocole défini sera considéré comme étant un chemin relatif vers le dossier de configuration. L’emplacement du dossier de configuration peut varier selon l’environnement et le mode d’installation choisi pour Squash TM. Exemple : *saml/idp.xml* devient *$CONF_DIR/saml/idp.xml*.

##### Ancres de confiance 

Dans certains cas, il vous faudra définir quels certificats parmi ceux *présents dans le keystore doivent être considérés comme des ancres de confiance. Parmi ces cas, on trouve la validation de métadonnées SP, la validation de métadonnées IDP, et les messages IDP entrants. 

Lorsque une propriété (généralement nommée *.trusted-keys*) est rencontrée, les valeurs suivantes sont acceptées :

- **all :** Tous les certificats du keystore sont considérés comme des ancres de confiance ;
- **(blank) :**  Si aucune valeur n’est spécifiée, on admettra la valeur “all” ;
- **none :** Aucun certificat du keystore ne sera considéré comme une ancre de confiance. Dans ce cas, les certificats doivent être validés par une autorité de certification du keystore de la JVM.
- **une liste séparée par des virgules :** La liste explicite des clés du keystore, séparées par une virgule “,” à savoir : 

        context.trusted-keys = anchor1, anchor2, anchor3

Il est évident que si vous n’avez besoin que d’une seule ancre, il serait sage de ne pas la nommer “all” ou “none”.

##### Propriété Espaces de noms 

Les propriétés sont regroupées par des espaces de noms en relation avec les acteurs (par exemple l’IDP) ou les ressources (métadonnées). Elles sont listées sans l’espace de nom par souci de concision, et avec l’espace de nom au-dessus de la table correspondante. Ce format est pratique dans un contexte de documentation. Cependant, lors de la modification du fichier de configuration, il sera nécessaire de déclarer le nom complet de la propriété.

#### Point d'entrée principal

Comme dit précédemment dans le chapitre Utilisation, les utilisateurs peuvent choisir de s’identifier en utilisant l’un des deux systèmes d’authentification (page habituelle du formulaire d’authentification ou point d’entrée SAML). Cependant, l’un de ces deux systèmes est défini comme étant le point d’entrée principal, et si l’utilisateur n’exprime aucune préférence pour l’un des deux systèmes, il sera redirigé vers ce point d’entrée principal lors de l’authentification. 

Par défaut, Squash TM instaure la page de formulaire en tant que point d’entrée principal. Ceci peut être outrepassé dans la configuration. 

##### Options

Espace de nom : *squash.security*

| Propriété | Commentaire |
|--|--|
| preferred-auth-url | Cette propriété guide le comportement de Squash TM en matière de requêtes d’utilisateur non-identifiés.  <br/>Elle définit l’URL vers laquelle ce type d’utilisateurs doit être redirigé, c.-à-d. le point d’entrée principal. <br/>Si cette propriété est définie par /auth/saml/login, le point d’entrée principal sera SAML.<br/>Veuillez noter que cette propriété appartient à Squash TM et n’est pas spécifique au plugin SAML.<br/>Si vous utilisez d’autres plugins d’authentification, veuillez vérifier leurs configurations respectives afin d’éviter tout conflit en lien avec ce sujet. <br/>Valeur par défaut :/login (page du formulaire d’authentification par défaut)

#### Traitement des métadonnées

Les métadonnées de Squash TM (en tant que fournisseur de service) et celles du fournisseur d’identité peuvent être récupérées localement ou à distance (voir la partie ci-dessus). Du fait de la nature sensible de ces fichiers, leur contenu doit être contrôlé de près. Il existe deux manières d’effectuer ce contrôle : 

- Soit les métadonnées sont signées, et la signature vérifiée ;
- Soit la source des métadonnées est de confiance et vérifiée par d’autres mécanismes (elles proviennent d’un serveur https ou d’un dossier très privé). 

!!! warning "Focus"
    Dans le cas où vous choisiriez de récupérer les métadonnées via une connexion https, la chaîne TLS est soutenue par le keystore de la JVM ou le keystore Java par défaut. C’est la seule exception au paradigme du keystore dédié aux plugins (voir [Keystore](#Keystore)). Si le fournisseur de métadonnées a un certificat distinct, veuillez vous assurer qu’il est installé dans le keystore par défaut. Nous nous excusons pour cette divergence par rapport au canon de ce plugin. 

##### Options des métadonnées IDP

Espace de nom : *saml.idp.metadata*

| Propriété| Commentaire |
|--|--|
| url (requise) | L’URL où se trouvent les métadonnées. Elle respecte les conventions indiquées dans la partie [Formats de chemins de fichiers](#Formats-de-chemins-de-fichiers). <br/>Valeur par défaut : non applicable |
| require-signature | Indique si Squash TM n’accepte que des métadonnées signées. Si la valeur indiquée est true, les métadonnées non-signées seront refusées.  <br/>Valeur par défaut : false |
| check-signature | Si les métadonnées sont signées, la signature sera vérifiée. Cela implique une vérification du digest des métadonnées et de la confiance pouvant être accordée au certificat. Ce comportement est indépendant de la propriété précédente (ci-dessus). Si les métadonnées ne sont pas signées, cette propriété ne prendra pas effet.<br/>Valeur par défaut : true |
| check-certificate-revocation | Si la signature des métadonnées est conforme, il faut choisir s’il faut également vérifier la révocation du certificat. Veuillez noter que cela active simplement ce comportement et que le mécanisme spécifique qui lui est lié (CLR, OSCP) ne peut pas être configuré ici. Étant donné que nous nous appuyons sur le fournisseur JCE de votre JDK, cette configuration dépend des propriétés système et de la configuration du keystore à ce sujet (en ce qui concerne SUN JCE)<br/>Veuillez noter que les points de terminaisonss ajoutées au keystore manuellement seront respectées dans tous les cas.<br/>Valeur par défaut : false |
| trusted-keys | Parmi tous les certificats installés dans le keystore, cette propriété définit lesquels d’entre eux seront considérés en tant qu’ancres de confiance. Le format est défini dans la partie Ancres de confiance.<br/>Valeur par défaut : all |

##### Options des métadonnées SP

Les options de métadonnées SP sont exactement les mêmes que celles des métadonnées IP ci-dessus, avec comme espace de nom : *saml.sp.metadata*

#### Exposition des métadonnées du Fournisseur de service

Squash TM expose les métadonnées SP à l’URL */auth/saml/metadata* et les signe si vous le souhaitez, sauf si elles étaient déjà signées (Les métadonnées ne peuvent être pas signées plus d’une fois)

Les algorithmes impliqués dans la signature sont définis dans les standards de sécurité [Spécifications XML-Security](https://www.w3.org/TR/xmlsec-algorithms/). Veuillez noter que l’URI complète doit être précisée, et que l’algorithme doit être compatible avec le fournisseur JCE de votre JDK. La clé privée utilisée dans ce cas est définie à un autre emplacement, voir la propriété property *saml.sp.signing-key*.

##### Options

Espace de nom : *saml.sp.metadata-exposition*

| Propriété | Commentaires |
|--|--|
| signed | Les métadonnées exposées seront signées par Squash TM. Cependant, si les métadonnées étaient déjà signées, la signature originale sera conservée.  <br/>Valeur par défaut : false |
| signing-algorithm | L’algorithme pour la signature des métadonnées. Exemples:<br/>- *http://www.w3.org/2000/09/xmldsig#rsa-sha1* <br/>- *http://www.w3.org/2001/04/xmldsig-more#rsa-sha256* <br/>Valeur par défaut : si laissée vide, l’algorithme associé à la clé est déterminé par *saml.sp.signing-key*
| digest-algorithm | L’algorithme pour la génération du digest. Exemples :<br/>- *http://www.w3.org/2000/09/xmldsig#sha1* <br/>- *http://www.w3.org/2001/04/xmldsig-more#sha224* <br/>Valeur par défaut : si laissée vide, *http://www.w3.org/2000/09/xmldsig#sha1* |

####	Keystore

!!! info "Info"
    Toutes les propriétés de cette partie ont déjà été présentées dans la partie[Configuration minimale](#Configuration-minimale). Elles sont présentées de nouveau ici par souci de complétude.  

En ce qui concerne Squash TM, le keystore est le centre de l’ICP. Le plugin SAML utilise un keystore qui lui est dédié en plus de celui de la JVM. Les opérations de chiffrement impliquées dans les cas d’utilisation de SAML sont soutenues par son keystore dédié, à la seule exception du téléchargement de métadonnées avec une connexion TLS (voir [Traitement des métadonnées](#Traitement-des-métadonnées)). Parmi les cas d’utilisation, on trouve : le chiffrement, le déchiffrement, la signature et la vérification, la validation du certificat, et éventuellement la connexion TLS directe entre les deux parties. 

Le keystore doit contenir les certificats des CA intermédiaires et racines qui garantissent la signature des certificats, ainsi que les certificats auto-signés. Il doit également contenir au moins une paire de clés pour une utilisation par Squash TM. Plusieurs paires de clés peuvent également être définies, chacune pour une utilisation distincte (facultatif ; voir [Messages du Fournisseur de services](#Messages-du-fournisseur-de-services)).

##### Options

Toutes les options listées ci-dessous sont requises.

Espace de nom : *saml.keystore*

| Propriété | Commentaire |
|--|--|
| url | L’URL à partir de laquelle on peut avoir accès au keystore. Elle respecte les conventions décrites dans la partie  [Formats de chemins de fichiers](#Formats-de-chemins-de-fichiers),  avec pour restriction le seul support de *file://* ou des formats de chemin relatifs. <br/>Valeur par défaut : non applicable |
| password | Le mot de passe qui donne accès au keystore. <br/>Valeur par défaut : not applicable. |
| credentials.<?> | La liste de toutes les clés alias/mot de passe pour les couples de clés publique/privée dont Squash TM a besoin pour la signature, le chiffrement, ou l’initiation de connexions TLS. Dans ce format, le point d’interrogation représente l’alias. La valeur est le mot de passe. Au moins une clé doit être déclarée de cette façon. Cette propriété peut être déclarée plusieurs fois (une fois par clé) <br/>Les alias de clés doivent sagement être nommés afin que la propriété ne soit pas mal interprétée. Ils ne doivent contenir que des lettres, chiffres, tirets et underscore, et ils ne doivent pas contenir d’espace ( [a-zA-Z0-9-_]+ ).<br/>Exemple : *saml.keystore.identifiants.clé = mot de passe*<br/>Valeur par défaut : non applicable  |
| default-key | La clé par défaut utilisée par Squash TM lorsqu’il a besoin d’une clé privée pour l’accomplissement d’une tâche, sauf si une clé spécifique a été configurée pour cette tâche (par ex. saml.sp.signing-key). <br/>Valeur par défaut : non applicable  |

#### SSO

Les options listées ici sont en relation avec le processus SSO en lui-même. Vous pouvez modifier les éléments et les conditions des échanges de messages. Si les métadonnées SP ont plusieurs alternatives pour une même clause (telle que <NameIDFormat/>), vous pouvez également indiquer au plugin quelle valeur devrait être utilisée pour cette clause au lieu de la valeur par défaut.

Dans certains cas, la valeur property doit être choisie parmi les spécifications SAML. Voici quelques liens utiles :

- [Bindings](https://docs.oasis-open.org/security/saml/v2.0/saml-bindings-2.0-os.pdf)
- [Name ID, Scoping](https://docs.oasis-open.org/security/saml/v2.0/saml-core-2.0-os.pdf)
- [Authentication contexts](http://docs.oasis-open.org/security/saml/v2.0/saml-authn-context-2.0-os.pdf)


##### Options standard

Espace de nom : *saml.sso*

| Propriété | Commentaire |
|--|--|
| force-authN | Si cette option est activée, l’utilisateur devra se ré-authentifier auprès de l’IDP à chaque fois qu’il s’authentifie sur Squash. Cela garantit que l’assertion d’authentification de l’utilisateur est bien à jour. Toutefois, cela peut devenir pénible si le processus d’authentification auprès de l’IDP implique une interaction humaine et cela désactiverait effectivement le mécanisme SSO. <br/>Valeur par défaut : false |
| provider-name |Le nom lisible par un humain qui sera inclus dans les messages envoyés à l’IDP. Utile à des fins d’historique<br/>Valeur par défaut : vide
| binding | Le type lien que Squash TM utilisera pour initier le SSO avec l’IDP. Ce type de lien doit être présent dans les métadonnées de l’IDP. Exemples : <br/>- *urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST*<br/>- *urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect*<br/>En pratique, il vous faudra extraire la valeur souhaitée des métadonnées de l’IDP. <br/>Valeur par défaut : la première méthode de binding listée dans la clause <SingleSignonService/> des métadonnées de l’IDP. |
| assertion-consumer-index | Demande à l’IDP d’envoyer ses réponses au service consommateurs donné. La liste des services consommateurs disponibles peut être retrouvée dans les clauses <AssertionConsumerService/> des métadonnées SP. L'ordre des services consommateurs est listé avec l’attribut “index” au sein du libellé. <br/>Valeur par défaut : si laissée vide, la valeur par défaut sera utilisée (avec l’attribut “isDefault”, ou avec “index”=0). |
| nameID | Le NameID qui doit être renvoyé par l’IDP, et celui qui sera utilisé pour représenter le nom principal de l’utilisateur (son identifiant). Exemples : <br/>- *urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress*<br/>- *urn:oasis:names:tc:SAML:2.0:nameid-format:transient*<br/>Valeur par défaut : l’IDP  choisira parmi celles spécifiées dans sa copie des métadonnées SP.  |
| allow-create | Notifie l’IDP que la création d’un nouvel utilisateur est permise lorsque celui-ci est inconnu. Notez qu’il s’agit ici de la création d’un nouvel utilisateur sur l’IDP. Squash TM ne donne que son opinion sur les actions que devrait effectuer l’IDP ; la décision finale demeure avec l’IDP. <br/>Dans tous les cas, lorsqu’un utilisateur parvient à s’identifier auprès de l’IDP, Squash TM lui crée un compte utilisateur si ce compte n’existe pas déjà, comme expliqué précédemment dans la partie [Utilisation](#utilisation). La propriété ci-présente n’influe aucunement sur cette création.<br/>Valeur par défaut : false  |
| passive | Si cette option est activée, Squash TM informera l’IDP qu’il ne considère pas que l’interaction utilisateur soit nécessaire à l’authentification, ce qui peut nuire à l’expérience utilisateur. Notez que l’IDP peut (naturellement) être en désaccord avec cette décision.<br/>Valeur par défaut : false |
| relay-state | Un token arbitraire qui sera échangé avec l’IDP.  Cela fait partie des spécifications SAML, mais Squash n’en fait pas d’utilisation concrète. Cette propriété peut prendre toute valeur. <br/>Valeur par défaut : vide |

##### Scoping

L’utilisation de l’élément Scoping permet d’avoir des échanges avancés avec le SSO. Par exemple, vous pouvez demander des garanties sur la qualité de l’authentification des utilisateurs du côté de l’IDP (en spécifiant le contexte d’authentification) ou encore restreindre les IDPs de confiance au sein d’une architecture IDP multiniveaux. 

Les propriétés suivantes seront prises en compte seulement si le scoping est activé.

Espace de nom : *saml.sso* (identique à celui des options standard)

| Propriété | Commentaire |
|--|--|
| include-scoping | L’interrupteur principal pour le scoping. Si la valeur est false, les autres options listées ci-dessous seront désactivées.<br/>Valeur par défaut : false |
| allowed-idps | Cette propriété configure la clause <IDPList>. Au sein d’une architecture IDP multiniveaux, cette liste définit les IDPs autorisés à traiter les demandes d’authentification. La valeur est une liste d’entityIDs IDP séparés par des virgules. <br/>Valeur par défaut : vide (aucune restriction) |
| proxy-count | Le nombre maximum de proxys autorisés pour les messages d’authentification. Dans ce contexte, un proxy est égal à un IDP au sein d’une architecture IDP multiniveaux. Une valeur égale à 0 entraîne une interdiction de l’utilisation de proxys. L’IDP qui reçoit la demande d’authentification ne peut pas la déléguer ; c’est lui qui doit identifier l’utilisateur.<br/>Valeur par défaut : 2 |
| authn-contexts | Une liste des contextes où l’authentification de l’utilisateur qui doit être prise en charge par l’IDP, où les contextes sont séparés par des virgules. Cette propriété est utile lorsque la méthode d'authentification standard n’est pas considérée comme étant suffisamment sécurisée et que Squash TM demande les garanties d’un processus plus rigoureux. Exemples ou valeurs possibles : <br/>- *urn:oasis:names:tc:SAML:2.0:ac:classes:SmartcardPKI* <br/>- *urn:oasis:names:tc:SAML:2.0:ac:classes:MobileTwoFactorContract* <br/>Valeur par défaut : vide (aucune restriction)|
| authn-context-comparison | Les exigences supplémentaires en ce qui concerne le traitement des identifiants fournis par l’utilisateur lors de l’authentification auprès de l’IDP. Les valeurs autorisées sont : exact \| minimum \| maximum \| better.<br/>Valeur par défaut : exact. |

#### Messages du Fournisseur de service

Les métadonnées SP décrivent la plus grande partie du comportement de Squash TM dans un contexte SAML. Les propriétés suivantes permettent de compléter les métadonnées ou d’en changer une partie. Par exemple, vous pouvez changer le NameIDFormat souhaité sans toucher aux métadonnées. 

Plusieurs des options suivantes concernent l’utilisation de clés privées. Lorsqu’elle n’est pas définie, la clé par défaut sera celle du keystore (voir la partie [Keystore](#Keystore)).

##### Options

Espace de nom : saml.sp

| Propriété | Commentaire |
|--|--|
| signing-key | Si la clé de signature est différente de la clé par défaut, cette propriété indique l’alias de la clé SP privée pour la signature des messages SAML sortants.<br/>Valeur par défaut : non définie, et dans ce cas il s’agira de la clé par défaut |
| encryption-key | Si la clé de chiffrement est différente de la clé par défaut, cette propriété indique l’alias de la clé SP privée pour le déchiffrement des messages SAML entrants.<br/>Valeur par défaut : non définie, et dans ce cas il s’agira de la clé par défaut |
| tls-key | Si une connexion HTTPS directe est requise (par ex. HTTP-Artifact) et que Squash TM en tant que client doit s’authentifier avec un certificat, cette propriété indique l’alias de la clé SP privée à utiliser. <br/>Valeur par défaut : non définie, et dans ce cas il s’agira de la clé par défaut |
| require-logout-request-signed | Si l’IDP initie une déconnexion, cette propriété indique que de telles demandes doivent être signées pour Squash TM<br/>Valeur par défaut : true  |
| require-logout-response-signed | Si Squash TM initie une déconnexion, cette propriété indique que la réponse à cette demande de déconnexion doit être signée.<br/>Valeur par défaut : false |
| signature-security-profile | La politique de vérification de signatures. Les valeurs autorisées sont metaiop ou pkix, veuillez vous référer à la partie [Certificats](#Certificats) pour plus de détails sur cette politique. <br/>Valeur par défaut : metaiop |
| ssl-security-profile | La politique de vérification des certificats SSL/TLS lorsqu’une connexion HTTPS directe est requise. Les valeurs autorisées sont metaiop ou pkix, veuillez vous référer à la partie [Certificats](#Certificats) pour plus de détails sur cette politique.<br/>Valeur par défaut : pkix |
| ssl-hostname-verification | Dans le cas d’une connexion HTTPS directe, il peut y avoir une vérification du nom d’hôte en plus de la vérification des certificats. Les valeurs autorisées sont : default, defaultAndLocalhost, strict ou allowAll. Veuillez noter que la valeur allowAll désactive la vérification du nom d’hôte.<br/>Valeur par défaut : default |

#### Messages du Fournisseur d’idéntité

Le traitement des messages envoyés par le Fournisseur d’identité se fait principalement grâce aux métadonnées IDP connues par Squash. Cependant, il se peut que les métadonnées soient incomplètes, ou qu’elles doivent être changées provisoirement. Par exemple, dans le cas où un IDP a signé son message, mais qu’aucun certificat pour l’utilisation de la signature n’est spécifié dans les métadonnées, vous pouvez fournir l’information manquante en définissant la propriété *saml.idp.signing-key*. 

Outre l’ajustement du traitement des messages entrants, les propriétés suivantes vous permettent également d’autres caractéristiques de l’IDP, telles que le contenu des messages sortants de Squash TM.

##### Options

Espace de nom : *saml.idp*

| Propriété | Commentaire |
|--|--|
| signing-key | La clé pour vérifier la signature d’un message entrant de l’IDP.<br/>Valeur par défaut : le certificat à usage des signatures spécifié dans les métadonnées de l’IDP |
| encryption-key | La clé pour chiffrer les messages sortants vers l’IDP.<br/>Valeur par défaut : le certificat avec l’utilisation de la signature spécifiée dans les métadonnées de l’IDP. |
| trusted-keys | Si Squash TM doit faire une vérification PKIX sur les [Certificats](#Certificats) des messages entrants, cette propriété définit les clés qui seront considérées comme des ancres de confiance. Le format est défini dans la partie Ancres de confiance.<br/>Valeur par défaut : all |
| require-logout-request-signed | Indique qu’il est nécessaire pour l’IDP que toute demande de déconnexion initiée par le SP soit signée, même lorsque ce dernier est Squash TM.<br/>Valeur par défaut : true |
| require-logout-response-signed | Indique qu’il est nécessaire pour l’IDP que toutes les réponses aux demandes de déconnexion initiées par l’IDP soient signées.<br/>Valeur par défaut : false |
| require-artifact-resolve-signed | Si cette option est activée, les messages de résolution HTTP-Artifact seront signés.<br/>Valeur par défaut : true |
| allow-idp-initiated-sso | Cette propriété indique si l’IDP est autorisé à initier un SSO. Squash TM peut toujours initier le SSO comme d’habitude.<br/>Valeur par défaut : true |

##### Options de connexion directe

Si le profil HTTP-Artifact est activée pour les conversations SSO, une connexion directe entre Squash TM et le fournisseur d’identité sera nécessaire pour la résolution de l’artifact. 
Dans le cas où un proxy se trouve entre les deux serveurs, vous pouvez configurer l’information relative à ce proxy avec les propriétés suivantes.
 

Espace de nom : *saml.idp* (même espace de nom que pour les options standard)

| Propriété | Commentaire |
|--|--|
| proxy-host | L’hôte du proxy. Cette propriété active les propriétés suivantes.<br/>Valeur par défaut : non définie |
| proxy-port | Le port du proxy.<br/>Default value: 8080 |
| basic-username | Si l'IDP demande une authentification Basic Auth, cette propriété indique le nom d’utilisateur à utiliser.<br/>Valeur par défaut : aucune |
| basic-password | Le mot de passe pour les challenges Basic-Authentication.<br/>Valeur par défaut : aucune |

#### Expiration de l’authentification

Régulièrement, Squash TM invalidera uniltatéralement une assertion d’authentification lorsqu’elle aura dépassé la durée de validité entrée dans sa configuration (tout comme le Fournisseur d’identité peut lui-même établir une date d’expiration pour ses propres assertions d’authentification). Dans ce cas, l’utilisateur devra donc s’authentifier à nouveau auprès du Fournisseur d’identité. Cette expiration peut entraîner une perte de travail si le serveur n’a pas été en mesure de sauvegarder le travail non sauvegardé. Pour cette raison, nous encourageons les utilisateurs à se réauthentifier par eux-mêmes auprès du Fournisseur d’identité lorsqu’ils le souhaitent. 

!!! warning "Focus"
    L’utilisateur **doit impérativement** se ré-authentifier auprès du Fournisseur d’Identité même lorsque leur session sur l’IDP est toujours valide car Squash rejettera l’authentification si la même assertion est présentée. 

##### Options

Espace de nom : *saml.session*

| Propriété | Commentaire |
|--|--|
| max-auth-time | Définit en secondes la durée de vie d’une assertion d’authentification avant que l’utilisateur doive se réauthentifier auprès du Fournisseur d’identité.<br/>Valeur par défaut : 864000 (environ 10 jours) |

#### Proxy

Si Squash TM est déployé derrière un mandataire inverse (reverse proxy) ou un équilibreur de charge, les modifications appliquées à vos URLs et projets peuvent conduire à des différences entre la requête actuelle et la requête attendue. Vous pouvez entrer ce proxy dans la configuration pour indiquer à Squash TM comment justifier ces remaniements lors des échanges de messages avec le Fournisseur d’identité (IDP).

##### Options

Espace de nom : *saml.proxy*

| Property | Comments |
|--|--|
| enabled | L’interrupteur principal qui active le mandataire (*proxy*) et les options ci-dessous.<br/>Valeur par défaut : false |
| server-name | Le nom d’hôte du mandataire inverse. Cette propriété n’a pas de valeur par défaut et doit être fournie. <br/>Valeur par défaut : aucune |
| scheme | Le protocole utilisé par le mandataire inverse (*reverse-proxy*) pour les communications sortantes. <br/>Valeur par défaut : https |
| server-port | Le port utilisé par le mandataire inverse pour les communications sortantes.<br/>Valeur par défaut : 443 |
| context-path | Le chemin de contexte de l’application, tel que servi par le mandataire inverse.<br/>Valeur par défaut : /squash |
| include-port-in-url | Cette propriété indique si le numéro de port doit être explicitement inclus dans l’URL de requête (même lorsqu’un port par défaut est utilisé et qu’il pourrait donc être ommis). <br/>Valeur par défaut : true |

#### Informations Compte utilisateur 

Selon la configuration du serveur IDP, les assertions en réponse aux demandes d’authentification peuvent contenir des informations supplémentaires sur le compte utilisateur. Dans le document de l’assertion, ces attributs supplémentaires sont listés sous le libellé <AttributeStatement\>. Les propriétés listées ci-dessous autorisent Squash TM à utiliser ces attributs. 

Veuillez noter que Squash TM ne demandera pas ces attributs via une clause Requested Attributes (voir [Protocole SAML - Extension pour la demande d’attributs par requête](https://lists.oasis-open.org/archives/security-services/201506/msg00001/protocol_extension_draft.pdf)) car cette fonctionnalité n’est pas toujours supportée et dépend de l’IDP. Au lieu de cela, vous devez vous assurer que l'IDP ajoute ces attributs à l'assertion dans tous les cas.

##### Options

Espace de nom : *saml.user-mapping*

| Propriété | Commentaire |
|--|--|
| alternate-username | Utilise la valeur de cet attribut en tant que nom d’utilisateur dans Squash TM au lieu du NameID nominal. Cela vous permet de travailler autour des limites de l’IDP si ce dernier ne peut pas vous fournire le NameIDFormat dont vous avez besoin (par ex. : d’anciennes versions d’Azure).<br/>Valeur par défaut : aucune (fonctionnalité désactivée) |
| first-name | Désigne l’une des valeurs d’un attribut supplémentaire en tant que prénom d’un compte utilisateur dans Squash TM. Cette fonctionnalité est seulement utilisée lorsqu’un compte utilisateur est sur le point d’être créé (nouvel utilisateur) : les comptes utilisateurs déjà existants ne seront pas mis à jour de cette façon.  <br/>Valeur par défaut : aucune |
| last-name | Désigne l’une des valeurs d’un attribut supplémentaire en tant que nom de famille d’un compte utilisateur dans Squash TM. Cette fonctionnalité est seulement utilisée lorsqu’un compte utilisateur est sur le point d’être créé (nouvel utilisateur) : les comptes utilisateurs déjà existants ne seront pas mis à jour de cette façon.<br/>Valeur par défaut : aucune |
| email | Désigne l’une des valeurs d’un attribut supplémentaire en tant qu’adresse email d’un compte utilisateur dans Squash TM. Cette fonctionnalité est seulement utilisée lorsqu’un compte utilisateur est sur le point d’être créé (nouvel utilisateur) : les comptes utilisateurs déjà existants ne seront pas mis à jour de cette façon. <br/>Valeur par défaut : aucune |

#### Historique

Si vous rencontrez des difficultés à utiliser SAML, vous pouvez activer une journalisation plus détaillé afin de savoir ce qui ne fonctionne pas. Pour cela, ajoutez les lignes suivantes à la configuration :

    logging.level.org.squashtest.tm.plugin.saml=TRACE
    logging.level.sqsaml.org.springframework.security=TRACE
    logging.level.org.springframework.security.web.authentication=TRACE
    logging.level.org.opensaml=TRACE


**TRACE** est le niveau d’historique le plus précis. Si vous trouvez cela trop verbeux, vous pouvez plutôt utiliser le niveau **DEBUG**. 

!!! warning "Focus"
    Squash TM doit être redémarré afin de prendre en compte votre nouvelle configuration. 

### Appendice
### À propos de la cryptographie 

Ce chapitre aborde brièvement quelques concepts utilisés dans un scénario SAML. Il tente de clarifier quelques détails pour le lecteur non-familier avec SAML. Cette partie restera concise car le but n’est pas d’expliquer en détails ces mécanismes, mais plutôt d’expliciter à quoi réfèrent les options listées dans les chapitres précédents. 

##### Utilisation

Les données échangées entre les acteurs de conversations SSO font un usage important de matériel cryptographique. Les buts sont d’assurer que : 

1. Le contenu des messages reste inaccessible aux parties non-désirées ;
2. Le contenu n'a pas été altéré ;
3. Le contenu soit envoyé de l’origine voulue ;
4. L’origine voulue soit bien celle qu’elle prétend être.

La première préoccupation de la liste est couverte par le **chiffrement**, qui rend les données indéchiffrables, sauf par le destinataire du message.
La deuxième préoccupation est couverte par la **signature** du contenu, un condensé (digest) chiffré du contenu qui peut être comparé avec le message reçu pour la détection d’erreurs. 
La troisième préoccupation est couvert par le **certificat** de l’expéditeur, qui lie l’identité de l’expéditeur à la clé qui a généré la signature. 
La quatrième préoccupation est couverte par la **validation du chemin de certificat**, qui établit une chaîne de confiance entre le certificat et l’autorité de certification. 

#### Couples clé publique / clé privée

Le chiffrement et la signature sont basés sur l’utilisation de couples de clé publique / clé privée, qui sont utilisés dans des rôles quelque peu (mais pas tout à fait) symétriques. La clé publique est contenue dans un certificat indiquant son propriétaire, puis elle est publiée à toute personne intéressée. D’autre part, la clé privée correspondante, n’est connue que de son propriétaire. Chaque serveur impliqué, dans notre cas il s’agit de Squash TM et du Fournisseur d’identité, possède un ou plusieurs couples de clés et certificats de ce type.

##### Chiffrement

Pour chiffrer un message, l’expéditeur applique un algorithme de chiffrement sur le message avec la **clé publique du destinataire**. 

Une fois chiffré, le destinataire peut récupérer le contenu original en utilisant sa clé privée de destinataire. Étant donné qu’il est le seul détenteur de cette clé privée, il est le seul à pouvoir accéder à ce contenu. 

##### Signature

Pour signer un message, l’expéditeur génère d’abord un digest du message avec d’un algorithme de digest. Le résultat est ensuite chiffré à l’aide d’un algorithme de chiffrement avec la **clé privée de l’expéditeur** et le résultat est joint au message signé.

Le destinataire peut ensuite vérifier la signature. Pour cela, il déchiffre d’abord la signature avec la **clé publique de l’expéditeur**. Puis, il applique le même algorithme de digest au message reçu. Enfin, les deux digests sont comparés. S’ils correspondent parfaitement, on peut conclure que : 

- Quel que soit l’expéditeur, celui-ci est bien le propriétaire de la clé privée ;
- Le message provient bien du bon expéditeur, tel que déclaré dans le certificat lié à la clé publique correspondante ;
- Aucun tiers n’a altéré le message après signature, car les deux digests correspondent.

!!! info "Info"
    Dû à la relation étroite entre une clé publique et un certificat, ces deux termes sont souvent employés de façon peu rigoureuse. Mais cette confusion est en général inoffensive et elle est également faite au sein de ce document.  

#### Certificats

La première mission d’un certificat est de lier une identité à une clé publique. Le format de certificat standard dans le monde Java est X.509. Il contient de nombreuses informations, notamment : 

- La clé publique ;
- L’identité (plus qu’un nom, il s’agit d’une collection d’informations sur le sujet) ;
- Les moyens de vérifier l’Autorité de certification elle-même (dans les versions récentes du format) ;
- La période de validité du certificat ;
- Etc.

Cette liste n’est pas exhaustive. Si cela vous intéresse, vous pouvez trouver plus d’informations sur ce sujet sur Internet. 

Étant donné qu’un certificat détient une clé publique et une identité, il permet à la fois la vérification de signatures, d’identité, ou des deux. La vérification de signature est abordée dans la partie [Signature](#Signature) et il s’agit uniquement d’une question de calcul. La vérification d’identité implique la construction d’une chaîne de confiance, selon si un certificat donné est à priori considéré comme fiable ou non par le serveur qui le reçoit, et s’il requiert une ICP. 

##### Infrastructure à clés publiques (ICP ou PKI = Public Key Infrastructure)

L’ICP est le système global qui établit la confiance entre les parties. Une partie importante de l’ICP est la hiérarchie des Autorités de Certification (AC). Les autorités de niveau supérieur se portent garantes des autorités inférieures. Physiquement, elles sont implémentées en tant que famille de certificats. Une AC se porte garante d’un autre certificat (dont le propriétaire peut être une autre AC) en le signant avec son propre certificat. L’AC de plus haut niveau est appelée l’AC Racine et est **de facto** considérée comme fiable au sein de l’organisation. Les autres certificats de niveaux inférieurs qui agissent en tant qu’autorités sont appelées des AC Intermédiaires. 

Dans une application Java, le keystore est le fichier dans lequel sont installés tous les ACs et certificats de confiance. 

##### Validation du chemin de certification par le PKIX

Un serveur Java tel que Squash TM peut vérifier l’authenticité d’un certificat inconnu en validant le chemin de certification entre le certificat et l’AC Racine. Ce processus est connu en tant que l'algorithme PKIX.

L’algorithme PKIX permet l’inspection du chemin de certification déclaré par le certificat sous surveillance et de vérifier par rapport au keystore. Si le chemin déclaré concorde avec le contenu du keystore, le certificat sera alors de confiance. 

Le chemin de certificat est validé “de bas en haut” jusqu’à ce que l’AC Racine ou une ancre de confiance soit rencontrée. Une ancre de confiance est tout certificat signalé comme étant automatiquement de confiance pour un contexte spécifique. Différents certificats peuvent être utilisés en tant qu’ancres de confiance pour différents contextes, par exemple la validation d’un certificat ou d’une signature SSL peut être basée sur différentes ancres de confiance. 

Des vérifications supplémentaires peuvent également avoir lieu, notamment la vérification d’une date d’expiration, ou celle d’une abrogation prématurée émise par une AC de niveau supérieur.

Si à un moment un certificat déclaré dans le chemin est absent du keystore, la validation échouera. Par conséquent, tous les certificats, CA, etc. concernés par la mise en oeuvre de Squash + SAML doivent être installés dans le keystore de Squash TM. 

##### Certificats signés


Les certificats signés sont des certificats qui ont été délivrés et approuvés par une Autorité de Certification. L’approbation ici signifie que l’autorité s’est assurée que l’émetteur du certificat candidat est réellement qui il prétend être avant de signer le certificat avec son propre certificat. Le certificat signé peut être vérifié par plus tard en un validant un chemin de certification. 

Un certificat signé est intrinsèquement plus sécurisé qu’un certificat autosigné. 

##### Certificats auto-signés

Ces certificats sont communs dans les entreprises car ils sont rapides et peu chers à produire, tant que le risque d’usurpation d'identité est atténué par d’autres moyens dans un environnement contrôlé. 

Par définition, un certificat autosigné ne nécessite pas de chemin de certification ascendant, puisque son identité ne peut être vérifiée par la PKIX. De ce fait, il doit être installé et considéré comme une ancre de confiance dans le keystore de l'entité utilisatrice, sinon la validation de l’identité échouera.

!!! info "Info"
    Un certificat autosigné n’est pas néccessairement une mauvaise chose : après tout, l’AC Racine elle-même est un certificat autosigné et aucun autre certificat ne s’en porte garant. 

##### Profil d'interopérabilité des métadonnées (Meta IOP)

Les métadonnées SAML sont des candidates indéniables à une inspection minutieuse lors du démarrage, mais le cas des échanges de messages durant le SSO est contestable. D’après les [Spécifications des métadonnées IOP SAML](http://docs.oasis-open.org/security/saml/Post2.0/sstc-metadata-iop.pdf), la validation du chemin de certification pour les messages SAML pourrait ne pas être effectuée sans que la sécurité ne soit compromise :

“Un fichier de métadonnées signé répondant à cette spécification est sémantiquement équivalent à une infrastructure à clés publiques basées sur X.509. De ce fait, il y a peu d’intérêt à rajouter ce niveau de complexité lié à la validation de ce certificat, tel que spécifié dans la [RFC5280]”

Cela s'explique ainsi : tant que tous les certificats impliqués dans les conversations SAML sont déclarées dans les métadonnées et que ces métadonnées ont déjà passé des contrôles de sécurité, il est inutile de les revérifier à chaque fois qu’un message est reçu. 
Les vérifications du chiffrement et de la signature sont toujours nécessaires pour s’assurer que le message n’a pas été compromis mais l’expéditeur sera considéré comme étant de confiance du seul fait qu’il détienne les clés déclarées dans les métadonnées. 

!!! info "Info"
    Cette approche est seulement applicable aux messages SAML. La connexion HTTPS qui les transmet est un tout autre sujet et elle fait appel à ses propres contrôles de sécurité.

<div  class="footnotes">
<hr/>
<ol>
<li  id="fn:1">
<p>La seule exception est couverte dans Traitement des métadonnées <a  href="#fnref:1"  rev="footnote">&#8617;</a></p></li>
</ol>
</div>
