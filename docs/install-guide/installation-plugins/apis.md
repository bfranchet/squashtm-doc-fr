# Les APIs Squash TM

Squash TM dispose de deux plugins d'API : l'API REST et l'API REST Admin. Il existe en plus de ces plugins deux autres APIs embarquées dans les plugins Xsquash4Jira et Bibliothèque d'actions.
<br/>L’API Xsquash4Jira est conditionnée par la présence du plugin API REST Admin. Elle n’est donc accessible que si le plugin API REST Admin est installé sur Squash TM.

Ces APIs ne nécessitent pas de configuration particulière mais elles empbarquent toutes une documentation.

Pour accéder à la documentation des APIs de Squash TM, renseigner les URL suivantes dans le navigateur :

- API REST : *url_squash/api/rest/latest/docs/api-documentation.html*
- API REST Admin : *url_squash/api/rest/latest/docs/admin-api-documentation.html*
- API REST Xsquash4Jira : *url_squash/api/rest/latest/docs/jira-sync-api-documentation.html*
- API REST Bibliothèque d’actions : *url_squash/api/rest/latest/docs/action-word-api-documentation.html*
