# Installer les plugins et la licence Squash TM

## Installation des plugins Squash TM

Pour installer les plugins sur Squash TM, il faut suivre la procédure suivante quel que soit l'OS d'installation :

1. Télécharger le plugin depuis le site [https://www.squashtest.com/](https://www.squashtest.com/community-download) ou depuis les liens fournis par l'Équipe Support Squash
2. Décompresser le plugin
3. Récupérer la totalité des fichiers .jar qu'il contient et les déposer dans le dossier suivant : 
    - */opt/squash-tm/plugins* : pour un tarball
    - *C:\<rep>\squash-tm\plugins* : pour Windows
4. Sous Linux, attribuer les droits de lecture à l’utilisateur squash-tm sur les .jar.
5. S'il s'agit d'un plugin commercial, s'assurer que le fichier de licence 'squash-tm.lic' est bien présent dans le dossier 'license'
6. Redémarrer Squash TM pour que l'installation des plugins soit prise en compte

!!! info "Info"
    La liste des plugins installés peut être consultée directement dans Squash TM par un administrateur fonctionnel dans les [informations système](../../../admin-guide/gestion-systeme/infos-systeme).

Une fois installés, certains plugins sont directement opérationnels, c'est le cas pour les plugins d'API, de reporting ou encore la Bibliothèque d'actions. D'autres nécessitent par contre une configuration au niveau serveur ou au niveau de l'administration.

!!! tip "En savoir plus"
    Pour en savoir plus sur la configuration des plugins au niveau serveur, consulter la page [Configurer les plugins](./configurer-plugins-bt-outils-tiers.md). Pour les autres configurations, consulter le [Guide Administrateur](../../admin-guide/presentation-generale/presentation-administration-squash.md) pour plus d'informations.

## Installation de la licence commerciale

L'utilisation des plugins commerciaux de Squash TM est conditionnée par la présence d'un fichier de licence : *squash-tm.lic*. Ce fichier est nécessaire pour démarrer Squash TM avec les plugins commerciaux.
<br/>Il est fourni au client par le Support Squash suite à la contractualisation d'une nouvelle licence ou d'un renouvellement.

Le fichier de licence est **unique** pour chaque client. Il comporte les informations suivantes :

- une date de fin de validité,
- un nombre d'utilisateurs,
- la liste des plugins commerciaux autorisés en fonction de la licence ou de la combinaison de licences contractées

Ce fichier de licence peut être utilisé par le client aussi bien pour ses instances de production et que pour ses instances de préproduction. 

Pour installer le fichier sur Squash TM, voici les actions à réaliser : 

1. Dans le répertoire 'plugins' de Squash TM, créer un sous-répertoire intitulé 'license'. Attention, le répertoire s'écrit 'license', avec un S.
2. Déposer le fichier 'squash-tm.lic' communiqué par l'Équipe Support Squash
3. Puis redémarrer Squash TM

Le fichier de licence contrôle le nombre d'utilisateurs actifs sur Squash TM. Un message s'affiche pour alerter les administrateurs lorsque le nombre maximum d'utilisateurs est dépassé. Si ce nombre est dépassé de plus de 20%, la création de nouveaux utilisateurs est bloquée.

Deux mois avant la date de fin de validité de la licence, un message alerte les administrateurs. Passé cette date, un message indique à l'ensemble des utilisateurs que la licence est expirée. Si la licence n'est pas renouvellée deux mois après la date de fin, Squash TM s'éteint. Il faut alors retirer les plugins commerciaux pour pouvoir redémarrer l'application.

## Installation du Xsquash pour Jira Server

Xsquash est un composant additionnel disponible sur l’Atlassian Marketplace pour Jira Server et Jira Data Center qui renforce l’intégration entre Squash TM et Jira. Il affiche les données des cas de tests et des exécutions Squash TM directement dans les tickets Jira qui ont été synchronisés dans Squash TM via Xsquash4jira.

!!! info "Info"
    Les données sont affichées dans Jira via l'API REST de Squash TM, ce plugin n'écrit pas dans la base de données de Jira.

Ce plugin requiert à minima les prérequis suivants pour son installation : 

-	Jira Server ou Data Center 8+
-	Squash TM version 1.19.0+
-	Squash REST API 1.1.0+
-	Xsquash4Jira 1.2.0+ : Le plugin Xsquash4Jira doit avoir été installé dans Squash TM et être configuré pour au moins un projet. 

!!! tip "En savoir plus"
    Pour plus d’informations sur la configuration du plugin Xsquash4Jira, se référer à la page dédiée [Configurer Xsquash4jira dans Squash TM](../../admin-guide/configuration-xsquash/configurer-xsquash4jira.md)

Pour installer le plugin Xsquash sur Jira Server ou Jira Data Center, voici la procédure à suivre : 

1.	Se connecter à l’instance Jira en tant qu’administrateur.
2.	Aller sur l’'Administration Jira' et cliquer sur 'Gérer les Apps'. 'L'Atlassian Marketplace pour Jira' s'affiche.
3.	Rechercher Xsquash sur la Marketplace. Les résultats de recherche incluent les versions de l’application compatibles avec l'instance Jira.
5.	Cliquer sur **[Installer]** pour télécharger et installer l’application. 
6.	Cliquer sur Fermer dans la fenêtre de dialogue.

Le plugin Xsquash nécessite une configuration pour être opérationnel.

!!! tip "En savoir plus"
    Pour plus d’informations sur la configuration du plugin Xsquash, se référer à la page dédiée [Configurer Xsquash dans Jira](../../admin-guide/configuration-xsquash/configurer-xsquash.md)

## Installation de l'application Xsquash Cloud sur Jira Cloud

Xsquash Cloud est une application hébergée sur un serveur interne à l’entreprise Henix qui renforce l’intégration entre Squash TM et Jira Cloud. Il affiche les données des cas de tests et des exécutions Squash TM directement dans les tickets Jira qui ont été synchronisés dans Squash TM via Xsquash4jira.

Cette application n'est accessible qu'aux clients disposant d'une licence.

!!! info "Info"
    Les données sont affichées dans Jira via l'API REST de Squash TM, cette application n'écrit pas dans la base de données de Jira Cloud.

Elle requiert à minima les prérequis suivants pour son installation : 

- Jira Cloud
- Squash TM version 1.19.0+
- Squash REST API 1.1.0+
- Xsquash4Jira 1.2.0+ : le plugin Xsquash4Jira doit avoir été installé dans Squash TM et être configuré pour au moins un projet. 

!!! tip "En savoir plus"
    Pour plus d’informations sur la configuration du plugin Xsquash4Jira, se référer à la page dédiée [Configurer Xsquash4jira dans Squash TM](../../admin-guide/configuration-xsquash/configurer-xsquash4jira.md)

Il existe deux façons d’installer l’application Xsquash Cloud sur Jira Cloud :

### Installer manuellement Xsquash Cloud

Pour installer manuellement Xsquash Cloud, voici la procédure à suivre : 

1. Se connecter à l’instance Jira Cloud en tant qu’administrateur.
2. Cliquer sur le bouton ![Paramètres](resources/parametres.png) et choisir 'Apps' puis l’option 'Gérer les apps'.
3. Cliquer sur 'Paramètres' pour ouvrir la fenêtre 'Settings'.

    ![Settings](resources/settings.png){class="pleinepage"}

4. Cocher l’option 'Enable development mode' puis cliquer sur le bouton **[Apply]**.
5. Sur la page 'Gérer les apps', cliquer sur 'App importable' pour ouvrir la fenêtre 'Upload app'.
6. Renseigner l’url qui vous aura été fournie par l’Équipe Support Squash puis cliquer le bouton **[Upload]**.

    ![Upload app](resources/upload-app.png){class="pleinepage"}

7. L’application Xsquash Cloud apparaît ensuite dans liste des applications installées.

    ![Application installée](resources/installed-apps.png){class="pleinepage"}

### Installer Xsquash Cloud via l’API

Avant d’installer l’application Xsquash Cloud dans Jira Cloud par API, il faut au préalable un compte administrateur disposant d’un jeton d’API. 

Pour générer un jeton d’API, il faut suivre la procédure suivante :

1. Être connecté avec le compte administrateur puis dans "Votre profil et vos paramètres", cliquer sur "Profil"
2. Cliquer sur "Gérer votre compte"
3. Cliquer sur "Sécurité"
4. Dans la section 'Jeton d'API', cliquer sur "Créer et gérer des jetons d'API"
5. Cliquer sur "Créer un jeton d'API" puis saisir un libellé
6. Saisir un libellé puis cliquer sur "Créer"
7. Copier le jeton


Voici la procédure à suivre pour l’installation de l’application Xsquash Cloud via l’API : 

1.	Faire une requête GET en BASIC AUTH à l’URL suivante :

        https://Url-de-Jira.atlassian.net/rest/plugins/1.0/?os_authType=basic

    avec dans le header, l’information suivante : 

        Accept: application/vnd.atl.plugins.installed+json
    
    Pour l’authentification, utiliser le Username de l’administrateur et son jeton d’API.

2. Récupérer la valeur du header « upm-token » dans la réponse

3. Faire une requête POST en BASIC AUTH à l’URL suivante en remplaçant 'Upm-Token' par la valeur du header « upm-token » récupéré précédemment :

        https://Url-de-Jira.atlassian.net/rest/plugins/1.0/?token=Upm-Token
    
    avec dans le header, l’information suivante : 

        Content-type: application/vnd.atl.plugins.uri+json
        Accept: application/json
    
    et dans le body la commande suivante avec l’url qui vous aura été fournie par l’Équipe Support Squash :

        {
        "pluginUri": "https://Url-Xsquash-Cloud.com/atlassian-connect.json",
        "pluginName": "Xsquash Cloud"
        }

    et pour l’authentification, utiliser le Username de l’administrateur et son jeton d’API.

4. L’application Xsquash Cloud apparaît ensuite dans liste des applications installées au niveau de l'administration Jira Cloud.

Il faut ensuite configurer l'application pour pouvoir l'utiliser.

!!! tip "En savoir plus"
    Pour plus d’informations sur la configuration de l'application Xsquash Cloud, se référer à la page dédiée [Configurer Xsquash Cloud sur Jira Cloud](../../admin-guide/configuration-xsquash/configurer-xsquash.md#configurer-xsquash-cloud-sur-jira-cloud)
