# Configurer les plugins de connexion aux outils tiers 

Squash TM se connecte grâce à ses plugins à plusieurs outils tiers.
<br/>Dans la plupart des cas, il suffit d'[installer le plugin](./installer-plugins.md#installation-des-plugins-squash-tm) puis de le configurer dans l'administration de Squash TM pour l'utiliser.

!!! tip "En savoir plus"
    Pour en savoir plus sur la configuration des plugins, consulter la page  [Gestion des serveurs](../../admin-guide/gestion-serveurs/gerer-bugtracker-serveur-synchro.md) dans le Guide Administrateur de Squash TM.

Néanmoins, certains plugins nécessitent une configuration complémentaire au niveau serveur : 

- Xsquash4jira 
- Workflow Automatisation Jira
- Jira Bugtracker
- Redmine Bugtracker
- Redmine Exigences
- Tuleap

## Xsquash4Jira et Workflow Automatisation Jira

Xsquash4Jira et Workflow Automatisation Jira sont des plugins de synchronisation fonctionnant avec Jira Server, Jira Data Center et Jira Cloud.

L'intervalle de synchronisation de ces deux plugins est géré au niveau du fichier *squash.tm.cfg.properties* de Squash TM via cette propriété : 

    squash.external.synchronisation.delay = 300

Le délai entre deux synchronisations est par défaut fixé à 300 secondes soit 5 min. Pour la modifier, il suffit d'indiquer la valeur souhaitée en secondes, d'enregistrer et de redémarrer Squash TM. 

Une deuxième propriété dans le fichier *squash.tm.cfg.properties* de Squash TM permet de définir la taille du batch pour les synchronisations (nombre de tickets traités par paquet) :

    plugin.synchronisation.jira.batchSize = 50

La taille du batch est réglé par défaut sur 50 ce qui correspond à la valeur par défaut côté Jira. Dans Jira Server, cette valeur est gérée par la propriété *jira.search.views.default.max*. Il est préférable que les valeurs soient identiques sur Jira et Squash TM.

Le plugin Workflow Automatisation Jira ne fonctionne que si le plugin Jira Bugtracker Server ou Jira Bugtracker Cloud est au préalable installé sur Squash TM.


## Configuration de Oauth pour Jira

Pour les connecteurs Jira (Jira Bugtracker et Xsquash4Jira), il est possible d’utiliser un protocole OAuth 1a pour l’authentification.

Cela nécessite de configurer les URLs impliquées dans l’échange de jetons et les modalités de signature des requêtes.

Les éléments de configuration spécifiques à Jira sont :

- URL jetons temporaires : POST [adresse de Jira] /plugins/servlet/oauth/request-token
- URL jetons d’accès : POST [adresse de Jira] /plugins/servlet/oauth/access-token
- URL d’autorisation : [adresse de Jira]/plugins/servlet/oauth/authorize
- Méthode de signature : RSA-SHA1
- Consumer Key et Secret : voir ci-dessous

Il est nécessaire de configurer un lien d’application côté Jira Server, avec l’option « créer un lien entrant ». En effet, la configuration d’un lien entrant permet de configurer un lien dans les deux sens, c’est-à-dire de Jira vers Squash et de Squash vers Jira. Pour ce faire il faut, en tant qu’administrateur :

1. Aller dans le menu Administration > Application
2. Cliquer sur « Liens entre applications » sous l’item « Intégrations » dans la barre de menu à gauche. La page 'Configurer les liens entre applications' s'affiche.
3. Renseigner l'url simple de Squash TM dans le champ puis cliquer sur **[Créer un nouveau lien]**
4. Dans la pop-up 'Configurer l'URL d'application', valider l'utilisation de l'url de Squash TM pour le lien puis cliquer sur **[Continuer]**
5. Dans la pop-up 'Lier applications', renseigner librement la première partie du formulaire et bien cocher la case **"Créer un lien entrant"** puis cliquer sur **[Continuer]**
6. Renseigner la seconde partie du formulaire qui permet de définir les informations suivantes :
    - Clé de consommateur : c’est la consumer key à renseigner côté Squash TM, et qui identifie le service d’échange de jetons pour le SSO.
    - Nom du consommateur : le nom sous lequel Squash TM sera connu de Jira, et qui permettra à l’utilisateur lors de l’autorisation des jetons de reconnaître qu’il est bien en train d’autoriser Squash TM.
    - Clé publique : la clé publique (c'est-à-dire le « secret partagé » en langage OAuth côté Jira) qui correspond à la clé privée (c'est-à-dire le « secret » côté Squash).

![Créer un lien entrant](resources/oauth.png){class="centre"}

Le secret et secret partagé sont respectivement une clé privée et une clé publique. Il est possible de les générer grâce à openssl :

    openssl genrsa -out jira_privatekey.pem 1024
    openssl req -newkey rsa:1024 -x509 -key jira_privatekey.pem -out jira_publickey.cer -days 365
    openssl pkcs8 -topk8 -nocrypt -in jira_privatekey.pem -out jira_privatekey.pcks8
    openssl x509 -pubkey -noout -in jira_publickey.cer  > jira_publickey.pem

!!! warning "Focus"
    C’est la clé publique qui est demandée par Jira et non le certificat (d’où le jira_publickey.pem)
    <br/>La clé privée est au format PKCS#8 (jira_privatekey.pkcs8). 
    
Une fois le secret et le secret partagé créés, il faut les renseigner respectivement dans Squash TM et Jira. Attention à ne pas oublier de retirer les en-têtes et les espaces de la clé privée au préalable.

!!! tip "En savoir plus"
    Pour configurer le protocole OAuth 1a dans Squash TM, consulter la page  [Configurer le protocole d'authentification OAuth 1a](../../admin-guide/gestion-serveurs/configurer-oauth.md) dans le Guide Administrateur de Squash TM.


## Redmine Bugtracker et Redmine Exigences

Les plugins Redmine Bugtracker et Redmine Exigences de Squash TM nécessitent la présence d'un plugin complémentaire côté Redmine.

!!! warning "Focus"
    L’installation du plugin côté Redmine n’est pas optionnelle. Ce plugin Redmine est nécessaire pour le fonctionnement des plugins Squash TM.

Pour installer le plugin côté Redmine, il faut :

1. Télécharger le plugin redmine-rest-ext-api-plugin-1.0.0.zip pour Redmine 3 ou redmine4-rest-ext-api-plugin-1.0.0.zip pour Redmine 4 
2. Arrêter l’application Redmine
3. Extraire puis déposer le contenu du plugin dans le dossier "plugins" de Redmine
4. Redémarrer l'application Redmine

Puis dans l'interface Redmine avec un compte administrateur :

1. Activer l'API en cochant la case « Activer l’API REST » dans Administration > Configuration > Onglet API
2. Vérifier que le plugin Squash TM est bien installé sur Redmine depuis Administration > Plugins

![Plugin Squash Tm sur Redmine](resources/redmine-plugin.png){class="pleinepage"}

Outre l'installation de ce plugin, le plugin Redmine Exigences ne peut fonctionner que si le plugin Redmine Bugtracker est au préalable installé sur l'instance Squash TM.

## Tuleap

### Configuration des propriétés de mapping

Le plugin Tuleap Bugtracker peut être paramétré avec des propriétés à renseigner dans le fichier de configuration de Squash TM. Un fichier livré avec le plugin présente une configuration exemple. Il est possible de copier-coller le contenu de ce fichier d’exemple vers le fichier *squash.tm.cfg.properties* de Squash TM.

Les propriétés attendues sont les suivantes :

| Propriété | Commentaire |
|--|--|
| plugin.bugtracker.tuleap.field.summary | Nom du champ « Résumé » dans Tuleap |
| plugin.bugtracker.tuleap.field.details | Nom du champ « Détail » dans Tuleap |
| plugin.bugtracker.tuleap.field.assignee | Nom du champ « Assigné à » dans Tuleap |
| plugin.bugtracker.tuleap.field.severity | Nom du champ « Sévérité » dans Tuleap |
| plugin.bugtracker.tuleap.field.status | Nom du champ « Statut » dans Tuleap |
| plugin.bugtracker.tuleap.field.attachment | Nom du champ « Pièce jointe » dans Tuleap |

Toutes les propriétés du groupe plugin.bugtracker.tuleap.field sont des mappings qui indiquent à Squash le nom technique de certains champs Tuleap. Ces propriétés sont utilisées uniquement pour l’affichage des anomalies dans les blocs anomalies connues des Exécutions et Pas d’exécution ainsi que dans les onglets anomalies connues des différents espaces.

### Import du certificat dans la JVM

Par défaut l’API Tuleap n’est accessible que par protocole https. Pour communiquer en protocole https, la JVM hébergeant Squash TM doit connaître le certificat de Tuleap. La procédure dépend du système considéré mais fait généralement intervenir l’outil keytool pour importer le certificat dans la JVM. La procédure est décrite ci-dessous.


## Connexion à un outil tiers en https

Pour utiliser un outil tiers configuré en https, il faut d'aborder installer les .jar du plugin correspondants dans Squash TM puis importer le certificat de l'outil dans la JVM de Squash TM.

Voici la procédure à suivre pour récupérer et importer le certificat de l'outil dans la JVM de Squash TM : 

1. Arrêter Squash TM
2. Télécharger le certificat de l'outil : 
    - Renseigner l'url de outil en https dans le navigateur
    - Cliquer sur le cadenas dans la barre d’adresse
    - Dans Firefox par exemple, cliquer sur la flèche à côté de "Connexion sécurisée", puis sur "Plus d'informations", [Afficher le certificat], et télécharger le fichier PEM (cert) dans la section 'Divers'.
3. Identifier la JVM de Squash TM
4. Sur une machine Linux :
    - Passer la commande (compatible système Unix) en modifiant les valeurs des variables selon l'environnement :
    
            export hostname= serveurOutil
            export cacerts=/usr/lib/jvm/java-openjdk/jre/lib/security/cacerts
            export cert=/etc/ssl/certs/ssl-cert-outil.pem
            keytool -importcert -alias $hostname -file $cert -keystore $cacerts -storepass changeit -noprompt

5. Sur une machine Windows :
    - Ouvrir l'invite de commande
    - Renseigner la commande pour entrer dans le bin de la JVM :
    
            cd C:\ProgramFiles\Java\jre\bin
    
    - Passer la commande pour importer le certificat : 

            keytool -importcert -alias Outil -file "C:/Downloads/outil-org.pem" -keystore "C:/ProgramFiles/Java/jre/lib/security/cacerts" -storepass changeit -noprompt
    
        Outil : alias du serveur
        <br/>"C:/Downloads/outil-org.pem" : Emplacement du certificat de l'outil
        <br/>"C:/ProgramFiles/Java/jre/lib/security/cacerts" : Emplacement du cacert de la JVM

6. Redémarrer Squash TM pour la prise en compte du certificat
