# Les plugins de Squash TM

Squash TM est un logiciel basé sur un cœur open source qui est téléchargeable gratuitement depuis le site [https://www.squashtest.com/](https://www.squashtest.com/community-download). Ses fonctionnalités peuvent être enrichies par divers plugins :

- plugins d'API
- plugins de reporting
- plugins pour la remontée d'anomalies vers les bugtrackers
- plugins de synchronisation d'exigences
- plugins pour l'automatisation des tests
- plugins de type assistant
- plugins d'authentification

Les plugins sont soit open source, freeware ou soumis à une licence commerciale.

## Plugins Freeware / Open source 

Voici la liste des plugins open source et freeware de Squash TM : 

| Catégorie | Plugin | Fichiers | Compatibilité | Fonctionnalités | Commentaire |
|--|--|--|--|--|--|
| API | API REST Squash TM | - plugin.api.rest.core-2.0.0.RELEASE.jar <br/>- plugin.api.rest.services-2.0.0.RELEASE.jar | 2.0+ | Permet de réaliser des requêtes portant sur les fonctionnalités de Squash TM accessibles à un utilisateur et d'accéder à la documentation associée  | Embarqué dans la distribution de Squash TM <br/>Open source |
| Reporting | Cahier des exigences éditable | - report.books.requirements.editable-2.0.0.RELEASE.jar | 2.0+ | Permet de générer un rapport contenant une sélection de fiches d'exigences au format éditable | Embarqué dans la distribution de Squash TM <br/>Open source |
| Reporting | Cahier de test éditable | - report.books.testcases.editable-2.0.0.RELEASE.jar | 2.0+ | Permet de générer un rapport contenant une sélection de fiches de cas de tests au format éditable | Embarqué dans la distribution de Squash TM <br/>Open source |
| Reporting | Cahier des exigences PDF  | - report.books.requirements.pdf-2.0.0.RELEASE.jar | 2.0+ | Permet de générer un rapport contenant une sélection de fiches d'exigences au format PDF | Plugin additionnel à télécharger <br/>Open source |
| Reporting | Cahier de test PDF | - report.books.testcases.pdf-2.0.0.RELEASE.jar | 2.0+ | Permet de générer un rapport contenant une sélection de fiches de cas de tests au format PDF | Plugin additionnel à télécharger <br/>Open source |
| Reporting | Rapport Avancement qualitatif | - report.qualitativecoverage-2.0.0.RELEASE.jar | 2.0+ | Permet de générer un tableau de bord de suivi des exigences testées | Embarqué dans le répertoire 'plugins' de Squash TM <br/>Open source |
| Bugtracker | Bugzilla Bugtracker | - plugin.bugtracker.bugzilla-2.0.0.RELEASE.jar | 2.0+ | Permet depuis Squash TM de déclarer des anomalies préremplies avec les données du test dans Bugzilla | Plugin additionnel à télécharger <br/>Freeware |
| Synchronisation Exigences | Xsquash4jira | - plugin.requirement.xsquash4jira-2.0.0.RELEASE.jar | 2.0+ | Permet de synchroniser des objets agiles Jira sous forme d'exigences dans Squash TM et de concevoir un plan d'exécution à partir d'une version ou d'un sprint définis dans Jira | Embarqué dans la distribution de Squash TM <br/>Freeware |
| Automatisation | Git connector | - plugin.scm.git-2.0.0.RELEASE.jar <br/>- org.eclipse.jgit-5.1.2.201810061102-r.jar | 2.0+ | Permet de transmettre des cas de test scriptés rédigés dans Squash TM vers un gestionnaire de source de type Git | Plugin additionnel à télécharger <br/>Freeware |
| Automatisation | Squash Autom | - plugin.testautomation.squashautom. community-2.0.0.RELEASE | 2.0+ | Permet de lancer l’exécution simple de tests automatisés depuis les plans d’exécution de Squash TM via l'orchestrateur Squash | Plugin additionnel à télécharger <br/>Freeware |
| Automatisation | Result publisher | - squash.tm.rest.result.publisher.community-2.0.0.RELEASE | 2.0+ | Permet de récupérer les résultats et les rapports d'exécution des tests automatisés exécutés avec l'orchestrateur Squash | Plugin additionnel à télécharger <br/>Freeware |
| Automatisation | Test plan retriever | - squash.tm.rest.test.plan.retriever.community-2.0.0.RELEASE | 2.0+ | Permet de récupérer certaines données des tests d'un plan d'exécution de Squash TM lorsque l'ordre d'exécution dans l'orchestrateur Squash est déclenché depuis un Pipeline | Plugin additionnel à télécharger <br/>Freeware |

Une partie de ces plugins est embarquée dans la distribution de Squash TM, ils sont donc déjà 'installés' au démarrage de Squash TM. Les autres sont à télécharger depuis le site [https://www.squashtest.com/](https://www.squashtest.com/community-download), puis à installer en suivant la procédure indiquée dans la partie [Installation des plugins Squash](./installer-plugins.md).

En outre, Squash TM s'interface nativement avec Mantis pour la remontée des anomalies et Jenkins pour l'automatisation des tests.

## Plugins sous licence commerciale 

Squash TM dispose également de plugins qui ne peuvent être utilisés qu'avec une licence commerciale. Voici la liste de ces plugins : 

| Catégorie | Plugin | Fichiers | Compatibilité | Fonctionnalités | Commentaire |
|--|--|--|--|--|--|
| API | API REST Admin | - plugin.api.rest.admin-2.0.0.RELEASE.jar | 2.0+ | Permet de réaliser des requêtes portant sur l'administration fonctionnelle de Squash TM et d'accéder à la documentation associée | Plugin de la licence Squash TM Premium |
| Authentification | LDAP | - security.ldap.fragment-2.0.0.RELEASE.jar <br/>- spring-ldap-core-2.3.2.RELEASE.jar <br/>- spring-security-ldap-5.0.6.RELEASE.jar | 2.0+ | Permet de déléguer l'authentification des utilisateurs à Squash TM à un annuaire de type LDAP | Plugin de la licence Squash TM Premium |
| Authentification | Active Directory | - security.ad.fragment-2.0.0.RELEASE.jar <br/>- spring-ldap-core-2.3.2.RELEASE.jar <br/>- spring-security-ldap-5.0.6.RELEASE.jar | 2.0+ | Permet de déléguer l'authentification des utilisateurs à Squash TM à un annuaire de type Active Directory | Plugin de la licence Squash TM Premium |
| Authentification | SAML | - security.saml-2.0.0.RELEASE.jar | 2.0+ | Permet de gérer l'authentification unique des utilisateurs à Squash TM via le protocole SAML 2.0 | Plugin de la licence Squash TM Premium |
| Bugtracker | Jira Bugtracker Server et Data Center | - plugin.bugtracker.jirarest-2.0.0.RELEASE.jar <br/>- jaxb-api-2.2.2.jar <br/>- jaxb-impl-2.2.3.jar | 2.0+ | Permet depuis Squash TM de déclarer des anomalies préremplies avec les données du test dans Jira Server et Data Center | Plugin de la licence Squash TM Premium |
| Bugtracker | Jira Bugtracker Cloud | - plugin.bugtracker.jiracloud-2.0.0.RELEASE.jar <br/>- jaxb-api-2.2.2.jar <br/>- jaxb-impl-2.2.3.jar | 2.0+ | Permet depuis Squash TM de déclarer des anomalies préremplies avec les données du test dans Jira Cloud | Plugin de la licence Squash TM Premium |
| Bugtracker | Redmine Bugtracker | - plugin.bugtracker.redmine3rest-2.0.0.RELEASE.jar <br/>- redmine-java-api-1.24.jar | 2.0+ | Permet depuis Squash TM de déclarer des anomalies préremplies avec les données du test dans Redmine | Plugin de la licence Squash TM Premium |
| Bugtracker | RTC Bugtracker | - plugin.bugtracker.rtc-2.0.0.RELEASE.jar | 2.0+ | Permet depuis Squash TM de déclarer des anomalies préremplies avec les données du test dans RTC | Plugin de la licence Squash TM Premium |
| Bugtracker | Tuleap Bugtracker | - plugin.bugtracker.tuleap-2.0.0.RELEASE.jar | 2.0+ | Permet depuis Squash TM de déclarer des anomalies préremplies avec les données du test dans Tuleap | Plugin de la licence Squash TM Premium |
| Synchronisation Exigences | Redmine Exigences | - plugin.requirement.redminerest-2.0.0.RELEASE.jar | 2.0+ | Permet de synchroniser des demandes Redmine sous forme d'exigences dans Squash TM | Plugin de la licence Squash TM Premium - Il nécessite l'installation du plugin Redmine Bugtracker |
| Reporting | Bilan de campagne et d'itération | - report.campaign.execution-2.0.0.RELEASE.jar | 2.0+ | Permet de générer un bilan de campagne ou d'itération au format éditable | Plugin de la licence Squash TM Premium |
| Assistant | Assistant campagne | - wizard.campaignassistant-2.0.0.RELEASE.jar | 2.0+ | Permet de concevoir un plan d'exécution à partir du résultat des exécutions précédentes | Plugin de la licence Squash TM Premium |
| Automatisation | Bibliothèque d'actions | - plugin.workspace.actionword-2.0.0.RELEASE.jar | 2.0+ | Permet de gérer les actions des cas de test BDD via une bibliothèque | Plugin de la licence Squash AUTOM Premium |
| Automatisation | Workflow Automatisation Jira | - plugin.workflow.automjira-2.0.0.RELEASE.jar | 2.0+ | Permet d'externaliser le process d'automatisation des tests via un workflow d'automatisation personnalisé dans Jira | Plugin de la licence Squash AUTOM Premium - Il nécessite l'installation du plugin Jira Bugtracker Server ou Cloud |
| Automatisation | Squash Autom | - plugin.testautomation.squashautom. premium-2.0.0.RELEASE | 2.0+ | Permet de lancer l’exécution avancée de tests automatisés depuis les plans d’exécution de Squash TM via l'orchestrateur Squash | Plugin de la licence Squash AUTOM Premium |
| Automatisation | Result publisher | - squash.tm.rest.result.publisher.premium-2.0.0.RELEASE | 2.0+ | Permet de récupérer les résultats et le détail des exécutions ainsi que les rapports d'exécution des tests automatisés exécutés avec l'orchestrateur Squash | Plugin de la licence Squash AUTOM Premium |
| Automatisation | Test plan retriever | - squash.tm.rest.test.plan.retriever.premium-2.0.0.RELEASE | 2.0+ | Permet de récupérer les données complètes des tests d'un plan d'exécution de Squash TM lorsque l'ordre d'exécution dans l'orchestrateur Squash est déclenché depuis un Pipeline | Plugin de la licence Squash DEVOPS Premium |

<!-- | Synchronisation Exigences | Polarion Exigences | plugin.requirement.polarion-2.0.0.RELEASE.jar | 2.0+ | Permet de synchroniser des Work items Polarion sous forme d'exigences dans Squash TM | Plugin de la licence Squash TM Premium |-->
