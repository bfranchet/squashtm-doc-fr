# Externalisation des pièces jointes

Squash TM permet de stocker les pièces jointes en dehors de la base de données, directement dans un système de fichiers. 
Par défaut, le stockage des pièces jointes se fait en base de données car il s’agit de la solution la plus simple à mettre en œuvre. 
En outre, elle convient parfaitement à des volumétries de données standard. 

Sur une nouvelle instance Squash TM, il suffit de paramétrer la fonctionnalité pour profiter de ce mode de stockage. 
Sur une instance existante, il faut d’abord extraire les pièces jointes existantes avant de procéder au changement de paramétrage.

!!! warning "Focus"
    Le passage d’un stockage en base de données vers un stockage fichiers a des conséquences, notamment en termes de sauvegarde des données. 
    Il incombe à l’administrateur système de faire les modifications nécessaires : 

    - Prévoir un système de sauvegarde des pièces jointes qui soit synchrone avec celui de la base de données.
    - Prévoir suffisamment d’espace pour stocker les pièces jointes et suivre l’évolution de cet espace
    - Donner les droits de lecture et d'écriture à l’utilisateur lançant le process Squash TM sur le dossier choisi pour accueillir les pièces jointes.

## Paramétrage

!!! danger "Attention"
    Si des pièces jointes existent déjà dans la base de données, il faut d'abord les [extraire](#extraire-les-pieces-jointes-existantes) **AVANT** de changer la configuration de Squash TM.

~SQUASH est le dossier de déploiement de Squash TM.

1. Arrêter Squash TM et éditer le fichier *squash.tm.cfg.properties* dans le répertoire ~SQUASH/conf
2. Ajouter la propriété suivante pour activer le stockage externe des pièces jointes : 

        squashtm.feature.file.repository = true

3. Ajouter la propriété suivante pour définir l'emplacement de stockage des pièces jointes : 
    
        squash.path.file.repository=<Chemin choisi pour le stockage des fichiers>. 
    
    Par défaut, si cette propriété est absente, Squash TM stockera les pièces jointes dans le répertoire ~SQUASH/attachments. 
    Cette étape est par contre nécessaire dans le cadre d'une extraction des pièces jointes depuis la base de données. 

4. Faire toutes les modifications système nécessaires, notamment si le chemin choisi n’est pas le chemin par défaut 
   (cela concerne notamment l’attribution des droits à l’utilisateur, qui doit pouvoir écrire et modifier les process côté Squash TM et aussi côté machine).
5. Redémarrer Squash TM 
6. Ajouter une nouvelle pièce jointe depuis Squash TM et vérifier qu'elle est bien stockée à l'emplacement choisi. 
   Vérifier que le téléchagement et la suppression de la pièce jointe fonctionnent correctement.

!!! warning "Focus"
    Une fois les pièces jointes stockées dans un système de fichiers, il n'est plus possible de revenir à un stockage en base de données à moins de risquer de perdre toutes les pièces jointes stockées dans le répertoire externe.

## Extraire les pièces jointes existantes

Pour passer d’un stockage en base de données à un stockage sur système de fichiers sur une installation existante de Squash TM, il faut migrer les pièces jointes de la base de données, vers le format de fichiers attendu par Squash TM.

Henix met à disposition un outil pour faire cette migration, sur le site [https://www.squashtest.com/](https://www.squashtest.com/community-download-plugins-tm). 
L’artefact à utiliser s'appelle *squash-tm-extract-attachment-tool-1.0.0.RELEASE*.

### Préconditions

!!! danger "Attention"
    Il est impératif de faire une sauvegarde de la base de données **AVANT** d’entamer la procédure de migration, afin de pouvoir restaurer en cas de problème.

### Procédure sur Linux

Pour extraire les pièces jointes existantes d'une base de données Squash TM vers un système de fichiers sur une machine Linux : 

~SQUASH est le dossier de déploiement de Squash TM.

1. Stopper Squash TM

2. Décompresser l’outil *squash-tm-extract-attachment-tool-1.0.0.RELEASE* dans le répertoire racine de Squash TM ~SQUASH.

3. Ouvrir le fichier *extract-attachments.sh* dans un éditeur de texte

4. Chercher les lignes :

        DB_TYPE=h2
        DB_URL=jdbc:h2:../data/squash-tm
        DB_USERNAME=sa
        DB_PASSWORD=sa

    Puis les modifier avec les valeurs nécessaires pour accéder à la base de données de l'instance Squash TM. Elles doivent être identiques à celles du script de démarrage de Squash TM (~SQUASH/bin/startup.sh).

5. Chercher la ligne : 

        REPO_PATH=./attachments

    Editer la propriété pour indiquer le chemin du répertoire dans lequel seront extraites les pièces jointes. Si la valeur de cette propriété n'est pas modifiée, les pièces jointes seront directement placées dans (~SQUASH/attachments) qui est aussi le chemin par défaut dans lequel Squash TM ira les chercher après migration.

6. Exécuter le fichier *extract-attachments.sh*. La migration se lance automatiquement. Elle s’effectue en 3 phases :
    - Extraction des pièces jointes
    - Vérification des fichiers extraits
    - Suppression des pièces jointes en base de données

7. Une fois la migration terminée, configurer le fichier *squash.tm.cfg.properties* comme indiquer dans la section [Paramétrage](#parametrage)

### Procédure sur Windows

Pour extraire les pièces jointes existantes d'une base de données Squash TM vers un système de fichiers sur une machine Windows : 

~SQUASH est le dossier de déploiement de Squash TM.

1. Stopper Squash TM

2.  Décompresser l’outil *squash-tm-extract-attachment-tool-1.0.0.RELEASE* dans le répertoire racine de Squash TM ~SQUASH.

3. Ouvrir le fichier *extract-attachments.bat* dans un éditeur de texte

4. Chercher les lignes :

        set DB_TYPE=h2
        set DB_URL=jdbc:h2:./data/squash-tm
        set DB_USERNAME=sa
        set DB_PASSWORD=sa

    Puis les modifier avec les valeurs nécessaires pour accéder à la base de données de l'instance Squash TM. Elles doivent être identiques à celles du script de démarrage de Squash TM (~SQUASH/bin/startup.bat).

5. Chercher la ligne : 

        set REPO_PATH=./attachments

    Editer la propriété pour indiquer le chemin du répertoire dans lequel seront extraites les pièces jointes. Si la valeur de cette propriété n'est pas modifiée, les pièces jointes seront directement placées dans (~SQUASH/attachments) qui est aussi le chemin par défaut dans lequel Squash TM ira les chercher après migration.

6. Exécuter le fichier *extract-attachments.bat*. La migration se lance automatiquement. Elle s’effectue en 3 phases :
    - Extraction des pièces jointes 
    - Vérification des fichiers extraits
    - Suppression des pièces jointes en base de données

7. Une fois la migration terminée, configurer le fichier *squash.tm.cfg.properties* comme indiqué dans la section [Paramétrage](#parametrage)


### Post migration

Selon les SGBDR, il peut être nécessaire d’effectuer une opération de maintenance sur la base de données pour libérer l’espace disque occupé par les pièces jointes, et ce même si le migrateur les a supprimées, au moyen de requêtes SQL.

Pour une base de données PostgreSQL, utiliser les programmes *vacuumlo* ou *vaccumdb* pour supprimer définitivement les larges objects.
<br/>Pour MySQL ou MariaDB, exécuter un *optimize table* depuis l’outil Table Inspector pour supprimer définitivement les larges objects.

Se reporter à la documentation du SGBDR pour plus d’informations.

