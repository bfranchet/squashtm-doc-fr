# Effectuer une montée de version

Cette section indique la procédure à suivre pour réaliser une montée de version de l'application Squash TM. Toute montée de version se fait en deux phases : la mise à jour du schéma de la base de données via des scripts d'upgrade et l'installation de la nouvelle version de l'applicatif.

!!! warning "Focus"
	Il est fortement recommandé de tester la montée de version sur un environnement isoprod avant de la réaliser en production.

## Procédure classique

La procédure de montée de version se fait sur deux plans : d'un côté la base de données et de l'autre l'applicatif.

### Montée de version de la base de données :

1. Commencer par télécharger la nouvelle version de Squash TM
2. Décompresser l'archive de Squash TM
3. Arrêter le Squash TM en utilisation
4. Faire un dump de la base de données (voir la section sur les [Sauvegardes](../exploitation/sauvegarde-optimisation-bdd.md#sauvegardes)) pour sauvegarde
5. Passer les scripts de montée de version (DBType-upgrade-to-xxx.sql) présents dans le répertoire "*database-scripts*" de la nouvelle version de Squash TM pour mettre à jour la base de données. Attention, les scripts d'upgrade doivent être passés en fonction du **type de base de données dans l'ordre des versions**. Il faut toujours partir du script portant le numéro de la version supérieure à celle du Squash TM en cours d'utilisation et s'arrêter à celle qui porte le numéro de la version ciblée.
	
	*Exemple :* 
	<br/>Pour réaliser une montée de version d'un Squash TM 1.21.x vers la version 2.0.0 sur une base de données MariaDB, voici la liste des scripts à passer : 
	
	- mysql-upgrade-to-1.22.0.sql
	- mysql-upgrade-to-1.22.2.sql
	- mysql-upgrade-to-1.22.3.sql
	- mysql-upgrade-to-1.22.5.sql
	- mysql-upgrade-to-2.00.0.sql

6. S'assurer que l'utilisateur *squash-tm* dispose bien des droits nécessaires sur la base de données. Réattribuer les droits à l'utilisateur si nécessaire.
	<br/>Sur PostgreSQL :

		GRANT ALL PRIVILEGES ON DATABASE squashtm TO "squash-tm";

	Sur MariaDB :

		GRANT ALL ON squashtm.* TO 'squash-tm'@'localhost';
		FLUSH PRIVILEGES;


!!! warning "Focus"
	Pour les bases de données PostgreSQL, les actions sur la base de données telles que les sauvegardes, les restaurations et les passages de scripts sont à effectuer avec l’utilisateur *squash-tm* et non avec l’utilisateur *postgres*. Donner à l'utilisateur *squash-tm* le rôle de superuser le temps de passer les scripts d'upgrade puis lui retirer ce rôle.

!!! danger "Attention"
	Les scripts d'upgrade doivent impérativement être passés dans l'ordre des versions. Si les scripts sont passés dans le désordre, il faut recommencer la montée de version à partir du dump de sauvegarde.

### Montée de version de l'application :

1. Après avoir téléchargé la nouvelle version de Squash TM, installer l'application *squash-tm* décompressée à l'emplacement souhaité
2. Télécharger et décompresser les plugins compatibles avec la version de Squash TM
3. Arrêter le Squash TM en utilisation
4. Conserver une copie des fichiers de configuration présents dans le dossier *'conf'* ainsi qu'une copie du fichier *startup* présent dans le dossier *'bin'* de l'ancien *squash-tm*.
5. Reporter manuellement les informations spécifiques à l'environnement dans les fichiers de démarrage et de configuration du nouveau *squash-tm*. Enregistrer les modifications.
6. Installer tous les .jar des plugins dans le répertoire *'plugins'*. Ne pas oublier de supprimer les anciens .jar.
7. Ne pas oublier de déposer le fichier de licence dans le sous-répertoire *'license'* du répertoire *'plugins'*
8. Si possible vider les caches tomcat (dans tomcat-home/work supprimer le dossier Tomcat) et vider l’intérieur du dossier tmp
9. Démarrer la nouvelle version de *squash-tm*
10. Demander aux utilisateurs de vider les caches de leur navigateur après la montée de version pour éviter tout problème d'affichage.

!!! tip "En savoir plus"
	Pour les détails de l'installation de Squash TM et de ses plugins se référer aux pages suivantes : [Installation de l'applicatif](../installation-squash/installation-applicatif.md) et [Installation des plugins Squash TM](../installation-plugins/installer-plugins.md#installation-des-plugins-squash-tm)


## Procédure avec Docker

Pour réaliser la montée de version de Squash TM avec l'image Docker, consulter la documentation spécifique sur [Docker Hub](https://hub.docker.com/r/squashtest/squash-tm).
