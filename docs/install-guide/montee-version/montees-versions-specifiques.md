# Informations complémentaires sur les montées de version

## Montée de version depuis 2.0 vers 2.1

Pour réaliser la montée de version d'un Squash 2.0 vers la version 2.1, il y a un script d'upgrade à exécuter sur la base de données : *<database\>-upgrade-to-2.1.0.sql*.
<br/>Il faut donc suivre la [procédure d'upgrade](./procedure-montee-version.md) habituelle.

### Compatiblité avec les plugins

La version 2.1 de Squash TM est compatible avec les plugins en version 2.1. Pour les plugins ne disposant pas d'une version 2.1, la version 2.0 est compatible avec Squash TM 2.1.
<br/>Attention, les plugins en version 2.1 ne sont pas compatibles avec la version 2.0 de Squash TM. Pour avoir les dernières fonctionnalités, il est nécessaire de réaliser la montée de version de Squash TM et de ses plugins en version 2.1.

## Montée de version depuis 1.22.5+ vers 2.0

La montée de version d'un Squash 1.22.5+ vers la version 2.0 se déroule de la même manière qu’une montée de version habituelle. Il faut :

- sauvegarder les fichiers de configuration et la base de données,
- télécharger la version 2.0
- passer le script d'upgrade *<database\>-upgrade-to-2.0.0.sql* sur la base de données 
- puis mettre à jour l’applicatif. 

!!! danger "Attention"
    À partir de la version 2.0, les paquets Debian et Red Hat ne sont plus supportés. 
    <br/>Consulter la page contenant la [procédure](../installation-squash/installation-applicatif.md#passage-dune-installation-packagee-1x-en-tarball-targz) à suivre.

!!! warning "Focus"
    Squash TM 2.0 propose une toute nouvelle interface qui s'appuie sur la technologie Angular. Ce changement de technologie induit des points de vigilances qui sont listés ci-dessous.

### Compatiblité avec les plugins

Les plugins de Squash TM compatibles avec une version 1.X ne sont plus compatibles avec la version 2.0. Il est donc nécessaire d’installer la nouvelle version des plugins pour bénéficier de leurs fonctionnalités avec Squash TM 2.0.

Pour éviter les problèmes de compatibilité, tous les plugins compatibles avec la version 2.0 de Squash TM sont en version 2.0.0.RELEASE.

!!! tip "En savoir plus"
    Pour voir la liste des pugins de la version 2.0.0 de Squash TM, consulter la page [Les plugins de Squash TM](../installation-plugins/plugins-squash.md)

Certains plugins compatibles avec les versions 1.X de Squash TM étaient déjà en version 2.X. Pour assurer une cohérence de versioning entre Squash TM et l’ensemble de ses plugins, ces derniers ont été renommés.

Voici le tableau des correspondances pour ces plugins :

| Plugin | Nom en version 1.X de Squash TM | Nom en version 2.0 de Squash TM |
|-----|------------|------|
| Cahier de test PDF | report.books.testcases-1.x | report.books.testcases.pdf |
| Cahier de test éditable <br />(inclus dans la distribution de Squash TM) | report.books.testcases-2.x | report.books.testcases.editable |
| Cahier des exigences PDF | report.books.requirements-1.x | report.books.requirements.pdf |
| Cahier des exigences éditable <br /> (inclus dans la distribution de Squash TM) | report.books.requirements-2.x | report.books.requirements.editable |
| Bilan de campagne & d'itération | report.campaignassessment | report.campaign.execution |


### Modification des fichiers de langue

L'externalisation des fichiers de langue évolue en version 2.0. En effet, l'interface de l'application ayant changé avec Angular, les messages visibles dans l'IHM sont stockés dans de nouveaux fichiers de langue au format .json. Des fichiers *custom* (vides) pour chacune des langues supportées (français, anglais, espagnol, allemand) sont à disposition dans le dossier serveur *conf/lang* de Squash TM.

Si les fichiers de langue ont été modifiés en version 1.X pour personnaliser des messages visibles dans l'IHM, il est nécessaire de reporter ces modifications dans les fichiers .json compatibles avec la 2.0.

Les fichiers de langue modifiés en version 1.X doivent néanmoins être conservés car ils sont utilisés en version 2.X pour certains messages du backend et présents dans les rapports.

!!! tip "En savoir plus"
    Pour en savoir plus sur le fonctionnement de ces fichiers, consulter la section [Externalisation des fichiers de langues](../parametrage-squash/externalisation-langues.md)

### Présence de plugins personnalisés

Le changement de technologie impacte également les plugins. Ainsi, si des plugins personnalisés ont été créés pour Squash TM, et qu'ils ajoutent des éléments graphiques (boutons, champs…) ou des nouveaux écrans dans Squash TM, ils ne fonctionneront plus avec la version 2.0. Il faut apporter les changements nécessaires aux plugins avant de pouvoir les utiliser avec la nouvelle version.

## Montée de version depuis 1.21.X vers 1.22.5

Pour passer à une version 1.22, la procédure habituelle s’applique : remplacement des binaires et exécution des scripts SQL nécessaires.

!!! danger "Attention"
    Pour passer en version 1.22.2+, il est nécessaire de passer l'ensemble des scripts d'upgrade entre la version d'origine de la base de données jusqu'à la version ciblée de Squash TM. 
    Pour atteindre la version 1.22.5 de Squash depuis une 1.21.X, il faut passer les scripts suivants dans l'ordre :

    - <databse\>-upgrade-to-1.22.0.sql
    - <databse\>-upgrade-to-1.22.2.sql
    - <databse\>-upgrade-to-1.22.3.sql
    - <databse\>-upgrade-to-1.22.5.sql

Lors du passage du script de montée de version vers la 1.22 des warnings sur les triggers peuvent apparaître. Ces warnings sont uniquement à titre informatif et n’ont aucun impact sur l’application.

Suite à la montée de version de Squash TM en 1.22, les identifiants de connexion aux serveurs d’exécution automatisée seront perdus car stockés différemment en base de données. Il faudra les renseigner à nouveau sur la page de consultation du serveur d’exécution automatisée dans le bloc ‘Politique d’authentification’ :

![Authentification à Jenkins](resources/authentification-jenkins.png){class="pleinepage"}

Une nouvelle option permettant l’[autoconnect aux bugtrackers](../installation-plugins/configurer-plugins-authentification.md#autoconnect-aux-bugtrackers) dans le cas de l’utilisation d’un annuaire AD ou LDAP a été ajoutée à l’administration de Squash.

Avec la version 1.22, trois nouvelles API ont vu le jour : l’API Admin, l’API Xsquash4Jira et l’API Bibliothèque d’actions. 

!!! tip "En savoir plus"
    Pour plus d'informations sur les APIs Squash TM consulter la page dédiée aux [APIs Squash TM](../installation-plugins/apis.md)

!!! warning "Focus"
    En outre, l’installation du service systemd sous Debian en 1.22 a évolué. Veuillez vous référer à la [section concernée](../installation-squash/installation-applicatif.md#installation-du-service-systemd-sous-debian).


## Montée de version depuis 1.20.X vers 1.21.X

La montée de version depuis une 1.20.x de Squash TM vers une 1.21.x doit se faire en suivant la [procédure de montée de version](./procedure-montee-version.md) classique.

Il faut juste noter que le passage du script de montée de version vers la version 1.21 peut entraîner un temps de traitement plus long que la normale lorsqu’il y a un nombre important d’exécutions dans la base de données de Squash TM. 
