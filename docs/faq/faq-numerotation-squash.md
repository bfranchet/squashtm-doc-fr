# FAQ Nouveau système de numérotation des versions de Squash

[TOC]

### Pourquoi une nouvelle politique de numérotation des versions de Squash (TM, AUTOM et DEVOPS) et de ses plugins ?

Le but est de profiter pleinement des trois nombres du numéro de version, d'avoir ainsi plus de visibilité sur la nature des évolutions d'une version, et de simplifier le choix des versions des plugins compatibles à installer avec une version du cœur de Squash TM.

### Qu'apporte cette nouvelle politique de numérotation des versions ?

Cette nouvelle politique de numérotation des versions a deux objectifs :

- donner aux administrateurs et utilisateurs de Squash une meilleure idée sur l'importance des évolutions d'une version à l'autre
- avoir une meilleure visibilité sur la compatibilité du cœur de Squash TM et de ses plugins

### Comment fonctionne la nouvelle numérotation ?

La nouvelle numérotation fonctionne de la manière suivante : les évolutions majeures sont marquées par le premier nombre, les évolutions mineures par le second, et les corrections d'anomalies par le troisième.

Les plugins sont eux numérotés en fonction de la version du cœur de Squash TM avec laquelle ils sont compatibles, comme sur le schéma suivant, quitte à devoir sauter des valeurs (ainsi le plugin P2 passe de la 3.0.2 à la 3.2.0) :

![Numérotation Squash](resources/schema-numerotation-squash.png){class="center"}

### Comment interpréter chacun des trois nombres du numéro de version X.Y.Z ?

- **X correspond à la version majeure :** passer de X à X+1 implique des changements potentiellement non rétrocompatibles (modifications de la base de données), ou en tout cas dont la rétrocompatibilité n'aura pas été testée. Les évolutions majeures apportent de nouvelles fonctionnalités ; la procédure de mise à jour demande de la vigilance (mise à jour de la base de données, éventuellement de fichiers de configuration).
- **Y correspond à la version mineure :** ajouts de fonctionnalités rétrocompatibles, principalement des corrections de bugs, ajouts de quelques fonctionnalités non rétrocompatibles. La procédure de mise à jour demande également de la vigilance.
- **Z correspond à une correction d’anomalie :** la version est rétrocompatible avec la précédente. La procédure de mise à jour simplifiée (souvent simple remplacement du fichier .war)

### Les versions des plugins suivent-elles les mêmes règles ?

Non, les plugins suivent les règles de numérotation suivantes :

- Chaque sortie de version majeure du cœur de Squash TM génère une version majeure de chaque plugin, quitte à ce qu'il n'y ait pas eu d'évolution
- Chaque évolution du plugin prend pour version mineure le minimum de la version mineure du cœur de Squash TM avec laquelle elle est compatible (quitte à sauter une étape)
- Le troisième numéro augmente de un, indépendamment de la version du cœur de Squash TM, à moins que la version mineure de cette évolution ait augmenté, auquel cas il passe à 0

### Auriez-vous un exemple pratique de cette nouvelle numérotation des versions ?

Pour expliciter cela, voici à nouveau le graphique présenté plus haut :

![Numérotation Squash](resources/schema-numerotation-squash.png){class="center"}

Ainsi, la version 3.1.0 du cœur de Squash TM fonctionne avec les versions :

- 3.0.1 et 3.1.0 du plugin P1
- 3.0.1 et 3.0.2 du plugin P2

et la version 3.2.0 du cœur de Squash TM avec les versions :

- 3.1.0 du plugin P1
- 3.0.2, 3.2.0 et 3.2.1 du plugin P2

### En quoi la nouvelle numérotation me simplifie-t-elle la vie ?

En considérant le numéro qui change dans la nouvelle version, je sais de quel type d'évolution il s'agit. Je sais à quoi m'attendre, et quel niveau de précaution je dois respecter lors de l'installation de la nouvelle version.

Concernant la compatibilité des plugins, en regardant leur version dans le répertoire des plugins, il sera plus simple de s'apercevoir s'ils sont compatibles ou pas avec le cœur de Squash TM.

### Quel va être le nouveau rythme de sortie des versions de Squash ?

Une version majeure de Squash TM est prévue en mai et novembre de chaque année.

Le rythme de sortie des versions AUTOM /DEVOPS est mensuel, les deuxièmes mercredis de chaque mois, sauf en août en raison des congés estivaux.

### Quid de la compatibilité entre les versions TM, AUTOM et DEVOPS ?

Les numéros des versions du cœur de Squash TM et des modules Squash AUTOM et Squash DEVOPS sont indépendants.

Concernant spécifiquement Squash AUTOM et Squash DEVOPS, chaque sortie est constituée d’un ensemble de composants compatibles entre eux, qui sont visibles sur notre page [Téléchargements](https://www.squashtest.com/community-download) ou dans les [Release notes par version](https://autom-devops-fr.doc.squashtest.com/latest/release-note/release-note-by-version.html). Ces composants incluent :

- Des plugins de Squash TM qui sont numérotés selon la version du coeur de Squash TM avec laquelle ils sont compatibles
- Des composants indépendants de Squash TM qui ont leur propre numéro de version et suivent les mêmes principes que le cœur de Squash TM (c’est-à-dire version majeure, version mineure et version corrective)

### Comment dois-je aborder une mise à jour de Squash TM majeure ? mineure ? corrective ?

L'installation d'une version majeure X.0.0 de Squash TM demande un respect attentif de la documentation d'installation :

- mise à jour de la base de données
- éventuelle mise à jour des fichiers de configuration
- remplacement du fichier squash-tm.war
- mise à jour de tous les plugins en version X.0.0

La montée de version vers une version mineure est plus simple. Indiquant l'ajout d'une nouvelle fonctionnalité, la montée de version peut impliquer une mise à jour simple de la base de données, mais pas nécessairement.<br />
Sauf indication contraire, les plugins ne sont pas à mettre à jour.

L'installation d'une version corrective (changement du troisième chiffre) ne nécessite généralement que le remplacement du fichier squash-tm.war.