# FAQ Squash TM 2.0

[TOC]

### Quelles sont les nouveautés de Squash TM 2.0 ?

Squash TM 2.0 propose une toute nouvelle interface, plus moderne et basée sur la technologie Angular. <br />
Cette nouvelle interface s’accompagne d’évolutions ergonomiques et d’amélioration de fonctionnalités existantes.

!!! tip "En savoir plus"
	Pour plus d'informations sur les nouveautés de la version 2.0, consulter [l'article dédié](https://www.squashtest.com/post/releases-nouvelle-version-squash-tm-2-0-disponible).

### Quel est le périmètre fonctionnel de la version 2.0 par rapport à la version 1.22 ?

Squash TM 2.0 est iso-fonctionnel par rapport à la version 1.22, vous retrouverez ainsi toutes les fonctionnalités de la version 1.22 dans la version 2.0.

### La compatibilité ascendante est-elle assurée depuis une version 1.X ?

Oui.

### Est-ce que les modalités d’installation changent ?

À partir de la version 2.0, les versions packagées Debian et Redhat ne sont plus supportées. Il est désormais nécessaire de passer par l’archive .tar.gz pour ce type d’installation. 

!!! tip "En savoir plus"
	Pour les déploiements sur Debian et Redhat, consulter la [procédure d'installation pour Debian et Redhat](../install-guide/installation-squash/installation-applicatif.md#passage-dune-installation-packagee-1x-en-tarball-targz).

### Comment passer d’une version 1.X à la version 2.0 ? 

Le passage à la version 2.0 se déroule de la même manière qu’une montée de version habituelle, à savoir en sauvegardant les fichiers de configuration et la base de données, en passant un script sur la base de données, puis en mettant à jour l’applicatif.

!!! tip "En savoir plus"
	Consulter la procédure de montée de version vers Squash TM 2.0 dans la partie [Montée de version depuis 1.22.5 vers 2.0+](../install-guide/montee-version/montees-versions-specifiques.md).

### Quels sont les impacts de la version 2.0 sur la base de données ?

Comme pour les précédentes versions, la compatibilité ascendante est assurée avec Squash TM 2.0.<br />
Ainsi, les données existantes ne seront ni perdues, ni altérées lors du passage à la nouvelle version.<br />
Comme pour une montée de version classique, le passage d’un script pour mettre à jour le schéma de la base de données est nécessaire.

### Qu’en est-il de la compatibilité avec les plugins ?

Les plugins de Squash TM compatibles avec une version 1.X ne sont plus compatibles avec la version 2.0. <br />
Il est donc nécessaire d’installer la dernière version des plugins pour bénéficier de leurs fonctionnalités.<br />
Pour éviter les problèmes de compatibilité, tous les plugins compatibles avec la version 2.0 de Squash TM sont en version 2.0.0.RELEASE.  

!!! info "Info"
	Attention, certains plugins compatibles avec les versions 1.X étaient déjà en version 2.X. Pour assurer une cohérence de versioning entre Squash TM et l’ensemble des plugins, ces derniers ont été renommés. Les correspondances sont indiquées dans le tableau de la partie [Montée de version depuis 1.22.5 vers 2.0+](../install-guide/montee-version/montees-versions-specifiques.md).

Le plugin Xsquash de Jira est également impacté, il sera nécessaire de le mettre à jour.

### Les API de Squash TM sont-elles toujours compatibles avec la version 2.0 ?

Les API fonctionnent comme des plugins de Squash TM. Ainsi, le passage à la version 2.0 nécessite d’installer une nouvelle version des API.<br />
Néanmoins, les interfaces restent inchangées : les requêtes effectuées sur une version 1.X sont compatibles avec la version 2.0.

### J’ai personnalisé les fichiers de langue de Squash TM pour avoir une terminologie adaptée à mon organisation, comment faire pour récupérer mes modifications en version 2.0 ?

L’externalisation des fichiers de langue est toujours possible avec Squash TM 2.0. Néanmoins, le format des fichiers de langue change avec la version 2.0. Ainsi, les modifications effectuées en version 1.X ne pourront pas être reprises telles quelles en version 2.0.<br />
Pour personnaliser la terminologie dans l’application, il est donc nécessaire de reporter ces modifications dans les fichiers compatibles avec la version 2.0.

!!! tip "En savoir plus"
	Consulter la procédure pour [externaliser les fichiers de langue](../install-guide/parametrage-squash/externalisation-langues.md).

### J’ai développé mes propres plugins, sont-ils toujours compatibles avec la Squash TM 2.0 ?

Non, le changement de technologie impacte également les plugins. Si vos plugins ajoutent des éléments graphiques (boutons, champs…) ou des nouveaux écrans dans Squash TM, il sera nécessaire de réécrire ces parties pour rendre compatible vos plugins avec Squash TM 2.0. 

### Les versions 1.X seront-elles toujours maintenues ?

Le support et la maintenance des versions de Squash TM est assuré pendant deux ans à compter de leur sortie. La maintenance de la version 1.22 sera donc assurée jusqu’au 31 décembre 2022. 

### Est-ce que les futures nouvelles fonctionnalités seront reportées sur la version 1.X ?

Non, la version 1.22 sera la dernière version majeure avec l'ancienne interface. Si des versions correctives de la version 1.22 pourront être amenées à sortir dans le cadre de la maintenance corrective, les nouvelles fonctionnalités seront ajoutées uniquement dans les versions supérieures à la 2.0.

### Est-ce que de nouveaux guides d’utilisation sont disponibles ?

La documentation de Squash TM fait également peau neuve. Vous retrouverez l’ensemble des guides de références (guide d’installation, administrateur et utilisateur) mais également des guides de démarrage, tutoriaux et FAQ. <br />
Cette documentation est disponible en français et en anglais et sera versionnée.

!!! tip "En savoir plus"
	Consulter la [documentation de Squash TM 2.0](https://squashtest.gitlab.io/doc/squashtm-doc-fr).

### Est-ce que la documentation des versions 1.X sera toujours accessible ?

Oui, la documentation des versions 1.X reste accessible sur le Wiki Squash TM. 

!!! tip "En savoir plus"
	Consulter la [documentation de Squash TM 1.X](https://sites.google.com/a/henix.fr/wiki-squash-tm/).
